package de.colibriengine.lambda

/**
 * Consumes one argument and produces an output.
 *
 * @param C The type of the first argument consumed by the [call] method.
 * @param P The type of the object which gets produced.
 * @since ColibriEngine 0.0.7.2
 */
@FunctionalInterface
@Deprecated("use kotlin instead :)")
interface UnaryProducerLambda<C, P> {

    fun call(argument: C): P

}
