package de.colibriengine.lambda

/**
 * This lambda consumes one argument and produces no output.
 *
 * @param C The type of the first argument consumed by the [call] method.
 * @since ColibriEngine 0.0.7.2
 */
@FunctionalInterface
@Deprecated("use kotlin instead :)")
interface UnaryConsumerLambda<C> {

    fun call(argument: C)

}
