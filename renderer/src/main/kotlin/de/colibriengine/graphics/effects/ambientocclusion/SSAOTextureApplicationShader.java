package de.colibriengine.graphics.effects.ambientocclusion;

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader;
import de.colibriengine.graphics.shader.ShaderManager;
import de.colibriengine.graphics.shader.MVPShader;
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor;
import org.jetbrains.annotations.NotNull;

/**
 * SSAOTextureApplicationShader
 *
 * @version 0.0.0.1
 * @created 29.12.2018 22:22
 * @time 29.12.2018 22:22
 * @since ColibriEngine 0.0.7.3
 */
public class SSAOTextureApplicationShader extends AbstractOpenGLShader implements MVPShader {

    /* UNIFORMS LOCATIONS */
    private final int MVP_MATRIX_UNIFORM;
    private final int SSAO_TEXTURE_UNIT_UNIFORM;
    private final int SOURCE_TEXTURE_UNIT_UNIFORM;

    public SSAOTextureApplicationShader(final @NotNull ShaderManager shaderManager) {
        super();

        attachVertexShader(shaderManager.requestShaderSource(
                "shaders/basic/BasicPosTexPassThrough_vs.glsl"
        ));
        attachFragmentShader(shaderManager.requestShaderSource(
                "shaders/fx/ambientocclusion/SSAOTextureApplicator_fs.glsl"
        ));

        linkProgram();

        // Add uniform locations.
        MVP_MATRIX_UNIFORM = addUniformLocation("mvpMatrix");
        SSAO_TEXTURE_UNIT_UNIFORM = addUniformLocation("ssaoTex");
        SOURCE_TEXTURE_UNIT_UNIFORM = addUniformLocation("sourceTex");
    }

    @Override
    public void setMVPMatrix(final @NotNull Mat4fAccessor mvpMatrix) {
        setUniformM4f(MVP_MATRIX_UNIFORM, mvpMatrix);
    }

    public void setSSAOTextureUnit(final int ssaoTextureUnit) {
        setUniform1i(SSAO_TEXTURE_UNIT_UNIFORM, ssaoTextureUnit);
    }

    public void setSourceTextureUnit(final int textureUnit) {
        setUniform1i(SOURCE_TEXTURE_UNIT_UNIFORM, textureUnit);
    }

}
