package de.colibriengine.graphics.effects.ambientocclusion;

import de.colibriengine.exception.InstantiationNotAllowedException;
import de.colibriengine.math.FEMath;
import de.colibriengine.math.Random;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import org.jetbrains.annotations.NotNull;

/**
 * Provides utility / helper functions for the {@link SSAOEffect}.
 */
public final class SSAOEffectHelper {

    /**
     * Prevent any instantiation of this class as it is a utility class.
     */
    private SSAOEffectHelper() {
        throw new InstantiationNotAllowedException(getClass().getName());
    }

    public static @NotNull StdVec3f[] computeKernel(final @NotNull Random random, final int kernelSize) {
        final StdVec3f[] kernel = new StdVec3f[kernelSize];

        for (int i = 0; i < kernelSize; i++) {
            final StdVec3f sample;

            // Generate a sample positions which (after normalization) lies on the surface of the hemisphere
            // oriented along the z axis.
            sample = new StdVec3f(
                    random.nextFloat(-1.0f, 1.0f),
                    random.nextFloat(-1.0f, 1.0f),
                    random.nextFloat(0.0f, 1.0f)
            ).normalize();

            // Distribute the sample position in the volume of the hemisphere. Favor the origin point.
            float scale = (float) i / (float) kernelSize;
            scale = FEMath.INSTANCE.lerp(0.1f, 1.0f, scale * scale);
            sample.times(scale);

            // Store the sample position in the kernel array.
            kernel[i] = sample;
        }

        return kernel;
    }

    public static @NotNull StdVec3f[] computeNoise(final @NotNull Random random, final int dimension) {
        final int noiseSize = dimension * dimension;

        final StdVec3f[] noise = new StdVec3f[noiseSize];

        for (int i = 0; i < noiseSize; i++) {
            // Add a rotation vector. (Where to rotate the hemisphere around its z axis.)
            noise[i] = new StdVec3f(
                    random.nextFloat(-1.0f, 1.0f),
                    random.nextFloat(-1.0f, 1.0f),
                    0.0f
            );
        }

        return noise;
    }


}
