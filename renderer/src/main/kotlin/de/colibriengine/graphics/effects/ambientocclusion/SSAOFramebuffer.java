package de.colibriengine.graphics.effects.ambientocclusion;

import de.colibriengine.graphics.GBuffer;
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOAttachment;
import de.colibriengine.graphics.opengl.buffer.fbo.FixedResolutionFBO;
import de.colibriengine.graphics.opengl.texture.GLTexelDataFormat;
import de.colibriengine.graphics.opengl.texture.GLTexelDataType;
import de.colibriengine.graphics.texture.ImageFormat;
import de.colibriengine.graphics.texture.Texture;
import de.colibriengine.graphics.texture.TextureFilterType;
import de.colibriengine.graphics.texture.TextureWrapType;
import de.colibriengine.math.vector.vec4f.StdVec4fFactory;
import org.jetbrains.annotations.NotNull;

public class SSAOFramebuffer extends FixedResolutionFBO {

    /**
     * Constructs a new {@link SSAOFramebuffer} without initializing it (no OpenGL initialization!).
     * The {@link SSAOFramebuffer#setResolution(int, int)} method must be called manually.
     * The {@link SSAOFramebuffer#init()} function must be called manually.
     */
    public SSAOFramebuffer() {
        super();
    }

    /**
     * Constructs and initializes a new {@link SSAOFramebuffer}.
     * Manually calling {@link GBuffer#init()} after this is not necessary.
     *
     * @param textureWidth  The width of the FBO textures.
     * @param textureHeight The height of the FBO textures.
     */
    public SSAOFramebuffer(final int textureWidth, final int textureHeight) {
        super(textureWidth, textureHeight);

        // Initialize this ShadowMap instance for the first time.
        init();
    }

    /**
     * Initializes this {@code GBuffer} instance.
     * Each created instance is automatically initialized upon creation.
     * This method must be called manually after this instance got invalidated. For example through a call to the
     * {@link GBuffer#setXResolution(int)} method.
     */
    @Override
    public void init() {
        makeValid();
        ensureResolutionIsSet();

        // The underlying FBO must be destroyed first if it is already created (was previously initialized).
        if (super.isCreated()) {
            super.destroy();
        }

        // Initialize the underlying frame buffer object.
        super.create();

        // Texture 0: Main
        super.addTexture(
                getResolution().getX(), getResolution().getY(),
                ImageFormat.R32F, GLTexelDataFormat.GL_RED,
                GLTexelDataType.GL_FLOAT,
                TextureFilterType.NEAREST, TextureFilterType.NEAREST,
                TextureWrapType.CLAMP_TO_BORDER, StdVec4fFactory.Companion.getWHITE()
        );

        // Texture 1: For post processing
        super.addTexture(
                getResolution().getX(), getResolution().getY(),
                ImageFormat.R32F, GLTexelDataFormat.GL_RED,
                GLTexelDataType.GL_FLOAT,
                TextureFilterType.LINEAR, TextureFilterType.LINEAR,
                TextureWrapType.CLAMP_TO_BORDER, StdVec4fFactory.Companion.getWHITE()
        );

        // Add depth texture. (Allows lighting calculations)
        super.initDepthTexture(ImageFormat.DEPTH_COMPONENT32, GLFBOAttachment.GL_DEPTH_ATTACHMENT);

        setDrawBufferToNONE();
        setReadBufferToNONE();

        // Create the FBO.
        super.compile();

        // The creation of the underlying FBO might have left us with that FBO being bound to the context.
        // We have to make sure that the default FBO is bound.
        bindDefaultFBO();
    }

    public int getMainTextureIndex() {
        return 0;
    }

    public @NotNull Texture getMainTexture() {
        return getTextures().get(getMainTextureIndex());
    }

    public int getPostProcessedTextureIndex() {
        return 1;
    }

    public @NotNull Texture getPostProcessedTexture() {
        return getTextures().get(getPostProcessedTextureIndex());
    }

}
