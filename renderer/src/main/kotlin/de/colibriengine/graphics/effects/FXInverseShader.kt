package de.colibriengine.graphics.effects

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class FXInverseShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/fx/passthrough_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/fx/inverse_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("tex")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    fun setTexture(texture: Int) {
        setUniform1i(textureUniform, texture)
    }

}
