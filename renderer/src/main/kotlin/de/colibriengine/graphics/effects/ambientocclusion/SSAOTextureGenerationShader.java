package de.colibriengine.graphics.effects.ambientocclusion;

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader;
import de.colibriengine.graphics.shader.ShaderManager;
import de.colibriengine.graphics.shader.MVPShader;
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor;
import de.colibriengine.math.vector.vec2f.Vec2fAccessor;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import org.jetbrains.annotations.NotNull;

/**
 * SSAOTextureGenerationShader
 *
 * @since ColibriEngine 0.0.7.3
 */
public class SSAOTextureGenerationShader extends AbstractOpenGLShader implements MVPShader {

    /* UNIFORMS LOCATIONS */
    private final int MVP_MATRIX_UNIFORM;
    private final int SCREEN_SIZE_UNIFORM;
    private final int VIEW_MATRIX_UNIFORM;
    private final int PROJECTION_MATRIX_UNIFORM;
    private final int INVERSE_PROJECTION_MATRIX_UNIFORM;

    private final int SAMPLE_RADIUS_UNIFORM;
    private final int SAMPLE_POS_DEPTH_BIAS_UNIFORM;
    private final int POWER_UNIFORM;

    private final int KERNEL_UNIFORM;

    private final int DEPTH_TEXTURE_UNIT_UNIFORM;
    private final int NORMAL_TEXTURE_UNIT_UNIFORM;
    private final int NOISE_TEXTURE_UNIT_UNIFORM;

    private final int NOISE_TEXTURE_COORD_SCALE_UNIFORM;

    SSAOTextureGenerationShader(final @NotNull ShaderManager shaderManager) {
        super();

        attachVertexShader(shaderManager.requestShaderSource(
                "shaders/basic/BasicPosNormTexPassThrough_vs.glsl"
        ));
        attachFragmentShader(shaderManager.requestShaderSource(
                "shaders/fx/ambientocclusion/SSAOTextureGenerator_fs.glsl"
        ));

        linkProgram();

        // Add uniform locations.
        MVP_MATRIX_UNIFORM = addUniformLocation("mvpMatrix");
        SCREEN_SIZE_UNIFORM = addUniformLocation("screenSize");
        PROJECTION_MATRIX_UNIFORM = addUniformLocation("projectionMatrix");
        INVERSE_PROJECTION_MATRIX_UNIFORM = addUniformLocation("inverseProjectionMatrix");
        VIEW_MATRIX_UNIFORM = addUniformLocation("viewMatrix");

        SAMPLE_RADIUS_UNIFORM = addUniformLocation("sampleRadius");
        SAMPLE_POS_DEPTH_BIAS_UNIFORM = addUniformLocation("samplePosDepthBias");
        POWER_UNIFORM = addUniformLocation("power");

        KERNEL_UNIFORM = addUniformLocation("kernel");

        DEPTH_TEXTURE_UNIT_UNIFORM = addUniformLocation("depthTexture");
        NORMAL_TEXTURE_UNIT_UNIFORM = addUniformLocation("normalTexture");
        NOISE_TEXTURE_UNIT_UNIFORM = addUniformLocation("noiseTexture");

        NOISE_TEXTURE_COORD_SCALE_UNIFORM = addUniformLocation("noiseTexCoordScale");
    }

    @Override
    public void setMVPMatrix(final @NotNull Mat4fAccessor mvpMatrix) {
        setUniformM4f(MVP_MATRIX_UNIFORM, mvpMatrix);
    }

    public void setScreenSize(final @NotNull Vec2iAccessor screenSize) {
        setUniform2i(SCREEN_SIZE_UNIFORM, screenSize);
    }

    public void setProjectionMatrix(final @NotNull Mat4fAccessor projectionMatrix) {
        setUniformM4f(PROJECTION_MATRIX_UNIFORM, projectionMatrix);
    }

    public void setInverseProjectionMatrix(final @NotNull Mat4fAccessor inverseProjectionMatrix) {
        setUniformM4f(INVERSE_PROJECTION_MATRIX_UNIFORM, inverseProjectionMatrix);
    }

    public void setViewMatrix(final @NotNull Mat4fAccessor viewMatrix) {
        setUniformM4f(VIEW_MATRIX_UNIFORM, viewMatrix);
    }

    public void setSampleRadius(final float sampleRadius) {
        setUniform1f(SAMPLE_RADIUS_UNIFORM, sampleRadius);
    }

    public void setSamplePosDepthBias(final float samplePosDepthBias) {
        setUniform1f(SAMPLE_POS_DEPTH_BIAS_UNIFORM, samplePosDepthBias);
    }

    public void setPower(final float power) {
        setUniform1f(POWER_UNIFORM, power);
    }

    public void setKernel(final @NotNull StdVec3f[] kernel) {
        setUniform3fv(KERNEL_UNIFORM, kernel);
    }

    public void setDepthTextureUnit(final int depthTextureUnit) {
        setUniform1i(DEPTH_TEXTURE_UNIT_UNIFORM, depthTextureUnit);
    }

    public void setNormalTextureUnit(final int normalTextureUnit) {
        setUniform1i(NORMAL_TEXTURE_UNIT_UNIFORM, normalTextureUnit);
    }

    public void setNoiseTextureUnit(final int noiseTextureUnit) {
        setUniform1i(NOISE_TEXTURE_UNIT_UNIFORM, noiseTextureUnit);
    }

    public void setNoiseTextureCoordScale(final float onX, final float onY) {
        setUniform2f(NOISE_TEXTURE_COORD_SCALE_UNIFORM, onX, onY);
    }

    public void setNoiseTextureCoordScale(final @NotNull Vec2fAccessor noiseTextureCoordScale) {
        setNoiseTextureCoordScale(noiseTextureCoordScale.getX(), noiseTextureCoordScale.getY());
    }

}
