package de.colibriengine.graphics.effects.kernel

import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat3f.Kernel3f
import de.colibriengine.math.matrix.mat3f.StdMat3f

class FXKernel(shaderManager: ShaderManager) {

    val shader: FXKernelShader = FXKernelShader(shaderManager)
    var kernel: Kernel3f = Kernel3f(StdMat3f()).initIdentityKernel()

}
