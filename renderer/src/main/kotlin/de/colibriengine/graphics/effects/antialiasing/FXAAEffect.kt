package de.colibriengine.graphics.effects.antialiasing

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.fbo.buildFBOTextureRenderer
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBO
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRenderer
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

class FXAAEffect(shaderManager: ShaderManager, meshFactory: MeshFactory, ecs: ECS) {

    private val fxaaApplicationRenderer: GLFBOTextureRenderer = buildFBOTextureRenderer(meshFactory) {
        this.ecs = ecs
        shader = FXAAApplicationShader(shaderManager)
    }

    fun apply(
        to: Texture,
        storeInFBO: GLFBO, storeInFBOTextureIndex: Int,
        position: Vec2iAccessor,
        dimension: Vec2iAccessor,
        centeredCam: AbstractCamera
    ) {
        GLUtil.disableDepthTest() // TODO: necessary?
        GLUtil.disableDepthMask()
        to.bind(0)
        fxaaApplicationRenderer.updateDestFBO(storeInFBO, storeInFBOTextureIndex)
        fxaaApplicationRenderer.updateDestWritePosAndDimension(position, dimension)
        fxaaApplicationRenderer.shaderSetup = {
            val shader = it.shader as FXAAApplicationShader
            shader.bind()
            shader.setMVPMatrix(it.getQuadOrthographicMVP())
            shader.setTexture(0)
            shader.setScreenSize(dimension.x.toFloat(), dimension.y.toFloat())
            shader.setOffsets(1.0f / dimension.x, 1.0f / dimension.y)
        }
        fxaaApplicationRenderer.render(centeredCam)
        GLUtil.enableDepthMask()
        GLUtil.enableDepthTest()
    }

}
