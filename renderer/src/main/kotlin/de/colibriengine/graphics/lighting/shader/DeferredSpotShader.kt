package de.colibriengine.graphics.lighting.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class DeferredSpotShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPShader {

    /* UNIFORM LOCATIONS */
    private val mvpMatrixUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/deferredRendering/deferredSpot_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/deferredRendering/deferredSpot_fs.glsl"))
        linkProgram()

        //VS Uniforms
        mvpMatrixUniform = addUniformLocation("mvpMatrix")

        //FS Uniforms
        // TODO: Add FS uniforms!
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

}
