package de.colibriengine.graphics.lighting.shader;

import de.colibriengine.graphics.lighting.light.AbstractLight;
import de.colibriengine.graphics.lighting.light.DirectionalLightImpl;
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader;
import de.colibriengine.graphics.shader.ShaderManager;
import de.colibriengine.graphics.shader.ShaderManagerImpl;
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec3f.StdVec3f;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.math.vector.vec3f.Vec3fAccessor;
import org.jetbrains.annotations.NotNull;

public class DeferredDirectionalShader extends AbstractOpenGLShader implements DeferredLightingShader {

    /* UNIFORM LOCATIONS */
    private final int MVP_MATRIX_UNIFORM;

    private final int SCREEN_SIZE_UNIFORM;
    private final int CAMERA_POSITION_UNIFORM;
    private final int INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM;

    private final int DIFFUSE_TEXTURE_UNIFORM;
    private final int NORMAL_TEXTURE_UNIFORM;
    private final int SPECULAR_TEXTURE_UNIFORM;
    private final int DEPTH_TEXTURE_UNIFORM;

    private final int LIGHT_SPACE_MATRIX_UNIFORM;
    private final int SHADOW_MAP_UNIFORM;
    private final int VARIANCE_MAX_BOUND_UNIFORM;
    private final int EARLY_RETURN_REFERENCE_DEPTH_BIAS_UNIFORM;
    private final int PROBABILITY_MIN_BOUND_UNIFORM;

    private final int LIGHT_COLOR_UNIFORM;
    private final int LIGHT_INTENSITY_UNIFORM;
    private final int LIGHT_DIRECTION_UNIFORM;

    private final Vec3f tmpVec3f = new StdVec3f();

    /**
     * Constructs a new directional shader.
     *
     * @param shaderManager The {@link ShaderManagerImpl} which should be used to construct the shader.
     */
    public DeferredDirectionalShader(final @NotNull ShaderManager shaderManager) {
        super();

        attachVertexShader(
                shaderManager.requestShaderSource(
                        "shaders/deferredRendering/deferredDirectional_vs.glsl"
                )
        );
        attachFragmentShader(
                shaderManager.requestShaderSource(
                        "shaders/deferredRendering/deferredDirectional_fs.glsl"
                )
        );

        linkProgram();

        //VS Uniforms
        MVP_MATRIX_UNIFORM = addUniformLocation("mvpMatrix");

        //FS Uniforms
        SCREEN_SIZE_UNIFORM = addUniformLocation("screenSize");
        CAMERA_POSITION_UNIFORM = addUniformLocation("cameraPosition");
        INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM = addUniformLocation("inverseViewProjectionMatrix");

        DIFFUSE_TEXTURE_UNIFORM = addUniformLocation("diffuseTexture");
        NORMAL_TEXTURE_UNIFORM = addUniformLocation("normalTexture");
        SPECULAR_TEXTURE_UNIFORM = addUniformLocation("specularTexture");
        DEPTH_TEXTURE_UNIFORM = addUniformLocation("depthTexture");

        LIGHT_SPACE_MATRIX_UNIFORM = addUniformLocation("lightSpaceMatrix");
        SHADOW_MAP_UNIFORM = addUniformLocation("shadowMap");
        EARLY_RETURN_REFERENCE_DEPTH_BIAS_UNIFORM = addUniformLocation("earlyReturnReferenceDepthBias");
        VARIANCE_MAX_BOUND_UNIFORM = addUniformLocation("varianceMaxBound");
        PROBABILITY_MIN_BOUND_UNIFORM = addUniformLocation("probabilityMinBound");

        LIGHT_COLOR_UNIFORM = addUniformLocation("directionalLight.baseLight.color");
        LIGHT_INTENSITY_UNIFORM = addUniformLocation("directionalLight.baseLight.intensity");
        LIGHT_DIRECTION_UNIFORM = addUniformLocation("directionalLight.lightDirection");
    }

    @Override
    public void setMVPMatrix(final @NotNull Mat4fAccessor mvpMatrix) {
        setUniformM4f(MVP_MATRIX_UNIFORM, mvpMatrix);
    }

    public void setLightSpaceMatrix(final @NotNull Mat4fAccessor lightSpaceMatrix) {
        setUniformM4f(LIGHT_SPACE_MATRIX_UNIFORM, lightSpaceMatrix);
    }

    @Override
    public void setScreenSize(final @NotNull Vec2iAccessor screenSize) {
        setUniform2i(SCREEN_SIZE_UNIFORM, screenSize);
    }

    @Override
    public void setCameraPosition(final @NotNull Vec3fAccessor cameraPosition) {
        setUniform3f(CAMERA_POSITION_UNIFORM, cameraPosition);
    }

    @Override
    public void setInverseVP(final @NotNull Mat4fAccessor inverseViewProjectionMatrix) {
        setUniformM4f(INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM, inverseViewProjectionMatrix);
    }

    @Override
    public void setDiffuseTextureUnit(final int textureUnit) {
        setUniform1i(DIFFUSE_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setNormalTextureUnit(final int textureUnit) {
        setUniform1i(NORMAL_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setSpecularTextureUnit(final int textureUnit) {
        setUniform1i(SPECULAR_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setDepthTextureUnit(final int textureUnit) {
        setUniform1i(DEPTH_TEXTURE_UNIFORM, textureUnit);
    }

    public void setShadowMap(final int textureUnit) {
        setUniform1i(SHADOW_MAP_UNIFORM, textureUnit);
    }

    public void setEarlyReturnReferenceDepthBias(final float earlyReturnReferenceDepthBias) {
        setUniform1f(EARLY_RETURN_REFERENCE_DEPTH_BIAS_UNIFORM, earlyReturnReferenceDepthBias);
    }

    public void setVarianceMaxBound(final float varianceMaxBound) {
        setUniform1f(VARIANCE_MAX_BOUND_UNIFORM, varianceMaxBound);
    }

    public void setProbabilityMinBound(final float probabilityMinBound) {
        setUniform1f(PROBABILITY_MIN_BOUND_UNIFORM, probabilityMinBound);
    }

    @Override
    public void setBaseLight(final @NotNull AbstractLight baseLight) {
        // setUniform3f(LIGHT_POSITION_UNIFORM, baseLight.getTransform().getTranslation());
        setUniform3f(LIGHT_COLOR_UNIFORM, baseLight.getColor());
        setUniform1f(LIGHT_INTENSITY_UNIFORM, baseLight.getIntensity());
    }

    public void setDirectionalLight(final @NotNull DirectionalLightImpl directionalLight) {
        setBaseLight(directionalLight);
        setUniform3f(LIGHT_DIRECTION_UNIFORM, directionalLight.getTransform().getRotation().forward(tmpVec3f));
    }

}
