package de.colibriengine.graphics.lighting.shader;

import de.colibriengine.graphics.lighting.light.AbstractLight;
import de.colibriengine.graphics.lighting.light.PointLightImpl;
import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader;
import de.colibriengine.graphics.shader.ShaderManager;
import de.colibriengine.graphics.shader.ShaderManagerImpl;
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec3f.Vec3fAccessor;
import org.jetbrains.annotations.NotNull;

public class DeferredPointShader extends AbstractOpenGLShader implements DeferredLightingShader {

    /* UNIFORM LOCATIONS */
    private final int MVP_MATRIX_UNIFORM;
    private final int SCREEN_SIZE_UNIFORM;
    private final int CAMERA_POSITION_UNIFORM;
    private final int INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM;
    private final int DIFFUSE_TEXTURE_UNIFORM;
    private final int NORMAL_TEXTURE_UNIFORM;
    private final int SPECULAR_TEXTURE_UNIFORM;
    private final int DEPTH_TEXTURE_UNIFORM;
    private final int LIGHT_POSITION_UNIFORM;
    private final int LIGHT_COLOR_UNIFORM;
    private final int LIGHT_INTENSITY_UNIFORM;
    private final int LIGHT_ATTENUATION_QUADRATIC_UNIFORM;
    private final int LIGHT_ATTENUATION_LINEAR_UNIFORM;
    private final int LIGHT_ATTENUATION_CONSTANT_UNIFORM;
    private final int LIGHT_DROP_OFF_FACTOR_UNIFORM;

    /**
     * Constructs a new point shader.
     *
     * @param shaderManager The {@link ShaderManagerImpl} which should be used to construct the shader.
     */
    public DeferredPointShader(final @NotNull ShaderManager shaderManager) {
        super();

        attachVertexShader(
                shaderManager.requestShaderSource("shaders/deferredRendering/deferredPoint_vs.glsl")
        );
        attachFragmentShader(
                shaderManager.requestShaderSource("shaders/deferredRendering/deferredPoint_fs.glsl")
        );

        linkProgram();

        //VS Uniforms
        MVP_MATRIX_UNIFORM = addUniformLocation("mvpMatrix");

        //FS Uniforms
        SCREEN_SIZE_UNIFORM = addUniformLocation("screenSize");
        CAMERA_POSITION_UNIFORM = addUniformLocation("cameraPosition");
        INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM = addUniformLocation("inverseViewProjectionMatrix");

        DIFFUSE_TEXTURE_UNIFORM = addUniformLocation("colorTexture");
        NORMAL_TEXTURE_UNIFORM = addUniformLocation("normalTexture");
        SPECULAR_TEXTURE_UNIFORM = addUniformLocation("specularTexture");
        DEPTH_TEXTURE_UNIFORM = addUniformLocation("depthTexture");

        LIGHT_POSITION_UNIFORM = addUniformLocation("pointLight.baseLight.position");
        LIGHT_COLOR_UNIFORM = addUniformLocation("pointLight.baseLight.color");
        LIGHT_INTENSITY_UNIFORM = addUniformLocation("pointLight.baseLight.intensity");
        LIGHT_ATTENUATION_QUADRATIC_UNIFORM = addUniformLocation("pointLight.attenuation.quadratic");
        LIGHT_ATTENUATION_LINEAR_UNIFORM = addUniformLocation("pointLight.attenuation.linear");
        LIGHT_ATTENUATION_CONSTANT_UNIFORM = addUniformLocation("pointLight.attenuation.constant");
        LIGHT_DROP_OFF_FACTOR_UNIFORM = addUniformLocation("pointLight.dropOffFactor");
    }

    @Override
    public void setMVPMatrix(final @NotNull Mat4fAccessor mvpMatrix) {
        setUniformM4f(MVP_MATRIX_UNIFORM, mvpMatrix);
    }

    @Override
    public void setScreenSize(final @NotNull Vec2iAccessor screenSize) {
        setUniform2i(SCREEN_SIZE_UNIFORM, screenSize);
    }

    @Override
    public void setCameraPosition(final @NotNull Vec3fAccessor cameraPosition) {
        setUniform3f(CAMERA_POSITION_UNIFORM, cameraPosition);
    }

    @Override
    public void setInverseVP(final @NotNull Mat4fAccessor inverseViewProjectionMatrix) {
        setUniformM4f(INVERSE_VIEW_PROJECTION_MATRIX_UNIFORM, inverseViewProjectionMatrix);
    }

    @Override
    public void setDiffuseTextureUnit(final int textureUnit) {
        setUniform1i(DIFFUSE_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setNormalTextureUnit(final int textureUnit) {
        setUniform1i(NORMAL_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setSpecularTextureUnit(final int textureUnit) {
        setUniform1i(SPECULAR_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setDepthTextureUnit(final int textureUnit) {
        setUniform1i(DEPTH_TEXTURE_UNIFORM, textureUnit);
    }

    @Override
    public void setBaseLight(final @NotNull AbstractLight baseLight) {
        setUniform3f(LIGHT_POSITION_UNIFORM, baseLight.getTransform().getTranslation());
        setUniform3f(LIGHT_COLOR_UNIFORM, baseLight.getColor());
        setUniform1f(LIGHT_INTENSITY_UNIFORM, baseLight.getIntensity());
    }

    public void setPointLight(final @NotNull PointLightImpl pointLight) {
        setBaseLight(pointLight);
        setUniform1f(LIGHT_ATTENUATION_QUADRATIC_UNIFORM, pointLight.getAttenuation().getX());
        setUniform1f(LIGHT_ATTENUATION_LINEAR_UNIFORM, pointLight.getAttenuation().getY());
        setUniform1f(LIGHT_ATTENUATION_CONSTANT_UNIFORM, pointLight.getAttenuation().getZ());
        setUniform1f(LIGHT_DROP_OFF_FACTOR_UNIFORM, pointLight.getDropOffFactor());
    }

}
