package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class ReinhardToneMappingWithGammaAdjustmentShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val gammaUniform: Int
    private val exposureUniform: Int

    init {
        attachVertexShader(
            shaderManager.requestShaderSource(
                "shaders/basic/BasicPosTexPassThrough_vs.glsl"
            )
        )
        attachFragmentShader(
            shaderManager.requestShaderSource(
                "shaders/fx/tonemapping/ReinhardToneMappingWithGammaAdjustment_fs.glsl"
            )
        )
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("hdrTex")
        gammaUniform = addUniformLocation("gamma")
        exposureUniform = addUniformLocation("exposure")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUniform, textureUnit)
    }

    fun setGamma(gamma: Float) {
        setUniform1f(gammaUniform, gamma)
    }

    fun setExposure(exposure: Float) {
        setUniform1f(exposureUniform, exposure)
    }

}
