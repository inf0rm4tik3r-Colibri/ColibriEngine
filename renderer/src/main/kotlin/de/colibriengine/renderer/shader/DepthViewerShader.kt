package de.colibriengine.renderer.shader

import de.colibriengine.graphics.opengl.shader.AbstractOpenGLShader
import de.colibriengine.graphics.shader.MVPTextureShader
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

class DepthViewerShader(shaderManager: ShaderManager) : AbstractOpenGLShader(), MVPTextureShader {

    /* UNIFORMS LOCATIONS */
    private val mvpMatrixUniform: Int
    private val textureUniform: Int
    private val zNearUniform: Int
    private val zFarUniform: Int

    init {
        attachVertexShader(shaderManager.requestShaderSource("shaders/basic/basic_vs.glsl"))
        attachFragmentShader(shaderManager.requestShaderSource("shaders/depth_viewer_fs.glsl"))
        linkProgram()

        // Add uniform locations.
        mvpMatrixUniform = addUniformLocation("mvpMatrix")
        textureUniform = addUniformLocation("depthMap")
        zNearUniform = addUniformLocation("zNear")
        zFarUniform = addUniformLocation("zFar")
    }

    override fun setMVPMatrix(mvpMatrix: Mat4fAccessor) {
        setUniformM4f(mvpMatrixUniform, mvpMatrix)
    }

    override fun setTexture(textureUnit: Int) {
        setUniform1i(textureUniform, textureUnit)
    }

    fun setZNear(zNear: Float) {
        setUniform1f(zNearUniform, zNear)
    }

    fun setZFar(zFar: Float) {
        setUniform1f(zFarUniform, zFar)
    }

}
