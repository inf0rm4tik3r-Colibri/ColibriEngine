package de.colibriengine.renderer

@Deprecated("Use multiple ECS systems instead!")
enum class RenderPass {

    UNSPECIFIED,
    DEFERRED_GEOMETRY,
    SHADOW_MAP_GENERATION,
    GUI_GEOMETRY

}
