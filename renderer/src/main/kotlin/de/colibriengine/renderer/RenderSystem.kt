@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")

package de.colibriengine.renderer

import de.colibriengine.components.TransformCalculationComponent
import de.colibriengine.ecs.System2
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.opengl.rendering.GLBlendFactor
import de.colibriengine.graphics.shader.MVPShader
import de.colibriengine.renderer.shader.BasicTransparencyFilterShader
import de.colibriengine.util.Timer
import org.lwjgl.opengl.GL11

class RenderSystem(
    private var mainRenderShader: MVPShader,
    private var mainTransparencyFilterRenderShader: BasicTransparencyFilterShader,
    private var shadowGenerationShader: MVPShader
) : System2<RenderComponent, TransformCalculationComponent> {

    var renderPass: RenderPass = RenderPass.UNSPECIFIED

    override fun beforeAll() {}

    override fun processEntity(
        render: RenderComponent,
        transformCalculation: TransformCalculationComponent,
        timer: Timer.View
    ) {
        var shader: MVPShader = mainRenderShader

        // Disable face culling if necessary.
        if (!render.cullBackFaces) {
            GL11.glDisable(GL11.GL_CULL_FACE)
        }

        if (render.transparency.isTransparent) {
            shader = mainTransparencyFilterRenderShader
        }

        // Enable blending if necessary.
        if (render.blending.active) {
            GL11.glEnable(GL11.GL_BLEND)
            GL11.glBlendFunc(
                GLBlendFactor.from(render.blending.blendFuncSourceFactor).id,
                GLBlendFactor.from(render.blending.blendFuncDestinationFactor).id
            )
        }

        when {
            // SHADOW MAP GENERATION
            renderPass === RenderPass.SHADOW_MAP_GENERATION && render.shadowCaster -> {
                shadowGenerationShader.bind()
                shadowGenerationShader.setMVPMatrix(
                    transformCalculation.orthographicTransformation
                )
                render.model.render()
            }
            // DEFERRED
            renderPass === RenderPass.DEFERRED_GEOMETRY -> {
                shader.bind()
                shader.setMVPMatrix(
                    transformCalculation.perspectiveTransformation
                )
                if (render.transparency.isTransparent) {
                    (shader as BasicTransparencyFilterShader).setAlphaThreshold(
                        render.transparency.alphaThreshold
                    )
                }
                render.model.render()
            }
            // GEOMETRY
            renderPass === RenderPass.GUI_GEOMETRY -> {
                shader.bind()
                shader.setMVPMatrix(transformCalculation.perspectiveTransformation)
                if (render.transparency.isTransparent) {
                    (shader as BasicTransparencyFilterShader).setAlphaThreshold(
                        render.transparency.alphaThreshold
                    )
                }
                render.model.render()
            }
        }

        // Disable blending
        if (render.blending.active) {
            GL11.glDisable(GL11.GL_BLEND)
        }

        // Enable face culling
        if (!render.cullBackFaces) {
            GL11.glEnable(GL11.GL_CULL_FACE)
        }
    }

}
