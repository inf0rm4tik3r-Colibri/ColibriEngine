package de.colibriengine.renderer

import de.colibriengine.graphics.lighting.shadow.VarianceShadowMapSettings
import de.colibriengine.math.vector.vec2i.StdImmutableVec2i.Companion.builder
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

class RenderOptionsImpl : RenderOptions {

    /** Listeners which must be informed about changes made to this object. */
    private val listeners: ArrayList<RenderOptionsListener> = ArrayList(LISTENERS_INITIAL_CAPACITY)

    /**
     * Specifies if the renderer using these options should be able to dynamically adjust the rendering resolutions
     * based on the current load of the system.
     */
    override var useDynamicResolution = false
        set(useDynamicResolution) {
            field = useDynamicResolution
            for (listener in listeners) {
                listener.rolUseDynamicResolutionChanged(useDynamicResolution)
            }
        }

    /** The width and height (x and y - resolution) in which the main parts/scene (non GUI parts) will be rendered. */
    override var sceneResolution: Vec2iAccessor = StdVec2i().set(DEFAULT_SCENE_RESOLUTION)
        set(sceneResolution) {
            field = sceneResolution
            for (listener in listeners) {
                listener.rolSceneResolutionChanged(sceneResolution)
            }
        }

    /** The minimum width and height (x and y - resolution) in which the scene will be rendered. */
    override var minSceneResolution: Vec2iAccessor = StdVec2i().set(DEFAULT_MIN_SCENE_RESOLUTION)
        set(minSceneResolution) {
            field = minSceneResolution
            for (listener in listeners) {
                listener.rolMinSceneResolutionChanged(minSceneResolution)
            }
        }

    /** The width and height (x and y - resolution) in which the GUI's will be rendered. */
    override var guiResolution: Vec2iAccessor = StdVec2i().set(DEFAULT_GUI_RESOLUTION)
        set(guiResolution) {
            field = guiResolution
            for (listener in listeners) {
                listener.rolGuiResolutionChanged(guiResolution)
            }
        }

    override var targetFramerate = 0f
        set(targetFramerate) {
            field = targetFramerate
            for (listener in listeners) {
                listener.rolTargetFramerateChanged(targetFramerate)
            }
        }

    /** Specifies whether or not FXAA should be applied in post processing. */
    override var useFXAA = true

    override var globalVarianceShadowMapSettings: VarianceShadowMapSettings = VarianceShadowMapSettings()

    /**
     * Adds the specified [RenderOptionsListener] to this [RenderOptionsImpl] instance. Manipulating this
     * instance will result in it being informing every registered listener about the changes made.
     * Adding the same object twice is not allowed and will result in an exception!
     * Call [RenderOptionsImpl.isListening] to check if an object is registered.
     *
     * @param listener The listener to add.
     * @throws IllegalArgumentException If the specified instance is already listening to this instance.
     */
    override fun addListener(listener: RenderOptionsListener) {
        if (isListening(listener)) {
            throw UnsupportedOperationException("This object ($listener) is already listening/registered!")
        }
        listeners.add(listener)
    }

    /**
     * @return Whether or not the specified [RenderOptionsListener] is already listening/registered.
     */
    fun isListening(listener: RenderOptionsListener): Boolean {
        return listeners.contains(listener)
    }

    /**
     * Removes the specified [RenderOptionsListener] from the list of registered objects. It will not be informed
     * anymore.
     * Removing a listener which is not currently listening results in an exception.
     *
     * @param listener The listener to remove.
     * @throws IllegalArgumentException If  the specified instance is not listening to this instance.
     */
    override fun removeListener(listener: RenderOptionsListener) {
        if (!isListening(listener)) {
            throw UnsupportedOperationException("This object ($listener) is not listening/registered!")
        }
        listeners.remove(listener)
    }

    companion object {
        private const val LISTENERS_INITIAL_CAPACITY = 1

        const val DEFAULT_USES_DYNAMIC_RESOLUTION = false
        private val DEFAULT_SCENE_RESOLUTION = builder().of(2560, 1440)
        private val DEFAULT_MIN_SCENE_RESOLUTION = builder().of(1280, 720)
        private val DEFAULT_GUI_RESOLUTION = builder().of(2560, 1440)
    }

}
