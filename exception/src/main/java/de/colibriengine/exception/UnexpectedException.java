package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * This exception should be thrown if the application runs into a state or condition which should never be reached.
 *
 * @version 0.0.0.1
 * @time 26.11.2018 21:17
 * @since ColibriEngine 0.0.7.2
 */
public class UnexpectedException extends IllegalStateException {
    
    public UnexpectedException(final @NotNull String message) {
        super(message);
    }
    
}
