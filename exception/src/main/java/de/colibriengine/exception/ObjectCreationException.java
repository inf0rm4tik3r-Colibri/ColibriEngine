package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

/**
 * ObjectCreationException - Thrown if an object was unable to be created or an object couldn't create itself.
 * TODO: comment, add options
 * 
 *
 * @version 0.0.0.1
 * @time 09.05.2018 17:32
 * @since ColibriEngine
 */
public class ObjectCreationException extends RuntimeException {
    
    public static final String DEFAULT_MSG = "This \"%s\" could not be created!";
    
    public ObjectCreationException(final @NotNull String message) {
        super(message);
    }
    
}
