package de.colibriengine.exception;

public class EndOfStreamException extends RuntimeException {

    private static final String DEFAULT_MESSAGE = "The end of the stream was reached.";

    public EndOfStreamException() {
        this(DEFAULT_MESSAGE);
    }

    public EndOfStreamException(final String message) {
        this(message, null);
    }

    public EndOfStreamException(final Throwable throwable) {
        this(DEFAULT_MESSAGE, throwable);
    }

    public EndOfStreamException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
