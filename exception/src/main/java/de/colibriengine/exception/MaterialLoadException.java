package de.colibriengine.exception;

public class MaterialLoadException extends RuntimeException {

    public MaterialLoadException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
