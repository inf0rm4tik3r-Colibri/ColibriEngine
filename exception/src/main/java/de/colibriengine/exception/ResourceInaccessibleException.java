package de.colibriengine.exception;

import org.jetbrains.annotations.NotNull;

import java.net.URL;

/**
 * This exception indicates that some resource could not be accessed.
 * Might get thrown when one of the *manager classes tries to load a resource.
 *
 * @since ColibriEngine 0.0.7.2
 */
@SuppressWarnings("WeakerAccess")
public class ResourceInaccessibleException extends RuntimeException {

    private static final @NotNull String NOT_EXISTS_OR_IS_INACCESSIBLE =
            "Resource \"%s\" could not be accessed. This resource may not exist or is inaccessible.";

    public static ResourceInaccessibleException forPath(final @NotNull String path) {
        return new ResourceInaccessibleException(String.format(NOT_EXISTS_OR_IS_INACCESSIBLE, path));
    }

    public static ResourceInaccessibleException forPath(final @NotNull String path,
                                                        final @NotNull Throwable throwable) {
        return new ResourceInaccessibleException(String.format(NOT_EXISTS_OR_IS_INACCESSIBLE, path), throwable);
    }

    public ResourceInaccessibleException(final @NotNull String message) {
        super(message, null);
    }

    public ResourceInaccessibleException(final @NotNull String message, final @NotNull Throwable throwable) {
        super(message, throwable);
    }

}
