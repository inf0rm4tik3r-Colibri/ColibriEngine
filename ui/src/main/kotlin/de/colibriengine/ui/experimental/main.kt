import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executors
import kotlin.reflect.KProperty

val threads = 4
val dispatcher = Executors.newFixedThreadPool(threads).asCoroutineDispatcher()
val std = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

fun main() = runBlocking {
    val ui = UI()
    val comp = ui.buildUi { TestComp() }

    while (true) {
        println("__ready__")
        when (val l = readLine()!!) {
            "q" -> break
            "t" -> comp.showSecond = !comp.showSecond
            "c" -> {
                launch(dispatcher) { comp.children.first().processClick(ClickEvent()) }
            }
            else -> comp.x = l.toInt()
        }
    }
}

class TestComp/*(cdr: ChangeDetector)*/ : Component(), ChangeDetectable {
    var x: Int by remember(9)
    var showSecond: Boolean by remember(false)

    init {
        println("CONSTRUCT TestComp")
        onClick = { _ -> println("TestComp clicked") }
    }

    // Called every time a "remember"ed property changed!
    override fun ui() {
        println("TestComp ui with value: ${x}")
        include<ChildComp>("child1") {
            xIn = x
            onClick = { _ ->
                println("ChildComp clicked. Computing....")
                delay(4000)
                println("ChildComp clicked. Computation successful")
                x = 99
            }
        }
        if (showSecond) {
            include<ChildComp>("child2") {
                xIn = 2 * x
            }
        }
    }

    override fun <T : Any> propChanged(prop: KProperty<*>, old: T, new: T) {
        println("Prop $prop changed from $old to $new")
    }
}

class ChildComp : Component(), ChangeDetectable {
    var xIn: Int by remember(-1)

    init {
        println("CONSTRUCT ChildComp!")
    }

    override fun ui() {
        println("Child ui with value: $xIn")
    }

    override fun <T : Any> propChanged(prop: KProperty<*>, old: T, new: T) {
        println("Child prop $prop changed from $old to $new")
    }
}

class ClickEvent {
    var stopPropagation = false
}

interface ChangeDetectable {
    fun <T : Any> propChanged(prop: KProperty<*>, old: T, new: T)
}

class ChangeDetector
