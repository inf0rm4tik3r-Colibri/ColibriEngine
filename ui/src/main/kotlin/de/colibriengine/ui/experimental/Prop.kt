import kotlin.reflect.KProperty

class Prop<T : Any>(private val thisRef: Component, initial: T) {
    /*
    private var value: T by Delegates.observable(initial) { property, oldValue, newValue ->
        onPropertyChange(thisRef, this, property, oldValue, newValue)
    }
    */
    private var value: T = initial

    operator fun getValue(thisRef: Component, prop: KProperty<*>): T = value

    operator fun setValue(thisRef: Component, prop: KProperty<*>, value: T) {
        onPropertyChange(this.thisRef, this, prop, this.value, value)
        this.value = value
        if (this.thisRef.needsUpdate) {
            this.thisRef.update()
            this.thisRef.needsUpdate = false
        }
    }
}


fun <T : Any> onPropertyChange(comp: Component, prop: Prop<T>, kprop: KProperty<*>, old: T, new: T) {
    if (new != old) {
        if (comp is ChangeDetectable) {
            comp.propChanged(kprop, old, new)
        }
        comp.needsUpdate = true
    }
}
