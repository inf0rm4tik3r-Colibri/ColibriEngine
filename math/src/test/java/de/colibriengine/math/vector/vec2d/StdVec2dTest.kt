package de.colibriengine.math.vector.vec2d

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class StdVec2dTest {

  @Test
  fun shouldBeInitializedToZeroAfterCreation() {
    val vec = StdVec2d()
    assertEquals(0.0, vec.x)
    assertEquals(0.0, vec.y)
  }

}
