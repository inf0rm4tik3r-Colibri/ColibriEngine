package de.colibriengine.math

/**
 * Random.
 * TODO: Check which comments like "inclusive"/"exclusive" are true.
 *
 * @since ColibriEngine 0.0.7.0
 */
class Random {

    /** Java's random number generator used to generate random numbers. */
    private val random: java.util.Random

    /**
     * Constructs a new [Random] number generator. The seed gets computed from the current time. A collision with the
     * seed of a previously created instance is highly unlikely.
     */
    constructor() {
        random = java.util.Random()
    }

    /** Constructs a new [Random] number generator with the given [seed]. */
    constructor(seed: Long) {
        random = java.util.Random(seed)
    }

    /**
     * @return A random boolean value.
     */
    fun nextBoolean(): Boolean {
        return random.nextBoolean()
    }

    /**
     * All 2^32 possible int values are produced with (approximately) equal probability.
     *
     * @return A random integer value.
     */
    fun nextInt(): Int {
        return random.nextInt()
    }

    /**
     * Returns a random int between **0** and **max (inclusive)**.
     *
     * @param max
     * The upper limit.
     *
     * @return A random integer value.
     */
    fun nextInt(max: Int): Int {
        return nextInt(0, max)
    }

    /**
     * Returns a random int between **min (inclusive)** and **max (inclusive)**.
     *
     * @param min
     * The lower limit.
     * @param max
     * The upper limit.
     *
     * @return A random integer value.
     */
    fun nextInt(min: Int, max: Int): Int {
        return min + random.nextInt(max - min + 1)
    }

    /**
     * Returns a random float between **0 (inclusive)** and **1 (exclusive)**.
     *
     * @return A random float value.
     */
    fun nextFloat(): Float {
        return random.nextFloat()
    }

    /**
     * Returns a random float between **0** and **[max] (exclusive)**.
     *
     * @param max
     * The upper limit.
     *
     * @return A random float value.
     */
    fun nextFloat(max: Float): Float {
        return random.nextFloat() * max
    }

    /**
     * Returns a random float value between **[min] (inclusive)** and **[max] (exclusive)**.
     *
     * @param min
     * The lower limit.
     * @param max
     * The upper limit.
     *
     * @return A random float value.
     */
    fun nextFloat(min: Float, max: Float): Float {
        return min + random.nextFloat() * (max - min)
    }

    /**
     * Returns a random double between **0** and **1 (exclusive)**.
     *
     * @return A random double value.
     */
    fun nextDouble(): Double {
        return random.nextFloat().toDouble()
    }

    /**
     * Returns a random double between **0** and **max (exclusive)**.
     *
     * @param max
     * The upper limit.
     *
     * @return A random double value.
     */
    fun nextDouble(max: Double): Double {
        return random.nextFloat() * max
    }

    /**
     * Returns a random double value between **min (inclusive)** and **max (exclusive)**.
     *
     * @param min
     * The lower limit.
     * @param max
     * The upper limit.
     *
     * @return A random double value.
     */
    fun nextDouble(min: Double, max: Double): Double {
        return min + random.nextDouble() * (max - min)
    }

}
