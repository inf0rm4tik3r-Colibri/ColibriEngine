package de.colibriengine.math

import de.colibriengine.math.matrix.mat2d.Mat2dFactory
import de.colibriengine.math.matrix.mat2d.StdMat2dFactory
import de.colibriengine.math.matrix.mat2f.Mat2fFactory
import de.colibriengine.math.matrix.mat2f.StdMat2fFactory
import de.colibriengine.math.matrix.mat3d.Mat3dFactory
import de.colibriengine.math.matrix.mat3d.StdMat3dFactory
import de.colibriengine.math.matrix.mat3f.Mat3fFactory
import de.colibriengine.math.matrix.mat3f.StdMat3fFactory
import de.colibriengine.math.matrix.mat4d.Mat4dFactory
import de.colibriengine.math.matrix.mat4d.StdMat4dFactory
import de.colibriengine.math.matrix.mat4f.Mat4fFactory
import de.colibriengine.math.matrix.mat4f.StdMat4fFactory
import de.colibriengine.math.quaternion.quaternionD.QuaternionDFactory
import de.colibriengine.math.quaternion.quaternionD.StdQuaternionDFactory
import de.colibriengine.math.quaternion.quaternionF.QuaternionFFactory
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionFFactory
import de.colibriengine.math.vector.vec2d.StdVec2dFactory
import de.colibriengine.math.vector.vec2d.Vec2dFactory
import de.colibriengine.math.vector.vec2f.StdVec2fFactory
import de.colibriengine.math.vector.vec2f.Vec2fFactory
import de.colibriengine.math.vector.vec2i.StdVec2iFactory
import de.colibriengine.math.vector.vec2i.Vec2iFactory
import de.colibriengine.math.vector.vec2l.StdVec2lFactory
import de.colibriengine.math.vector.vec2l.Vec2lFactory
import de.colibriengine.math.vector.vec3d.StdVec3dFactory
import de.colibriengine.math.vector.vec3d.Vec3dFactory
import de.colibriengine.math.vector.vec3f.StdVec3fFactory
import de.colibriengine.math.vector.vec3f.Vec3fFactory
import de.colibriengine.math.vector.vec3i.StdVec3iFactory
import de.colibriengine.math.vector.vec3i.Vec3iFactory
import de.colibriengine.math.vector.vec3l.StdVec3lFactory
import de.colibriengine.math.vector.vec3l.Vec3lFactory
import de.colibriengine.math.vector.vec4d.StdVec4dFactory
import de.colibriengine.math.vector.vec4d.Vec4dFactory
import de.colibriengine.math.vector.vec4f.StdVec4fFactory
import de.colibriengine.math.vector.vec4f.Vec4fFactory
import de.colibriengine.math.vector.vec4i.StdVec4iFactory
import de.colibriengine.math.vector.vec4i.Vec4iFactory
import de.colibriengine.math.vector.vec4l.StdVec4lFactory
import de.colibriengine.math.vector.vec4l.Vec4lFactory

/**
 * StdMathFactories.
 *
 * 
 */
class StdMathFactories : MathFactories {

    private val quaternionFFactory = StdQuaternionFFactory()
    private val quaternionDFactory = StdQuaternionDFactory()
    private val vec2fFactory = StdVec2fFactory()
    private val vec2dFactory = StdVec2dFactory()
    private val vec2iFactory = StdVec2iFactory()
    private val vec2lFactory = StdVec2lFactory()
    private val vec3fFactory = StdVec3fFactory()
    private val vec3dFactory = StdVec3dFactory()
    private val vec3iFactory = StdVec3iFactory()
    private val vec3lFactory = StdVec3lFactory()
    private val vec4fFactory = StdVec4fFactory()
    private val vec4dFactory = StdVec4dFactory()
    private val vec4iFactory = StdVec4iFactory()
    private val vec4lFactory = StdVec4lFactory()
    private val mat2fFactory = StdMat2fFactory()
    private val mat2dFactory = StdMat2dFactory()
    private val mat3fFactory = StdMat3fFactory()
    private val mat3dFactory = StdMat3dFactory()
    private val mat4fFactory = StdMat4fFactory()
    private val mat4dFactory = StdMat4dFactory()

    override fun quaternionF(): QuaternionFFactory {
        return quaternionFFactory
    }

    override fun quaternionD(): QuaternionDFactory {
        return quaternionDFactory
    }

    override fun vec2f(): Vec2fFactory {
        return vec2fFactory
    }

    override fun vec2d(): Vec2dFactory {
        return vec2dFactory
    }

    override fun vec2i(): Vec2iFactory {
        return vec2iFactory
    }

    override fun vec2l(): Vec2lFactory {
        return vec2lFactory
    }

    override fun vec3f(): Vec3fFactory {
        return vec3fFactory
    }

    override fun vec3d(): Vec3dFactory {
        return vec3dFactory
    }

    override fun vec3i(): Vec3iFactory {
        return vec3iFactory
    }

    override fun vec3l(): Vec3lFactory {
        return vec3lFactory
    }

    override fun vec4f(): Vec4fFactory {
        return vec4fFactory
    }

    override fun vec4d(): Vec4dFactory {
        return vec4dFactory
    }

    override fun vec4i(): Vec4iFactory {
        return vec4iFactory
    }

    override fun vec4l(): Vec4lFactory {
        return vec4lFactory
    }

    override fun mat2f(): Mat2fFactory {
        return mat2fFactory
    }

    override fun mat2d(): Mat2dFactory {
        return mat2dFactory
    }

    override fun mat3f(): Mat3fFactory {
        return mat3fFactory
    }

    override fun mat3d(): Mat3dFactory {
        return mat3dFactory
    }

    override fun mat4f(): Mat4fFactory {
        return mat4fFactory
    }

    override fun mat4d(): Mat4dFactory {
        return mat4dFactory
    }

}
