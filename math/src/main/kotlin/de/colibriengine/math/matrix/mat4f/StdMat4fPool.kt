package de.colibriengine.math.matrix.mat4f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat4fPool.
 *
 * 
 */
class StdMat4fPool : ObjectPool<Mat4f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat4f()
    },
    { instance: Mat4f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat4fPool::class.java)
    }

}
