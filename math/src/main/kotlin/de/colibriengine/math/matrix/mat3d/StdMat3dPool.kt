package de.colibriengine.math.matrix.mat3d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat3dPool.
 *
 *
 */
class StdMat3dPool : ObjectPool<Mat3d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat3d()
    },
    { instance: Mat3d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat3dPool::class.java)
    }

}
