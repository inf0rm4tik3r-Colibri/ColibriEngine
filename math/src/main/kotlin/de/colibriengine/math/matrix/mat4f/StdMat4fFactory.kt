package de.colibriengine.math.matrix.mat4f

/**
 * StdMat4fFactory.
 *
 * 
 */
class StdMat4fFactory : Mat4fFactory {

    private val pool: StdMat4fPool = StdMat4fPool()

    override fun acquire(): Mat4f = pool.acquire()

    override fun acquireDirty(): Mat4f = pool.acquireDirty()

    override fun free(mat4f: Mat4f) = pool.free(mat4f)

    override fun immutableMat4fBuilder(): ImmutableMat4f.Builder = StdImmutableMat4f.builder()

    override fun zero(): ImmutableMat4f = ZERO

    override fun one(): ImmutableMat4f = ONE

    override fun identity(): ImmutableMat4f = IDENTITY

    companion object {
        val ZERO = StdImmutableMat4f.builder().of(0.0f)
        val ONE = StdImmutableMat4f.builder().of(1.0f)
        val IDENTITY = StdImmutableMat4f.builder().set(
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f
        ).build()
    }

}
