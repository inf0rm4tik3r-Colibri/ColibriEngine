package de.colibriengine.math.matrix.mat2d

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/**
 * Mat2dPool.
 *
 * 
 */
class StdMat2dPool : ObjectPool<Mat2d>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdMat2d()
    },
    { instance: Mat2d -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdMat2dPool::class.java)
    }

}
