package de.colibriengine.math.matrix.mat4f

import de.colibriengine.math.matrix.mat3f.Mat3f
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/**
 * StdImmutableMat4f.
 *
 * 
 */
@Suppress("DuplicatedCode")
class StdImmutableMat4f private constructor(
    m00: Float, m01: Float, m02: Float, m03: Float,
    m10: Float, m11: Float, m12: Float, m13: Float,
    m20: Float, m21: Float, m22: Float, m23: Float,
    m30: Float, m31: Float, m32: Float, m33: Float
) : ImmutableMat4f {

    private val mutableAccessor: Mat4fAccessor = StdMat4f(
        m00, m01, m02, m03,
        m10, m11, m12, m13,
        m20, m21, m22, m23,
        m30, m31, m32, m33
    )

    override val m00: Float = mutableAccessor.m00
    override val m01: Float = mutableAccessor.m01
    override val m02: Float = mutableAccessor.m02
    override val m03: Float = mutableAccessor.m03
    override val m10: Float = mutableAccessor.m10
    override val m11: Float = mutableAccessor.m11
    override val m12: Float = mutableAccessor.m12
    override val m13: Float = mutableAccessor.m13
    override val m20: Float = mutableAccessor.m20
    override val m21: Float = mutableAccessor.m21
    override val m22: Float = mutableAccessor.m22
    override val m23: Float = mutableAccessor.m23
    override val m30: Float = mutableAccessor.m30
    override val m31: Float = mutableAccessor.m31
    override val m32: Float = mutableAccessor.m32
    override val m33: Float = mutableAccessor.m33

    override fun get(row: Int, column: Int): Float = mutableAccessor[row, column]
    override fun getRow(row: Int, storeIn: Vec4f): Vec4f = mutableAccessor.getRow(row, storeIn)
    override fun getRow1(storeIn: Vec4f): Vec4f = mutableAccessor.getRow1(storeIn)
    override fun getRow2(storeIn: Vec4f): Vec4f = mutableAccessor.getRow2(storeIn)
    override fun getRow3(storeIn: Vec4f): Vec4f = mutableAccessor.getRow3(storeIn)
    override fun getRow4(storeIn: Vec4f): Vec4f = mutableAccessor.getRow4(storeIn)

    override fun getColumn(column: Int, storeIn: Vec4f): Vec4f = mutableAccessor.getColumn(column, storeIn)
    override fun getColumn1(storeIn: Vec4f): Vec4f = mutableAccessor.getColumn1(storeIn)
    override fun getColumn2(storeIn: Vec4f): Vec4f = mutableAccessor.getColumn2(storeIn)
    override fun getColumn3(storeIn: Vec4f): Vec4f = mutableAccessor.getColumn3(storeIn)
    override fun getColumn4(storeIn: Vec4f): Vec4f = mutableAccessor.getColumn4(storeIn)

    override fun getMajorDiagonal(storeIn: Vec4f): Vec4f = mutableAccessor.getMajorDiagonal(storeIn)

    override fun getRight(storeIn: Vec3f): Vec3f = mutableAccessor.getRight(storeIn)
    override fun getUp(storeIn: Vec3f): Vec3f = mutableAccessor.getUp(storeIn)
    override fun getForward(storeIn: Vec3f): Vec3f = mutableAccessor.getForward(storeIn)

    override fun asQuaternion(storeIn: QuaternionF): QuaternionF = mutableAccessor.asQuaternion(storeIn)

    override fun asArray(): Array<out FloatArray> = mutableAccessor.asArray()
    override fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray> = mutableAccessor.asArray(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun getSubmatrix(row: Int, column: Int, storeIn: Mat3f): Mat3f =
        mutableAccessor.getSubmatrix(row, column, storeIn)

    override fun transform(vector: Vec4f): Vec4f = mutableAccessor.transform(vector)

    override fun determinant(): Float = mutableAccessor.determinant()

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString()

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableMat4f.Builder {
        private var m00: Float = 0.0f
        private var m01: Float = 0.0f
        private var m02: Float = 0.0f
        private var m03: Float = 0.0f
        private var m10: Float = 0.0f
        private var m11: Float = 0.0f
        private var m12: Float = 0.0f
        private var m13: Float = 0.0f
        private var m20: Float = 0.0f
        private var m21: Float = 0.0f
        private var m22: Float = 0.0f
        private var m23: Float = 0.0f
        private var m30: Float = 0.0f
        private var m31: Float = 0.0f
        private var m32: Float = 0.0f
        private var m33: Float = 0.0f

        override fun of(value: Float): ImmutableMat4f = set(value).build()

        override fun of(
            m00: Float, m01: Float, m02: Float, m03: Float,
            m10: Float, m11: Float, m12: Float, m13: Float,
            m20: Float, m21: Float, m22: Float, m23: Float,
            m30: Float, m31: Float, m32: Float, m33: Float
        ): ImmutableMat4f {
            return set(
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33
            ).build()
        }

        override fun of(other: Mat4fAccessor): ImmutableMat4f = set(other).build()

        override fun ofColumns(
            column1: Vec4fAccessor,
            column2: Vec4fAccessor,
            column3: Vec4fAccessor,
            column4: Vec4fAccessor
        ): ImmutableMat4f {
            return setColumn1(column1)
                .setColumn2(column2)
                .setColumn3(column3)
                .setColumn4(column4)
                .build()
        }

        override fun ofRows(
            row1: Vec4fAccessor,
            row2: Vec4fAccessor,
            row3: Vec4fAccessor,
            row4: Vec4fAccessor
        ): ImmutableMat4f {
            return setRow1(row1)
                .setRow2(row2)
                .setRow3(row3)
                .setRow4(row4)
                .build()
        }

        override fun set00(m00: Float): ImmutableMat4f.Builder {
            this.m00 = m00
            return this
        }

        override fun set01(m01: Float): ImmutableMat4f.Builder {
            this.m01 = m01
            return this
        }

        override fun set02(m02: Float): ImmutableMat4f.Builder {
            this.m02 = m02
            return this
        }

        override fun set03(m03: Float): ImmutableMat4f.Builder {
            this.m03 = m03
            return this
        }

        override fun set10(m10: Float): ImmutableMat4f.Builder {
            this.m10 = m10
            return this
        }

        override fun set11(m11: Float): ImmutableMat4f.Builder {
            this.m11 = m11
            return this
        }

        override fun set12(m12: Float): ImmutableMat4f.Builder {
            this.m12 = m12
            return this
        }

        override fun set13(m13: Float): ImmutableMat4f.Builder {
            this.m13 = m13
            return this
        }

        override fun set20(m20: Float): ImmutableMat4f.Builder {
            this.m20 = m20
            return this
        }

        override fun set21(m21: Float): ImmutableMat4f.Builder {
            this.m21 = m21
            return this
        }

        override fun set22(m22: Float): ImmutableMat4f.Builder {
            this.m22 = m22
            return this
        }

        override fun set23(m23: Float): ImmutableMat4f.Builder {
            this.m23 = m23
            return this
        }

        override fun set30(m30: Float): ImmutableMat4f.Builder {
            this.m30 = m30
            return this
        }

        override fun set31(m31: Float): ImmutableMat4f.Builder {
            this.m31 = m31
            return this
        }

        override fun set32(m32: Float): ImmutableMat4f.Builder {
            this.m32 = m32
            return this
        }

        override fun set33(m33: Float): ImmutableMat4f.Builder {
            this.m33 = m33
            return this
        }

        override fun set(value: Float): ImmutableMat4f.Builder {
            m00 = value
            m01 = value
            m02 = value
            m03 = value
            m10 = value
            m11 = value
            m12 = value
            m13 = value
            m20 = value
            m21 = value
            m22 = value
            m23 = value
            m30 = value
            m31 = value
            m32 = value
            m33 = value
            return this
        }

        override fun set(
            m00: Float, m01: Float, m02: Float, m03: Float,
            m10: Float, m11: Float, m12: Float, m13: Float,
            m20: Float, m21: Float, m22: Float, m23: Float,
            m30: Float, m31: Float, m32: Float, m33: Float
        ): ImmutableMat4f.Builder {
            this.m00 = m00
            this.m01 = m01
            this.m02 = m02
            this.m03 = m03
            this.m10 = m10
            this.m11 = m11
            this.m12 = m12
            this.m13 = m13
            this.m20 = m20
            this.m21 = m21
            this.m22 = m22
            this.m23 = m23
            this.m30 = m30
            this.m31 = m31
            this.m32 = m32
            this.m33 = m33
            return this
        }

        override fun set(other: Mat4fAccessor): ImmutableMat4f.Builder {
            m00 = other.m00
            m01 = other.m01
            m02 = other.m02
            m03 = other.m03
            m10 = other.m10
            m11 = other.m11
            m12 = other.m12
            m13 = other.m13
            m20 = other.m20
            m21 = other.m21
            m22 = other.m22
            m23 = other.m23
            m30 = other.m30
            m31 = other.m31
            m32 = other.m32
            m33 = other.m33
            return this
        }

        override fun setColumn1(column1: Vec4fAccessor): ImmutableMat4f.Builder {
            m00 = column1.x
            m10 = column1.y
            m20 = column1.z
            m30 = column1.w
            return this
        }

        override fun setColumn2(column2: Vec4fAccessor): ImmutableMat4f.Builder {
            m01 = column2.x
            m11 = column2.y
            m21 = column2.z
            m31 = column2.w
            return this
        }

        override fun setColumn3(column3: Vec4fAccessor): ImmutableMat4f.Builder {
            m02 = column3.x
            m12 = column3.y
            m22 = column3.z
            m32 = column3.w
            return this
        }

        override fun setColumn4(column4: Vec4fAccessor): ImmutableMat4f.Builder {
            m03 = column4.x
            m13 = column4.y
            m23 = column4.z
            m33 = column4.w
            return this
        }

        override fun setRow1(row1: Vec4fAccessor): ImmutableMat4f.Builder {
            m00 = row1.x
            m01 = row1.y
            m02 = row1.z
            m03 = row1.w
            return this
        }

        override fun setRow2(row2: Vec4fAccessor): ImmutableMat4f.Builder {
            m10 = row2.x
            m11 = row2.y
            m12 = row2.z
            m13 = row2.w
            return this
        }

        override fun setRow3(row3: Vec4fAccessor): ImmutableMat4f.Builder {
            m20 = row3.x
            m21 = row3.y
            m22 = row3.z
            m23 = row3.w
            return this
        }

        override fun setRow4(row4: Vec4fAccessor): ImmutableMat4f.Builder {
            m30 = row4.x
            m31 = row4.y
            m32 = row4.z
            m33 = row4.w
            return this
        }

        override fun build(): ImmutableMat4f {
            return StdImmutableMat4f(
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33
            )
        }
    }

    companion object {
        fun builder(): ImmutableMat4f.Builder = Builder()
    }

}
