package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.vector.vec3i.StdVec3i
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable four dimensional vector in integer precision. */
@Suppress("DuplicatedCode")
class StdVec4i(
    override var x: Int = 0,
    override var y: Int = 0,
    override var z: Int = 0,
    override var w: Int = 0
) : Vec4i {

    private var _view: Vec4iView? = null
    override val view: Vec4iView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Int): Vec4i {
        this.x = x
        return this
    }

    override fun setY(y: Int): Vec4i {
        this.y = y
        return this
    }

    override fun setZ(z: Int): Vec4i {
        this.z = z
        return this
    }

    override fun setW(w: Int): Vec4i {
        this.w = w
        return this
    }
    */

    override fun set(value: Int): Vec4i {
        x = value
        y = value
        z = value
        w = value
        return this
    }

    override fun set(x: Int, y: Int, z: Int, w: Int): Vec4i {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
        return this
    }

    override fun set(other: Vec4fAccessor): Vec4i {
        this.x = other.x.toInt()
        this.y = other.y.toInt()
        this.z = other.z.toInt()
        this.w = other.w.toInt()
        return this
    }

    override fun set(other: Vec4dAccessor): Vec4i {
        this.x = other.x.toInt()
        this.y = other.y.toInt()
        this.z = other.z.toInt()
        this.w = other.w.toInt()
        return this
    }

    override fun set(other: Vec4iAccessor): Vec4i {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = other.w
        return this
    }

    override fun set(other: Vec4lAccessor): Vec4i {
        this.x = other.x.toInt()
        this.y = other.y.toInt()
        this.z = other.z.toInt()
        this.w = other.w.toInt()
        return this
    }

    override fun set(other: Vec3iAccessor, w: Int): Vec4i {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = w
        return this
    }

    override fun initZero(): Vec4i {
        w = 0
        z = w
        y = z
        x = y
        return this
    }

    override operator fun plus(addend: Int): Vec4i {
        x += addend
        y += addend
        z += addend
        w += addend
        return this
    }

    override fun plus(addendX: Int, addendY: Int, addendZ: Int, addendW: Int): Vec4i {
        x += addendX
        y += addendY
        z += addendZ
        w += addendW
        return this
    }

    override operator fun plus(addends: Vec4iAccessor): Vec4i {
        x += addends.x
        y += addends.y
        z += addends.z
        w += addends.w
        return this
    }

    override operator fun minus(subtrahend: Int): Vec4i {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        w -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Int, subtrahendY: Int, subtrahendZ: Int, subtrahendW: Int): Vec4i {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        w -= subtrahendW
        return this
    }

    override operator fun minus(subtrahends: Vec4iAccessor): Vec4i {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        w -= subtrahends.w
        return this
    }

    override operator fun times(factor: Int): Vec4i {
        x *= factor
        y *= factor
        z *= factor
        w *= factor
        return this
    }

    override fun times(factorX: Int, factorY: Int, factorZ: Int, factorW: Int): Vec4i {
        x *= factorX
        y *= factorY
        z *= factorZ
        w *= factorW
        return this
    }

    override operator fun times(factors: Vec4iAccessor): Vec4i {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        w *= factors.w
        return this
    }

    override operator fun div(divisor: Int): Vec4i {
        require(divisor != 0) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        w /= divisor
        return this
    }

    override fun div(divisorX: Int, divisorY: Int, divisorZ: Int, divisorW: Int): Vec4i {
        require(!(divisorX == 0 || divisorY == 0 || divisorZ == 0 || divisorW == 0)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        w /= divisorW
        return this
    }

    override operator fun div(divisors: Vec4iAccessor): Vec4i {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        w /= divisors.w
        return this
    }

    override fun normalize(): Vec4i {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length.toInt()
        y /= length.toInt()
        z /= length.toInt()
        w /= length.toInt()
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec4i {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toInt()
        y *= scaleFactor.toInt()
        z *= scaleFactor.toInt()
        w *= scaleFactor.toInt()
        return this
    }

    override fun invert(): Vec4i {
        x = -x
        y = -y
        z = -z
        w = -w
        return this
    }

    override fun abs(): Vec4i {
        x = abs(x)
        y = abs(y)
        z = abs(z)
        w = abs(w)
        return this
    }

    override fun xyz(): StdVec3i = StdVec3i().set(x, y, z)

    override fun xyw(): StdVec3i = StdVec3i().set(x, y, w)

    override fun xzw(): StdVec3i = StdVec3i().set(x, z, w)

    override fun yzw(): StdVec3i = StdVec3i().set(y, z, w)

    override fun hasZeroComponent(): Boolean = x == 0 || y == 0 || z == 0 || w == 0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y + z * z + w * w).toDouble()

    override fun min(): Int = min(x, min(y, min(z, w)))

    override fun max(): Int = max(x, max(y, max(z, w)))

    override fun lerp(target: Vec4iAccessor, lerpFactor: Int): Vec4i {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec4iAccessor, lerpFactor: Int): Vec4i =
        StdVec4i().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec4i.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec4i [ ")
            .appendAligned(x.toFloat()).append(", ")
            .appendAligned(y.toFloat()).append(", ")
            .appendAligned(z.toFloat()).append(", ")
            .appendAligned(w.toFloat())
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putInt(x).putInt(y).putInt(z).putInt(w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat()).put(w.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toDouble()).put(y.toDouble()).put(z.toDouble()).put(w.toDouble())
    }

    override fun copy(): Vec4i = StdVec4i().set(x, y, z, w)

    override fun asImmutable(): ImmutableVec4i = StdImmutableVec4i.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        hashCode = 31 * hashCode + z.hashCode()
        hashCode = 31 * hashCode + w.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec4i -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
                && this.w == other.w
        }
    }

    override fun toString(): String = "Vec4i [ $x, $y, $z, $w ]"

    /** Vec4i viewer, granting read-only access to its parent data. */
    private inner class Viewer : Vec4iView {
        override val x: Int = this@StdVec4i.x
        override val y: Int = this@StdVec4i.y
        override val z: Int = this@StdVec4i.y
        override val w: Int = this@StdVec4i.y

        override fun xyz(): StdVec3i = this@StdVec4i.xyz()
        override fun xyw(): StdVec3i = this@StdVec4i.xyw()
        override fun xzw(): StdVec3i = this@StdVec4i.xzw()
        override fun yzw(): StdVec3i = this@StdVec4i.yzw()

        override fun hasZeroComponent(): Boolean = this@StdVec4i.hasZeroComponent()

        override fun length(): Double = this@StdVec4i.length()
        override fun squaredLength(): Double = this@StdVec4i.squaredLength()

        override fun min(): Int = this@StdVec4i.min()
        override fun max(): Int = this@StdVec4i.max()

        override fun lerp(target: Vec4iAccessor, lerpFactor: Int): Vec4i = this@StdVec4i.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec4iAccessor, lerpFactor: Int): Vec4i =
            this@StdVec4i.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec4i.bytes()

        override fun toFormattedString(): String = this@StdVec4i.toFormattedString()
        override fun toString(): String = this@StdVec4i.toString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec4i.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec4i.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec4i.storeIn(buffer)

        override fun asImmutable(): ImmutableVec4i = StdImmutableVec4i.builder().of(this)
        override fun asMutable(): Vec4i = StdVec4i().set(this)
    }

}
