package de.colibriengine.math.vector.vec3f

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec3fPool : ObjectPool<Vec3f>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec3f()
    },
    { instance: Vec3f -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec3fPool::class.java)
    }

}
