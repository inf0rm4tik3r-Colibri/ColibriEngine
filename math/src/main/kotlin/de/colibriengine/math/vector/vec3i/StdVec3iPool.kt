package de.colibriengine.math.vector.vec3i

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec3iPool : ObjectPool<Vec3i>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec3i()
    },
    { instance: Vec3i -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec3iPool::class.java)
    }

}
