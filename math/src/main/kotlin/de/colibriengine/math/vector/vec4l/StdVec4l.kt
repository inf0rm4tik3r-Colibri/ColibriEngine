package de.colibriengine.math.vector.vec4l

import de.colibriengine.math.vector.vec3l.StdVec3l
import de.colibriengine.math.vector.vec3l.Vec3l
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

/** Mutable four dimensional vector in long precision. */
@Suppress("DuplicatedCode")
class StdVec4l(
    override var x: Long = 0,
    override var y: Long = 0,
    override var z: Long = 0,
    override var w: Long = 0
) : Vec4l {

    private var _view: Vec4lView? = null
    override val view: Vec4lView
        get() {
            if (_view == null) {
                _view = Viewer()
            }

            return _view!!
        }

    override fun set(value: Long): Vec4l {
        x = value
        y = value
        z = value
        w = value
        return this
    }

    override fun set(x: Long, y: Long, z: Long, w: Long): Vec4l {
        this.x = x
        this.y = y
        this.z = z
        this.w = w
        return this
    }

    override fun set(other: Vec4fAccessor): Vec4l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        this.z = other.z.toLong()
        this.w = other.w.toLong()
        return this
    }

    override fun set(other: Vec4dAccessor): Vec4l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        this.z = other.z.toLong()
        this.w = other.w.toLong()
        return this
    }

    override fun set(other: Vec4iAccessor): Vec4l {
        this.x = other.x.toLong()
        this.y = other.y.toLong()
        this.z = other.z.toLong()
        this.w = other.w.toLong()
        return this
    }

    override fun set(other: Vec4lAccessor): Vec4l {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = other.w
        return this
    }

    override fun set(other: Vec3lAccessor, w: Long): Vec4l {
        this.x = other.x
        this.y = other.y
        this.z = other.z
        this.w = w
        return this
    }

    override fun initZero(): Vec4l {
        x = 0
        y = 0
        z = 0
        w = 0
        return this
    }

    override operator fun plus(addend: Long): Vec4l {
        x += addend
        y += addend
        z += addend
        w += addend
        return this
    }

    override fun plus(addendX: Long, addendY: Long, addendZ: Long, addendW: Long): Vec4l {
        x += addendX
        y += addendY
        z += addendZ
        w += addendW
        return this
    }

    override operator fun plus(addends: Vec4lAccessor): Vec4l {
        x += addends.x
        y += addends.y
        z += addends.z
        w += addends.w
        return this
    }

    override operator fun minus(subtrahend: Long): Vec4l {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        w -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Long, subtrahendY: Long, subtrahendZ: Long, subtrahendW: Long): Vec4l {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        w -= subtrahendW
        return this
    }

    override operator fun minus(subtrahends: Vec4lAccessor): Vec4l {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        w -= subtrahends.w
        return this
    }

    override operator fun times(factor: Long): Vec4l {
        x *= factor
        y *= factor
        z *= factor
        w *= factor
        return this
    }

    override fun times(factorX: Long, factorY: Long, factorZ: Long, factorW: Long): Vec4l {
        x *= factorX
        y *= factorY
        z *= factorZ
        w *= factorW
        return this
    }

    override operator fun times(factors: Vec4lAccessor): Vec4l {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        w *= factors.w
        return this
    }

    override operator fun div(divisor: Long): Vec4l {
        require(divisor != 0L) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        w /= divisor
        return this
    }

    override fun div(divisorX: Long, divisorY: Long, divisorZ: Long, divisorW: Long): Vec4l {
        require(!(divisorX == 0L || divisorY == 0L || divisorZ == 0L || divisorW == 0L)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        w /= divisorW
        return this
    }

    override operator fun div(divisors: Vec4lAccessor): Vec4l {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        w /= divisors.w
        return this
    }

    override fun normalize(): Vec4l {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length.toLong()
        y /= length.toLong()
        z /= length.toLong()
        w /= length.toLong()
        return this
    }

    override fun setLengthTo(targetValue: Double): StdVec4l {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toLong()
        y *= scaleFactor.toLong()
        z *= scaleFactor.toLong()
        w *= scaleFactor.toLong()
        return this
    }

    override fun invert(): Vec4l {
        x = -x
        y = -y
        z = -z
        w = -w
        return this
    }

    override fun abs(): Vec4l {
        x = abs(x)
        y = abs(y)
        z = abs(z)
        w = abs(w)
        return this
    }

    override fun xyz(): Vec3l = StdVec3l().set(x, y, z)

    override fun xyw(): Vec3l = StdVec3l().set(x, y, w)

    override fun xzw(): Vec3l = StdVec3l().set(x, z, w)

    override fun yzw(): Vec3l = StdVec3l().set(y, z, w)

    override fun hasZeroComponent(): Boolean = x == 0L || y == 0L || z == 0L || w == 0L

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y + z * z + w * w).toDouble()

    override fun min(): Long = min(x, min(y, min(z, w)))

    override fun max(): Long = max(x, max(y, max(z, w)))

    override fun lerp(target: Vec4lAccessor, lerpFactor: Long): Vec4l {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec4lAccessor, lerpFactor: Long): Vec4l =
        StdVec4l().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec4l.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec4l [ ")
            .appendAligned(x.toFloat()).append(", ")
            .appendAligned(y.toFloat()).append(", ")
            .appendAligned(z.toFloat()).append(", ")
            .appendAligned(w.toFloat())
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.putLong(x).putLong(y).putLong(z).putLong(w)
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat()).put(w.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 4)
        return buffer.put(x.toDouble()).put(y.toDouble()).put(z.toDouble()).put(w.toDouble())
    }

    override fun copy(): Vec4l = StdVec4l().set(x, y, z, w)

    override fun asImmutable(): ImmutableVec4l = StdImmutableVec4l.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        hashCode = 31 * hashCode + z.hashCode()
        hashCode = 31 * hashCode + w.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec4l -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
                && this.w == other.w
        }
    }

    override fun toString(): String = "Vec4l [ $x, $y, $z, $w ]"

    /**
     * Vec4l viewer, granting read-only access to its parent data.
     *
     * @author Lukas Potthast
     * @since ColibriEngine 0.0.7.2
     */
    inner class Viewer : Vec4lView {
        override val x: Long = this@StdVec4l.x
        override val y: Long = this@StdVec4l.y
        override val z: Long = this@StdVec4l.y
        override val w: Long = this@StdVec4l.y

        override fun xyz(): Vec3l = this@StdVec4l.xyz()
        override fun xyw(): Vec3l = this@StdVec4l.xyw()
        override fun xzw(): Vec3l = this@StdVec4l.xzw()
        override fun yzw(): Vec3l = this@StdVec4l.yzw()

        override fun hasZeroComponent(): Boolean = this@StdVec4l.hasZeroComponent()

        override fun length(): Double = this@StdVec4l.length()
        override fun squaredLength(): Double = this@StdVec4l.squaredLength()

        override fun min(): Long = this@StdVec4l.min()
        override fun max(): Long = this@StdVec4l.max()

        override fun lerp(target: Vec4lAccessor, lerpFactor: Long): Vec4l = this@StdVec4l.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec4lAccessor, lerpFactor: Long): Vec4l =
            this@StdVec4l.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec4l.bytes()

        override fun toFormattedString(): String = this@StdVec4l.toFormattedString()
        override fun toString(): String = this@StdVec4l.toString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec4l.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec4l.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec4l.storeIn(buffer)

        override fun asImmutable(): ImmutableVec4l = this@StdVec4l.asImmutable()
        override fun asMutable(): Vec4l = StdVec4l().set(this)
    }

}
