package de.colibriengine.math.vector.vec2l

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec2lPool : ObjectPool<Vec2l>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec2l()
    },
    { instance: Vec2l -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec2lPool::class.java)
    }

}
