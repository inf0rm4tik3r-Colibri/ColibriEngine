package de.colibriengine.math.vector.vec3i

/** StdVec3iFactory. */
class StdVec3iFactory : Vec3iFactory {

    private val pool = StdVec3iPool()

    override fun acquire(): Vec3i = pool.acquire()

    override fun acquireDirty(): Vec3i = pool.acquireDirty()

    override fun free(vec3i: Vec3i) = pool.free(vec3i)

    override fun immutableVec3iBuilder(): ImmutableVec3i.Builder = StdImmutableVec3i.builder()

    override fun zero(): ImmutableVec3i = ZERO

    override fun one(): ImmutableVec3i = ONE

    override fun unitXAxis(): ImmutableVec3i = UNIT_X_AXIS

    override fun unitYAxis(): ImmutableVec3i = UNIT_Y_AXIS

    override fun unitZAxis(): ImmutableVec3i = UNIT_Z_AXIS

    override fun negativeUnitXAxis(): ImmutableVec3i = NEG_UNIT_X_AXIS

    override fun negativeUnitYAxis(): ImmutableVec3i = NEG_UNIT_Y_AXIS

    override fun negativeUnitZAxis(): ImmutableVec3i = NEG_UNIT_Z_AXIS

    companion object {
        val ZERO = StdImmutableVec3i.builder().of(0)
        val ONE = StdImmutableVec3i.builder().of(1)
        val UNIT_X_AXIS = StdImmutableVec3i.builder().of(1, 0, 0)
        val UNIT_Y_AXIS = StdImmutableVec3i.builder().of(0, 1, 0)
        val UNIT_Z_AXIS = StdImmutableVec3i.builder().of(0, 0, 1)
        val NEG_UNIT_X_AXIS = StdImmutableVec3i.builder().of(-1, 0, 0)
        val NEG_UNIT_Y_AXIS = StdImmutableVec3i.builder().of(0, -1, 0)
        val NEG_UNIT_Z_AXIS = StdImmutableVec3i.builder().of(0, 0, -1)
    }

}
