package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.vector.vec2l.StdVec2l
import de.colibriengine.math.vector.vec2l.Vec2l
import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.*

/** Mutable three dimensional vector in long precision. */
@Suppress("DuplicatedCode")
class StdVec3l(
    override var x: Long = 0L,
    override var y: Long = 0L,
    override var z: Long = 0L
) : Vec3l {

    private var _view: Vec3lView? = null
    override val view: Vec3lView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Long): StdVec3l {
        this.x = x
        return this
    }

    override fun setY(y: Long): StdVec3l {
        this.y = y
        return this
    }

    override fun setZ(z: Long): StdVec3l {
        this.z = z
        return this
    }
    */

    override fun set(value: Long): StdVec3l {
        x = value
        y = value
        z = value
        return this
    }

    override fun set(x: Long, y: Long, z: Long): StdVec3l {
        this.x = x
        this.y = y
        this.z = z
        return this
    }

    override fun set(other: Vec3fAccessor): StdVec3l {
        x = other.x.toLong()
        y = other.y.toLong()
        z = other.z.toLong()
        return this
    }

    override fun set(other: Vec3dAccessor): StdVec3l {
        x = other.x.toLong()
        y = other.y.toLong()
        z = other.z.toLong()
        return this
    }

    override fun set(other: Vec3iAccessor): StdVec3l {
        x = other.x.toLong()
        y = other.y.toLong()
        z = other.z.toLong()
        return this
    }

    override fun set(other: Vec3lAccessor): StdVec3l {
        x = other.x
        y = other.y
        z = other.z
        return this
    }

    override fun set(other: Vec2lAccessor, z: Long): Vec3l {
        x = other.x
        y = other.y
        this.z = z
        return this
    }

    override fun initZero(): StdVec3l {
        x = 0L
        y = 0L
        z = 0L
        return this
    }

    override operator fun plus(addend: Long): StdVec3l {
        x += addend
        y += addend
        z += addend
        return this
    }

    override fun plus(addendX: Long, addendY: Long, addendZ: Long): StdVec3l {
        x += addendX
        y += addendY
        z += addendZ
        return this
    }

    override operator fun plus(addends: Vec3lAccessor): StdVec3l {
        x += addends.x
        y += addends.y
        z += addends.z
        return this
    }

    override operator fun minus(subtrahend: Long): StdVec3l {
        x -= subtrahend
        y -= subtrahend
        z -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Long, subtrahendY: Long, subtrahendZ: Long): StdVec3l {
        x -= subtrahendX
        y -= subtrahendY
        z -= subtrahendZ
        return this
    }

    override operator fun minus(subtrahends: Vec3lAccessor): StdVec3l {
        x -= subtrahends.x
        y -= subtrahends.y
        z -= subtrahends.z
        return this
    }

    override operator fun times(factor: Long): StdVec3l {
        x *= factor
        y *= factor
        z *= factor
        return this
    }

    override fun times(factorX: Long, factorY: Long, factorZ: Long): StdVec3l {
        x *= factorX
        y *= factorY
        z *= factorZ
        return this
    }

    override operator fun times(factors: Vec3lAccessor): StdVec3l {
        x *= factors.x
        y *= factors.y
        z *= factors.z
        return this
    }

    override operator fun div(divisor: Long): StdVec3l {
        require(divisor != 0L) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        z /= divisor
        return this
    }

    override fun div(divisorX: Long, divisorY: Long, divisorZ: Long): StdVec3l {
        require(!(divisorX == 0L || divisorY == 0L || divisorZ == 0L)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        z /= divisorZ
        return this
    }

    override operator fun div(divisors: Vec3lAccessor): StdVec3l {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        z /= divisors.z
        return this
    }

    override fun normalize(): StdVec3l {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!" }
        x /= length.toLong()
        y /= length.toLong()
        z /= length.toLong()
        return this
    }

    override fun setLengthTo(targetValue: Double): StdVec3l {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor.toLong()
        y *= scaleFactor.toLong()
        z *= scaleFactor.toLong()
        return this
    }

    override fun invert(): Vec3l {
        x = -x
        y = -y
        z = -z
        return this
    }

    override fun abs(): Vec3l {
        x = abs(x)
        y = abs(y)
        z = abs(z)
        return this
    }

    override fun cross(other: Vec3lAccessor): Vec3l {
        val xNew = y * other.z - z * other.y
        val yNew = z * other.x - x * other.z
        val zNew = x * other.y - y * other.x
        x = xNew
        y = yNew
        z = zNew
        return this
    }

    override fun xy(): Vec2l = StdVec2l(x, y)

    override fun yz(): Vec2l = StdVec2l(y, z)

    override fun xz(): Vec2l = StdVec2l(x, z)

    override fun hasZeroComponent(): Boolean = x == 0L || y == 0L || z == 0L

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = (x * x + y * y + z * z).toDouble()

    override fun min(): Long = min(x, min(y, z))

    override fun mid(): Long = min(max(x, y), max(y, z))

    override fun max(): Long = max(x, max(y, z))

    override infix fun dot(other: Vec3lAccessor): Double = (x * other.x + y * other.y + z * other.z).toDouble()

    override fun angleRadians(other: Vec3lAccessor): Double {
        val fromLength = this.length()
        val toLength = other.length()
        check(!(fromLength == 0.0 || toLength == 0.0)) {
            "Both vectors need a length greater than 0! A vector of length 0 has no orientation, " +
                "and therefore no angel to another vector."
        }
        return acos(this.dot(other) / (fromLength * toLength))
    }

    override fun angleDegrees(other: Vec3lAccessor): Double = Math.toDegrees(angleRadians(other))

    override fun lerp(target: Vec3lAccessor, lerpFactor: Long): Vec3l {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return lerpFree(target, lerpFactor)
    }

    override fun lerpFree(target: Vec3lAccessor, lerpFactor: Long): Vec3l =
        StdVec3l().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec3l.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec3l [ ")
            .appendAligned(x.toFloat()).append(", ")
            .appendAligned(y.toFloat()).append(", ")
            .appendAligned(z.toFloat())
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.putFloat(x.toFloat()).putFloat(y.toFloat()).putFloat(z.toFloat())
    }

    override fun storeIn(buffer: FloatBuffer): FloatBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x.toFloat()).put(y.toFloat()).put(z.toFloat())
    }

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer {
        assert(buffer.remaining() >= 3)
        return buffer.put(x.toDouble()).put(y.toDouble()).put(z.toDouble())
    }

    override fun copy(): Vec3l = StdVec3l(x, y, z)

    override fun asImmutable(): ImmutableVec3l = StdImmutableVec3l.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        hashCode = 31 * hashCode + z.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec3l -> false
            other === this -> true
            else -> this.x == other.x
                && this.y == other.y
                && this.z == other.z
        }
    }

    override fun toString(): String = "Vec3l [ $x, $y, $z ]"

    /** Vec3l viewer, granting read-only access to its parent data. */
    inner class Viewer : Vec3lView {
        override val x: Long = this@StdVec3l.x
        override val y: Long = this@StdVec3l.y
        override val z: Long = this@StdVec3l.y

        override fun xy(): Vec2l = this@StdVec3l.xy()
        override fun yz(): Vec2l = this@StdVec3l.yz()
        override fun xz(): Vec2l = this@StdVec3l.xz()

        override fun hasZeroComponent(): Boolean = this@StdVec3l.hasZeroComponent()

        override fun length(): Double = this@StdVec3l.length()
        override fun squaredLength(): Double = this@StdVec3l.squaredLength()

        override fun min(): Long = this@StdVec3l.min()
        override fun mid(): Long = this@StdVec3l.mid()
        override fun max(): Long = this@StdVec3l.max()

        override infix fun dot(other: Vec3lAccessor): Double = this@StdVec3l.dot(other)

        override fun angleRadians(other: Vec3lAccessor): Double = this@StdVec3l.angleRadians(other)
        override fun angleDegrees(other: Vec3lAccessor): Double = this@StdVec3l.angleDegrees(other)

        override fun lerp(target: Vec3lAccessor, lerpFactor: Long): Vec3l = this@StdVec3l.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec3lAccessor, lerpFactor: Long): Vec3l =
            this@StdVec3l.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec3l.bytes()

        override fun toFormattedString(): String = this@StdVec3l.toFormattedString()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec3l.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec3l.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec3l.storeIn(buffer)

        override fun asImmutable(): ImmutableVec3l = this@StdVec3l.asImmutable()
        override fun asMutable(): Vec3l = this@StdVec3l.copy()
    }

}
