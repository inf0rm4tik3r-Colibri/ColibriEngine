package de.colibriengine.math.vector.vec4l

/** StdVec4lFactory. */
class StdVec4lFactory : Vec4lFactory {

    private val pool = StdVec4lPool()

    override fun acquire(): Vec4l = pool.acquire()

    override fun acquireDirty(): Vec4l = pool.acquireDirty()

    override fun free(vec4l: Vec4l) = pool.free(vec4l)

    override fun immutableVec4lBuilder(): ImmutableVec4l.Builder = StdImmutableVec4l.builder()

    override fun zero(): ImmutableVec4l = ZERO

    override fun one(): ImmutableVec4l = ONE

    companion object {
        val ZERO = StdImmutableVec4l.builder().of(0)
        val ONE = StdImmutableVec4l.builder().of(1)
    }

}
