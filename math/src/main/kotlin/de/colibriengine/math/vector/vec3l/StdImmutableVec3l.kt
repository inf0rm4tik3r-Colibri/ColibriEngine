package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.vector.vec2l.StdVec2l
import de.colibriengine.math.vector.vec2l.Vec2l
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec3l private constructor(
    x: Long,
    y: Long,
    z: Long
) : ImmutableVec3l {

    private val mutableAccessor: Vec3lAccessor = StdVec3l(x, y, z)

    override val x: Long = mutableAccessor.x
    override val y: Long = mutableAccessor.y
    override val z: Long = mutableAccessor.z

    override fun asMutable(): Vec3l = StdVec3l().set(this)

    override fun xy(): Vec2l = StdVec2l(mutableAccessor.x, mutableAccessor.y)
    override fun yz(): Vec2l = StdVec2l(mutableAccessor.y, mutableAccessor.z)
    override fun xz(): Vec2l = StdVec2l(mutableAccessor.x, mutableAccessor.z)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Long = mutableAccessor.min()
    override fun mid(): Long = mutableAccessor.mid()
    override fun max(): Long = mutableAccessor.max()

    override infix fun dot(other: Vec3lAccessor): Double = mutableAccessor.dot(other)

    override fun angleRadians(other: Vec3lAccessor): Double = mutableAccessor.angleRadians(other)
    override fun angleDegrees(other: Vec3lAccessor): Double = mutableAccessor.angleDegrees(other)

    override fun lerp(target: Vec3lAccessor, lerpFactor: Long): Vec3l = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec3lAccessor, lerpFactor: Long): Vec3l =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec3l", "ImmutableVec3l")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec3l.Builder {

        private var x: Long = 0
        private var y: Long = 0
        private var z: Long = 0

        override fun of(value: Long): ImmutableVec3l = set(value).build()

        override fun of(x: Long, y: Long, z: Long): ImmutableVec3l = set(x, y, z).build()

        override fun of(vec3lAccessor: Vec3lAccessor): ImmutableVec3l = set(vec3lAccessor).build()

        override fun setX(x: Long): ImmutableVec3l.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Long): ImmutableVec3l.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Long): ImmutableVec3l.Builder {
            this.z = z
            return this
        }

        override fun set(value: Long): ImmutableVec3l.Builder {
            x = value
            y = value
            z = value
            return this
        }

        override fun set(x: Long, y: Long, z: Long): ImmutableVec3l.Builder {
            this.x = x
            this.y = y
            this.z = z
            return this
        }

        override fun set(vec3lAccessor: Vec3lAccessor): ImmutableVec3l.Builder {
            x = vec3lAccessor.x
            y = vec3lAccessor.y
            z = vec3lAccessor.z
            return this
        }

        override fun build(): ImmutableVec3l = StdImmutableVec3l(x, y, z)
    }

    companion object {
        fun builder(): ImmutableVec3l.Builder = Builder()
    }

}

inline fun buildImmutableVec3l(builderAction: ImmutableVec3l.Builder.() -> Unit): ImmutableVec3l {
    return StdImmutableVec3l.builder().apply(builderAction).build()
}
