package de.colibriengine.math.vector.vec4f

/** StdVec4fFactory. */
class StdVec4fFactory : Vec4fFactory {

    private val pool = StdVec4fPool()

    override fun acquire(): Vec4f = pool.acquire()

    override fun acquireDirty(): Vec4f = pool.acquireDirty()

    override fun free(vec4f: Vec4f) = pool.free(vec4f)

    override fun immutableVec4fBuilder(): ImmutableVec4f.Builder = StdImmutableVec4f.builder()

    override fun zero(): ImmutableVec4f = ZERO

    override fun one(): ImmutableVec4f = ONE

    companion object {
        val ZERO = StdImmutableVec4f.builder().of(0f)
        val ONE = StdImmutableVec4f.builder().of(1f)
        
        val RED = StdImmutableVec4f.builder().of(1.0f, 0.0f, 0.0f, 1.0f)
        val GREEN = StdImmutableVec4f.builder().of(0.0f, 1.0f, 0.0f, 1.0f)
        val BLUE = StdImmutableVec4f.builder().of(0.0f, 0.0f, 1.0f, 1.0f)
        val YELLOW = StdImmutableVec4f.builder().of(1.0f, 1.0f, 0.0f, 1.0f)
        val CYAN = StdImmutableVec4f.builder().of(0.0f, 1.0f, 1.0f, 1.0f)
        val MAGENTA = StdImmutableVec4f.builder().of(1.0f, 0.0f, 1.0f, 1.0f)
        val MAROON = StdImmutableVec4f.builder().of(0.5f, 0.0f, 0.0f, 1.0f)
        val OLIVE = StdImmutableVec4f.builder().of(0.5f, 0.5f, 0.0f, 1.0f)
        val LIME = StdImmutableVec4f.builder().of(0.0f, 0.5f, 0.0f, 1.0f)
        val PURPLE = StdImmutableVec4f.builder().of(0.5f, 0.0f, 0.5f, 1.0f)
        val TEAL = StdImmutableVec4f.builder().of(0.0f, 0.5f, 0.5f, 1.0f)
        val NAVY = StdImmutableVec4f.builder().of(0.0f, 0.0f, 0.5f, 1.0f)
        val BLACK = StdImmutableVec4f.builder().of(0.0f, 0.0f, 0.0f, 1.0f)
        val GREY_4 = StdImmutableVec4f.builder().of(0.2f, 0.2f, 0.2f, 1.0f)
        val GREY_3 = StdImmutableVec4f.builder().of(0.4f, 0.4f, 0.4f, 1.0f)
        val GREY_2 = StdImmutableVec4f.builder().of(0.6f, 0.6f, 0.6f, 1.0f)
        val SILVER = StdImmutableVec4f.builder().of(0.75f, 0.75f, 0.75f, 1.0f)
        val GREY_1 = StdImmutableVec4f.builder().of(0.8f, 0.8f, 0.8f, 1.0f)
        val WHITE = StdImmutableVec4f.builder().of(1.0f, 1.0f, 1.0f, 1.0f)
        val TRANSPARENT = StdImmutableVec4f.builder().of(0.0f, 0.0f, 0.0f, 0.0f)
    }

}
