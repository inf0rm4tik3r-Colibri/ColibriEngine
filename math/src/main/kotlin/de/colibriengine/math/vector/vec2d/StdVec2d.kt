package de.colibriengine.math.vector.vec2d

import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.util.NumberUtil.toFourDecimalPlaces
import de.colibriengine.util.appendAligned
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer
import kotlin.math.abs
import kotlin.math.sqrt

/** Mutable two dimensional vector in double precision. */
class StdVec2d(
    override var x: Double = 0.0,
    override var y: Double = 0.0
) : Vec2d {

    private var _view: Vec2dView? = null
    override val view: Vec2dView
        get() {
            if (_view == null) {
                _view = Viewer()
            }
            return _view!!
        }

    /*
    override fun setX(x: Double): Vec2d {
        this.x = x
        return this
    }

    override fun setY(y: Double): Vec2d {
        this.y = y
        return this
    }
    */

    override fun set(value: Double): Vec2d {
        x = value
        y = value
        return this
    }

    override fun set(x: Double, y: Double): Vec2d {
        this.x = x
        this.y = y
        return this
    }

    override fun set(other: Vec2fAccessor): Vec2d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        return this
    }

    override fun set(other: Vec2dAccessor): Vec2d {
        this.x = other.x
        this.y = other.y
        return this
    }

    override fun set(other: Vec2iAccessor): Vec2d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        return this
    }

    override fun set(other: Vec2lAccessor): Vec2d {
        this.x = other.x.toDouble()
        this.y = other.y.toDouble()
        return this
    }

    override fun initZero(): Vec2d {
        x = 0.0
        y = 0.0
        return this
    }

    override operator fun plus(addend: Double): Vec2d {
        x += addend
        y += addend
        return this
    }

    override fun plus(addendX: Double, addendY: Double): Vec2d {
        x += addendX
        y += addendY
        return this
    }

    override operator fun plus(addends: Vec2dAccessor): Vec2d {
        x += addends.x
        y += addends.y
        return this
    }

    override operator fun minus(subtrahend: Double): Vec2d {
        x -= subtrahend
        y -= subtrahend
        return this
    }

    override fun minus(subtrahendX: Double, subtrahendY: Double): Vec2d {
        x -= subtrahendX
        y -= subtrahendY
        return this
    }

    override operator fun minus(subtrahends: Vec2dAccessor): Vec2d {
        x -= subtrahends.x
        y -= subtrahends.y
        return this
    }

    override operator fun times(factor: Double): Vec2d {
        x *= factor
        y *= factor
        return this
    }

    override fun times(factorX: Double, factorY: Double): Vec2d {
        x *= factorX
        y *= factorY
        return this
    }

    override operator fun times(factors: Vec2dAccessor): Vec2d {
        x *= factors.x
        y *= factors.y
        return this
    }

    override operator fun div(divisor: Double): Vec2d {
        require(divisor != 0.0) { "Divisor parameter must not be 0!" }
        x /= divisor
        y /= divisor
        return this
    }

    override fun div(divisorX: Double, divisorY: Double): Vec2d {
        require(!(divisorX == 0.0 || divisorY == 0.0)) { "Divisor parameter must not be 0!" }
        x /= divisorX
        y /= divisorY
        return this
    }

    override operator fun div(divisors: Vec2dAccessor): Vec2d {
        require(!divisors.hasZeroComponent()) { "Vector component must not be 0!" }
        x /= divisors.x
        y /= divisors.y
        return this
    }

    override fun normalize(): Vec2d {
        val length = length()
        check(length != 0.0) {
            "This vector has a length of 0, and can therefore not be normalized (scaled to 1)!"
        }
        x /= length
        y /= length
        return this
    }

    override fun setLengthTo(targetValue: Double): Vec2d {
        val length = length()
        check(length != 0.0) { "This vector has a length of 0! Unable to calculate the scale factor." }
        val scaleFactor = targetValue / length
        x *= scaleFactor
        y *= scaleFactor
        return this
    }

    override fun invert(): Vec2d {
        x = -x
        y = -y
        return this
    }

    override fun abs(): Vec2d {
        x = abs(x)
        y = abs(y)
        return this
    }

    override fun shorten(): Vec2d {
        x = x.toFourDecimalPlaces()
        y = y.toFourDecimalPlaces()
        return this
    }

    override fun hasZeroComponent(): Boolean = x == 0.0 || y == 0.0

    override fun length(): Double = sqrt(squaredLength())

    override fun squaredLength(): Double = x * x + y * y

    override fun min(): Double = kotlin.math.min(x, y)

    override fun max(): Double = kotlin.math.max(x, y)

    override infix fun dot(other: Vec2dAccessor): Double = x * other.x + y * other.y

    override fun lerp(target: Vec2dAccessor, lerpFactor: Double): Vec2d {
        require(!(lerpFactor < 0 || lerpFactor > 1)) { "Lerp factor argument was not in the [0,...,1] range!" }
        return StdVec2d().set(target).minus(this).times(lerpFactor).plus(this)
    }

    override fun lerpFree(target: Vec2dAccessor, lerpFactor: Double): Vec2d =
        StdVec2d().set(target).minus(this).times(lerpFactor).plus(this)

    override fun bytes(): ByteArray = MemoryStack.stackPush().use {
        return storeIn(it.malloc(Vec2d.BYTES)).array()
    }

    override fun toFormattedString(): String {
        return StringBuilder()
            .append("Vec2d [ ")
            .appendAligned(x).append(", ")
            .appendAligned(y)
            .append(" ]").toString()
    }

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = buffer.putDouble(x).putDouble(y)

    override fun storeIn(buffer: FloatBuffer): FloatBuffer = buffer.put(x.toFloat()).put(y.toFloat())

    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = buffer.put(x).put(y)

    override fun copy(): Vec2d = StdVec2d(x, y)

    override fun asImmutable(): ImmutableVec2d = StdImmutableVec2d.builder().of(this)

    override fun hashCode(): Int {
        var hashCode = 17
        hashCode = 31 * hashCode + x.hashCode()
        hashCode = 31 * hashCode + y.hashCode()
        return hashCode
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other !is Vec2d -> false
            other === this -> true
            else -> this.x == other.x && this.y == other.y
        }
    }

    override fun toString(): String = "Vec2d [ $x, $y ]"

    /** Vec2d viewer, granting read only access to its parent data. */
    inner class Viewer : Vec2dView {
        override val x: Double = this@StdVec2d.x
        override val y: Double = this@StdVec2d.y

        override fun hasZeroComponent(): Boolean = this@StdVec2d.hasZeroComponent()

        override fun length(): Double = this@StdVec2d.length()
        override fun squaredLength(): Double = this@StdVec2d.squaredLength()

        override fun min(): Double = this@StdVec2d.min()
        override fun max(): Double = this@StdVec2d.max()

        override infix fun dot(other: Vec2dAccessor): Double = this@StdVec2d.dot(other)

        override fun lerp(target: Vec2dAccessor, lerpFactor: Double): Vec2d = this@StdVec2d.lerp(target, lerpFactor)
        override fun lerpFree(target: Vec2dAccessor, lerpFactor: Double): Vec2d =
            this@StdVec2d.lerpFree(target, lerpFactor)

        override fun bytes(): ByteArray = this@StdVec2d.bytes()

        override fun toFormattedString(): String = this@StdVec2d.toFormattedString()
        override fun toString(): String = this@StdVec2d.toString()

        override fun asImmutable(): ImmutableVec2d = this@StdVec2d.asImmutable()
        override fun asMutable(): Vec2d = this@StdVec2d.copy()

        override fun storeIn(buffer: ByteBuffer): ByteBuffer = this@StdVec2d.storeIn(buffer)
        override fun storeIn(buffer: FloatBuffer): FloatBuffer = this@StdVec2d.storeIn(buffer)
        override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = this@StdVec2d.storeIn(buffer)
    }

}
