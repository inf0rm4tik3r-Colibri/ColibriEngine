package de.colibriengine.math.vector.vec2l

import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

class StdImmutableVec2l private constructor(
    x: Long,
    y: Long
) : ImmutableVec2l {

    private val mutableAccessor: Vec2lAccessor = StdVec2l(x, y)

    override val x: Long = mutableAccessor.x
    override val y: Long = mutableAccessor.y

    override fun asMutable(): Vec2l = StdVec2l().set(this)

    override fun hasZeroComponent(): Boolean = mutableAccessor.hasZeroComponent()

    override fun length(): Double = mutableAccessor.length()
    override fun squaredLength(): Double = mutableAccessor.squaredLength()

    override fun min(): Long = mutableAccessor.min()
    override fun max(): Long = mutableAccessor.max()

    override infix fun dot(other: Vec2lAccessor): Double = mutableAccessor.dot(other)

    override fun lerp(target: Vec2lAccessor, lerpFactor: Long): Vec2l = mutableAccessor.lerp(target, lerpFactor)
    override fun lerpFree(target: Vec2lAccessor, lerpFactor: Long): Vec2l =
        mutableAccessor.lerpFree(target, lerpFactor)

    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String = mutableAccessor.toFormattedString().replace("Vec2l", "ImmutableVec2l")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableVec2l.Builder {

        private var x: Long = 0
        private var y: Long = 0

        override fun of(value: Long): ImmutableVec2l = set(value).build()

        override fun of(x: Long, y: Long): ImmutableVec2l = set(x, y).build()

        override fun of(other: Vec2lAccessor): ImmutableVec2l = set(other).build()

        override fun setX(x: Long): ImmutableVec2l.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Long): ImmutableVec2l.Builder {
            this.y = y
            return this
        }

        override fun set(value: Long): ImmutableVec2l.Builder {
            x = value
            y = value
            return this
        }

        override fun set(x: Long, y: Long): ImmutableVec2l.Builder {
            this.x = x
            this.y = y
            return this
        }

        override fun set(other: Vec2lAccessor): ImmutableVec2l.Builder {
            x = other.x
            y = other.y
            return this
        }

        override fun build(): ImmutableVec2l = StdImmutableVec2l(x, y)
    }

    companion object {
        fun builder(): ImmutableVec2l.Builder = Builder()
    }

}

inline fun buildImmutableVec2l(builderAction: ImmutableVec2l.Builder.() -> Unit): ImmutableVec2l {
    return StdImmutableVec2l.builder().apply(builderAction).build()
}
