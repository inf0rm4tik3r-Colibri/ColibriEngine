package de.colibriengine.math.vector.vec4i

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

class StdVec4iPool : ObjectPool<Vec4i>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdVec4i()
    },
    { instance: Vec4i -> instance.initZero() }
) {

    companion object {
        private val LOG = getLogger(StdVec4iPool::class.java)
    }

}
