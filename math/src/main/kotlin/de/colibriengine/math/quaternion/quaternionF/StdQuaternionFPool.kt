package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.objectpooling.ObjectPool
import de.colibriengine.options.Options

/** QuaternionFPool. */
class StdQuaternionFPool : ObjectPool<QuaternionF>(
    {
        if (Options.DEBUG_OBJECT_CREATION) {
            LOG.trace("created")
        }
        StdQuaternionF()
    },
    { instance: QuaternionF -> instance.initIdentity() }
) {

    companion object {
        private val LOG = getLogger(StdQuaternionFPool::class.java)
    }

}
