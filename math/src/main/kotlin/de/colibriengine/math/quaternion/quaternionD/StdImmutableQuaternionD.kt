package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.math.matrix.mat4d.Mat4d
import de.colibriengine.math.vector.vec3d.Vec3d
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/** StdImmutableQuaternionD. */
class StdImmutableQuaternionD private constructor(
    x: Double,
    y: Double,
    z: Double,
    w: Double
) : ImmutableQuaternionD {

    private val mutableAccessor: QuaternionDAccessor = StdQuaternionD(x, y, z, w)

    override val x: Double = mutableAccessor.x
    override val y: Double = mutableAccessor.y
    override val z: Double = mutableAccessor.z
    override val w: Double = mutableAccessor.w

    override fun xyz(storeIn: Vec3d): Vec3d = mutableAccessor.xyz(storeIn)
    override fun vectorPart(storeIn: Vec3d): Vec3d = mutableAccessor.vectorPart(storeIn)
    override fun scalarPart(): Double = mutableAccessor.scalarPart()

    override fun isIdentity(): Boolean = mutableAccessor.isIdentity()

    override fun lengthSquared(): Double = mutableAccessor.lengthSquared()
    override fun length(): Double = mutableAccessor.length()

    override fun forward(storeIn: Vec3d): Vec3d = mutableAccessor.forward(storeIn)
    override fun forward(storeIn: DoubleArray, index: Int) = mutableAccessor.forward(storeIn, index)
    override fun backward(storeIn: Vec3d): Vec3d = mutableAccessor.backward(storeIn)
    override fun backward(storeIn: DoubleArray, index: Int) = mutableAccessor.backward(storeIn, index)
    override fun up(storeIn: Vec3d): Vec3d = mutableAccessor.up(storeIn)
    override fun up(storeIn: DoubleArray, index: Int) = mutableAccessor.up(storeIn, index)
    override fun down(storeIn: Vec3d): Vec3d = mutableAccessor.down(storeIn)
    override fun down(storeIn: DoubleArray, index: Int) = mutableAccessor.down(storeIn, index)
    override fun right(storeIn: Vec3d): Vec3d = mutableAccessor.right(storeIn)
    override fun right(storeIn: DoubleArray, index: Int) = mutableAccessor.right(storeIn, index)
    override fun left(storeIn: Vec3d): Vec3d = mutableAccessor.left(storeIn)
    override fun left(storeIn: DoubleArray, index: Int) = mutableAccessor.left(storeIn, index)

    override fun asMatrix(storeIn: Mat4d): Mat4d = mutableAccessor.asMatrix(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String =
        mutableAccessor.toFormattedString().replace("QuaternionD", "ImmutableQuaternionD")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableQuaternionD.Builder {
        private var x: Double = 0.0
        private var y: Double = 0.0
        private var z: Double = 0.0
        private var w: Double = 0.0

        override fun of(x: Double, y: Double, z: Double, w: Double): ImmutableQuaternionD = set(x, y, z, w).build()

        override fun of(quaternionDAccessor: QuaternionDAccessor): ImmutableQuaternionD =
            set(quaternionDAccessor).build()

        override fun setX(x: Double): ImmutableQuaternionD.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Double): ImmutableQuaternionD.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Double): ImmutableQuaternionD.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Double): ImmutableQuaternionD.Builder {
            this.w = w
            return this
        }

        override operator fun set(x: Double, y: Double, z: Double, w: Double): ImmutableQuaternionD.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(quaternionDAccessor: QuaternionDAccessor): ImmutableQuaternionD.Builder {
            x = quaternionDAccessor.x
            y = quaternionDAccessor.y
            z = quaternionDAccessor.z
            w = quaternionDAccessor.w
            return this
        }

        override fun build(): ImmutableQuaternionD = StdImmutableQuaternionD(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableQuaternionD.Builder = Builder()
    }

}

inline fun buildImmutableQuaternionD(builderAction: ImmutableQuaternionD.Builder.() -> Unit): ImmutableQuaternionD {
    return StdImmutableQuaternionD.builder().apply(builderAction).build()
}
