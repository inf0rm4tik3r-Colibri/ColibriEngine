package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.matrix.mat4f.Mat4f
import de.colibriengine.math.vector.vec3f.Vec3f
import java.nio.ByteBuffer
import java.nio.DoubleBuffer
import java.nio.FloatBuffer

/** StdImmutableQuaternionF. */
class StdImmutableQuaternionF private constructor(
    x: Float,
    y: Float,
    z: Float,
    w: Float
) : ImmutableQuaternionF {

    private val mutableAccessor: QuaternionFAccessor = StdQuaternionF().set(x, y, z, w)

    override val x: Float = mutableAccessor.x
    override val y: Float = mutableAccessor.y
    override val z: Float = mutableAccessor.z
    override val w: Float = mutableAccessor.w

    override fun xyz(storeIn: Vec3f): Vec3f = mutableAccessor.xyz(storeIn)
    override fun vectorPart(storeIn: Vec3f): Vec3f = mutableAccessor.vectorPart(storeIn)
    override fun scalarPart(): Float = mutableAccessor.scalarPart()

    override fun isIdentity(): Boolean = mutableAccessor.isIdentity()

    override fun lengthSquared(): Float = mutableAccessor.lengthSquared()
    override fun length(): Float = mutableAccessor.length()

    override fun forward(storeIn: Vec3f): Vec3f = mutableAccessor.forward(storeIn)
    override fun forward(storeIn: FloatArray, index: Int) = mutableAccessor.forward(storeIn, index)
    override fun backward(storeIn: Vec3f): Vec3f = mutableAccessor.backward(storeIn)
    override fun backward(storeIn: FloatArray, index: Int) = mutableAccessor.backward(storeIn, index)
    override fun up(storeIn: Vec3f): Vec3f = mutableAccessor.up(storeIn)
    override fun up(storeIn: FloatArray, index: Int) = mutableAccessor.up(storeIn, index)
    override fun down(storeIn: Vec3f): Vec3f = mutableAccessor.down(storeIn)
    override fun down(storeIn: FloatArray, index: Int) = mutableAccessor.down(storeIn, index)
    override fun right(storeIn: Vec3f): Vec3f = mutableAccessor.right(storeIn)
    override fun right(storeIn: FloatArray, index: Int) = mutableAccessor.right(storeIn, index)
    override fun left(storeIn: Vec3f): Vec3f = mutableAccessor.left(storeIn)
    override fun left(storeIn: FloatArray, index: Int) = mutableAccessor.left(storeIn, index)

    override fun asMatrix(storeIn: Mat4f): Mat4f = mutableAccessor.asMatrix(storeIn)
    override fun bytes(): ByteArray = mutableAccessor.bytes()

    override fun toFormattedString(): String =
        mutableAccessor.toFormattedString().replace("QuaternionF", "ImmutableQuaternionF")

    override fun storeIn(buffer: ByteBuffer): ByteBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: FloatBuffer): FloatBuffer = mutableAccessor.storeIn(buffer)
    override fun storeIn(buffer: DoubleBuffer): DoubleBuffer = mutableAccessor.storeIn(buffer)

    private class Builder : ImmutableQuaternionF.Builder {
        private var x: Float = 0.toFloat()
        private var y: Float = 0.toFloat()
        private var z: Float = 0.toFloat()
        private var w: Float = 0.toFloat()

        override fun of(x: Float, y: Float, z: Float, w: Float): ImmutableQuaternionF = set(x, y, z, w).build()

        override fun of(quaternionFAccessor: QuaternionFAccessor): ImmutableQuaternionF =
            set(quaternionFAccessor).build()

        override fun setX(x: Float): ImmutableQuaternionF.Builder {
            this.x = x
            return this
        }

        override fun setY(y: Float): ImmutableQuaternionF.Builder {
            this.y = y
            return this
        }

        override fun setZ(z: Float): ImmutableQuaternionF.Builder {
            this.z = z
            return this
        }

        override fun setW(w: Float): ImmutableQuaternionF.Builder {
            this.w = w
            return this
        }

        override operator fun set(x: Float, y: Float, z: Float, w: Float): ImmutableQuaternionF.Builder {
            this.x = x
            this.y = y
            this.z = z
            this.w = w
            return this
        }

        override fun set(quaternionFAccessor: QuaternionFAccessor): ImmutableQuaternionF.Builder {
            x = quaternionFAccessor.x
            y = quaternionFAccessor.y
            z = quaternionFAccessor.z
            w = quaternionFAccessor.w
            return this
        }

        override fun build(): ImmutableQuaternionF = StdImmutableQuaternionF(x, y, z, w)
    }

    companion object {
        fun builder(): ImmutableQuaternionF.Builder = Builder()
    }

}

inline fun buildImmutableQuaternionF(builderAction: ImmutableQuaternionF.Builder.() -> Unit): ImmutableQuaternionF {
    return StdImmutableQuaternionF.builder().apply(builderAction).build()
}
