package de.colibriengine.asset.font

data class BitmapFontCharacter(
    val id: UInt,
    val x: Float,
    val y: Float,
    val width: Float,
    val height: Float,
    val xOffset: Float,
    val yOffset: Float,
    val xAdvance: Float,
    val page: UByte,
    val channel: UByte
)
