package de.colibriengine.asset.font

data class BitmapFontCommon(
    val lineHeight: UShort,
    val base: UShort,
    val scaleW: UShort,
    val scaleH: UShort,
    val pageAmt: UShort,
    val isPacked: Boolean,
    val alphaChannel: UByte,
    val redChannel: UByte,
    val greenChannel: UByte,
    val blueChannel: UByte
) {
    companion object {
        const val CHANNEL_GLYPH_DATA = 0
        const val CHANNEL_OUTLINE = 1
        const val CHANNEL_GLYPH_DATA_W_OUTLINE = 2
        const val CHANNEL_ZERO = 3
        const val CHANNEL_ONE = 4
    }
}
