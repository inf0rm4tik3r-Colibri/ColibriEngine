package de.colibriengine.objectpooling

/** Provides a method to reset an objects state. */
interface Reusable<T> {

  /** Resets this object to its initial state and return it. */
  fun reset(): T

}
