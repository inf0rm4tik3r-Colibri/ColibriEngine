package de.colibriengine.renderer

import de.colibriengine.graphics.PickingInfo
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

interface RenderingEngine {

    val renderOptions: RenderOptions

    val debugRenderer: DebugRenderer

    /**
     * Reads the picking texture at ([x],[y]) and returns ObjectID, DrawCallID and PrimitiveID in a [PickingInfo].
     *
     * @param x Position from the left
     * @param y Position from the bottom
     */
    fun readPickingInfo(x: Int, y: Int): PickingInfo

    /** Overload for [readPickingInfo]. */
    fun readPickingInfo(atPosition: Vec2iAccessor): PickingInfo = readPickingInfo(atPosition.x, atPosition.y)

}
