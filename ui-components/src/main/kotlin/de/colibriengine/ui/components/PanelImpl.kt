package de.colibriengine.ui.components

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.font.BitmapFont
import de.colibriengine.graphics.font.TextFactory
import de.colibriengine.graphics.font.TextMode
import de.colibriengine.graphics.material.MaterialFactory
import de.colibriengine.graphics.model.Model
import de.colibriengine.ui.GUI
import de.colibriengine.ui.structure.AbstractGUIComponent
import de.colibriengine.util.Timer

open class PanelImpl(
    val ecs: ECS,
    val textFactory: TextFactory,
    val materialFactory: MaterialFactory,
    val quad: Model,
    mother: GUI,
    name: String = "PANEL"
) : AbstractGUIComponent(ecs.createEntity(), name, mother), Panel {

    override val background = BackgroundImpl(materialFactory)

    init {
        val renderComponent = RenderComponent(quad)
        // TODO: Optionally: Create UI components with a different ecs all along?...
        // Necessary to not be render when generating shadow maps.
        renderComponent.shadowCaster = false
        entity.add(renderComponent)
    }

    inline fun <T : AbstractGUIComponent> doInit(child: T, performInitialization: T.() -> Unit): T {
        child.performInitialization()
        add(child)
        return child
    }

    inline fun panel(init: PanelImpl.() -> Unit): PanelImpl =
        doInit(child = PanelImpl(ecs, textFactory, materialFactory, quad, mother), init)

    inline fun text2D(mode: TextMode, length: UInt, font: BitmapFont, init: TEXT2D.() -> Unit) =
        doInit(child = TEXT2D(ecs, textFactory, mode, length, font, mother), init)

    inline fun slider(init: SLIDER.() -> Unit): SLIDER =
        doInit(child = SLIDER(ecs, mother), init)

    override fun init() {}
    override fun update(timing: Timer.View) {}

    override fun destroy() {}

    override fun toString(): String {
        return "PANEL(name=$name, ${super.toString()})"
    }

}
