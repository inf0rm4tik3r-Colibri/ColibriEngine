package de.colibriengine.ui.components

import de.colibriengine.graphics.material.Material
import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialFactory
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.ui.Gradient

class BackgroundImpl(materialFactory: MaterialFactory) : Background {

    override var useColor = false
    override var useGradient = false
    override var useImage = false

    override val color = StdVec3f().set(0f, 0f, 0f)
    override var opacity: Float = 0.8f

    override var gradient: Gradient = Gradient.black()

    private val material: Material

    var image: Texture?
        set(value) {
            material.ambientTextures.removeAt(0)
            // TODO: Handle null differently?
            if (value != null) {
                material.ambientTextures.add(0, value)
            }
        }
        get() = material.ambientTextures[0]

    init {
        this.material = materialFactory.createFrom(MaterialDefinition())
    }

}
