package de.colibriengine.ui.components

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.font.BitmapFont
import de.colibriengine.graphics.font.Text
import de.colibriengine.graphics.font.TextFactory
import de.colibriengine.graphics.font.TextMode
import de.colibriengine.graphics.rendering.BlendFactor
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.ui.GUI
import de.colibriengine.ui.structure.AbstractGUIComponent
import de.colibriengine.util.PhysicalUnit
import de.colibriengine.util.Timer
import org.koin.core.component.KoinComponent

class TEXT2D(ecs: ECS, textFactory: TextFactory, mode: TextMode, length: UInt, font: BitmapFont, parent: GUI) :
    AbstractGUIComponent(ecs.createEntity(), "TEXT2D", parent), KoinComponent {

    // TODO: add font property. pass on to text
    val text: Text = textFactory.create(mode, length, font)

    val length: UInt
        get() = text.size

    val color: Vec4f
        get() = text.color

    val font: BitmapFont
        get() = text.font

    var size: Float = 1f
        set(value) {
            field = value
            width(value, PhysicalUnit.PX)
            height(value, PhysicalUnit.PX)
        }

    init {
        val renderComponent = RenderComponent(text.model)
        renderComponent.blending.active = true
        renderComponent.blending.blendFuncSourceFactor = BlendFactor.SRC_ALPHA
        renderComponent.blending.blendFuncDestinationFactor = BlendFactor.ONE_MINUS_SRC_ALPHA
        renderComponent.shadowCaster = false
        entity.add(renderComponent)
    }

    override fun init() {
        size = 1f // Necessary to call setter!
    }

    override fun update(timing: Timer.View) {}

    override fun destroy() {}

    override fun toString(): String = this::class.simpleName + "(text=\"" + text + "\")"

}
