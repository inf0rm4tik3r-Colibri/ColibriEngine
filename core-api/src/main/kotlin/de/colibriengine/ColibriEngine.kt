package de.colibriengine

import de.colibriengine.asset.ResourceLoader
import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.font.FontManager
import de.colibriengine.graphics.shader.ShaderManager
import de.colibriengine.graphics.window.WindowManager
import de.colibriengine.input.InputManager
import de.colibriengine.renderer.DebugRenderer
import de.colibriengine.renderer.RenderingEngine
import de.colibriengine.util.Timer
import de.colibriengine.util.Version
import de.colibriengine.vr.OvrSessionManager
import de.colibriengine.worker.WorkerManager

val ENGINE_VERSION: Version = Version(0, 0, 9, 0)
val ENGINE_NAME = "ColibriEngine - v.$ENGINE_VERSION"

interface ColibriEngine {

    /** The user-facing [ECS] instance which should be used to construct all types of game entities. */
    val ecs: ECS

    val internalEcs: ECS

    /**
     * Condition variable that determines whether the engine should terminate.
     * As long as it remains true the call cycle in the main loop will be repeated.
     */
    var shouldTerminate: Boolean

    /**
     * States whether the engine should terminate when all of its windows were closed.
     */
    var terminateWithoutWindows: Boolean

    val windowManager: WindowManager

    val inputManager: InputManager

    /**
     * @return The currently active camera of the currently active window.
     * If either no window is active or no camera is active in the active window, `null` is returned.
     */
    val activeCameraOfActiveWindow: Camera?
        get() = windowManager.activeWindow?.cameraManager?.activeCamera

    val resourceLoader: ResourceLoader
    val workerManager: WorkerManager
    val shaderManager: ShaderManager
    val fontManager: FontManager
    val ovrSessionManager: OvrSessionManager

    val timer: Timer

    // TODO: remove?
    var currentRenderer: RenderingEngine?
    val debugRenderer: DebugRenderer?

    fun start(application: ColibriApplication)
    fun terminate()

}
