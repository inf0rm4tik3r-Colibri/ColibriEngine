package de.colibriengine.ecs

import de.colibriengine.util.Timer

// TODO: BeforeAll, BeforeEach, AfterEach, AfterAll?
interface System {
    fun beforeAll()
}

interface System1<C1 : Component> : System {
    fun processEntity(c1: C1, timer: Timer.View)
}

interface System2<C1 : Component, C2 : Component> : System {
    fun processEntity(c1: C1, c2: C2, timer: Timer.View)
}

interface System3<C1 : Component, C2 : Component, C3: Component> : System {
    fun processEntity(c1: C1, c2: C2, c3: C3, timer: Timer.View)
}
