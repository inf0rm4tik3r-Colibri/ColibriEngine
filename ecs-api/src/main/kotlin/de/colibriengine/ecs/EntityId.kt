package de.colibriengine.ecs

// TODO: Do we want to use a 46bit identifier instead? (ULong)
@JvmInline
value class EntityId(val id: UInt) : Comparable<EntityId> {

    override fun compareTo(other: EntityId): Int {
        return id.compareTo(other.id)
    }

}
