package de.colibriengine.ecs

/**
 * Note: Each component implementation must be a real class. That means it must not be a local or anonymous class.
 * because then, no unique name of the component-class can be resolved, making it impossible to create the components
 * unique type id.
 */
interface Component
