package de.colibriengine.asset.audio.wav

import de.colibriengine.asset.audio.Wav

private const val NUM_CHANNELS_ERROR = "Number of channels specified in header is equal to zero!"
private const val BLOCK_ALIGN_ERROR = "Block Align specified in header is equal to zero!"
private const val BLOCK_ALIGN_VALIDATION_ERROR = "Block Align does not agree " +
    "with bytes required for validBits and number of channels"
private const val VALID_BITS_TO_LOW_ERROR = "Valid Bits specified in header is less than 2"
private const val VALID_BITS_TO_HIGH_ERROR = "Valid Bits specified in header is greater than 64, " +
    "this is greater than a long can hold"

data class WavFileHeader(
    override val numChannels: Int = 0,
    override val sampleRate: Long = 0, // TODO: Use Kotlins unsigned types
    override val blockAlign: Int = 0,
    override val validBits: Int = 0
): Wav.WavHeader {

    init {
        if (numChannels == 0) throw WavFileException(NUM_CHANNELS_ERROR)
        if (blockAlign == 0) throw WavFileException(BLOCK_ALIGN_ERROR)
        if (validBits < 2) throw WavFileException(VALID_BITS_TO_LOW_ERROR)
        if (validBits > 64) throw WavFileException(VALID_BITS_TO_HIGH_ERROR)
    }

    fun calculateBytesRequiredToStoreOneSample(): Int {
        val bytes = (validBits + 7) / 8
        if (bytes * numChannels != blockAlign) throw WavFileException(BLOCK_ALIGN_VALIDATION_ERROR)
        return bytes
    }

}
