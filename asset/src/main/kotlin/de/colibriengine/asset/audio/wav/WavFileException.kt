package de.colibriengine.asset.audio.wav

class WavFileException(message: String) : Exception(message)
