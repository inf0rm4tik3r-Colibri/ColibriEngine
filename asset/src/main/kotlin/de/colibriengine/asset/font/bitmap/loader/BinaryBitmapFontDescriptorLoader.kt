package de.colibriengine.asset.font.bitmap.loader

import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.font.*
import de.colibriengine.exception.EndOfStreamException
import de.colibriengine.exception.UnsupportedResourceException
import de.colibriengine.util.*
import de.colibriengine.util.Bitfield.AccessDirection.LEFT
import de.colibriengine.util.Bitfield.AccessDirection.RIGHT
import java.io.BufferedInputStream
import java.io.IOException
import java.nio.ByteOrder

// TODO: Optimize bitfield operations by using extension functions.
object BinaryBitmapFontDescriptorLoader {

    /**
     * @throws UnsupportedResourceException
     * @throws BitmapFontException
     */
    fun parse(bis: BufferedInputStream, resourceName: ResourceName): BitmapFontDescriptor {
        return try {
            checkSignature(expectedFileSignature = byteArrayOf(66, 77, 70, 3), bis)
            readData(bis, resourceName)
        } catch (exception: IOException) {
            exception.printStackTrace()
            throw BitmapFontException("Could not read from BitmapFont input stream.", exception)
        }
    }

    private fun readData(bis: BufferedInputStream, resourceName: ResourceName): BitmapFontDescriptor {
        // Order is important!
        val info = parseBlock1(bis)
        val common = parseBlock2(bis)
        val pages = parseBlock3(common.pageAmt, bis)
        val characters = parseBlock4(bis)
        val kerning = parseBlock5(bis)
        return BitmapFontDescriptor(resourceName, info, common, pages, characters, kerning)
    }

    private fun checkSignature(expectedFileSignature: ByteArray, bis: BufferedInputStream) {
        val signatureBytes = ByteArray(expectedFileSignature.size)
        val signatureReadResult = bis.read(signatureBytes, 0, expectedFileSignature.size)
        if (signatureReadResult != expectedFileSignature.size) {
            throw UnsupportedResourceException("Unable to read the file signature.")
        }
        for (i in expectedFileSignature.indices) {
            if (signatureBytes[i] != expectedFileSignature[i]) {
                throw UnsupportedResourceException("File signature does not match binary signature.")
            }
        }
    }

    private fun parseBlock1(bis: BufferedInputStream): BitmapFontInfo {
        bis.readUByte() // blockType
        bis.readUInt(ByteOrder.LITTLE_ENDIAN) // blockSize
        val fontSize = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val bitField = bis.readByteAsBitField()
        val smooth = bitField.getAsBoolean(0, LEFT)
        val unicode = bitField.getAsBoolean(1, LEFT)
        val italic = bitField.getAsBoolean(2, LEFT)
        val bold = bitField.getAsBoolean(3, LEFT)
        val charSet: String = bis.readUByte().toString(10)
        val stretchH = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val aa = bis.readUByte()
        val paddingTop = bis.readUByte()
        val paddingRight = bis.readUByte()
        val paddingBottom = bis.readUByte()
        val paddingLeft = bis.readUByte()
        val spacingH = bis.readUByte()
        val spacingV = bis.readUByte()
        val outlineThickness = bis.readUByte()
        val fontName = bis.readNullTerminatedString()
        return BitmapFontInfo(
            fontName = fontName, fontSize = fontSize, isBold = bold, isItalic = italic,
            charSet = charSet, isUnicode = unicode,
            stretchH = stretchH.toFloat(), isSmooth = smooth, aa = aa,
            paddingTop = paddingTop, paddingRight = paddingRight,
            paddingBottom = paddingBottom, paddingLeft = paddingLeft,
            spacingH = spacingH, spacingV = spacingV, outlineThickness = outlineThickness
        )
    }

    private fun parseBlock2(bis: BufferedInputStream): BitmapFontCommon {
        bis.readUByte() // blockType
        bis.readUInt(ByteOrder.LITTLE_ENDIAN) // blockSize
        val lineHeight = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val base = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val scaleW = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val scaleH = bis.readUShort(ByteOrder.LITTLE_ENDIAN)
        val pageAmt = bis.readUShort( ByteOrder.LITTLE_ENDIAN)
        val bitField = bis.readByteAsBitField()
        val packed = bitField.getAsBoolean(0,RIGHT)
        val alphaChannel = bis.readUByte()
        val redChannel = bis.readUByte()
        val greenChannel = bis.readUByte()
        val blueChannel = bis.readUByte()
        return BitmapFontCommon(
            lineHeight = lineHeight,
            base = base,
            scaleW = scaleW,
            scaleH = scaleH,
            pageAmt = pageAmt,
            isPacked = packed,
            alphaChannel = alphaChannel,
            redChannel = redChannel,
            greenChannel = greenChannel,
            blueChannel = blueChannel
        )
    }

    private fun parseBlock3(pageAmt: UShort, bis: BufferedInputStream): List<BitmapFontCharacterPage> {
        bis.readUByte() // blockType
        bis.readUInt(ByteOrder.LITTLE_ENDIAN) // blockSize
        val pages: MutableList<BitmapFontCharacterPage> = ArrayList()
        for (i in 0.toUShort() until pageAmt) {
            pages.add(BitmapFontCharacterPage(i, bis.readNullTerminatedString()))
        }
        return pages
    }

    private fun parseBlock4(bis: BufferedInputStream): Map<UInt, BitmapFontCharacter> {
        bis.readUByte() // blockType
        val blockSize = bis.readUInt(ByteOrder.LITTLE_ENDIAN)
        val characterInfoStructSizeInBytes = 20u
        val numChars = blockSize / characterInfoStructSizeInBytes
        val characters: MutableMap<UInt, BitmapFontCharacter> = HashMap()
        for (i in 0.toUInt() until numChars) {
            val character = BitmapFontCharacter(
                id = bis.readUInt(ByteOrder.LITTLE_ENDIAN),
                x = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                y = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                width = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                height = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                xOffset = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                yOffset = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                xAdvance = bis.readUShort(ByteOrder.LITTLE_ENDIAN).toFloat(),
                page = bis.readUByte(),
                channel = bis.readUByte()
            )
            characters[character.id] = character
        }
        return characters
    }

    private fun parseBlock5(bis: BufferedInputStream): Map<UInt, MutableMap<UInt, BitmapFontKerning>> {
        val blockSize: UInt
        try {
            bis.readUByte() // blockType
            blockSize = bis.readUInt(ByteOrder.LITTLE_ENDIAN)
        } catch (kerningInformationNotAvailable: EndOfStreamException) {
            return emptyMap()
        }
        val kerningInfoStructSizeInBytes = 10u
        val numKerning = blockSize / kerningInfoStructSizeInBytes
        val outerKerningMap: MutableMap<UInt, MutableMap<UInt, BitmapFontKerning>> = HashMap()
        for (i in 0.toUInt() until numKerning) {
            val kerning = BitmapFontKerning(
                firstCharacter = bis.readUInt(ByteOrder.LITTLE_ENDIAN),
                secondCharacter = bis.readUInt(ByteOrder.LITTLE_ENDIAN),
                kerning = bis.readShort(ByteOrder.LITTLE_ENDIAN)
            )
            // Ensure that the inner kerning map for (first) character exists.
            val innerKerningMap = outerKerningMap.computeIfAbsent(kerning.firstCharacter) { HashMap(1) }
            // Add an item for the (first <-> second) character relation to the inner map.
            innerKerningMap[kerning.secondCharacter] = kerning
        }
        return outerKerningMap
    }

}
