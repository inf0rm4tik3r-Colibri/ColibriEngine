package de.colibriengine.audio.openal

import de.colibriengine.audio.AudioMaster
import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.openal.ALC11

object OpenALMaster: AudioMaster {

    private val LOG = getLogger(this)

    lateinit var device: OpenALDevice

    override fun initAudioSystem() {
        LOG.info("Initializing OpenAL...")
        val devices: List<String> = listAvailableDeviceNames()
        val defaultDevice: String? = getDefaultDeviceName()
        LOG.info("Available OpenAL devices: $devices")
        LOG.info("OpenAL default device: $defaultDevice")
        device = OpenALDevice(defaultDevice)
        LOG.info("-- OpenAL Version   :  ${openALVersion()}")
        LOG.info("-- OpenAL Vendor    :  ${openALVendor()}")
        LOG.info("-- OpenAL Renderer  :  ${openALRenderer()}")
        LOG.info("-- OpenAL Extensions:  ${openALExtensions()}")
    }

    private fun listAvailableDeviceNames(): List<String> {
        val devices: String? = ALC11.alcGetString(0, ALC11.ALC_DEVICE_SPECIFIER)
        checkForOpenALCErrors(0)
        return devices?.split(",") ?: listOf()
    }

    private fun getDefaultDeviceName(): String? {
        val defaultDevice: String? = ALC11.alcGetString(0, ALC11.ALC_DEFAULT_DEVICE_SPECIFIER)
        checkForOpenALCErrors(0)
        return defaultDevice
    }

    override fun destroyAudioSystem() {
        device.destroy()

        val context: Long = ALC11.alcGetCurrentContext()
        if (context == 0L) {
            LOG.error("No current context! Aborting OpenAL destruction...")
            return
        }
        val device: Long = ALC11.alcGetContextsDevice(context)
        if (device == 0L) {
            LOG.error("No device! Aborting OpenAL destruction...")
            return
        }
        val success: Boolean = ALC11.alcCloseDevice(device)
        checkForOpenALCErrors(device)
        if (!success) {
            LOG.error("Unable to close device! Aborting OpenAL destruction...")
            return
        }
        LOG.info("OpenAL device closed.")
        ALC11.alcMakeContextCurrent(0L)
        LOG.info("OpenAL context released.")
        ALC11.alcDestroyContext(context)
        LOG.info("OpenAL context destroyed.")
    }

}
