package de.colibriengine.audio.openal

import de.colibriengine.asset.AssetStore
import de.colibriengine.asset.ResourceLoader
import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.audio.Wav
import de.colibriengine.asset.load
import de.colibriengine.audio.AudioTrack
import de.colibriengine.audio.AudioTrackFactory
import org.lwjgl.openal.AL11

class OpenALTrackFactory(
    private val resourceLoader: ResourceLoader
) : AudioTrackFactory {

    private val tracks: AssetStore<ResourceName, OpenALAudioTrack> = AssetStore("OpenALAudioTracks") {
        buffer.destroy()
    }

    fun destroy() {
        tracks.destroy(callDestroyOnRemaining = true)
    }

    override fun get(resourceName: ResourceName): OpenALAudioTrack {
        return tracks.getOrPut(resourceName) { createTrack(resourceName) }
    }

    override fun giveBack(track: AudioTrack) {
        tracks.giveBack(track.resourceName)
    }

    private fun createTrack(resourceName: ResourceName): OpenALAudioTrack {
        resourceLoader.load<Wav>(resourceName).use { wav ->
            wav.logDebugInfo()
            val data = ShortArray(wav.numFrames.toInt() * wav.header.numChannels)
            wav.readFrames(into = data, amount = wav.numFrames)
            val buffer = OpenALBuffer()
            buffer.store(data, alFormat(wav.header.numChannels, wav.header.validBits), wav.header.sampleRate.toUInt())
            return OpenALAudioTrack(
                buffer,
                resourceName,
                wav.header.sampleRate.toFloat() / wav.header.sampleRate.toFloat(),
                wav.header.numChannels,
                wav.header.validBits,
                wav.numFrames,
                wav.header.sampleRate.toInt()
            )
        }
    }

    private fun alFormat(numChannels: Int, bitsPerChannel: Int): Int {
        return when (numChannels) {
            1 -> when (bitsPerChannel) {
                8 -> AL11.AL_FORMAT_MONO8
                16 -> AL11.AL_FORMAT_MONO16
                else -> throw OpenALException("Unsupported bits per channel ($bitsPerChannel) in PCM data")
            }
            2 -> when (bitsPerChannel) {
                8 -> AL11.AL_FORMAT_STEREO8
                16 -> AL11.AL_FORMAT_STEREO16
                else -> throw OpenALException("Unsupported bits per channel ($bitsPerChannel) in PCM data")
            }
            else -> throw OpenALException("Unsupported number of channels ($numChannels) in PCM data")
        }
    }

}
