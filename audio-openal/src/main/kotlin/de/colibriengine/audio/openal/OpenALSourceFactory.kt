package de.colibriengine.audio.openal

import de.colibriengine.audio.AudioSource2D
import de.colibriengine.audio.AudioSource3D
import de.colibriengine.audio.AudioSourceFactory

class OpenALSourceFactory : AudioSourceFactory {

    override fun createSource2D(): AudioSource2D {
        val source = OpenALSource()
        source.position.set(0f, 0f, 0f)
        source.rolloffFactor = 0f
        source.relativeToListener = true
        return source
    }

    override fun createSource3D(): AudioSource3D = OpenALSource()

}
