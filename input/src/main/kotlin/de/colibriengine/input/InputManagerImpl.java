package de.colibriengine.input;

import org.jetbrains.annotations.NotNull;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class InputManagerImpl implements InputManager {

    private InputType activeInputType = InputType.MOUSE_KEYBOARD;

    @NotNull
    @Override
    public InputType getActiveInputType() {
        return activeInputType;
    }

    @Override
    public boolean isMouseAndKeyboardInput() {
        return activeInputType == InputType.MOUSE_KEYBOARD;
    }

    @Override
    public boolean isJoystickInput() {
        return activeInputType == InputType.JOYSTICK;
    }

    private boolean automaticInputDetection = true;

    private final MouseKeyboardManagerImpl mkm;
    private final JoystickManagerImpl jm;

    private final List<MouseKeyboardInputCallbacks> mkCallbackObjects;
    private final List<JoystickInputCallbacks> jCallbackObjects;

    private static final int INITIAL_ARRAY_LIST_CAPACITY = 16;
    private static final String ON_ADD_ERROR =
            "InputManager: Specified object is already in the callback list and therefore can not be addN to it.";
    private static final String ON_REMOVE_ERROR =
            "InputManager: Specified object is not in the callback list and therefore can not be removed from it.";

    public InputManagerImpl() {
        mkCallbackObjects = new ArrayList<>(INITIAL_ARRAY_LIST_CAPACITY);
        jCallbackObjects = new ArrayList<>(INITIAL_ARRAY_LIST_CAPACITY);

        mkm = new MouseKeyboardManagerImpl(this);
        jm = new JoystickManagerImpl(this);
    }

    @Override
    public @NotNull MouseKeyboardManager getMouseKeyboardManager() {
        return mkm;
    }

    @NotNull
    @Override
    public JoystickManager getJoystickManager() {
        return jm;
    }

    /**
     * Polls the event queue and passes all occurred events to the event callbacks.
     * Do not call this method. The engines main loop does that once at the start of every new frame.
     *
     * @param waitForInput Specify whether the application should block until the next event occurs, or if the method
     *                     should return immediately after all recognized events got processed. Set this false if you
     *                     want to render constantly, true if your application only needs to update and render if an
     *                     event occurred.
     */
    @Override
    public void pollInput(boolean waitForInput) {
        if (waitForInput) {
            // currentInputType can only be one of MOUSE_KEYBOARD_INPUT and JOYSTICK_INPUT. Call either of their
            // waitEvent() method to let the thread freeze until an input gets recognized.
            if (activeInputType == InputType.MOUSE_KEYBOARD) {
                GLFW.glfwWaitEvents();
            } else {
                jm.waitEvents();
            }
        } else {
            // Process GLFWs eventqueue (invokes mouse and keyboard events) and inform the application about repeated
            // key presses.
            GLFW.glfwPollEvents();
            mkm.pushKeyRepeatEvents();

            // Update the stored state of all connected joysticks, generate an eventqueue for recognized changes
            // and
            jm.updateJoystickData();
            jm.pushJoystickEvents();
        }
    }

    /**
     * Pushes a dummy event to the event processing system to let the pollInput method return.
     */
    public void wakeUp() {
        glfwPostEmptyEvent();
        jm.wakeUp();
    }

    @Override
    public void addMKInputCallback(final @NotNull MouseKeyboardInputCallbacks object) {
        if (mkCallbackObjects.contains(object)) {
            new Exception(ON_ADD_ERROR).printStackTrace();
        }
        mkCallbackObjects.add(object);
    }

    @Override
    public void removeMKInputCallback(final @NotNull MouseKeyboardInputCallbacks object) {
        if (!mkCallbackObjects.remove(object)) {
            new Exception(ON_REMOVE_ERROR).printStackTrace();
        }
    }

    @Override
    public void clearMKInputCallbacks() {
        mkCallbackObjects.clear();
    }

    @Override
    public void addJInputCallback(final @NotNull JoystickInputCallbacks object) {
        if (jCallbackObjects.contains(object)) {
            new Exception(ON_ADD_ERROR).printStackTrace();
        }
        jCallbackObjects.add(object);
    }

    @Override
    public void removeJInputCallback(final @NotNull JoystickInputCallbacks object) {
        if (!jCallbackObjects.remove(object)) {
            new Exception(ON_REMOVE_ERROR).printStackTrace();
        }
    }

    @Override
    public void clearJInputCallbacks() {
        jCallbackObjects.clear();
    }

    /**
     * Checks whether or not the specified command is currently active.
     * A command is considered "active" if its key is pressed.
     *
     * @param command The command that is to be checked.
     * @return true, if active. False otherwise.
     */
    public boolean isCommand(final Command command) {
        switch (activeInputType) {
            case MOUSE_KEYBOARD -> {
                if (command.getKKeyAction() == GLFW_PRESS && mkm.isKeyPressed(command.getKKey())) {
                    return true;
                }
                if (command.getKKeyAction() == GLFW_RELEASE && mkm.isKeyReleased(command.getKKey())) {
                    return true;
                }
            }
            case JOYSTICK -> {
                if (command.getJButtonAction() == GLFW_PRESS && jm.isButtonDown(command.getJButton())) {
                    return true;
                }
                if (command.getJButtonAction() == GLFW_RELEASE && jm.isButtonDown(command.getJButton())) {
                    return true;
                }
            }
            default -> throw new IllegalStateException("currentInputType is in an illegal state!");
        }
        return false;
    }

    /**
     * Sets a specific input type as the current input type. The engines input system will fetch information from the
     * current input type. Gets automatically called for input X whenever an input on X is recognized.
     * Ensures that the object is in a correct state after this method returns. Guarantees the currentInputType to be
     * one of the specified input types in this class.
     *
     * @param inputType The input type to set active.
     * @throws IllegalArgumentException if {@code inputType} is not one of class defined *_INPUT constants.
     */
    @Override
    public void setActiveInputType(@NotNull InputType inputType) throws IllegalArgumentException {
        // Do nothing, if the current input type is the same as the one that should be activated.
        if (activeInputType == inputType) {
            return;
        }

        // Check the argument and print a notification message which input type got activated.
        switch (inputType) {
            case MOUSE_KEYBOARD -> {
                activeInputType = inputType;
                System.out.println("InputManager: Activated keyboard input.");
            }
            case JOYSTICK -> {
                activeInputType = inputType;
                System.out.println("InputManager: Activated joystick input");
            }
            default -> throw new IllegalArgumentException("InputManager: Unknown input type. Use MOUSE_KEYBOARD or " +
                    "JOYSTICK!");
        }
    }

    public boolean isAutomaticInputDetection() {
        return automaticInputDetection;
    }

    public void setAutomaticInputDetection(final boolean automaticInputDetection) {
        this.automaticInputDetection = automaticInputDetection;
    }


    @Override
    public @NotNull List<MouseKeyboardInputCallbacks> getMkCallbackObjects() {
        return mkCallbackObjects;
    }

    @Override
    public @NotNull List<JoystickInputCallbacks> getJCallbackObjects() {
        return jCallbackObjects;
    }

}
