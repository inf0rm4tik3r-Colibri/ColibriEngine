package de.colibriengine.input;

import de.colibriengine.input.events.AxisChangeEvent;
import de.colibriengine.input.events.ButtonChangeEvent;
import de.colibriengine.input.events.ConnectionLossEvent;
import de.colibriengine.input.gamepads.GamepadType;
import de.colibriengine.util.Timer;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import static org.lwjgl.glfw.GLFW.*;

/**
 * @since ColibriEngine 0.0.6.6
 * TODO: Remove static-ness!
 */
public class JoystickManagerImpl implements JoystickManager {

    private final InputManager inputManager;

    private final int POSSIBLE_JOYSTICK_CONNECTIONS;
    private final boolean[] JOYSTICK_CONNECTION;
    private final JoystickData[] JOYSTICK_DATA;
    private final JoystickEvents[] JOYSTICK_EVENTS;
    private int currentJoystick;

    private final float UPDATE_FREQUENCY = 50.0f; // in ms
    private final Timer UPDATE_TIMER;
    private boolean eventHappened;

    JoystickManagerImpl(InputManager inputManager) {
        this.inputManager = inputManager;

        POSSIBLE_JOYSTICK_CONNECTIONS = GLFW_JOYSTICK_LAST - GLFW_JOYSTICK_1 + 1;

        JOYSTICK_CONNECTION = new boolean[POSSIBLE_JOYSTICK_CONNECTIONS];
        JOYSTICK_DATA = new JoystickData[POSSIBLE_JOYSTICK_CONNECTIONS];
        JOYSTICK_EVENTS = new JoystickEvents[POSSIBLE_JOYSTICK_CONNECTIONS];

        // There is no currently active joystick.
        currentJoystick = -1;

        for (int i = 0; i < POSSIBLE_JOYSTICK_CONNECTIONS; i++) {
            JOYSTICK_EVENTS[i] = new JoystickEvents();
        }

        UPDATE_TIMER = new Timer();
    }

    /**
     * Blocks until a Joystick input occurs. After that the method pushes the event(s) to the application and returns.
     */
    public void waitEvents() {
        eventHappened = false;
        UPDATE_TIMER.reset();

        // Wait until an event happened.
        while (!eventHappened) {
            UPDATE_TIMER.tick();
            // Only check for updates every UPDATE_FREQUENCY milliseconds. Therefore convert UPDATE_FREQUENCY to sec.
            if (UPDATE_TIMER.view.elapsedTimeSP() > (UPDATE_FREQUENCY / 1000.0f)) {
                updateJoystickData();
            }
        }
    }

    /**
     * Lets the thread return from a waitEvents(). No events really occurred, therefore no events are getting pushed to
     * the application.
     */
    public void wakeUp() {
        eventHappened = true;
    }

    /**
     * Static method that can be called once in a while to check whether new joystick devices (e.g. gamepads) were
     * connected to the system.
     * Called by: ColibriEngine
     */
    @Override
    public void checkJoystickConnections() {
        final int iStart = GLFW_JOYSTICK_1;

        for (int i = iStart; i < GLFW_JOYSTICK_LAST; i++) {

            // Was the joystick not present in the previous frame?
            if (!JOYSTICK_CONNECTION[i - iStart]) {

                // Check if there is a now a joystick connected.
                if (glfwJoystickPresent(i)) {
                    final String name = glfwGetJoystickName(i);
                    final FloatBuffer axisData = glfwGetJoystickAxes(i);
                    final ByteBuffer buttonData = glfwGetJoystickButtons(i);

                    assert buttonData != null;
                    assert axisData != null;

                    System.out.printf("InputManager: JoystickManager \"%s\" connected at binding: %d.%n", name, i);
                    System.out.printf("\tAmount of buttons: %d%n", buttonData.capacity());
                    System.out.printf("\tAmount of axes:    %d%n", axisData.capacity());

                    final GamepadType type = determineJoystickType(name, axisData.capacity(), buttonData.capacity());
                    if (type != null) {
                        JOYSTICK_DATA[i - iStart] = new JoystickData(type, axisData.capacity(), buttonData.capacity());
                        JOYSTICK_CONNECTION[i - iStart] = true;
                        currentJoystick = i - iStart;
                    }
                }
            }
        }
    }

    private static GamepadType determineJoystickType(String name, final int axisCapacity, final int buttonCapacity) {
        name = name.toLowerCase();

        // XBOX controller.
        if (name.contains("microsoft") || name.contains("xbox")) {
            // single axis value for LT and RT -> XBOX 360 controller.
            if (axisCapacity == 5 && buttonCapacity == 14) {
                return GamepadType.XBOX_360;
            }
            // two values -> XBOX ONE controller.
            else if (axisCapacity == 6 && buttonCapacity == 14) {
                return GamepadType.XBOX_ONE;
            }
        }

        //TODO: add PS controller support.

        printGamepadUnsupportedError();
        return null;
    }

    private static void printGamepadUnsupportedError() {
        printGamepadUnsupportedError(null);
    }

    private static void printGamepadUnsupportedError(final @Nullable String msg) {
        System.err.println("JoystickManager: Gamepad is not supported. See the list below for supported gamepads.");
        if (msg != null) {
            System.err.println("JoystickManager: info: " + msg);
        }

        System.err.println("JoystickManager: supported gamepads: ");
        for (int i = 0; i < GamepadType.values().length - 1; i++) {
            System.err.print(GamepadType.values()[i] + ", ");
        }
        System.err.print(GamepadType.values()[GamepadType.values().length - 1]);
    }

    @Override
    public void updateJoystickData() {
        for (int i = 0; i < POSSIBLE_JOYSTICK_CONNECTIONS; i++) {

            // A fetch is only necessary if the joystick was connected (delivered data) in the previous frame.
            if (JOYSTICK_CONNECTION[i]) {
                assert JOYSTICK_DATA[i] != null : "InputManager: JoystickManager data should not be null!";

                // Fetch new data.
                final @Nullable FloatBuffer axisData = glfwGetJoystickAxes(i);
                final @Nullable ByteBuffer buttonData = glfwGetJoystickButtons(i);

                // It does not matter if the joystick got disconnected (axisData and/or buttonData null) or not.
                // If so, the updateData method will generate a JoystickEvent which represents the connection loss.
                // Also checks whether any event occurred, so that the program can exit the loop in waitEvents().
                eventHappened = JOYSTICK_DATA[i].updateDataAndGenerateEvents(axisData, buttonData, JOYSTICK_EVENTS[i]);
                if (eventHappened) {
                    inputManager.setActiveInputType(InputType.JOYSTICK);
                }

                // Check against null pointers.
                // If there are any, the connection to the JoystickManager at binding point i got lost.
                if (axisData == null || buttonData == null) {
                    JOYSTICK_CONNECTION[i] = false;
                    JOYSTICK_DATA[i] = null;
                    currentJoystick = -1;
                    inputManager.setActiveInputType(InputType.MOUSE_KEYBOARD);
                }
            }
        }
    }

    public void pushJoystickEvents() {
        for (final JoystickInputCallbacks jic : inputManager.getJCallbackObjects()) {
            invokeCallbacks(jic);
        }
    }

    private void invokeCallbacks(final JoystickInputCallbacks jic) {
        for (int i = 0; i < POSSIBLE_JOYSTICK_CONNECTIONS; i++) {
            var events = JOYSTICK_EVENTS[i];
            if (events.isNotEmpty()) {
                // Push connection loss events.
                for (final ConnectionLossEvent cle : events.getConnectionLossEvents()) {
                    // Gets called if getEventList(i) returned a list that was not empty.
                    // so -> i is an index of an active joystick.
                    jic.jLostConnectionCallback(i);
                }

                // Push axis change events.
                for (final AxisChangeEvent ace : events.getAxisChangeEvents()) {
                    jic.jAxisCallback(ace.getAxis(), ace.getValue());
                }

                // Push button change events.
                for (final ButtonChangeEvent bce : events.getButtonChangeEvents()) {
                    jic.jButtonCallback(bce.getButton(), bce.getAction());
                }
            }
        }
    }

    public JoystickData getCurrentJoystickData() {
        return JOYSTICK_DATA[currentJoystick];
    }

    public int getPossibleJoystickConnections() {
        return POSSIBLE_JOYSTICK_CONNECTIONS;
    }

    public JoystickEvents getJoystickEvents(final int joystickID) {
        return JOYSTICK_EVENTS[joystickID];
    }

    public boolean isButtonDown(final int button) {
        return JOYSTICK_DATA[currentJoystick].isButtonDown(button);
    }

    public boolean isButtonDown(final int joystickID, final int button) {
        if (joystickID < 0 || joystickID > POSSIBLE_JOYSTICK_CONNECTIONS) {
            throw new IllegalArgumentException(
                    "JoystickManager: joystickID needs to be in range: [0, ... , getJoystickAmt()]");
        }
        return !JOYSTICK_DATA[joystickID].isButtonDown(button);
    }

    public float getAxisData(final int joystickAxis) {
        return JOYSTICK_DATA[currentJoystick].getAxisValue(joystickAxis);
    }

    public float getAxisData(final Command command) {
        return getAxisData(command.getJAxis());
    }

}
