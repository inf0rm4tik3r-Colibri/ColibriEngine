package de.colibriengine.input

class NoopInputManager : InputManager {

    override var activeInputType: InputType = InputType.MOUSE_KEYBOARD

    override val mouseKeyboardManager: MouseKeyboardManager = NoopMouseKeyboardManager()

    override val joystickManager: JoystickManager = NoopJoystickManager()

    override val mkCallbackObjects: List<MouseKeyboardInputCallbacks> = emptyList()

    override val jCallbackObjects: List<JoystickInputCallbacks> = emptyList()

    override fun pollInput(waitForInput: Boolean) {
        // Do nothing
    }

    override fun addMKInputCallback(mouseKeyboardInputCallbacks: MouseKeyboardInputCallbacks) {
        // Do nothing
    }

    override fun addJInputCallback(joystickInputCallbacks: JoystickInputCallbacks) {
        // Do nothing
    }

    override fun removeMKInputCallback(mouseKeyboardInputCallbacks: MouseKeyboardInputCallbacks) {
        // Do nothing
    }

    override fun removeJInputCallback(joystickInputCallbacks: JoystickInputCallbacks) {
        // Do nothing
    }

    override fun clearMKInputCallbacks() {
        // Do nothing
    }

    override fun clearJInputCallbacks() {
        // Do nothing
    }

}
