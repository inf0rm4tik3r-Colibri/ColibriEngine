package de.colibriengine.input

import de.colibriengine.input.events.KeyEvent
import de.colibriengine.math.vector.vec2d.StdVec2d
import de.colibriengine.math.vector.vec2d.Vec2d
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import org.lwjgl.glfw.GLFW
import kotlin.math.floor

class MouseKeyboardManagerImpl(
    private val inputManager: InputManager
) : MouseKeyboardManager {

    private val keyStates = HashMap<Int, KeyEvent>(8)

    private var _mousePos: Vec2i = StdVec2i(0, 0)
    private var _mouseDelta: Vec2d = StdVec2d(0.0, 0.0)

    override val mousePos: Vec2iAccessor = _mousePos
    override val mouseDelta: Vec2dAccessor = _mouseDelta

    override fun invokeKeyCallbacks(key: Int, scancode: Int, action: Int, mods: Int) {
        // Keep track on which key is currently pressed down.
        when (action) {
            GLFW.GLFW_PRESS -> keyStates[key] = KeyEvent(key, scancode, mods)
            GLFW.GLFW_RELEASE -> keyStates.remove(key)
        }
        inputManager.mkCallbackObjects.forEach { it.mkKeyCallback(key, scancode, action, mods) }
    }

    override fun invokeMousePosCallbacks(x: Int, y: Int) {
        inputManager.mkCallbackObjects.forEach { it.mkPosCallback(x, y) }
    }

    override fun invokeMouseMoveCallbacks(dx: Int, dy: Int) {
        inputManager.mkCallbackObjects.forEach { it.mkMoveCallback(dx, dy) }
    }

    override fun invokeMouseButtonCallbacks(button: Int, action: Int, mods: Int) {
        inputManager.mkCallbackObjects.forEach { it.mkButtonCallback(button, action, mods) }
    }

    override fun isKeyPressed(keyCode: Int): Boolean {
        return keyCode in keyStates
        // return GLFW.glfwGetKey(window.id, keyCode) == GLFW.GLFW_PRESS
    }

    override fun isKeyReleased(keyCode: Int): Boolean {
        return keyCode !in keyStates
        // return GLFW.glfwGetKey(window.id, keyCode) == GLFW.GLFW_RELEASE
    }

    override fun trackCursor(xPos: Double, yPos: Double) {
        val movementOnX: Double = xPos - mousePos.x
        val movementOnY: Double = yPos - mousePos.y

        inputManager.activeInputType = InputType.MOUSE_KEYBOARD
        invokeMousePosCallbacks(xPos.toInt(), yPos.toInt())
        invokeMouseMoveCallbacks(floor(movementOnX).toInt(), floor(movementOnY).toInt())

        _mouseDelta.plus(movementOnX, movementOnY)
        _mousePos.set(floor(xPos).toInt(), floor(yPos).toInt())
    }

    override fun resetDelta() {
        _mouseDelta.initZero()
    }

    fun pushKeyRepeatEvents() {
        for ((_, ck) in keyStates) {
            for (mkic in inputManager.mkCallbackObjects) {
                mkic.mkKeyCallback(ck.key, ck.scancode, GLFW.GLFW_REPEAT, ck.mods)
            }
        }
    }

}
