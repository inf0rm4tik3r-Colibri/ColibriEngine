package de.colibriengine.input

import de.colibriengine.input.events.AxisChangeEvent
import de.colibriengine.input.events.ButtonChangeEvent
import de.colibriengine.input.events.ConnectionLossEvent
import de.colibriengine.input.gamepads.GamepadType
import de.colibriengine.input.gamepads.Xbox360Gamepad
import de.colibriengine.input.gamepads.XboxOneGamepad
import org.lwjgl.glfw.GLFW
import java.nio.ByteBuffer
import java.nio.FloatBuffer

/**
 * Represents the current state of a connected joystick.
 *
 * @since ColibriEngine 0.0.6.2
 */
class JoystickData(val type: GamepadType, axisDataCapacity: Int, buttonDataCapacity: Int) {

    /** Contains the current axis data. Values are typically near zero when axis controller is in default position. */
    private val axisData: FloatArray = FloatArray(axisDataCapacity)

    /** A true-value at index i corresponds to button i being pressed down. */
    private val buttonData: BooleanArray = BooleanArray(buttonDataCapacity)

    fun getAxisValue(axis: Int): Float {
        return axisData[axis]
    }

    fun isButtonDown(button: Int): Boolean {
        return buttonData[button]
    }

    /**
     * Updates this joysticks data with the data given to it in [axisData] and [buttonData]. The algorithm keeps track
     * on which value changed, compared to the last known data which is stored in this object. Every state change gets
     * stored in the given lists.
     *
     * @param axisData The new states of the joysticks axes.
     * @param buttonData The new states of the joysticks buttons.
     * @param clEventList The event list to store ConnectionLoss events.
     * @param acEventList The event list to store AxisChange events.
     * @param bcEventList The event list to store ButtonChange events.
     * @return whether any data differed from the last frames data.
     */
    fun updateDataAndGenerateEvents(
        axisData: FloatBuffer?,
        buttonData: ByteBuffer?,
        events: JoystickEvents
    ): Boolean {
        events.clear()

        // Generate a CONNECTION_LOSS event and return immediately if the joystick did not provided any data
        // (most likely because it was disconnected).
        if (axisData == null || buttonData == null) {
            events.addConnectionLossEvent(ConnectionLossEvent.acquire())
            return true
        }

        // Generate a runtime error if one of the given data buffers contains more data than it should.
        if (this.axisData.size != axisData.capacity() || this.buttonData.size != buttonData.capacity()) {
            throw RuntimeException("JoystickData: buffer length differs from initial length.")
        }

        // Copy axis data from the FloatBuffer into a float array.
        // Generate JoystickEvents for every value that changed.
        for (i in 0 until axisData.capacity()) {

            // Update the local array with the new information.
            this.axisData[i] = axisData.get()
            when (type) {
                GamepadType.XBOX_360 -> if (Xbox360Gamepad.outOfThreshold(i, this.axisData[i])) {
                    events.addAxisChangeEvent(AxisChangeEvent.acquire().set(i, this.axisData[i]))
                }
                GamepadType.XBOX_ONE -> if (XboxOneGamepad.outOfThreshold(i, this.axisData[i])) {
                    events.addAxisChangeEvent(AxisChangeEvent.acquire().set(i, this.axisData[i]))
                }
                else -> System.err.println("JoystickData: A gamepad type specified in \"Gamepads\" is not supported.")
            }
        }

        // Copy button data from the ByteBuffer into a boolean array.
        // Generate JoystickEvents for every value that changed.
        var oldButtonState: Boolean
        for (i in 0 until buttonData.capacity()) {
            // Save the old state for a comparison.
            oldButtonState = this.buttonData[i]
            // Given data is 1 if pressed down or 0 if not.
            this.buttonData[i] = buttonData.get().toInt() == 1
            if (!oldButtonState and this.buttonData[i]) {
                events.addButtonChangeEvent(ButtonChangeEvent.acquire().set(i, GLFW.GLFW_PRESS))
            } else if (oldButtonState and !this.buttonData[i]) {
                events.addButtonChangeEvent(ButtonChangeEvent.acquire().set(i, GLFW.GLFW_RELEASE))
            } else if (oldButtonState and this.buttonData[i]) {
                events.addButtonChangeEvent(ButtonChangeEvent.acquire().set(i, GLFW.GLFW_REPEAT))
            }
        }

        // The list will only be empty if there were no state changes. In contrary, if there were changes, the joystick
        // input needs to be activated.
        return events.isNotEmpty()
    }

}
