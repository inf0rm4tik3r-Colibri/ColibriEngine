package de.colibriengine.ui

import de.colibriengine.datastructures.binarytree.BinaryTree
import de.colibriengine.ecs.ECS
import de.colibriengine.ecs.EntityId
import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.camera.CameraFactory
import de.colibriengine.graphics.window.Window
import de.colibriengine.math.matrix.mat4f.Mat4f
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.matrix.mat4f.StdMat4f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.ui.structure.AbstractGUIComponent
import java.util.*

// TODO: Here (up-api) should only be an interface!

/*
    - Each GUI instance should have its own scenegraph.
    - This is a PURE 2D GUI!
    - The GUI should be rendered on top of the actual 3D content.
    - The GUI renderer must not necessarily render in the same resolution as the 3D world was rendered.
    - It should be a "retained mode" GUI.
    - "components" are the smallest individual elements which can be placed in a GUI.
    - "widgets" are predefined collections / arrangements of components and only exist to achieve faster development.
    
    CONSIDERATIONS:
    - The GUI must not necessarily be rendered each frame the world gets rendered.
    - It should be possible to only re-render parts of the GUI.
    
    QUESTIONS:
    - Can there be multiple GUI instances simultaneously?
    - How do the GUI components get their input?
            - The GUI must be bound to a windows InputManager...
   
    HOW TO:
    - Tell the GUI who its responsible InputManager is on creation: GUI gui = new GUI(window.getInputManager());
    - Create a component: Button b = new Button();
    - Add components to the GUI's SceneGraph: gui.
*/
class GUI(
    ecs: ECS,
    cameraFactory: CameraFactory,
    private val widthProvider: () -> Int,
    private val heightProvider: () -> Int
) {

    private val centeredCam: Camera = cameraFactory.simple()

    val rootNode: GUIRootNode = GUIRootNode(ecs.createEntity(), this)

    private val componentTree: BinaryTree<AbstractGUIComponent, AbstractSceneGraphNode> = BinaryTree()
    private val componentMap: HashMap<EntityId, AbstractGUIComponent?> = HashMap(COMPONENT_MAP_INITIAL_SIZE)
    private val componentMapValues: Collection<AbstractGUIComponent?> = componentMap.values

    val orthographicVP: Mat4fAccessor
        get() = centeredCam.orthographicViewProjectionMatrix

    val perspectiveVP: Mat4fAccessor
        get() = centeredCam.perspectiveViewProjectionMatrix

    /**
     * Computes the layout of this GUI instance. Traverses all GUI components and calculates their transforms. Prior to
     * a call to this function, the orthographic and perspective view projections must have been set!
     */
    fun computeLayout() {
        // TODO: The camera should be set up with the size of the GUIBuffer!!
        centeredCam.initProjectionMatrices(
            widthProvider.invoke(),
            heightProvider.invoke()
        )
        centeredCam.calculateViewProjectionMatrices()

        // Reset the transform of each GUI component. This will lead to a recalculation of it.
        for (component in componentMapValues) {
            component!!.invalidateTransform()
        }

        // Calculate the base components in the scene graph. All other components can then relate to them.
        val it = rootNode.childNodeIterator
        while (it.hasNext()) {
            val node = it.next() as AbstractGUIComponent
            node.calculateTransform()
        }

        // The condition-variable with which this method iterates over the map of GUI components as long as there
        // were still layout computations.
        var madeCalculation = true
        // TODO: replace with do-while
        while (madeCalculation) {
            // We must set the condition-variable of our loop to false. => Exit the loop if no calculations were made.
            madeCalculation = false
            for (component in componentMapValues) {
                // Only process this component if its transform was not jet (re)calculated.
                if (!component!!.isTransformCalculated) {
                    // This node might not be able to calculate its transform if an anchored component was not jet
                    // processed. We must catch that! This loop could otherwise run idefinitely if there is a circular
                    // reference in the components anchors.
                    if (component.calculateTransform()) {
                        madeCalculation = true
                    }
                }
            }
        }

        // Iterate a last time over the components to check if each got calculated.
        // If we weren't able to calculate the transform of one component, there must have been a recursive anchor
        // definition in play.
        var notCalculated: MutableList<AbstractGUIComponent?>? = null
        for (component in componentMapValues) {
            if (!component!!.isTransformCalculated) {
                if (notCalculated == null) {
                    notCalculated = ArrayList()
                }
                notCalculated.add(component)
            }
        }
        if (notCalculated != null) {
            throw RuntimeException("The transform of GUI components " + Arrays.toString(notCalculated.toTypedArray()) + " could not be calculated!")
        }
    }

    fun getComponentByID(objectID: EntityId): AbstractGUIComponent {
        return componentMap[objectID]
            ?: throw IllegalArgumentException("No GUI component with id \"$objectID\" exists!")
    }

    /** Must be called every time a GUI node got added to the hierarchy of nodes! */
    fun added(component: AbstractGUIComponent) {
        // Add the object to the binary search tree.
        require(!componentTree.contains(component)) { "Component with id " + component.entity.entityId + " already exists in the componentTree!" }
        componentTree.insert(component)

        // Add the object to the map of components.
        require(!componentMap.containsKey(component.entity.entityId)) { "Component with id " + component.entity.entityId + " already exists in the componentMap!" }
        componentMap[component.entity.entityId] = component
    }

    fun removed(component: AbstractGUIComponent) {
        componentTree.remove(component)
        componentMap.remove(component.entity.entityId)
    }

    companion object {
        private const val COMPONENT_MAP_INITIAL_SIZE = 32
    }

}
