package de.colibriengine.ui.structure.anchoring

class Anchors {

    @JvmField
    var topLeft: Anchor? = null

    @JvmField
    var topRight: Anchor? = null

    @JvmField
    var bottomLeft: Anchor? = null

    @JvmField
    var bottomRight: Anchor? = null

}
