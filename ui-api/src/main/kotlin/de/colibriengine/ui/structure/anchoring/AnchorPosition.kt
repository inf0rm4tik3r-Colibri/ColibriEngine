package de.colibriengine.ui.structure.anchoring

enum class AnchorPosition {
    /* Edge */
    // TOP, RIGHT, BOTTOM, LEFT,

    /* Point - Defining a point is the same as defining both of the involved edges. */
    TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
}
