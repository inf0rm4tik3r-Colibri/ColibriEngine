#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;

out vec4 color;

uniform sampler2D tex;
uniform vec2 screenSize;
uniform vec2 invScreenSize;

const vec3 LUM = vec3(0.299f, 0.587f, 0.114f);

// TODO: Provide these values as uniforms.

const float FXAA_SPAN_MAX = 8.0f;
const float FXAA_REDUCE_MIN = 1.0f / 128.0f;
const float FXAA_REDUCE_MUL = 1.0f / 8.0f;

const float NEAR_NEG_DIR = -(1.0f / 6.0f); // Look at close point in negative direction.
const float NEAR_POS_DIR = +(1.0f / 6.0f); // Look at close point in positive direction.
const float FAR_NEG_DIR  = -0.5f;          // Look at distant point in negative direction.
const float FAR_POS_DIR  = +0.5f;          // Look at distant point in positive direction.

/**
 * Returns a vec2 with values in the range [0 ... 1] perfectly suited to sample a texture with.
 * screenSize must be set to the actual size of the WINDOW, not the size of the underlying GBuffer!
 */
vec2 calcFragCoord() {
	return gl_FragCoord.xy / screenSize;
}

void main(void) {
    vec2 fragCoord = calcFragCoord();

    // Retrieve the color values of the current and the surrounding texels(s) and calculate their luminosity values.
    vec4 rgbM = texture(tex, fragCoord);
    float lumTL = dot(LUM, texture(tex, fragCoord + vec2(-1.0f, +1.0f) * invScreenSize).rgb);
    float lumTR = dot(LUM, texture(tex, fragCoord + vec2(+1.0f, +1.0f) * invScreenSize).rgb);
    float lumBL = dot(LUM, texture(tex, fragCoord + vec2(-1.0f, -1.0f) * invScreenSize).rgb);
    float lumBR = dot(LUM, texture(tex, fragCoord + vec2(+1.0f, -1.0f) * invScreenSize).rgb);
    float lumM  = dot(LUM, rgbM.rgb);
    float minLum = min(lumM, min(min(lumTL, lumTR), min(lumBL, lumBR))); // The minimum luminacity.
    float maxLum = max(lumM, max(max(lumTL, lumTR), max(lumBL, lumBR))); // The maximum luminacity.
    float avgLum = (lumTL + lumTR + lumBL + lumBR) * 0.25; // Of the surrounding texels.

    // Calculate the direction in which the image should be blurred.
    vec2 dir;
    dir.x = ((lumTL + lumTR) - (lumBL + lumBR)); // Luminance difference top to bottom.
    dir.y = ((lumTL + lumBL) - (lumTR + lumBR)); // Luminance difference left to right.

    float dirReduce = max(avgLum * FXAA_REDUCE_MUL, FXAA_REDUCE_MIN);

    // With this normalizatio factor we are able to scale the direction so that its smaller compoent is 1!
    float invDirNormFactor = 1.0f / (min(abs(dir.x), abs(dir.y)) + dirReduce); // Will never divide by 0!

    // We still want to clamp our vector to some boundaries.
    dir = clamp(dir * invDirNormFactor, vec2(-FXAA_SPAN_MAX), vec2(FXAA_SPAN_MAX));

    // We want to access the texture with our blur direction vector.
    dir = dir * invScreenSize;

    vec3 sampleNearNegDir = texture(tex, fragCoord + dir * NEAR_NEG_DIR).rgb;
    vec3 sampleNearPosDir = texture(tex, fragCoord + dir * NEAR_POS_DIR).rgb;
    vec3 sampleFarNegDir = texture(tex, fragCoord + dir * FAR_NEG_DIR).rgb;
    vec3 sampleFarPosDir = texture(tex, fragCoord + dir * FAR_POS_DIR).rgb;

    // Calculate the average of the near and far samples.
    vec3 nearAvg = 0.5f * (sampleNearNegDir + sampleNearPosDir);
    vec3 farAvg = 0.5f * (sampleFarNegDir + sampleFarPosDir);

    // Calculate the average of all samples.
    vec3 totalAvg = 0.5f * (nearAvg + farAvg);

    float totalAvgLum = dot(LUM, totalAvg);

    // If the luminacity of our total averaged color does not lie inside the minimum and maximum of our initially
    // sampled surrounding texel values, use the average color along the direction.
    // Otherwise use the total average color as it is a good candidate and save to use.
    if ((totalAvgLum < minLum) || (totalAvgLum > maxLum))
        color = vec4(nearAvg, rgbM.a);
    else
        color = vec4(totalAvg, rgbM.a);
}