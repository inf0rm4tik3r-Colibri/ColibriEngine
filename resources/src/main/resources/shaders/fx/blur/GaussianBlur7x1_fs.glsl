#version 330 core

in vec2 vsTexCoord;

layout (location = 0) out vec4 color;

uniform sampler2D tex;

uniform vec2 texelSize;

/**
 * This scale defines the axis on which the blur should be applied.
 */
uniform vec2 blurScale;

void main(void) {
    vec2 col = vec2(0.0);

    vec2 factor = blurScale * texelSize;

    col += texture(tex, vsTexCoord + vec2(-3.0f) * factor).rg * ( 1.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2(-2.0f) * factor).rg * ( 6.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2(-1.0f) * factor).rg * (15.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2( 0.0f) * factor).rg * (20.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2( 1.0f) * factor).rg * (15.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2( 2.0f) * factor).rg * ( 6.0f / 64.0f);
    col += texture(tex, vsTexCoord + vec2( 3.0f) * factor).rg * ( 1.0f / 64.0f);

    color = vec4(col, 0, 1);
}