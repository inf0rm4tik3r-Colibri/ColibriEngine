#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;

out vec4 color;

uniform sampler2D tex;

void main(void) {
    color = texture(tex, vsTexCoord);
	float average = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
    color = vec4(average, average, average, 1.0);
}