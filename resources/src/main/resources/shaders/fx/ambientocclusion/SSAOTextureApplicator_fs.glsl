#version 330 core

in vec2 vsTexCoord;

layout (location = 0) out vec4 fragColor;

uniform sampler2D ssaoTex;
uniform sampler2D sourceTex;

void main(void)
{
    float ssaoLightAmt = texture(ssaoTex, vsTexCoord).r;
    vec4 source = texture(sourceTex, vsTexCoord);

    fragColor = vec4(source.rgb * ssaoLightAmt, source.a);
}