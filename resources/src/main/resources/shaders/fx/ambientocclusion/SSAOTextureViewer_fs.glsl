#version 330 core

in vec2 vsTexCoord;

layout (location = 0) out vec4 fragColor;

uniform sampler2D tex;

void main(void)
{
    float occlusionFactor = texture(tex, vsTexCoord).r;
    fragColor = vec4(occlusionFactor, occlusionFactor, occlusionFactor, 1.0);
}