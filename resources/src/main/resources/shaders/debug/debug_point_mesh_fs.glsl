#version 330 core

in vec4 vsColor;

layout (location = 0) out vec4 diffuse_fs;
layout (location = 1) out vec4 normal_fs;
layout (location = 2) out vec2 specularData_fs;
layout (location = 3) out vec3 pickingData_fs;

void main(void) {
    diffuse_fs      = vsColor;
    normal_fs       = vec4(0, 0, 0, 0);
    specularData_fs = vec2(0, 0);
    pickingData_fs  = uvec3(0, 0, 0);
}
