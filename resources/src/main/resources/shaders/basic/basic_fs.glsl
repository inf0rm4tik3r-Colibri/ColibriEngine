#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor;

layout (location = 0) out vec4 diffuse_fs;
layout (location = 1) out vec4 normal_fs;
layout (location = 2) out vec2 specularData_fs;
layout (location = 3) out vec3 pickingData_fs;

uniform sampler2D ambientTexture;
uniform float specularIntensity = 1;
uniform float specularExponent = 4;
uniform uint objectID = 0u;
uniform uint drawCallID = 0u;

/**
 * See: https://aras-p.info/texts/CompactNormalStorage.html
 */
vec2 encodeNormal(vec3 normalizedNormal) {
    // Method #5: Cry Engine 3
    const float range = 0.001f;
    if (normalizedNormal.x > -range && normalizedNormal.x < range) {
        normalizedNormal.x = range;
    }
    if (normalizedNormal.y > -range && normalizedNormal.y < range) {
        normalizedNormal.y = range;
    }
    if (normalizedNormal.z > -range && normalizedNormal.z < range) {
        normalizedNormal.z = range;
    }
    normalize(normalizedNormal);
    vec2 encoded = normalize(normalizedNormal.xy) * (sqrt(-normalizedNormal.z * 0.5f + 0.5f));
    return encoded;
}

void main(void) {
    vec4 textureColor = texture(ambientTexture, vsTexCoord);

	diffuse_fs      = vsColor * textureColor; // * 2; factor produces a nice bright imege!
    //normal_fs       = encodeNormal(normalize(vsNormal) * 0.5 + 0.5);
    //normal_fs       = encodeNormal(normalize(vsNormal));
    normal_fs       = vec4(normalize(vsNormal) * 0.5 + 0.5, 0/* left blank for other data */);
    specularData_fs = vec2(specularIntensity, specularExponent);
    pickingData_fs  = uvec3(objectID, drawCallID, gl_PrimitiveID + 1);
}
