#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor;

layout (location = 0) out vec4  diffuse_fs;
layout (location = 1) out vec3  normal_fs;
layout (location = 2) out vec2  specularData_fs;
layout (location = 3) out uvec4 pickingData_fs;

uniform sampler2D ambientTexture;
uniform float specularIntensity = 1;
uniform float specularExponent = 4;
uniform uint objectID = 0u;
uniform uint drawCallID = 0u;

uniform float alphaThreshold = 0.0f;

/**
 * See: https://aras-p.info/texts/CompactNormalStorage.html
 */
vec2 encodeNormal(vec3 normal) {
    // Method #5: Cry Engine 3
    /*
    vec2 encoded = normalize(normal.xy) * sqrt(-normal.z * 0.5 + 0.5);
    return encoded * 0.5 + 0.5;
    */

    // Method #6: Lambert Azimuthal Equal-Area Projection
    float f = sqrt(normal.z * 8 + 8);
    return normal.xy / f + 0.5;
}

void main(void) {
    vec4 textureColor = texture(ambientTexture, vsTexCoord);

	diffuse_fs      = vsColor * textureColor;
    if (diffuse_fs.a < alphaThreshold) {
        discard;
    }

    //normal_fs     = encodeNormal(vsNormal);
    normal_fs       = vsNormal * 0.5 + 0.5;
    specularData_fs = vec2(specularIntensity, specularExponent);
    pickingData_fs  = uvec4(objectID, drawCallID, gl_PrimitiveID + 1, 0);
}
