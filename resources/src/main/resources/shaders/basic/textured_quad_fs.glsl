#version 330 core

in vec3 vsNormal;
in vec2 vsTexCoord;
in vec4 vsColor;

out vec4 color;

uniform sampler2D tex;

void main(void) {
    vec4 textureColor = texture(tex, vsTexCoord);

	color = vsColor * textureColor;
}