#version 330

in vec3 normal_vs;
in vec2 texCoord_vs;

layout (location = 0) out vec3 diffuse_fs;
layout (location = 1) out vec2 normal_fs;
layout (location = 2) out vec2 specularData_fs;
layout (location = 3) out uvec2 drawData_fs;

uniform sampler2D sampler1;
uniform vec3 objectColor;
uniform float specularIntensity;
uniform float specularExponent;

uniform int objectID;
uniform int drawCallID;

vec2 encodeNormal(vec3 normal) {
	vec2 encodedNormal = normalize(normal.xy) * (sqrt(-normal.z * 0.5 + 0.5));
	return encodedNormal * 0.5 + 0.5;
}

void main() {
	diffuse_fs = texture(sampler1, texCoord_vs).xyz * objectColor;
	//drawData_fs = uvec3(objectID, drawCallID, gl_PrimitiveID + 1);
	drawData_fs = uvec2(objectID, drawCallID);
	normal_fs = encodeNormal(normalize(normal_vs));
	//normal_fs = normalize(normal_vs);
	specularData_fs = vec2(specularIntensity, specularExponent);
}