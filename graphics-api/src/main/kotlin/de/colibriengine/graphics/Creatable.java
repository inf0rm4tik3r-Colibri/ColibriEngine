package de.colibriengine.graphics;

// TODO: Convert to Kotlin or remove entirely to allow classes to make access private.
public interface Creatable {

    /**
     * This error message will be thrown in an exceptions if the {@link Creatable#ensureNotCreated()}
     * method finds out that this objects is currently created.
     */
    String MSG_OBJECT_CREATED = "This \"%s\" is created but must not be.";

    /**
     * This error message will be thrown in an exceptions if the {@link Creatable#ensureCreated()}
     * method finds out that this objects is currently not created.
     */
    String MSG_OBJECT_NOT_CREATED = "This \"%s\" is not created when it should/must be.";

    /**
     * Sets this object to be in its "created" state.
     *
     * @param created
     *     true / false
     */
    void setCreated(final boolean created);

    /**
     * @return Whether or not this object is currently created.
     */
    boolean isCreated();

    /**
     * Ensures that this object is created by calling {@link Creatable#isCreated()} and throwing an exception if that
     * call returned {@code false}.
     *
     * @throws IllegalStateException
     *     If this object is not created.
     */
    default void ensureCreated() throws IllegalStateException {
        if (!isCreated()) {
            throw new IllegalStateException(
                String.format(MSG_OBJECT_NOT_CREATED, getClass().getSuperclass().getName())
            );
        }
    }

    /**
     * Ensures that this object is not created by calling {@link Creatable#isCreated()} and throwing an exception if
     * that call returned {@code true}.
     *
     * @throws IllegalStateException
     *     If this object is created.
     */
    default void ensureNotCreated() throws IllegalStateException {
        if (isCreated()) {
            throw new IllegalStateException(
                String.format(MSG_OBJECT_CREATED, getClass().getSuperclass().getName())
            );
        }
    }

}
