package de.colibriengine.graphics.texture

enum class TextureWrapType {
    
    CLAMP,
    CLAMP_TO_BORDER,
    REPEAT

}
