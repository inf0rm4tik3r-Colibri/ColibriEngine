package de.colibriengine.graphics

@JvmInline
value class Extension(val name: String) : Comparable<Extension> {

    init {
        require(name.isNotEmpty()) { "Extension name must not be empty! " }
    }

    override fun compareTo(other: Extension): Int = name.compareTo(other.name)

}
