package de.colibriengine.graphics.camera

import de.colibriengine.graphics.camera.type.ActionCamera
import de.colibriengine.graphics.camera.type.ArcBallCamera
import de.colibriengine.graphics.camera.type.SimpleCamera

interface CameraFactory {

    fun simple(): SimpleCamera
    fun action(): ActionCamera
    fun arcBall(): ArcBallCamera

}
