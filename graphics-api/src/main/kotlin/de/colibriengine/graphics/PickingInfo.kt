package de.colibriengine.graphics

data class PickingInfo(val objectID: Int, val drawCallID: Int, val primitiveID: Int)
