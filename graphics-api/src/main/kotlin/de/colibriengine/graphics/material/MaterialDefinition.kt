package de.colibriengine.graphics.material

import de.colibriengine.math.FEMath.clamp
import de.colibriengine.math.vector.vec3f.StdImmutableVec3f.Companion.builder
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f

open class MaterialDefinition(var name: String = DEFAULT_MATERIAL_NAME) {

    /** The ambient reflectivity. */
    val ambientColor: Vec3f = StdVec3f().set(AMBIENT_COLOR_DEFAULT)

    /** The diffuse reflectivity. */
    val diffuseColor: Vec3f = StdVec3f().set(DIFFUSE_COLOR_DEFAULT)

    /** The specular reflectivity. */
    val specularColor: Vec3f = StdVec3f().set(SPECULAR_COLOR_DEFAULT)

    /**
     * Any light passing through the object is filtered by the transmission filter, which only allows the specific
     * colors to pass through. For example, 0 1 0 allows all the green to pass through and filters out all the red and
     * blue.
     */
    val transmissionFilter: Vec3f = StdVec3f().set(TRANSMISSION_FILTER_DEFAULT)

    /**
     * The illumination id specifies the illumination model to use in the material. Illumination models are
     * mathematical equations that represent various material lighting and shader effects. Can be a number from
     * 0 to 10. Defined in ILLUMINATION_MODELS.
     */
    var illuminationModelID: Int = ILLUMINATION_MODEL_ID_DEFAULT
        set(value) {
            require(ILLUMINATION_MODELS.containsKey(value)) {
                "Unknown illumination model specified: $value"
            }
            field = value
        }

    /**
     * The amount this material dissolves into the background. A factor of 1.0 is fully opaque. This is the default
     * when a new material is created. A factor of 0.0 is fully dissolved (completely transparent). Unlike a real
     * transparent material, the dissolve does not depend upon material thickness nor does it have any spectral
     * character. Dissolve works on all illumination models.
     */
    var dissolveFactor: Float = DEFAULT_DISSOLVE_FACTOR

    /**
     * Specifies that a dissolve is dependent on the surface orientation relative to the viewer.
     * For example, a sphere with the following dissolve, d -halo 0.0, will be fully dissolved at its center and will
     * appear gradually more opaque toward its edge.
     */
    var dissolveViewAngleFactor: Float = DEFAULT_DISSOLVE_VIEW_ANGLE_FACTOR

    /**
     * Specifies the specular exponent for the current material. This defines the focus of the specular highlight. A
     * high exponent results in a tight, concentrated highlight. Values normally range from 0 to 1000.
     */
    var specularExponent: Float = SPECULAR_EXPONENT_DEFAULT

    /**
     * Specifies the sharpness of the reflections from the local reflection map. If a material does not have a local
     * reflection map defined in its material definition, sharpness will apply to the global reflection map defined in
     * PreView.
     * Can be a number from 0 to 1000. The default is 60. A high value results in a clear reflection of objects in the
     * reflection map. Tip Sharpness values greater than 100 map introduce aliasing effects in flat surfaces that are
     * viewed at a sharp angle.
     */
    var sharpness: Float = SHARPNESS_DEFAULT
        set(value) {
            val clamped = clamp(value, SHARPNESS_MIN, SHARPNESS_MAX)
            if (clamped != value) {
                System.err.println("Clamped sharpness value from: $value to: $clamped")
            }
            field = clamped
        }

    /**
     * Specifies the optical density for the surface. This is also known as index of refraction. The values can range
     * from 0.001 to 10. A value of 1.0 means that light does not bend as it passes through an object. Increasing the
     * optical density increases the amount of bending. Glass has an index of refraction of about 1.5. Values of less
     * than 1.0 produce bizarre results and are not recommended.
     */
    var opticalDensity: Float = OPTICAL_DENSITY_DEFAULT
        set(value) {
            val clamped = clamp(value, OPTICAL_DENSITY_MIN, OPTICAL_DENSITY_MAX)
            if (clamped != value) {
                System.err.println("Clamped opticalDensity value from: $value to: $clamped")
            }
            field = clamped
        }

    /**
     * Specifies that a colour texture file is applied to the ambient color of the material. During rendering,
     * the ambient texture value is multiplied by the ambient color value.
     */
    var ambientColorTexture: MaterialTextureDefinition? = null

    /**
     * Specifies that a colour texture file is applied to the diffuse color of the material. During rendering,
     * the diffuse texture value is multiplied by the diffuse color value.
     */
    var diffuseColorTexture: MaterialTextureDefinition? = null

    /**
     * Specifies that a colour texture file is applied to the specular color of the material. During rendering,
     * the specular texture value is multiplied by the specular color value.
     */
    var specularColorTexture: MaterialTextureDefinition? = null

    /**
     * Specifies that a colour texture file is applied to the specular exponent of the material. During rendering,
     * the specular exponent texture value is multiplied by the specular exponent value.
     */
    var specularExponentTexture: MaterialTextureDefinition? = null

    /**
     * Specifies that a colour texture file is applied to the dissolve of the material. During rendering,
     * the dissolve texture value is multiplied by the dissolve value.
     */
    var dissolveTexture: MaterialTextureDefinition? = null

    /**
     * Decal texture.
     */
    var decalTexture: MaterialTextureDefinition? = null

    /**
     * Displacement texture.
     */
    var displacementTexture: MaterialTextureDefinition? = null

    /**
     * Bump texture.
     */
    var bumpTexture: MaterialTextureDefinition? = null

    /**
     * Normal texture.
     */
    var normalTexture: MaterialTextureDefinition? = null

    /**
     * Turns on anti-aliasing of textures in this material without anti-aliasing all textures in the scene.
     */
    var isTextureAA: Boolean = TEXTURE_AA_DEFAULT

    companion object {
        private const val DEFAULT_MATERIAL_NAME = "default"
        private val AMBIENT_COLOR_DEFAULT = builder()
            .of(0.2f, 0.2f, 0.2f)
        private val DIFFUSE_COLOR_DEFAULT = builder()
            .of(0.8f, 0.8f, 0.8f)
        private val SPECULAR_COLOR_DEFAULT = builder()
            .of(0.2f, 0.2f, 0.2f)
        private val TRANSMISSION_FILTER_DEFAULT = builder()
            .of(0.0f)
        private const val ILLUMINATION_MODEL_ID_DEFAULT = 0
        private const val DEFAULT_DISSOLVE_FACTOR = 1.0f
        private const val DEFAULT_DISSOLVE_VIEW_ANGLE_FACTOR = 1.0f
        private const val SPECULAR_EXPONENT_DEFAULT = 0.0f
        private const val SHARPNESS_DEFAULT = 60.0f
        private const val SHARPNESS_MIN = 0.0f
        private const val SHARPNESS_MAX = 1000.0f
        private const val OPTICAL_DENSITY_DEFAULT = 1.0f
        private const val OPTICAL_DENSITY_MIN = 0.001f
        private const val OPTICAL_DENSITY_MAX = 10.0f
        private const val TEXTURE_AA_DEFAULT = false
        private val ILLUMINATION_MODELS: HashMap<Int, String> = HashMap(10)

        init {
            ILLUMINATION_MODELS[0] = "Color on and Ambient off"
            ILLUMINATION_MODELS[1] = "Color on and Ambient on"
            ILLUMINATION_MODELS[2] = "Highlight on"
            ILLUMINATION_MODELS[3] = "Reflection on and Ray trace on"
            ILLUMINATION_MODELS[4] = "Transparency: Glass on; Reflection: Ray trace on"
            ILLUMINATION_MODELS[5] = "Reflection: Fresnel on and Ray trace on"
            ILLUMINATION_MODELS[6] = "Transparency: Refraction on; Reflection: Fresnel off and Ray trace on"
            ILLUMINATION_MODELS[7] = "Transparency: Refraction on; Reflection: Fresnel on and Ray trace on"
            ILLUMINATION_MODELS[8] = "Reflection on and Ray trace off"
            ILLUMINATION_MODELS[9] = "Transparency: Glass on; Reflection: Ray trace off"
            ILLUMINATION_MODELS[10] = "Casts shadows onto invisible surfaces"
        }
    }

}
