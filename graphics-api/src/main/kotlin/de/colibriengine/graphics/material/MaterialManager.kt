package de.colibriengine.graphics.material

interface MaterialManager {

    val materialFactory: MaterialFactory

    fun requestMaterialLibrary(relativePath: String): MaterialLibrary

}
