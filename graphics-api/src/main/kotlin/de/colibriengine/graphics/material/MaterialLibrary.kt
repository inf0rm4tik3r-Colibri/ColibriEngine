package de.colibriengine.graphics.material

interface MaterialLibrary {

    val materials: List<Material>

    fun free()

}
