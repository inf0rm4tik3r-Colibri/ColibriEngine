package de.colibriengine.graphics.window

import de.colibriengine.asset.ResourceName
import de.colibriengine.graphics.camera.CameraFactory
import de.colibriengine.graphics.camera.CameraManager
import de.colibriengine.graphics.material.MaterialManager
import de.colibriengine.graphics.model.ModelManager
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec4f.Vec4f

interface Window : WindowCallbacksInterface {

    /** The window handle. */
    val id: Long

    var title: String
    var icon: List<ResourceName>

    /**
     * The [CameraManager] of this window. Provides functionality for adding (and removing) cameras to this window and
     * switching between registered cameras.
     */
    val cameraManager: CameraManager

    //val cameraFactory: CameraFactory

    // TODO: remove?
    val textureManager: TextureManager

    // TODO: remove?
    val modelManager: ModelManager

    // TODO: remove?
    val materialManager: MaterialManager

    /**
     * Controls whether the window is currently visible. If that is not the case the window is hidden and will therefore
     * not be updated. An invisible window does still exist but wont not get updated in the background.
     */
    var visible: Boolean

    var cursorMode: CursorMode

    val position: Vec2iAccessor

    val size: Vec2iAccessor

    val framebufferSize: Vec2iAccessor

    val contentScale: Vec2fAccessor

    val monitorContentScale: Vec2fAccessor

    fun activate()

    /** Centers the window in the middle of the screen/monitor. */
    fun center()

    var fullscreen: Boolean

    /** The index of the monitor on which the window is currently displayed in full. */
    var fullscreenMonitor: Long

    fun toggleFullscreen() {
        fullscreen = !fullscreen
    }

    val clearColor: Vec4f

    /** Clears the current back buffer of this window with the stored [clearColor]. */
    fun clear()

    /** Swaps this windows front and back buffer. */
    fun swap()

    /**
     * States whether this window should be closed. If the close flag of a window is set true, this window is removed
     * from the window manager it is attached to and terminated through its release() method at the start of the next
     * frame.
     */
    var shouldClose: Boolean

    /**
     * // TODO: Only true in OpenGLWindow!
     * Frees all callbacks and destroys this window and its OpenGL context.
     * <p>
     * TODO: Check if that statement is true.
     * This windows context needs to be current in the calling thread! Otherwise we could release assets from the
     * wrong context and would therefore produce interference in another worker windows model manager.
     */
    fun release()

    /**
     * Makes the OpenGL context of this window current in the current thread.
     * Only performs the context switch if the context of this window is not already current.
     */
    // TODO: move to OpenGLWindow!
    // TODO: Replace this with a higher level method.
    fun makeContextCurrent()

}
