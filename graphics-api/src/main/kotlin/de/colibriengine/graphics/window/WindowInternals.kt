package de.colibriengine.graphics.window

interface WindowInternals {

    fun dropCallbackDecode(count: Int, names: Long)
    fun cacheWindowSize(width: Int, height: Int)
    fun cacheWindowPosition(xPos: Int, yPos: Int)
    fun cacheFramebufferSize(width: Int, height: Int)
    fun cacheWindowContentScale(xScale: Float, yScale: Float)
    fun cacheCursorPosition(xPos: Double, yPos: Double)

}
