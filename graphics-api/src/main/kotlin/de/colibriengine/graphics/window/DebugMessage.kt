package de.colibriengine.graphics.window

data class DebugMessage(

    val message: String,
    val isNotification: Boolean

)
