package de.colibriengine.graphics.window

import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWVidMode
import org.lwjgl.system.MemoryUtil

/**
 * Provides utility functions helpful when creating a new window.
 *
 * @since ColibriEngine 0.0.7.1
 */
object WindowHelper {

    @JvmStatic
    fun calculateMonitorToUse(desiredMonitor: Long): Long {
        // Specify the monitor on which the window should be initially created/placed.
        // Use the primary monitor or the one with the specified index.
        val monitorToUse: Long
        if (desiredMonitor >= 0) {
            // Store the handles for all connected monitors in an array.
            val monitorsBuffer = GLFW.glfwGetMonitors() ?: throw RuntimeException("Monitors could not be acquired.")
            val monitors = LongArray(monitorsBuffer.capacity())
            monitorsBuffer[monitors]
            if (desiredMonitor >= monitors.size) {
                throw RuntimeException("It seems there is not a monitor with the desired index: $desiredMonitor")
            }
            monitorToUse = monitors[desiredMonitor.toInt()]
        } else {
            monitorToUse = GLFW.glfwGetPrimaryMonitor()
            if (monitorToUse == MemoryUtil.NULL) {
                throw RuntimeException("No primary monitor could be found.")
            }
        }
        return monitorToUse
    }

    @JvmStatic
    fun calculateVideoModeToUse(
        monitor: Long, desiredWidth: Int,
        desiredHeight: Int, desiredRefreshRate: Int
    ): GLFWVidMode {
        val videoModes = loadAvailableVideoModes(monitor)
        var chosen: GLFWVidMode? = null

        // Choose the video mode which matches the desired resolution and refresh rate.
        for (i in videoModes.indices.reversed()) {
            val mode = videoModes[i]
            if (mode.width() == desiredWidth && mode.height() == desiredHeight) {
                if (desiredRefreshRate >= 0) {
                    if (desiredRefreshRate == mode.refreshRate()) {
                        chosen = mode
                        break
                    }
                } else {
                    chosen = mode
                    break
                }
            }
        }

        // If no video mode got chosen, bind the best option.
        if (chosen == null) {
            chosen = chooseBestVideoMode(videoModes)
        }
        return chosen
    }

    @JvmStatic
    fun calculateDesktopVideoMode(monitor: Long): GLFWVidMode {
        return chooseBestVideoMode(loadAvailableVideoModes(monitor))
    }

    /**
     * Loads an array of GLFWVidMode objects, each representing a supported video mode of the specified monitor.
     *
     * @param monitor The monitor id for which video modes should ne loaded.
     * @return Array of loaded video modes.
     */
    @JvmStatic
    private fun loadAvailableVideoModes(monitor: Long): Array<GLFWVidMode> {
        val videoModesBuffer = GLFW.glfwGetVideoModes(monitor)
            ?: throw RuntimeException("Video modes could not be retrieved.")
        if (videoModesBuffer.capacity() == 0) {
            throw RuntimeException("There are no video modes available.")
        }

        // Parse buffer into an array.
        val videoModes = arrayOfNulls<GLFWVidMode>(videoModesBuffer.capacity())
        for (i in 0 until videoModesBuffer.capacity()) {
            val mode = videoModesBuffer[i]
            requireNotNull(mode)
            videoModes[i] = mode
        }
        return videoModes as Array<GLFWVidMode>
    }

    /**
     * Chooses the best video mode out of the supplied array of video modes.
     * This will be the one specifying the highest resolution and refresh rate.
     *
     * @param videoModes The video modes to choose from.
     * @return The chosen video mode.
     */
    @JvmStatic
    private fun chooseBestVideoMode(videoModes: Array<GLFWVidMode>): GLFWVidMode {
        return videoModes[videoModes.size - 1]
    }

}
