package de.colibriengine.graphics.window

interface WindowCallbacksInterface {

    // TODO: beforeCreate(builder: Builder)
    // TODO: afterCreate()

    /**
     * Gets called immediately before the destruction of this window begins.
     * Calling [AbstractWindow.release] inside this method is forbidden. As this would result in an infinite
     * loop.
     */
    fun beforeDestruction()

    /**
     * Gets called after this window got destroyed.
     * At this point, the [AbstractWindow.isCreated] will return `false`.
     */
    fun afterDestruction()

    /**
     * Gets called whenever an error regarding the windowing system (GLFW) occurs.
     *
     * @param error
     * An ID representing which error occurred.
     * @param description
     * A user friendly string representation (description) of the error.
     */
    fun errorCallback(error: Int, description: String)

    /**
     * @see GLFWWindowCloseCallback.invoke
     */
    fun closeCallback()

    /**
     * @see GLFWWindowFocusCallback.invoke
     */
    fun focusCallback(focused: Boolean)

    /**
     * @see GLFWWindowSizeCallback.invoke
     */
    fun sizeCallback(width: Int, height: Int)

    /**
     * @see GLFWWindowPosCallback.invoke
     */
    fun posCallback(xpos: Int, ypos: Int)

    /**
     * @see GLFWWindowIconifyCallback.invoke
     */
    fun iconifyCallback(iconified: Boolean)

    /**
     * @see GLFWWindowMaximizeCallback.invoke
     */
    fun maximizeCallback(maximized: Boolean)

    /**
     * @see GLFWWindowRefreshCallback.invoke
     */
    fun refreshCallback()

    /**
     * @see GLFWDropCallback.invoke
     */
    fun dropCallback(names: Array<String>)

    /**
     * @see GLFWFramebufferSizeCallback.invoke
     */
    fun framebufferSizeCallback(width: Int, height: Int)

    /**
     * @see GLFWWindowContentScaleCallback.invoke
     */
    fun contentScaleCallback(xScale: Float, yScale: Float)

    /**
     * @see GLFWKeyCallback.invoke
     */
    fun keyCallback(
        key: Int, scancode: Int, action: Int, mods: Int
    )

    /**
     * @see GLFWCharCallback.invoke
     */
    fun charCallback(codepoint: Int)

    /**
     * @see GLFWCharModsCallback.invoke
     */
    fun charModsCallBack(codepoint: Int, mods: Int)

    /**
     * @see GLFWCursorEnterCallback.invoke
     */
    fun cursorEnterCallback(entered: Boolean)

    /**
     * @see GLFWCursorPosCallback.invoke
     */
    fun cursorPosCallback(xpos: Double, ypos: Double)

    /**
     * @see GLFWMouseButtonCallback.invoke
     */
    fun mouseButtonCallback(button: Int, action: Int, mods: Int)

    /**
     * @see GLFWScrollCallback.invoke
     */
    fun scrollCallback(xoffset: Double, yoffset: Double)

}
