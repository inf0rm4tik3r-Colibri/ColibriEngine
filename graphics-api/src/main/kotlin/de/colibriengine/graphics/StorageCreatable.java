package de.colibriengine.graphics;

public interface StorageCreatable {
    
    /**
     * This error message will be thrown in an exceptions if the {@link StorageCreatable#ensureStorageNotCreated()}
     * method finds out that this objects storage is currently created.
     */
    String MSG_STORAGE_CREATED = "This \"%s\"'s storage is created but must not be.";
    
    /**
     * TThis error message will be thrown in an exceptions if the {@link StorageCreatable#ensureStorageCreated()}
     * method finds out that this objects storage is currently not created.
     */
    String MSG_STORAGE_NOT_CREATED = "This \"%s\"'s storage is not created but must be.";
    
    /**
     * Specifies whether the storage of this object is created.
     *
     * @param storageCreated
     *     true / false
     */
    void setStorageCreated(final boolean storageCreated);
    
    /**
     * @return Whether or not the storage of this object is currently created.
     */
    boolean isStorageCreated();
    
    /**
     * Ensures that this objects storage is created by calling {@link StorageCreatable#isStorageCreated()} and throwing
     * an exception if that call returned {@code false}.
     *
     * @throws IllegalStateException
     *     If this objects storage is not created.
     */
    default void ensureStorageCreated() throws IllegalStateException {
        if (!isStorageCreated()) {
            throw new IllegalStateException(String
                .format(MSG_STORAGE_NOT_CREATED, getClass().getSuperclass().getName()));
        }
    }
    
    /**
     * Ensures that this objects storage is not created by calling {@link StorageCreatable#isStorageCreated()} and
     * throwing an exception if that call returned {@code true}.
     *
     * @throws IllegalStateException
     *     If this objects storage is created.
     */
    default void ensureStorageNotCreated() throws IllegalStateException {
        if (isStorageCreated()) {
            throw new IllegalStateException(String.format(MSG_STORAGE_CREATED, getClass().getSuperclass().getName()));
        }
    }
    
}
