package de.colibriengine.graphics.model

import de.colibriengine.graphics.model.concrete.*
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

interface ModelFactory {

    val meshFactory: MeshFactory

    fun create(from: AbstractModelDefinition): Model

    fun coordinateSystem(
        start: Vec3fAccessor,
        min: Float, max: Float,
        subdivisionSmall: Float, subdivisionBig: Float
    ): CoordinateSystem

    fun circle(axisA: Vec3fAccessor, axisB: Vec3fAccessor, radius: Float, outerVertexCount: UInt): Circle

    fun quad(direction: QuadDirection, alignment: QuadAlignment): Quad
    fun quad(direction: QuadDirection, alignment: QuadAlignment, textureRepetitions: Float): Quad

}
