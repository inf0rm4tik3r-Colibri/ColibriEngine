package de.colibriengine.graphics.model

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer

class ModelNode(entity: Entity, model: Model) : AbstractSceneGraphNode(entity) {

    init {
        entity.add(RenderComponent(model))
    }

    override fun init() {
    }

    override fun update(timing: Timer.View) {
    }

    override fun destroy() {
    }

}
