package de.colibriengine.graphics.model

interface ModelManager {

    val modelFactory: ModelFactory

    /**
     * Returns the requested model completely loaded and ready for rendering in a Model object. Only the first
     * request call on a specific relativePath will load the model and its corresponding files (materials, etc.).
     * Every subsequent call will return a reference to the previously loaded model. The method only changes back to
     * actually loading the model specified by the relativePath parameter if it previously got removed from the manager.
     * <p>
     * The {@code ModelManager} is capable of loading the following model files:
     * <pre>
     *     -  .obj
     *     -  .bobj
     * </pre>
     *
     * @param relativePath Specify the model you want by handing a path relative to the working directory. Example:
     *     "models/myModel.obj"
     * @return The model object for the specified geometry file.
     * @throws ResourceNotFoundException if the specified relativePath does not point to a valid file.
     * @throws FileTypeUnsupportedException if there is no loader for the specified file type available.
     */
    fun requestModel(relativePath: String): Model

    // TODO: We might not want this to be publicly available..
    fun releaseAllModels()

}
