package de.colibriengine.graphics.model

import de.colibriengine.graphics.Buffer
import de.colibriengine.graphics.material.Material
import de.colibriengine.graphics.rendering.PrimitiveMode
import java.nio.ByteBuffer

interface Mesh {

    val primitiveMode: PrimitiveMode

    // val vao: VAO

    val indexBuffer: Buffer

    val dataBuffer: Buffer

    // val positionBuffer: Buffer
    // val normalBuffer: Buffer
    // val texCoordBuffer: Buffer
    // val colorBuffer: Buffer
    // val materialIndexBuffer: Buffer

    var indicesToDraw: Int

    var verticesToDraw: Int

    var material: Material?

    /** Renders this mesh. */
    fun render()

    fun release()

}
