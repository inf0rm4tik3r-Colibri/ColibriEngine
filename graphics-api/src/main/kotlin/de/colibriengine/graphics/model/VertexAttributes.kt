package de.colibriengine.graphics.model

import de.colibriengine.graphics.DataType
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec4f.Vec4f

class VertexAttributes(
    val set: List<Pair<String, VertexAttribute>>,
    val combinedAttributeSize: Int
) {
    val size: Int
        get() = set.size

    inline fun forEach(block: (String, VertexAttribute) -> Unit) {
        set.forEach { block.invoke(it.first, it.second) }
    }

    inline fun forEachIndexed(block: (Int, String, VertexAttribute) -> Unit) {
        set.forEachIndexed { index, it -> block.invoke(index, it.first, it.second) }
    }

    companion object {
        val POSITION = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
        }

        val POSITION_AND_NORMAL = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("normal", VertexAttribute(1, 3, DataType.FLOAT, Vec3f.BYTES))
        }

        val POSITION_NORMAL_AND_TEXCOORD = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("normal", VertexAttribute(1, 3, DataType.FLOAT, Vec3f.BYTES))
            add("texCoord", VertexAttribute(2, 2, DataType.FLOAT, Vec2f.BYTES))
        }

        val POSITION_NORMAL_AND_COLOR = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("normal", VertexAttribute(1, 3, DataType.FLOAT, Vec3f.BYTES))
            add("color", VertexAttribute(2, 4, DataType.FLOAT, Vec4f.BYTES))
        }

        val POSITION_AND_COLOR = buildVertexAttributes {
            add("position", VertexAttribute(0, 3, DataType.FLOAT, Vec3f.BYTES))
            add("color", VertexAttribute(1, 4, DataType.FLOAT, Vec4f.BYTES))
        }
    }
}

class VertexAttributeBuilder {

    val set: MutableList<Pair<String, VertexAttribute>> = mutableListOf()
    var combinedAttributeSize: Int = 0

    fun add(name: String, attribute: VertexAttribute) {
        set.add(Pair(name, attribute))
        combinedAttributeSize += attribute.size
    }

}

fun buildVertexAttributes(builderAction: VertexAttributeBuilder.() -> Unit): VertexAttributes {
    val builder = VertexAttributeBuilder()
    builder.builderAction()
    return VertexAttributes(builder.set, builder.combinedAttributeSize)
}
