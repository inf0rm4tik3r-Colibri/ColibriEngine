package de.colibriengine.graphics.lighting.shadow

import de.colibriengine.math.vector.vec2i.StdVec2i

/**
 * Stores the settings used to set up the generation of a variance shadow map.
 *
 * @since ColibriEngine 0.0.7.3
 */
class VarianceShadowMapSettings {
    var resolution = StdVec2i().set(1024, 1024)
    var blurTextureLookUpScale = 1.0f
    var varianceMaxBound = 0.00002f
    var earlyReturnReferenceDepthBias = 0.01f
    var probabilityMinBound = 0.4f
}
