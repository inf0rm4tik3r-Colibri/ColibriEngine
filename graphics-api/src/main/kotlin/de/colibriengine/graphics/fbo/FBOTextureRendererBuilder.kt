package de.colibriengine.graphics.fbo

import de.colibriengine.ecs.ECS
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.shader.Shader
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.objectpooling.Reusable
import de.colibriengine.util.IntList
import de.colibriengine.util.intListOf
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

inline fun <reified T : FBOTextureRenderer<T>> buildFBOTextureRenderer(
    meshFactory: MeshFactory,
    builderAction: FBOTextureRendererBuilder<T>.() -> Unit
): T {
    val builder = FBOTextureRendererBuilder<T>()
    builder.builderAction()
    return builder.build(meshFactory)
}

class FBOTextureRendererBuilder<T : FBOTextureRenderer<T>> : Reusable<FBOTextureRendererBuilder<T>>, KoinComponent {

    private val factory: FBOTextureRendererFactory<T> = get()

    var ecs: ECS? = null
    var srcFBO: FBO? = null
    val srcInputTextureIndices: IntList = intListOf()
    val srcSamplePos: Vec2f = StdVec2f().set(FBOTextureRenderer.DEFAULT_SRC_SAMPLE_POS)
    val srcSampleDimension: Vec2f = StdVec2f().set(FBOTextureRenderer.DEFAULT_SRC_SAMPLE_DIMENSION)
    var destFBO: FBO? = null
    val destOutputTextureIndices: IntList = intListOf()
    val destWritePos: Vec2f = StdVec2f().set(FBOTextureRenderer.DEFAULT_DEST_WRITE_POS)
    val destWriteDimension: Vec2f = StdVec2f().set(FBOTextureRenderer.DEFAULT_DEST_WRITE_DIMENSION)
    var shader: Shader? = null
    var shaderSetup: (T) -> Unit = {}

    override fun reset(): FBOTextureRendererBuilder<T> {
        srcFBO = null
        srcInputTextureIndices.clear()
        srcSamplePos.set(FBOTextureRenderer.DEFAULT_SRC_SAMPLE_POS)
        srcSampleDimension.set(FBOTextureRenderer.DEFAULT_SRC_SAMPLE_DIMENSION)
        destFBO = null
        destOutputTextureIndices.clear()
        destWritePos.set(FBOTextureRenderer.DEFAULT_DEST_WRITE_POS)
        destWriteDimension.set(FBOTextureRenderer.DEFAULT_DEST_WRITE_DIMENSION)
        shader = null
        shaderSetup = {}
        return this
    }

    fun build(meshFactory: MeshFactory): T {
        return factory.create(
            requireNotNull(ecs),
            meshFactory,
            srcFBO, srcInputTextureIndices, srcSamplePos, srcSampleDimension,
            destFBO, destOutputTextureIndices, destWritePos, destWriteDimension,
            requireNotNull(shader),
            shaderSetup
        )
    }

}
