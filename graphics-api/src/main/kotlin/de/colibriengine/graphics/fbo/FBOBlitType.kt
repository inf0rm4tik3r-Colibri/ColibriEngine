package de.colibriengine.graphics.fbo

enum class FBOBlitType {

    BLIT_NORMAL,
    BLIT_STRETCHED,
    BLIT_FILL

}
