package de.colibriengine.graphics.fbo

import de.colibriengine.graphics.Creatable
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.util.IntList

interface FBO : Creatable {

    val textures: List<Texture>

    /**
     * The depth texture. Only accessible when previously initialized. Access otherwise throws an
     * [IllegalStateException]
     */
    val depthTexture: Texture

    /**
     * The index of the depth texture. Only accessible when previously initialized. Access otherwise throws an
     * [IllegalStateException]
     */
    val depthTextureIndex: Int

    /** Binds this framebuffer so that it will be available for read operations. */
    fun bindForReading()

    fun bindTextures(textureIndices: IntList)

    /** Binds this framebuffer so that it will be available for write operations. */
    fun bindForDrawing()

    fun setDrawBuffers(textureIndex1: Int)
    fun setDrawBuffers(textureIndex1: Int, textureIndex2: Int)
    fun setDrawBuffers(textureIndex1: Int, textureIndex2: Int, textureIndex3: Int)
    fun setDrawBuffers(textureIndex1: Int, textureIndex2: Int, textureIndex3: Int, textureIndex4: Int)
    fun setDrawBuffers(textureIndices: IntList)

    // TODO: rename bindDefaultFBOForDrawing into unbind() or create unbind() that calls the static bindDefaultFBOForDrawing.
    // fun bindDefaultFBOForDrawing()

}
