package de.colibriengine.graphics.font

import de.colibriengine.graphics.model.MeshFactory

interface TextFactory {

    val meshFactory: MeshFactory

    fun create(mode: TextMode, size: UInt, font: BitmapFont): Text

}
