package de.colibriengine.graphics.font

import de.colibriengine.asset.ResourceName

interface FontManager {

    /**
     * Requests the font specified through {@code path}. Consider using {@link FontManagerImpl#requestFontByName} for
     * an easier alternative. This method returns the {@code BMFont} object immediately if it was already loaded. It
     * otherwise loads the font, caches it and returns it afterwards.
     *
     * @param resourceName The path under which the font can be found.
     * @return The {@link BMFont} object referenced by {@code relativePath}.
     * @throws ResourceNotFoundException see {@link BMFontLoader#load(URL)}
     * @throws UnsupportedResourceException see {@link BMFontLoader#load(URL)}
     */
    fun requestFont(resourceName: ResourceName): BitmapFont

    fun giveBack(font: BitmapFont)

    fun destroy()

}
