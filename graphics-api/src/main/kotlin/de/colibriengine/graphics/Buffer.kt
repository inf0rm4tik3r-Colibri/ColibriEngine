package de.colibriengine.graphics

import java.nio.ByteBuffer

interface Buffer : Creatable, Bindable, StorageCreatable {

    @Throws(IllegalStateException::class, UnsupportedOperationException::class)
    fun mapWriteOnly(): ByteBuffer

    fun unmap()

}
