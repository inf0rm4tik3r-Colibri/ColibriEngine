package de.colibriengine.graphics.shader

import de.colibriengine.math.matrix.mat4f.Mat4fAccessor

/** A shader which has a model-view-projection matrix. */
interface MVPShader : Shader {

    /** Sets the model-view-projection matrix of this shader to [mvpMatrix]. */
    fun setMVPMatrix(mvpMatrix: Mat4fAccessor)

}
