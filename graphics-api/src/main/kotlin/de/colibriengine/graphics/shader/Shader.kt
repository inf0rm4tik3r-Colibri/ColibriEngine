package de.colibriengine.graphics.shader

interface Shader {

    /** Activates this shader program. */
    fun bind()

}
