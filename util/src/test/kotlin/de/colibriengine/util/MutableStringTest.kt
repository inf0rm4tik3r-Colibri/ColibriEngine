package de.colibriengine.util

import org.junit.Test
import org.junit.jupiter.api.Assertions.*

internal class MutableStringTest {

    @Test
    fun feature() {
        val mutableString = MutableString(22u) { ' ' }
        mutableString.put("FrameTime: ")
        mutableString.putAtEnd("ms")
        mutableString.putAtEnd(3.14159f, 4, 2)
        assertEquals("FrameTime:    3.1416ms", mutableString.toString())
    }
/*
    @Test
    fun construct_fromSize_createsStringOfNullCharacters() {
        val mutableString = MutableString(2)
        assertEquals("\u0000\u0000", mutableString.toString())
    }

    @Test
    fun construct_fromSizeAndFill_createsStringRepeatingTheFillChar() {
        val mutableString = MutableString(5, 'f')
        assertEquals("fffff", mutableString.toString())
    }

    @Test
    fun construct_fromString_createsStringByCopyingTheGivenString() {
        val mutableString = MutableString("Hello")
        assertEquals("Hello", mutableString.toString())
    }

    @Test
    fun internal_givesAccessToInternalCharArray() {
        val mutableString = MutableString("Hello")
        assertEquals("Hello", String(mutableString.internal))
    }

    @Test
    fun update_withString_failIfStringIsTooLong() {
        val mutableString = MutableString(5)
        assertThrows(IllegalArgumentException::class.java) {
            mutableString.put("123456")
        }
    }

    @Test
    fun putAtEnd_() {
        val mutableString = MutableString(10, '_')
        mutableString.putAtEnd("ms", 0)

        assertEquals("Hello", mutableString.toString())
    }

    @Test
    fun putFloat_() {
        val mutableString = MutableString(10, '_')
        mutableString.put(13.14159f, 5, 2)

        assertEquals("Hello", mutableString.toString())
    }

    @Test
    fun putInt_() {
        val mutableString = MutableString(10, '_')
        mutableString.put(13, 2)

        assertEquals("Hello", mutableString.toString())
    }
*/
}
