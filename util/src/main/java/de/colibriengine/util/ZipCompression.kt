package de.colibriengine.util

import java.io.ByteArrayInputStream
import java.util.zip.DeflaterInputStream
import java.util.zip.InflaterInputStream

object ZipCompression {

    private const val INTERMEDIATE_READ_LENGTH = 4096

    fun compress(uncompressed: ByteArray): ByteArray =
        DeflaterInputStream(ByteArrayInputStream(uncompressed)).use { inputStream ->
            return inputStream.readAll(INTERMEDIATE_READ_LENGTH)
        }

    fun decompress(compressed: ByteArray): ByteArray =
        InflaterInputStream(ByteArrayInputStream(compressed)).use { inputStream ->
            return inputStream.readAll(INTERMEDIATE_READ_LENGTH)
        }

}
