package de.colibriengine.util

import de.colibriengine.reactive.BehaviorSubject

/** @since ColibriEngine 0.0.7.0 */
class ReactiveSystemMemoryReporter {

    val total: BehaviorSubject<Long> = BehaviorSubject(0L)
    val used: BehaviorSubject<Long> = BehaviorSubject(0L)
    var deltaUsed: BehaviorSubject<Long> = BehaviorSubject(0L)
    var free: BehaviorSubject<Long> = BehaviorSubject(0L)
    var deltaFree: BehaviorSubject<Long> = BehaviorSubject(0L)

    init {
        total.value = runtime.totalMemory()
        free.value = runtime.freeMemory()
        used.value = total.value - free.value
    }

    /**
     * Updates this objects memory information knowledge. If a change in memory usage is detected this objects
     * [SystemMemoryReporterCallback] will be called with the new memory usage information.
     */
    fun update() {
        val total = runtime.totalMemory()
        val free = runtime.freeMemory()

        if (total != this.total.value || free != this.free.value) {
            // The currently "used" memory.
            val used = total - free
            // Difference between the previously and currently "used" memory.
            val deltaUsed = used - this.used.value
            // The difference between the previously and currently "free" memory.
            val deltaFree = free - this.free.value

            this.total.value = total
            this.used.value = used
            this.deltaUsed.value = deltaUsed
            this.free.value = free
            this.deltaFree.value = deltaFree
        }
    }

    companion object {
        /** The JVM runtime. */
        private val runtime = Runtime.getRuntime()
    }

}
