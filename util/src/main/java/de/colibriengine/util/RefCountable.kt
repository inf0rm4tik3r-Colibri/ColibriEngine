package de.colibriengine.util

data class RefCountable<T>(
    val value: T,
    var refCount: UInt = 1u
) {

    fun getAndIncrement(): T {
        refCount++
        return value
    }

    operator fun inc(): RefCountable<T> {
        refCount++
        return this
    }

    operator fun dec(): RefCountable<T> {
        refCount--
        return this
    }

}
