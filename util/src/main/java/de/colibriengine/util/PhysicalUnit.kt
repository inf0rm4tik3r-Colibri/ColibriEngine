package de.colibriengine.util

/**
 * Defines names of different units.
 *
 * @since ColibriEngine 0.0.7.0
 */
enum class PhysicalUnit {

    // Stands for: "no unit"
    NONE,

    // Pixels
    PX,

    // Percentage
    PERCENT

}
