package de.colibriengine.util

@Suppress("UNCHECKED_CAST")
inline fun <reified T> Array<T>.copyAndAddItem(insert: T, atIndex: Int): Array<T> {
    val dest: Array<T?> = arrayOfNulls(this.size + 1)
    System.arraycopy(this, 0, dest, 0, atIndex)
    dest[atIndex] = insert
    System.arraycopy(this, atIndex, dest, atIndex + 1, this.size - atIndex)
    return dest as Array<T>
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T> Array<T>.copyAndRemoveItem(atIndex: Int): Array<T> {
    require(this.isNotEmpty()) { "Cannot remove from an empty array!" }
    val dest: Array<T?> = arrayOfNulls(this.size - 1)
    System.arraycopy(this, 0, dest, 0, atIndex)
    System.arraycopy(this, atIndex + 1, dest, atIndex, this.size - atIndex - 1)
    return dest as Array<T>
}
