@file:Suppress("RemoveRedundantQualifierName", "unused")

package de.colibriengine.util

import java.nio.ByteBuffer
import java.nio.ByteOrder

const val DEFAULT_OFFSET = 0
val DEFAULT_BYTE_ORDER: ByteOrder = ByteOrder.nativeOrder()

private val SINGLE_INT_BB = ByteBuffer.allocate(Integer.BYTES)

const val BYTES_PER_SHORT = java.lang.Short.BYTES
const val BYTES_PER_INT = java.lang.Integer.BYTES
const val BYTES_PER_LONG = java.lang.Long.BYTES
const val BYTES_PER_FLOAT = java.lang.Float.BYTES
const val BYTES_PER_DOUBLE = java.lang.Double.BYTES
const val BYTES_PER_CHARACTER = java.lang.Character.BYTES

private val SHORT_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_SHORT)
private val INT_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_INT)
private val LONG_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_LONG)
private val FLOAT_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_FLOAT)
private val DOUBLE_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_DOUBLE)
private val CHAR_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES_PER_CHARACTER)

private val AT_PARAMETER_MUST_BE_IN_RANGE_0_7 = "The \"at\" parameter must be in range [${0..7}]!"

fun bitsToUByte(bit0: Int, bit1: Int, bit2: Int, bit3: Int, bit4: Int, bit5: Int, bit6: Int, bit7: Int): UByte {
    require(bit0 in 0..1)
    require(bit1 in 0..1)
    require(bit2 in 0..1)
    require(bit3 in 0..1)
    require(bit4 in 0..1)
    require(bit5 in 0..1)
    require(bit6 in 0..1)
    require(bit7 in 0..1)

    var result: UByte = 0.toUByte()
    if (bit0 == 1) {
        result = (result + 128.toUByte()).toUByte() // or 0b10000000
    }
    if (bit1 == 1) {
        result = (result + 64.toUByte()).toUByte()
    }
    if (bit2 == 1) {
        result = (result + 32.toUByte()).toUByte()
    }
    if (bit3 == 1) {
        result = (result + 16.toUByte()).toUByte()
    }
    if (bit4 == 1) {
        result = (result + 8.toUByte()).toUByte()
    }
    if (bit5 == 1) {
        result = (result + 4.toUByte()).toUByte()
    }
    if (bit6 == 1) {
        result = (result + 2.toUByte()).toUByte()
    }
    if (bit7 == 1) {
        result = (result + 1.toUByte()).toUByte()
    }
    return result
}

fun UByte.bitFromRight(at: Int): Bit {
    require(at in 0..7) { AT_PARAMETER_MUST_BE_IN_RANGE_0_7 }
    return Bit((this.toInt() shr at) and 1)
}

@JvmName("bitFromRightBYTE")
fun Byte.bitFromRight(at: Int): Bit {
    require(at in 0..7) { AT_PARAMETER_MUST_BE_IN_RANGE_0_7 }
    return Bit((this.toInt() shr at) and 1)
}

fun UByte.bitFromLeft(at: Int): Bit {
    require(at in 0..7) { AT_PARAMETER_MUST_BE_IN_RANGE_0_7 }
    return Bit((this.toInt() shr (7 - at)) and 1)
}

/* ByteArray -> Short */
fun ByteArray.toShort(): Short = this.toShort(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toShort(byteOrder: ByteOrder): Short = this.toShort(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toShort(offset: Int): Short = this.toShort(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toShort(offset: Int, byteOrder: ByteOrder): Short {
    require(this.size - offset >= BYTES_PER_SHORT) { "Not enough data in $this using offset $offset." }
    SHORT_CONVERSION_BUFFER.clear()
    SHORT_CONVERSION_BUFFER.order(byteOrder)
    SHORT_CONVERSION_BUFFER.put(this, offset, BYTES_PER_SHORT)
    SHORT_CONVERSION_BUFFER.rewind()
    return SHORT_CONVERSION_BUFFER.short
}

/* ByteArray -> UShort */
fun ByteArray.toUShort(): UShort = this.toUShort(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toUShort(byteOrder: ByteOrder): UShort = this.toUShort(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toUShort(offset: Int): UShort = this.toUShort(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toUShort(offset: Int, byteOrder: ByteOrder): UShort = this.toShort(offset, byteOrder).toUShort()


/* ByteArray -> Int */
fun ByteArray.toInt(): Int = this.toInt(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toInt(byteOrder: ByteOrder): Int = this.toInt(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toInt(offset: Int): Int = this.toInt(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toInt(offset: Int, byteOrder: ByteOrder): Int {
    require(this.size - offset >= BYTES_PER_INT) { "Not enough data in $this using offset $offset." }
    INT_CONVERSION_BUFFER.clear()
    INT_CONVERSION_BUFFER.order(byteOrder)
    INT_CONVERSION_BUFFER.put(this, offset, BYTES_PER_INT)
    INT_CONVERSION_BUFFER.rewind()
    return INT_CONVERSION_BUFFER.int
}

/* ByteArray -> UInt */
fun ByteArray.toUInt(): UInt = this.toUInt(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toUInt(byteOrder: ByteOrder): UInt = this.toUInt(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toUInt(offset: Int): UInt = this.toUInt(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toUInt(offset: Int, byteOrder: ByteOrder): UInt = this.toInt(offset, byteOrder).toUInt()


/* ByteArray -> Long */
fun ByteArray.toLong(): Long = this.toLong(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toLong(byteOrder: ByteOrder): Long = this.toLong(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toLong(offset: Int): Long = this.toLong(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toLong(offset: Int, byteOrder: ByteOrder): Long {
    require(this.size - offset >= BYTES_PER_LONG) { "Not enough data in $this using offset $offset." }
    LONG_CONVERSION_BUFFER.clear()
    LONG_CONVERSION_BUFFER.order(byteOrder)
    LONG_CONVERSION_BUFFER.put(this, offset, BYTES_PER_LONG)
    LONG_CONVERSION_BUFFER.rewind()
    return LONG_CONVERSION_BUFFER.long
}

/* ByteArray -> ULong */
fun ByteArray.toULong(): ULong = this.toULong(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toULong(byteOrder: ByteOrder): ULong = this.toULong(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toULong(offset: Int): ULong = this.toULong(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toULong(offset: Int, byteOrder: ByteOrder): ULong = this.toLong(offset, byteOrder).toULong()


/* ByteArray -> Float */
fun ByteArray.toFloat(): Float = this.toFloat(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toFloat(byteOrder: ByteOrder): Float = this.toFloat(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toFloat(offset: Int): Float = this.toFloat(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toFloat(offset: Int, byteOrder: ByteOrder): Float {
    require(this.size - offset >= BYTES_PER_FLOAT) { "Not enough data in $this using offset $offset." }
    FLOAT_CONVERSION_BUFFER.clear()
    FLOAT_CONVERSION_BUFFER.order(byteOrder)
    FLOAT_CONVERSION_BUFFER.put(this, offset, BYTES_PER_FLOAT)
    FLOAT_CONVERSION_BUFFER.rewind()
    return FLOAT_CONVERSION_BUFFER.float
}

/* ByteArray -> Float */
fun ByteArray.toDouble(): Double = this.toDouble(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toDouble(byteOrder: ByteOrder): Double = this.toDouble(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toDouble(offset: Int): Double = this.toDouble(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toDouble(offset: Int, byteOrder: ByteOrder): Double {
    require(this.size - offset >= BYTES_PER_DOUBLE) { "Not enough data in $this using offset $offset." }
    DOUBLE_CONVERSION_BUFFER.clear()
    DOUBLE_CONVERSION_BUFFER.order(byteOrder)
    DOUBLE_CONVERSION_BUFFER.put(this, offset, BYTES_PER_DOUBLE)
    DOUBLE_CONVERSION_BUFFER.rewind()
    return DOUBLE_CONVERSION_BUFFER.double
}


/* ByteArray -> Char */
fun ByteArray.toChar(): Char = this.toChar(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toChar(byteOrder: ByteOrder): Char = this.toChar(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toChar(offset: Int): Char = this.toChar(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toChar(offset: Int, byteOrder: ByteOrder): Char {
    require(this.size - offset >= BYTES_PER_CHARACTER) { "Not enough data in $this using offset $offset." }
    CHAR_CONVERSION_BUFFER.clear()
    CHAR_CONVERSION_BUFFER.order(byteOrder)
    CHAR_CONVERSION_BUFFER.put(this, offset, BYTES_PER_CHARACTER)
    CHAR_CONVERSION_BUFFER.rewind()
    return CHAR_CONVERSION_BUFFER.char
}


/* ByteArray -> BooleanArray */
fun ByteArray.toBooleanArray(): BooleanArray = this.toBooleanArray(DEFAULT_OFFSET)
fun ByteArray.toBooleanArray(offset: Int): BooleanArray {
    val array = BooleanArray(size - offset)
    for ((index, i) in (offset until size).withIndex()) {
        // Note: data[i] == 0 => false
        //       data[i] != 0 => true
        array[index] = this[i] != 0.toByte()
    }
    return array
}


/* ByteArray -> ShortArray */
fun ByteArray.toShortArray(): ShortArray = this.toShortArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toShortArray(byteOrder: ByteOrder): ShortArray = this.toShortArray(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toShortArray(offset: Int): ShortArray = this.toShortArray(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toShortArray(offset: Int, byteOrder: ByteOrder): ShortArray {
    require((size - offset) % BYTES_PER_SHORT == 0) { "Not enough data in $this using offset $offset." }
    val shortArray = ShortArray((size - offset) / BYTES_PER_SHORT)
    for ((index, i) in (offset until size step BYTES_PER_SHORT).withIndex()) {
        shortArray[index] = toShort(i, byteOrder)
    }
    return shortArray
}

/* ByteArray -> UShortArray */
@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUShortArray(): UShortArray = this.toUShortArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUShortArray(byteOrder: ByteOrder): UShortArray = this.toUShortArray(DEFAULT_OFFSET, byteOrder)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUShortArray(offset: Int): UShortArray = this.toUShortArray(offset, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUShortArray(offset: Int, byteOrder: ByteOrder): UShortArray {
    require((size - offset) % BYTES_PER_SHORT == 0) { "Not enough data in $this using offset $offset." }
    val uShortArray = UShortArray((size - offset) / BYTES_PER_SHORT)
    for ((index, i) in (offset until size step BYTES_PER_SHORT).withIndex()) {
        uShortArray[index] = toUShort(i, byteOrder)
    }
    return uShortArray
}


/* ByteArray -> IntArray */
fun ByteArray.toIntArray(): IntArray = this.toIntArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toIntArray(byteOrder: ByteOrder): IntArray = this.toIntArray(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toIntArray(offset: Int): IntArray = this.toIntArray(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toIntArray(offset: Int, byteOrder: ByteOrder): IntArray {
    /*
    require((this.size - offset) % BYTES_PER_INT == 0) { "Not enough data in $this using offset $offset." }
    val intBuffer = ByteBuffer.wrap(this).order(byteOrder).asIntBuffer()
    val intArray = IntArray(intBuffer.remaining())
    intBuffer.get(intArray)
    return intArray
    */
    require((size - offset) % BYTES_PER_INT == 0) { "Not enough data in $this using offset $offset." }
    val intArray = IntArray((size - offset) / BYTES_PER_INT)
    for ((index, i) in (offset until size step BYTES_PER_INT).withIndex()) {
        intArray[index] = toInt(i, byteOrder)
    }
    return intArray
}

/* ByteArray -> UIntArray */
@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUIntArray(): UIntArray = this.toUIntArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUIntArray(byteOrder: ByteOrder): UIntArray = this.toUIntArray(DEFAULT_OFFSET, byteOrder)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUIntArray(offset: Int): UIntArray = this.toUIntArray(offset, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toUIntArray(offset: Int, byteOrder: ByteOrder): UIntArray {
    require((size - offset) % BYTES_PER_INT == 0) { "Not enough data in $this using offset $offset." }
    val uIntArray = UIntArray((size - offset) / BYTES_PER_INT)
    for ((index, i) in (offset until size step BYTES_PER_INT).withIndex()) {
        uIntArray[index] = toUInt(i, byteOrder)
    }
    return uIntArray
}


/* ByteArray -> LongArray */
fun ByteArray.toLongArray(): LongArray = this.toLongArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toLongArray(byteOrder: ByteOrder): LongArray = this.toLongArray(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toLongArray(offset: Int): LongArray = this.toLongArray(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toLongArray(offset: Int, byteOrder: ByteOrder): LongArray {
    require((size - offset) % BYTES_PER_LONG == 0) { "Not enough data in $this using offset $offset." }
    val longArray = LongArray((size - offset) / BYTES_PER_LONG)
    for ((index, i) in (offset until size step BYTES_PER_LONG).withIndex()) {
        longArray[index] = toLong(i, byteOrder)
    }
    return longArray
}

/* ByteArray -> UIntArray */
@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toULongArray(): ULongArray = this.toULongArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toULongArray(byteOrder: ByteOrder): ULongArray = this.toULongArray(DEFAULT_OFFSET, byteOrder)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toULongArray(offset: Int): ULongArray = this.toULongArray(offset, DEFAULT_BYTE_ORDER)

@OptIn(ExperimentalUnsignedTypes::class)
fun ByteArray.toULongArray(offset: Int, byteOrder: ByteOrder): ULongArray {
    require((size - offset) % BYTES_PER_LONG == 0) { "Not enough data in $this using offset $offset." }
    val uLongArray = ULongArray((size - offset) / BYTES_PER_LONG)
    for ((index, i) in (offset until size step BYTES_PER_LONG).withIndex()) {
        uLongArray[index] = toULong(i, byteOrder)
    }
    return uLongArray
}


/* ByteArray -> FloatArray */
fun ByteArray.toFloatArray(): FloatArray = this.toFloatArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toFloatArray(byteOrder: ByteOrder): FloatArray = this.toFloatArray(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toFloatArray(offset: Int): FloatArray = this.toFloatArray(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toFloatArray(offset: Int, byteOrder: ByteOrder): FloatArray {
    require((size - offset) % BYTES_PER_FLOAT == 0) { "Not enough data in $this using offset $offset." }
    val floatArray = FloatArray((size - offset) / BYTES_PER_FLOAT)
    for ((index, i) in (offset until size step BYTES_PER_FLOAT).withIndex()) {
        floatArray[index] = toFloat(i, byteOrder)
    }
    return floatArray
}


/* ByteArray -> DoubleArray */
fun ByteArray.toDoubleArray(): DoubleArray = this.toDoubleArray(DEFAULT_OFFSET, DEFAULT_BYTE_ORDER)
fun ByteArray.toDoubleArray(byteOrder: ByteOrder): DoubleArray = this.toDoubleArray(DEFAULT_OFFSET, byteOrder)
fun ByteArray.toDoubleArray(offset: Int): DoubleArray = this.toDoubleArray(offset, DEFAULT_BYTE_ORDER)
fun ByteArray.toDoubleArray(offset: Int, byteOrder: ByteOrder): DoubleArray {
    require((size - offset) % BYTES_PER_DOUBLE == 0) { "Not enough data in $this using offset $offset." }
    val doubleArray = DoubleArray((size - offset) / BYTES_PER_DOUBLE)
    for ((index, i) in (offset until size step BYTES_PER_DOUBLE).withIndex()) {
        doubleArray[index] = toDouble(i, byteOrder)
    }
    return doubleArray
}
