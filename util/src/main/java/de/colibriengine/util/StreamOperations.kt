@file:Suppress("unused")

package de.colibriengine.util

import de.colibriengine.exception.EndOfStreamException
import java.io.IOException
import java.io.InputStream
import java.nio.ByteOrder
import java.nio.charset.Charset

/**
 * Reads the next unsigned byte from this stream.
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readUByte(): UByte {
    val read: Int = read()
    if (read == -1) {
        throw EndOfStreamException()
    }
    return read.toUByte()
}

/**
 * Reads the next signed byte from this stream.
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readByte(): Byte {
    return readUByte().toByte()
}

/**
 * Reads the next byte from this stream and returns it as a [Bitfield].
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readByteAsBitField(): Bitfield {
    return Bitfield(readUByte())
}

/**
 * Reads the next two bytes as an unsigned short ([UShort]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readUShort(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): UShort {
    val byteArray = ByteArray(BYTES_PER_SHORT)
    val readResult = read(byteArray, 0, BYTES_PER_SHORT)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toUShort(byteOrder)
}

fun InputStream.readShort(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Short {
    val byteArray = ByteArray(BYTES_PER_SHORT)
    val readResult = read(byteArray, 0, BYTES_PER_SHORT)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toShort(byteOrder)
}

/**
 * Reads the next four bytes as a signed integer ([Int]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readInt(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Int {
    val byteArray = ByteArray(BYTES_PER_INT)
    val readResult = read(byteArray, 0, BYTES_PER_INT)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toInt(byteOrder)
}

/**
 * Reads the next four bytes as an unsigned integer ([UInt]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readUInt(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): UInt {
    val byteArray = ByteArray(BYTES_PER_INT)
    val readResult = read(byteArray, 0, BYTES_PER_INT)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toUInt(byteOrder)
}

/**
 * Reads the next eight bytes as a signed long ([Long]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readLong(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Long {
    val byteArray = ByteArray(BYTES_PER_LONG)
    val readResult = read(byteArray, 0, BYTES_PER_LONG)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toLong(byteOrder)
}

/**
 * Reads the next four bytes as an unsigned long ([ULong]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readULong(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): ULong {
    val byteArray = ByteArray(BYTES_PER_LONG)
    val readResult = read(byteArray, 0, BYTES_PER_LONG)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toULong(byteOrder)
}

/**
 * Reads the next four bytes as a float ([Float]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readFloat(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Float {
    val byteArray = ByteArray(BYTES_PER_FLOAT)
    val readResult = read(byteArray, 0, BYTES_PER_FLOAT)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toFloat(byteOrder)
}

/**
 * Reads the next eight bytes as a double ([Double]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readDouble(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Double {
    val byteArray = ByteArray(BYTES_PER_DOUBLE)
    val readResult = read(byteArray, 0, BYTES_PER_DOUBLE)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toDouble(byteOrder)
}

/**
 * Reads the next byte as an 8-bit character and returns it as a ([Char]) (ASCII interpreted).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.read1ByteAsChar(): Char {
    val readResult = read()
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return readResult.toChar()
}

/**
 * Reads the next 2 bytes as a 16-bit unicode character ([Char]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.read2BytesAsChar(byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): Char {
    val byteArray = ByteArray(BYTES_PER_CHARACTER)
    val readResult = read(byteArray, 0, BYTES_PER_CHARACTER)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toChar(byteOrder)
}

/**
 * Reads all bytes up to the next 0-Byte and returns the bytes read as a [String]. Every byte read makes one character
 * in the resulting string. If a string using a specific character set (see: [Charsets]) should be read, use the read
 * function taking in a specific length to read.
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readNullTerminatedString(): String = buildString {
    var result: Int
    while ((read().also { result = it }) != 0) {
        if (result == -1) {
            throw EndOfStreamException()
        }
        append(result.toChar())
    }
}

/** Reads [length] bytes from this stream and interprets them as a string using the given [charset]. */
fun InputStream.readString(length: Int, charset: Charset): String {
    val chars = ByteArray(length)
    val readResult = read(chars, 0, length)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return String(chars, charset)
}

/**
 * Reads [length] bytes and interprets each byte as a boolean (0=false, else true) returning a ([BooleanArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readBooleanArray(length: Int): BooleanArray {
    val byteArray = ByteArray(length)
    val readResult = read(byteArray, 0, length)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toBooleanArray()
}

fun InputStream.readBooleanArray(length: UInt): BooleanArray {
    return readBooleanArray(length.toInt())
}

/**
 * Reads [length] signed bytes in a [ByteArray].
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readByteArray(length: Int): ByteArray {
    val byteArray = ByteArray(length)
    val readResult = read(byteArray, 0, length)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray
}

fun InputStream.readByteArray(length: UInt): ByteArray {
    return readByteArray(length.toInt())
}

/**
 * Reads [length] signed bytes in a [ByteArray].
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
@OptIn(ExperimentalUnsignedTypes::class)
fun InputStream.readUByteArray(length: UInt): UByteArray {
    val readLength = length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toUByteArray()
}

/**
 * Reads [length] * 2 bytes and interprets each block of 2 bytes as a signed short, returning a ([ShortArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readShortArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): ShortArray {
    val readLength = BYTES_PER_INT * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toShortArray(byteOrder)
}

/**
 * Reads [length] * 2 bytes and interprets each block of 2 bytes as an unsigned short, returning a ([UShortArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
@OptIn(ExperimentalUnsignedTypes::class)
fun InputStream.readUShortArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): UShortArray {
    val readLength = BYTES_PER_SHORT * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toUShortArray(byteOrder)
}

/**
 * Reads [length] * 4 bytes and interprets each block of 4 bytes as a signed integer, returning an ([IntArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readIntArray(length: Int, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): IntArray {
    val readLength = BYTES_PER_INT * length
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toIntArray(byteOrder)
}

fun InputStream.readIntArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): IntArray {
    return readIntArray(length.toInt(), byteOrder)
}


/**
 * Reads [length] * 4 bytes and interprets each block of 4 bytes as an unsigned integer, returning a ([UIntArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
@OptIn(ExperimentalUnsignedTypes::class)
fun InputStream.readUIntArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): UIntArray {
    val readLength = BYTES_PER_INT * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toUIntArray(byteOrder)
}

/**
 * Reads [length] * 8 bytes and interprets each block of 8 bytes as a signed long, returning a ([LongArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readLongArray(length: Int, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): LongArray {
    val readLength = BYTES_PER_LONG * length
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toLongArray(byteOrder)
}

fun InputStream.readLongArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): LongArray {
    return readLongArray(length.toInt(), byteOrder)
}

/**
 * Reads [length] * 8 bytes and interprets each block of 8 bytes as an unsigned long, returning a ([ULongArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
@OptIn(ExperimentalUnsignedTypes::class)
fun InputStream.readULongArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): ULongArray {
    val readLength = BYTES_PER_LONG * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toULongArray(byteOrder)
}

/**
 * Reads [length] * 4 bytes and interprets each block of 4 bytes as a float, returning a ([FloatArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readFloatArray(length: Int, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): FloatArray {
    val readLength = BYTES_PER_FLOAT * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toFloatArray(byteOrder)
}

fun InputStream.readFloatArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): FloatArray {
    return readFloatArray(length.toInt(), byteOrder)
}

/**
 * Reads [length] * 8 bytes and interprets each block of 8 bytes as a double, returning a ([DoubleArray]).
 *
 * @throws EndOfStreamException If the end of the stream is reached.
 * @throws IOException If an I/O error occurs.
 */
fun InputStream.readDoubleArray(length: Int, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): DoubleArray {
    val readLength = BYTES_PER_DOUBLE * length.toInt()
    val byteArray = ByteArray(readLength)
    val readResult = read(byteArray, 0, readLength)
    if (readResult == -1) {
        throw EndOfStreamException()
    }
    return byteArray.toDoubleArray(byteOrder)
}

fun InputStream.readDoubleArray(length: UInt, byteOrder: ByteOrder = DEFAULT_BYTE_ORDER): DoubleArray {
    return readDoubleArray(length.toInt(), byteOrder)
}

fun InputStream.readAll(chunkSize: Int = 4096): ByteArray {
    val arrays = mutableListOf<ByteArray>()
    readAll(chunkSize, true) { readBuffer: ByteArray, bytesRead: Int ->
        if (readBuffer.size == bytesRead) {
            // Just use the read data if the readBuffer is fully defined.
            arrays.add(readBuffer)
        } else {
            // If the readBuffer is only partially defined, which happens when the last chunk of data is read,
            // copy the area read.
            arrays.add(ArrayHelper.copy(readBuffer, 0, bytesRead))
        }
    }
    return ArrayHelper.combineAllByte(arrays)
}

inline fun InputStream.readAll(
    chunkSize: Int,
    inSeparateResultArrays: Boolean,
    onChunk: (ByteArray, Int) -> Unit
) {
    // Stores the result of the read operations. Either holds -1 if the end of the stream was reached or the amount
    // of bytes actually read by the last read operation.
    var bytesRead: Int
    // This array is used to store the intermediate read results.
    var buffer = ByteArray(chunkSize)

    // Try to read buffer.length bytes from the input stream.
    // If less than buffer.length bytes are available, the buffer array will be filled partially with bytesRead bytes.
    while (read(buffer).also { bytesRead = it } != -1) {
        onChunk.invoke(buffer, bytesRead)
        if (inSeparateResultArrays) {
            buffer = ByteArray(chunkSize)
        }
    }
}
