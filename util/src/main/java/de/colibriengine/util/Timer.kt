package de.colibriengine.util

import org.lwjgl.glfw.GLFW.glfwGetTime

const val MS_IN_SECOND = 1000.0f

class Timer {

    private var current: Double = 0.0
    private var last: Double = 0.0
    private var delta: Double = 0.0
    private var maxDelta: Double = 0.0
    private var elapsed: Double = 0.0
    private var ticks: Long = 0L

    /**
     * The data view for this timer. Unsafe objects should only be granted access to a timers [ ] instance, as access to
     * the timer itself can lead to serious problems when used incorrectly.
     */
    @JvmField
    val view: View = View()

    init {
        reset()
    }

    fun reset() {
        current = glfwGetTime()
        last = current
        elapsed = 0.0
        maxDelta = 0.0
        delta = 0.0
        ticks = 0L
    }

    fun tick() {
        current = glfwGetTime()
        delta = current - last
        if (delta > maxDelta) {
            maxDelta = delta
        }
        elapsed += delta
        last = current
        ticks++ // Might overflow at some point!
    }

    // TODO: last = current = glfwGetTIme() in every reset method??
    fun resetElapsedTime() {
        elapsed = 0.0
    }

    fun resetTicks() {
        ticks = 0
    }

    fun resetMaxDelta() {
        maxDelta = 0.0
    }

    /** Simple "access only" view on this timers data. */
    inner class View {

        /** The past time since the last invocation of the timers [Timer.tick] method. Double precision. */
        fun delta(): Double = delta

        /** The past time since the last invocation of the timers [Timer.tick] method. Single precision. */
        fun deltaSP(): Float = delta.toFloat()

        /** The past time since the last invocation of the timers [Timer.tick] method. Single precision. */
        fun deltaInSeconds(): Double = delta * 1000.0

        /** The past time since the last invocation of the timers [Timer.reset] method. Double precision. */
        fun elapsedTime(): Double = elapsed

        /** The past time since the last invocation of the timers [Timer.reset] method. Single precision. */
        fun elapsedTimeSP(): Float = elapsed.toFloat()

        /** How often the timer [Timer.tick]'ed since its last reset. */
        fun ticks(): Long = ticks

        fun averageDeltaInMilliseconds(): Double = MS_IN_SECOND / ticks.toDouble()

        /** The maximum of all [Timer.delta] values computed since the last reset of this timer. */
        fun maxDelta(): Double = maxDelta

        /** The maximum of all [Timer.delta] values computed since the last reset of this timer. */
        fun maxDeltaInMilliseconds(): Double = maxDelta * 1000.0
    }

}
