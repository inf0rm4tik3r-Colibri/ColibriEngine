package de.colibriengine.reactive

class BehaviorSubject<T>(initialValue: T) : Subject<T>() {

    var value: T = initialValue
        set(value) {
            field = value
            subscribers.forEach { it.next(value) }
        }

    override fun subscribe(next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Subscription<T> {
        val subscription = super.subscribe(next, error, complete)
        subscription.next(value)
        return subscription
    }

}
