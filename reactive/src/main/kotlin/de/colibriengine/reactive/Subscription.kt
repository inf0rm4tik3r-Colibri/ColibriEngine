package de.colibriengine.reactive

class Subscription<T>(
    private val subject: Subject<T>,
    val next: (T) -> Unit,
    val error: (Throwable) -> Unit,
    val complete: () -> Unit
) {

    /** Destroys this subscription, making it unknown to the source it received updates from. */
    fun unsubscribe() {
        subject.unsubscribe(this)
    }

}
