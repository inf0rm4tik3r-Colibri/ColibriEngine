package de.colibriengine.ecs

import de.colibriengine.datastructures.graph.Edge
import de.colibriengine.datastructures.graph.Graph
import de.colibriengine.datastructures.graph.Node

enum class NodeRelation {
    SUBTYPE_OF, SUPERTYPE_OF
}

class EdgeWithNodeRelation<T>(
    start: Node<T>,
    val nodeRelation: NodeRelation,
    end: Node<T>
) : Edge<T>(start, end) {

    override fun toSimpleString(): String = "(${start.data},$nodeRelation,${end.data})"

    override fun toString(): String =
        "EdgeWithNodeRelation(\n      start=$start,\n      nodeRelation=$nodeRelation,\n      end=$end\n    )"

}

class ArchetypeGraphWithNodeRelation : Graph<Archetype, EdgeWithNodeRelation<Archetype>>() {

    /**
     * Creates a new node for the given archetype and connects that node with all other nodes, which either represent a
     * subtype or supertype of the given archetype.
     */
    fun ensureArchetypeExists(archetype: Archetype) {
        if (find(archetype) == null) {
            val newNode = addNode(archetype)
            nodes.forEach {
                if (it != newNode) {
                    when {
                        // The new archetype could be a supertype...
                        it.data.isSupertypeOf(archetype) -> {
                            it.makeSupertypeOf(newNode)
                        }
                        // a subtype...
                        it.data.isSubtypeOf(archetype) -> {
                            it.makeSubtypeOf(newNode)
                        }
                        // or be in no relation whatsoever.
                        else -> {
                        }
                    }
                }
            }
        }
    }

    /**
     * Removes the given [archetype] and all its connections.
     */
    fun removeArchetype(archetype: Archetype) {
        removeNode(find(archetype)!!)
    }

    private fun Node<Archetype>.makeSupertypeOf(node: Node<Archetype>) {
        addEdge(this, node) { a, b -> EdgeWithNodeRelation(a, NodeRelation.SUPERTYPE_OF, b) }
        addEdge(node, this) { a, b -> EdgeWithNodeRelation(a, NodeRelation.SUBTYPE_OF, b) }
    }

    private fun Node<Archetype>.makeSubtypeOf(node: Node<Archetype>) {
        addEdge(this, node) { a, b -> EdgeWithNodeRelation(a, NodeRelation.SUBTYPE_OF, b) }
        addEdge(node, this) { a, b -> EdgeWithNodeRelation(a, NodeRelation.SUPERTYPE_OF, b) }
    }

    fun traverseSupertypes(of: Archetype, action: (Archetype) -> Unit) {
        val startingAtNode = find(of) ?: throw NullPointerException("Node with archetype $of could not be found!")
        traverse(startingAtNode, { edge -> edge.nodeRelation == NodeRelation.SUBTYPE_OF }, action)
    }

}
