package de.colibriengine.components

import de.colibriengine.ecs.Component
import de.colibriengine.math.matrix.mat4f.StdMat4f
import de.colibriengine.math.quaternion.quaternionF.ChangeDetectableQuaternionF
import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionF
import de.colibriengine.math.quaternion.quaternionF.StdQuaternionFFactory.Companion.IDENTITY
import de.colibriengine.math.vector.vec3f.ChangeDetectableVec3f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.ONE
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.ZERO
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

private val INITIAL_TRANSLATION = ZERO
private val INITIAL_ROTATION = IDENTITY
private val INITIAL_SCALE = ONE

/**
 * A new transform instance initialized with the given [translation], [rotation] and [scale] or a translation of {0, 0,
 * 0}, the default rotation and a scale of {1, 1, 1} for non specified arguments.
 *
 * @since ColibriEngine 0.0.6.4
 */
data class TransformComponent @JvmOverloads constructor(
    val translation: ChangeDetectableVec3f = ChangeDetectableVec3f(StdVec3f().set(INITIAL_TRANSLATION)),
    val rotation: ChangeDetectableQuaternionF = ChangeDetectableQuaternionF(StdQuaternionF().set(INITIAL_ROTATION)),
    val scale: ChangeDetectableVec3f = ChangeDetectableVec3f(StdVec3f().set(INITIAL_SCALE))
) : Component {
    private val translationMatrix: StdMat4f = StdMat4f()
    private val rotationMatrix: StdMat4f = StdMat4f()
    private val scalingMatrix: StdMat4f = StdMat4f()
    private val transformation: StdMat4f = StdMat4f()
    private val tmpQuaternion: QuaternionF = StdQuaternionF()

    init {
        initChangedState(true)
        calculateTransformation()
    }

    /** Resets the translation to (0, 0, 0), the rotation to (0, 0, 0, 1) and the scale to (1, 1, 1). */
    fun initIdentity() {
        translation.set(INITIAL_TRANSLATION)
        rotation.set(INITIAL_ROTATION)
        scale.set(INITIAL_SCALE)
    }

    private fun initChangedState(changed: Boolean) {
        translation.changed = changed
        rotation.changed = changed
        scale.changed = changed
    }

    private fun wasChanged(): Boolean {
        return translation.changed || rotation.changed || scale.changed
    }

    /**
     * Calculates the combined transformation matrix of this [TransformComponent] object by combining the defined translation,
     * rotation and scale. This function returns immediately if the properties of this transform (translation, rotation,
     * scale) did not change since the last call to this method.
     */
    fun calculateTransformation() {
        if (!wasChanged()) return

        if (translation.changed) translationMatrix.initTranslation(translation)
        if (rotation.changed) rotationMatrix.initRotation(rotation)
        if (scale.changed) scalingMatrix.initScale(scale)

        transformation.set(translationMatrix).times(rotationMatrix).times(scalingMatrix)

        // Updated to the transformation were taken into account. We therefore set all *Changed variables to false.
        initChangedState(false)
    }

    fun getTransformation(): StdMat4f {
        if (wasChanged()) {
            throw RuntimeException(
                "This transform object was changed. The transformation matrix must be calculated first by calling " +
                    "calculateTransformation() on this object before retrieving the transformation."
            )
        }
        return transformation
    }

    fun setLike(other: TransformComponent): TransformComponent {
        translation.set(other.translation)
        rotation.set(other.rotation)
        scale.set(other.scale)
        return this
    }

    fun setTranslation(onX: Float, onY: Float, onZ: Float): TransformComponent {
        translation.set(onX, onY, onZ)
        return this
    }

    fun rotate(axis: Vec3fAccessor, angle: Float): TransformComponent {
        rotate(tmpQuaternion.initRotation(axis, angle))
        return this
    }

    fun rotate(by: QuaternionFAccessor): TransformComponent {
        rotation.times(by).normalize()
        return this
    }

    fun rotate(towards: QuaternionFAccessor, step: Float): TransformComponent {
        // Slower rotation at the end, because the angle between this and the given rotation shrinks over time..
        rotation.slerp(towards, step)
        return this
    }

}
