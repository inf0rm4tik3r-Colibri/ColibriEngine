package de.colibriengine.components

import de.colibriengine.ecs.Component
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.matrix.mat4f.StdMat4f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4f.StdVec4f
import de.colibriengine.math.vector.vec4f.Vec4f

class TransformCalculationComponent(private val transform: TransformComponent) : Component {

    private val _orthographicTransformation: StdMat4f = StdMat4f()
    val orthographicTransformation: Mat4fAccessor = _orthographicTransformation

    private val _perspectiveTransformation: StdMat4f = StdMat4f()
    val perspectiveTransformation: Mat4fAccessor = _perspectiveTransformation

    private val tmpVec3f: Vec3f = StdVec3f()
    private val tmpVec4f: Vec4f = StdVec4f()

    fun modifiableOrthographicTransformation(): StdMat4f = _orthographicTransformation
    fun modifiablePerspectiveTransformation(): StdMat4f = _perspectiveTransformation

    // First three values of the last column.
    fun position(storeIn: Vec3f): Vec3f = storeIn.set(
        perspectiveTransformation.m03,
        perspectiveTransformation.m13,
        perspectiveTransformation.m23
    )

    private fun getNormalizedProjectedVector(toTransform: Vec3fAccessor, storeIn: Vec3f): Vec3f {
        return perspectiveTransformation.transform(tmpVec4f.set(toTransform, 0.0f))
            .xyz(storeIn).normalize()
    }

    fun forward(storeIn: Vec3f): Vec3f {
        // TODO: check if the result of this function is the same as creating a quaternion with
        // TODO: initLookRotation(forward, up) nad then getting the forward of that quaternion!
        return getNormalizedProjectedVector(transform.rotation.forward(tmpVec3f), storeIn)
    }

    fun backward(storeIn: Vec3f): Vec3f {
        return forward(storeIn).invert()
    }

    fun up(storeIn: Vec3f): Vec3f {
        return getNormalizedProjectedVector(transform.rotation.up(tmpVec3f), storeIn)
    }

    fun down(storeIn: Vec3f): Vec3f {
        return up(storeIn).invert()
    }

    fun right(storeIn: Vec3f): Vec3f {
        return getNormalizedProjectedVector(transform.rotation.right(tmpVec3f), storeIn)
    }

    fun left(storeIn: Vec3f): Vec3f {
        return right(storeIn).invert()
    }

}
