package de.colibriengine.audio

import de.colibriengine.math.quaternion.quaternionF.QuaternionF
import de.colibriengine.math.vector.vec3f.Vec3f

interface AudioListener {

    var volume: Float
    val position: Vec3f
    val velocity: Vec3f
    val orientation: QuaternionF

}
