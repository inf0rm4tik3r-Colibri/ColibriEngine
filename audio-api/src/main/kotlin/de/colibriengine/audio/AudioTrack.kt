package de.colibriengine.audio

import de.colibriengine.asset.ResourceName

/** Sound data, ready to be used with an [AudioSource]. */
interface AudioTrack {

    val resourceName: ResourceName
    val length: Float
    val channels: Int
    val bitPerSample: Int
    val samples: Long
    val samplingRate: Int

}
