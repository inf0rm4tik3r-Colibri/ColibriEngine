package de.colibriengine.scene.graph

import de.colibriengine.components.TransformCalculationComponent
import de.colibriengine.components.TransformComponent
import de.colibriengine.datastructures.list.DoublyLinkedList
import de.colibriengine.datastructures.list.DoublyLinkedListIterator
import de.colibriengine.datastructures.list.DoublyLinkedListNode
import de.colibriengine.ecs.Entity
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.util.Timer
import java.util.function.Consumer

abstract class AbstractSceneGraphNode(final override val entity: Entity) : SceneGraphNode {

    /* Provide direct access to data of the TransformComponent store in the entity of this node. */
    final override val transform: TransformComponent = TransformComponent()
    final override val transformCalculation: TransformCalculationComponent = TransformCalculationComponent(transform)

    private val parents: DoublyLinkedList<SceneGraphNode> = DoublyLinkedList()
    private val parentsIterator = parents.dataIteratorHeadToLast()
    private val children: DoublyLinkedList<SceneGraphNode> = DoublyLinkedList()
    private val childrenIterator = children.dataIteratorHeadToLast()

    init {
        entity.add(transform)
        entity.add(transformCalculation)
    }

    abstract fun init()

    fun computeFinalOrthographicTransformation(orthographicParentMVP: Mat4fAccessor) {
        transform.calculateTransformation()
        transformCalculation.modifiableOrthographicTransformation()
            .set(orthographicParentMVP)
            .times(transform.getTransformation())
    }

    fun computeFinalPerspectiveTransformation(perspectiveParentMVP: Mat4fAccessor) {
        transform.calculateTransformation()
        transformCalculation.modifiablePerspectiveTransformation()
            .set(perspectiveParentMVP)
            .times(transform.getTransformation())
    }

    fun computeFinalTransformation(
        orthographicParentMVP: Mat4fAccessor,
        perspectiveParentMVP: Mat4fAccessor
    ) {
        // The components update() method might have changed its transform object.
        // The transformation must be recalculated.
        transform.calculateTransformation()
        computeFinalOrthographicTransformation(orthographicParentMVP)
        computeFinalPerspectiveTransformation(perspectiveParentMVP)
    }

    override fun updateNodeTransformation(
        orthographicParentMVP: Mat4fAccessor,
        perspectiveParentMVP: Mat4fAccessor
    ) {
        computeFinalTransformation(orthographicParentMVP, perspectiveParentMVP)
        for (child in children) {
            child.updateNodeTransformation(
                transformCalculation.orthographicTransformation,
                transformCalculation.perspectiveTransformation
            )
        }
    }

    @Deprecated("oold, implement custom logic in a LogicComponent!")
    abstract override fun update(timing: Timer.View)

    @Deprecated("use array of LogicComponents instead")
    override fun updateNode(timing: Timer.View) {
        update(timing)
        for (child in children) {
            child.updateNode(timing)
        }
    }

    abstract override fun destroy()

    override fun destroyNode() {
        children.forEach(Consumer { obj: SceneGraphNode -> obj.destroy() })
    }

    override val childNodeIterator: DoublyLinkedListIterator<SceneGraphNode, DoublyLinkedListNode<SceneGraphNode>>
        get() {
            childrenIterator.reinit()
            return childrenIterator
        }

    override fun add(child: SceneGraphNode) {
        assert(!children.contains(child)) { "Given node is already a child: $child" }
        child.addParent(this)
        children.add(child)
    }

    override fun remove(child: SceneGraphNode) {
        assert(children.contains(child)) { "Given node is not a child: $child" }
        child.removeParent(this)
        children.remove(child)
    }

    val parentNodeIterator: DoublyLinkedListIterator<SceneGraphNode, DoublyLinkedListNode<SceneGraphNode>>
        get() {
            parentsIterator.reinit()
            return parentsIterator
        }

    /** Do not call this directly! */
    override fun addParent(parent: SceneGraphNode) {
        assert(!parents.contains(parent)) { "Given node is already a parent: $parent" }
        parents.add(parent)
    }

    /** Do not call this directly! */
    override fun removeParent(parent: SceneGraphNode) {
        assert(parents.contains(parent)) { "Given node is not a parent: $parent" }
        parents.remove(parent)
    }

    override fun toString(): String {
        return "AbstractSceneGraphNode(children=\n\t$children)"
    }

}
