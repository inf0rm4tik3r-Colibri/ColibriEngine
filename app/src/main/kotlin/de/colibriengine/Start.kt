package de.colibriengine

import de.colibriengine.asset.ResourceLoader
import de.colibriengine.asset.ResourceLoaderImpl
import de.colibriengine.audio.AudioListener
import de.colibriengine.audio.AudioMaster
import de.colibriengine.audio.AudioSourceFactory
import de.colibriengine.audio.AudioTrackFactory
import de.colibriengine.audio.openal.OpenALMaster
import de.colibriengine.audio.openal.OpenALSourceFactory
import de.colibriengine.audio.openal.OpenALTrackFactory
import de.colibriengine.ecs.ECS
import de.colibriengine.ecs.ECSImpl
import de.colibriengine.graphics.camera.CameraManager
import de.colibriengine.graphics.camera.CameraManagerImpl
import de.colibriengine.graphics.fbo.FBOTextureRendererFactory
import de.colibriengine.graphics.font.FontManager
import de.colibriengine.graphics.font.FontManagerImpl
import de.colibriengine.graphics.material.MaterialFactory
import de.colibriengine.graphics.material.MaterialFactoryImpl
import de.colibriengine.graphics.material.MaterialManager
import de.colibriengine.graphics.material.MaterialManagerImpl
import de.colibriengine.graphics.model.*
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRenderer
import de.colibriengine.graphics.opengl.buffer.fbo.GLFBOTextureRendererFactory
import de.colibriengine.graphics.opengl.model.GLMeshFactory
import de.colibriengine.graphics.opengl.texture.OpenGLTextureFactory
import de.colibriengine.graphics.texture.TextureFactory
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.graphics.texture.TextureManagerImpl
import de.colibriengine.input.InputManager
import de.colibriengine.input.InputManagerImpl
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.waitForUserInput
import org.apache.logging.log4j.Logger
import org.koin.core.KoinApplication
import org.koin.core.context.GlobalContext.startKoin
import org.koin.dsl.module
import java.lang.management.ManagementFactory

const val COLIBRI_ASCII_ART = """
   ______        __ _  __           _    ______               _            
  / ____/____   / /(_)/ /_   _____ (_)  / ____/____   ____ _ (_)____   ___ 
 / /    / __ \ / // // __ \ / ___// /  / __/  / __ \ / __ `// // __ \ / _ \
/ /___ / /_/ // // // /_/ // /   / /  / /___ / / / // /_/ // // / / //  __/
\____/ \____//_//_//_.___//_/   /_/  /_____//_/ /_/ \__, //_//_/ /_/ \___/ 
                                                   /____/                  
"""

val LOG: Logger = getLogger("start")

/**
 * Execute the given function in the Colibri context.
 * Dependency injection will be set up and all required libraries are initialized.
 *
 *
 * Possible start parameters for debugging:
 *
 * - -XX:+PrintCompilation
 * - -XX:+PrintGCDetails
 */
inline fun startColibri(
    withArguments: Array<String>,
    awaitingUserInputToStart: Boolean,
    run: () -> Unit
) {
    COLIBRI_ASCII_ART.lines().forEach(LOG::info)

    LOG.info("Program arguments: ${withArguments.joinToString(separator = ", ")}")
    LOG.info("JVM runtime name: ${ManagementFactory.getRuntimeMXBean().name}")
    LOG.info("PID: ${ProcessHandle.current().pid()}")

    // We may want to wait for an input before launching the engine. This gives us a time window in which we can
    // use RenderDoc to the running JVM process.
    if (awaitingUserInputToStart) {
        waitForUserInput {}
    }

    initDependencyInjection()
    ColibriEngineImpl.initializeLibraries()
    run()
    ColibriEngineImpl.terminateLibraries()
    stopDependencyInjection()
}

/*-----------------------------------------------*/
/*-------------------MODULES---------------------*/
/*-----------------------------------------------*/

val engineModule = module {
    single<ColibriEngine> { ColibriEngineImpl() }
}

val resourceModule = module {
    single<ResourceLoader> { ResourceLoaderImpl() }
    single<TextureFactory> { OpenGLTextureFactory }
    factory<TextureManager> { TextureManagerImpl(get<ResourceLoader>(), get<TextureFactory>()) }
    factory<FontManager> { FontManagerImpl(get<ResourceLoader>(), get<TextureManager>()) }
    single<MeshFactory> { GLMeshFactory }
    single<ModelFactory> { ModelFactoryImpl(get<MeshFactory>()) }
    single<MaterialFactory> { MaterialFactoryImpl }
    factory<MaterialManager> { MaterialManagerImpl(get<MaterialFactory>(), get<TextureManager>()) }
    factory<ModelManager> { ModelManagerImpl(get<ModelFactory>(), get<MaterialManager>()) }
}

val cameraModule = module {
    factory<CameraManager> { CameraManagerImpl() }
}

val inputModule = module {
    factory<InputManager> { InputManagerImpl() }
}

val audioModule = module {
    single<AudioTrackFactory> { OpenALTrackFactory(get()) }
    single<AudioMaster> { OpenALMaster }
    single<AudioSourceFactory> { OpenALSourceFactory() }
    single<AudioListener> { OpenALMaster.device.context.listener }
}

val graphicsModule = module {
    single<FBOTextureRendererFactory<GLFBOTextureRenderer>> { GLFBOTextureRendererFactory() }
}

val ecsModule = module {
    factory<ECS> { ECSImpl() }
}

/*-----------------------------------------------*/
/*-----------------------------------------------*/

var koinApplication: KoinApplication? = null

fun initDependencyInjection() {
    LOG.info("Starting dependency injection")
    koinApplication = startKoin {
        modules(
            engineModule,
            resourceModule,
            cameraModule,
            inputModule,
            audioModule,
            graphicsModule,
            ecsModule
        )
    }
}

fun stopDependencyInjection() {
    LOG.info("Stopping dependency injection")
    koinApplication?.close()
}
