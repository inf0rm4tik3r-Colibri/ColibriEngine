package de.colibriengine.math.vector.vec2i

/** Every [Vec2i] factory implementation should expose these methods. */
interface Vec2iFactory {

    fun acquire(): Vec2i

    fun acquireDirty(): Vec2i

    fun free(vec2i: Vec2i)

    fun immutableVec2iBuilder(): ImmutableVec2i.Builder

    fun zero(): ImmutableVec2i

    fun one(): ImmutableVec2i

}
