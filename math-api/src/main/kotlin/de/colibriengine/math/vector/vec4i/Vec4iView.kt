package de.colibriengine.math.vector.vec4i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec4iView. */
interface Vec4iView : Vec4iAccessor, AsMutable<Vec4i>, AsImmutable<ImmutableVec4i>
