package de.colibriengine.math.vector.vec4f

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor

/** Provides the general functionality of a four dimensional vector in float precision. */
interface Vec4f : Vec4fAccessor, Vec4fMutator, Viewable<Vec4fView>, Copyable<Vec4f>, AsImmutable<ImmutableVec4f> {

    override var x: Float
    override var y: Float
    override var z: Float
    override var w: Float

    /*
    override fun setX(x: Float): Vec4f
    override fun setY(y: Float): Vec4f
    override fun setZ(z: Float): Vec4f
    override fun setW(w: Float): Vec4f
    */

    override fun set(value: Float): Vec4f
    override fun set(x: Float, y: Float, z: Float, w: Float): Vec4f
    override fun set(other: Vec4fAccessor): Vec4f
    override fun set(other: Vec4dAccessor): Vec4f
    override fun set(other: Vec4iAccessor): Vec4f
    override fun set(other: Vec4lAccessor): Vec4f
    override fun set(other: Vec3fAccessor, w: Float): Vec4f

    override fun initZero(): Vec4f

    override operator fun plus(addend: Float): Vec4f
    override operator fun plus(addends: Vec4fAccessor): Vec4f
    override fun plus(addendX: Float, addendY: Float, addendZ: Float, addendW: Float): Vec4f

    override operator fun minus(subtrahend: Float): Vec4f
    override operator fun minus(subtrahends: Vec4fAccessor): Vec4f
    override fun minus(subtrahendX: Float, subtrahendY: Float, subtrahendZ: Float, subtrahendW: Float): Vec4f

    override operator fun times(factor: Float): Vec4f
    override operator fun times(factors: Vec4fAccessor): Vec4f
    override fun times(factorX: Float, factorY: Float, factorZ: Float, factorW: Float): Vec4f

    override operator fun div(divisor: Float): Vec4f
    override operator fun div(divisors: Vec4fAccessor): Vec4f
    override fun div(divisorX: Float, divisorY: Float, divisorZ: Float, divisorW: Float): Vec4f

    override fun normalize(): Vec4f
    override fun setLengthTo(targetValue: Double): Vec4f
    override fun invert(): Vec4f
    override fun abs(): Vec4f
    override fun shorten(): Vec4f

    companion object {
        /** The number of float values used to represent this vector. */
        const val FLOATS = 4

        /** The number of bytes used to represent this vector object. */
        const val BYTES = FLOATS * java.lang.Float.BYTES
    }

}
