package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/**
 * Vec3lView.
 *
 * 
 */
interface Vec3lView : Vec3lAccessor, AsMutable<Vec3l>, AsImmutable<ImmutableVec3l>
