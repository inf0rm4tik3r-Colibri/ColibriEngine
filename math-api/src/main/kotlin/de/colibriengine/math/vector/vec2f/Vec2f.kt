package de.colibriengine.math.vector.vec2f

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec2l.Vec2lAccessor

/** Provides the general functionality of a two dimensional vector in float precision. */
interface Vec2f : Vec2fAccessor, Vec2fMutator, Viewable<Vec2fView>, Copyable<Vec2f>, AsImmutable<ImmutableVec2f> {

    override var x: Float
    override var y: Float

    /*
    override fun setX(x: Float): Vec2f
    override fun setY(y: Float): Vec2f
    */

    override fun set(value: Float): Vec2f
    override fun set(x: Float, y: Float): Vec2f
    override fun set(other: Vec2fAccessor): Vec2f
    override fun set(other: Vec2dAccessor): Vec2f
    override fun set(other: Vec2iAccessor): Vec2f
    override fun set(other: Vec2lAccessor): Vec2f

    override fun initZero(): Vec2f

    override operator fun plus(addend: Float): Vec2f
    override operator fun plus(addends: Vec2fAccessor): Vec2f
    override fun plus(addendX: Float, addendY: Float): Vec2f

    override operator fun minus(subtrahend: Float): Vec2f
    override operator fun minus(subtrahends: Vec2fAccessor): Vec2f
    override fun minus(subtrahendX: Float, subtrahendY: Float): Vec2f

    override operator fun times(factor: Float): Vec2f
    override operator fun times(factors: Vec2fAccessor): Vec2f
    override fun times(factorX: Float, factorY: Float): Vec2f

    override operator fun div(divisor: Float): Vec2f
    override operator fun div(divisors: Vec2fAccessor): Vec2f
    override fun div(divisorX: Float, divisorY: Float): Vec2f

    override fun normalize(): Vec2f
    override fun setLengthTo(targetValue: Double): Vec2f
    override fun invert(): Vec2f
    override fun abs(): Vec2f
    override fun shorten(): Vec2f

    companion object {
        /** The number of float values used to represent this vector. */
        const val FLOATS = 2

        /** The number of bytes used to represent this vector object. */
        const val BYTES = FLOATS * java.lang.Float.BYTES
    }

}
