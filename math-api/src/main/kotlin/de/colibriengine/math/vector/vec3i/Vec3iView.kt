package de.colibriengine.math.vector.vec3i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** Vec3iView. */
interface Vec3iView : Vec3iAccessor, AsMutable<Vec3i>, AsImmutable<ImmutableVec3i>
