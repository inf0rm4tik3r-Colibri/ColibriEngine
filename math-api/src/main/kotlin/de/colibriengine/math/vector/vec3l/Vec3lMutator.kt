package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor

/** Defines all mutating methods of the three dimensional long vector. */
interface Vec3lMutator {

    /** The first component (x) of this vector. */
    var x: Long

    /** The second component (y) of this vector. */
    var y: Long

    /** The third component (z) of this vector. */
    var z: Long

    /**
     * Sets the x, y and z components of this vector to the specified [value].
     *
     * @param value The new value assigned to each component.
     * @return This instance for method chaining.
     */
    fun set(value: Long): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the specified values.
     *
     * @param x The new value for the x component.
     * @param y The new value for the y component.
     * @param z The new value for the z component.
     * @return This instance for method chaining.
     */
    fun set(x: Long, y: Long, z: Long): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The other vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3fAccessor): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3dAccessor): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3iAccessor): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively.
     *
     * @param other The vector which component values should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: Vec3lAccessor): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to the x, y and z components of [other] respectively and uses the
     * specified w value for the last component.
     *
     * @param other The vector which component values should be taken / copied.
     * @param z The value for the last component.
     * @return This instance for method chaining.
     */
    fun set(other: Vec2lAccessor, z: Long): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to zero (0).
     *
     * @return This instance for method chaining.
     */
    fun initZero(): Vec3lMutator

    /**
     * Adds the given long value to the x, y and z components of this vector respectively.
     *
     * @param addend The value to add.
     * @return This instance for method chaining.
     */
    operator fun plus(addend: Long): Vec3lMutator

    /**
     * Adds the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param addendX The value to add to this vectors x component.
     * @param addendY The value to add to this vectors y component.
     * @param addendZ The value to add to this vectors z component.
     * @return This instance for method chaining.
     */
    fun plus(addendX: Long, addendY: Long, addendZ: Long): Vec3lMutator

    /**
     * Adds the values of the x, y and z components of the specified [addends] vector to the x, y and z components of
     * this vector respectively.
     *
     * @param addends The vector from which to read the addends.
     * @return This instance for method chaining.
     */
    operator fun plus(addends: Vec3lAccessor): Vec3lMutator

    /**
     * Subtracts the given long value from the x, y and z components of this vector respectively.
     *
     * @param subtrahend The value to subtract.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahend: Long): Vec3lMutator

    /**
     * Subtracts the given values (x, y and z) from the x, y and z components of this vector respectively.
     *
     * @param subtrahendX The value to subtract from this vectors x component.
     * @param subtrahendY The value to subtract from this vectors y component.
     * @param subtrahendZ The value to subtract from this vectors z component.
     * @return This instance for method chaining.
     */
    fun minus(subtrahendX: Long, subtrahendY: Long, subtrahendZ: Long): Vec3lMutator

    /**
     * Subtracts the values of the x, y and z components of the specified [subtrahends] vector from the x, y and z
     * components of this vector respectively.
     *
     * @param subtrahends The vector from which to read the subtrahends.
     * @return This instance for method chaining.
     */
    operator fun minus(subtrahends: Vec3lAccessor): Vec3lMutator

    /**
     * Multiplies the given long value to the x, y and z components of this vector respectively.
     *
     * @param factor The value to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(factor: Long): Vec3lMutator

    /**
     * Multiplies the given values (x, y and z) to the x, y and z components of this vector respectively.
     *
     * @param factorX The value to multiply this vectors x component with.
     * @param factorY The value to multiply this vectors y component with.
     * @param factorZ The value to multiply this vectors z component with.
     * @return This instance for method chaining.
     */
    fun times(factorX: Long, factorY: Long, factorZ: Long): Vec3lMutator

    /**
     * Multiplies the values of the x, y and z components of the specified [factors] vector to the x, y and z components
     * of this vector respectively.
     *
     * @param factors The vector from which to read the factors.
     * @return This instance for method chaining.
     */
    operator fun times(factors: Vec3lAccessor): Vec3lMutator

    /**
     * Divides the given long value from the x, y and z components of this vector respectively.
     *
     * @param divisor The value to divide by.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisor: Long): Vec3lMutator

    /**
     * Divides the x, y and z components of this vector by the given values (x, y and z) respectively.
     *
     * @param divisorX The value by which this vectors x component is to be divided.
     * @param divisorY The value by which this vectors y component is to be divided.
     * @param divisorZ The value by which this vectors z component is to be divided.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    fun div(divisorX: Long, divisorY: Long, divisorZ: Long): Vec3lMutator

    /**
     * Divides the x, y and z components of this vector by the values of the x, y and z components of the specified
     * [divisors] vector respectively.
     *
     * @param divisors The vector from which to read the divisors.
     * @return This instance for method chaining.
     * @throws IllegalArgumentException If the given divisor is 0.
     */
    operator fun div(divisors: Vec3lAccessor): Vec3lMutator

    /**
     * Normalizes this vector by setting its length to ~1.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun normalize(): Vec3lMutator

    /**
     * Sets the length of this vector to ~ the specified value.
     *
     * @param targetValue The new length to scale to.
     * @return This instance for method chaining.
     * @throws IllegalStateException If the current length of this vector is exactly 0.
     */
    fun setLengthTo(targetValue: Double): Vec3lMutator

    /**
     * Inverts the x, y and z components of this vector.
     *
     *      x = -x
     *      y = -y
     *      z = -z
     *
     * @return This instance for method chaining.
     */
    fun invert(): Vec3lMutator

    /**
     * Sets the x, y and z components of this vector to their absolute values respectively.
     *
     * @return This instance for method chaining.
     */
    fun abs(): Vec3lMutator

    /**
     * Sets this vector to be perpendicular to the plane spanned by this and the given vector. Order of calculation is
     * important. vector1.cross(vector2) != vector2.cross(vector1)! Direction will be reversed.
     *
     * @param other The vector with which the cross product gets calculated.
     * @return This instance for method chaining. It will be in a 90 degree angle to this and the given vector.
     */
    fun cross(other: Vec3lAccessor): Vec3lMutator

}
