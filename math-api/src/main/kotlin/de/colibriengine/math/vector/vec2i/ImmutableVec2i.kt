package de.colibriengine.math.vector.vec2i

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec2i]. */
interface ImmutableVec2i : Vec2iAccessor, AsMutable<Vec2i> {

    interface Builder {
        fun of(value: Int): ImmutableVec2i
        fun of(x: Int, y: Int): ImmutableVec2i
        fun of(other: Vec2iAccessor): ImmutableVec2i

        fun setX(x: Int): Builder
        fun setY(y: Int): Builder

        fun set(value: Int): Builder
        fun set(x: Int, y: Int): Builder
        fun set(other: Vec2iAccessor): Builder

        fun build(): ImmutableVec2i
    }

}
