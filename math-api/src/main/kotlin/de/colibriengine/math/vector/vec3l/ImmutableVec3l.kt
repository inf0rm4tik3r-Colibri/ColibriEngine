package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.AsMutable

/**
 * Immutable version of the [Vec3l].
 *
 * 
 */
interface ImmutableVec3l : Vec3lAccessor, AsMutable<Vec3l> {

    interface Builder {
        fun of(value: Long): ImmutableVec3l
        fun of(x: Long, y: Long, z: Long): ImmutableVec3l
        fun of(vec3lAccessor: Vec3lAccessor): ImmutableVec3l

        fun setX(x: Long): Builder
        fun setY(y: Long): Builder
        fun setZ(z: Long): Builder

        fun set(value: Long): Builder
        fun set(x: Long, y: Long, z: Long): Builder
        fun set(vec3lAccessor: Vec3lAccessor): Builder

        fun build(): ImmutableVec3l
    }

}
