package de.colibriengine.math.vector.vec4l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec3l.Vec3lAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor

/** Provides the general functionality of a four dimensional vector in long precision. */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec4l : Vec4lAccessor, Vec4lMutator, Viewable<Vec4lView>, Copyable<Vec4l>, AsImmutable<ImmutableVec4l> {

    override var x: Long
    override var y: Long
    override var z: Long
    override var w: Long

    /*
    override fun setX(x: Long): Vec4l
    override fun setY(y: Long): Vec4l
    override fun setZ(z: Long): Vec4l
    override fun setW(w: Long): Vec4l
    */

    override fun set(value: Long): Vec4l
    override fun set(x: Long, y: Long, z: Long, w: Long): Vec4l
    override fun set(other: Vec4fAccessor): Vec4l
    override fun set(other: Vec4dAccessor): Vec4l
    override fun set(other: Vec4iAccessor): Vec4l
    override fun set(other: Vec4lAccessor): Vec4l
    override fun set(other: Vec3lAccessor, w: Long): Vec4l

    override fun initZero(): Vec4l

    override operator fun plus(addend: Long): Vec4l
    override operator fun plus(addends: Vec4lAccessor): Vec4l
    override fun plus(addendX: Long, addendY: Long, addendZ: Long, addendW: Long): Vec4l

    override operator fun minus(subtrahend: Long): Vec4l
    override operator fun minus(subtrahends: Vec4lAccessor): Vec4l
    override fun minus(subtrahendX: Long, subtrahendY: Long, subtrahendZ: Long, subtrahendW: Long): Vec4l

    override operator fun times(factor: Long): Vec4l
    override operator fun times(factors: Vec4lAccessor): Vec4l
    override fun times(factorX: Long, factorY: Long, factorZ: Long, factorW: Long): Vec4l

    override operator fun div(divisor: Long): Vec4l
    override operator fun div(divisors: Vec4lAccessor): Vec4l
    override fun div(divisorX: Long, divisorY: Long, divisorZ: Long, divisorW: Long): Vec4l

    override fun normalize(): Vec4l
    override fun setLengthTo(targetValue: Double): Vec4l
    override fun invert(): Vec4l
    override fun abs(): Vec4l

    companion object {
        /** The number of long values used to represent this vector. */
        const val LONGS = 4

        /** The number of bytes used to represent this vector object. */
        const val BYTES = LONGS * java.lang.Long.BYTES
    }

}
