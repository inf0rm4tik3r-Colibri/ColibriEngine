package de.colibriengine.math.vector.vec4d

import de.colibriengine.buffers.BufferStorable
import de.colibriengine.math.vector.vec3d.Vec3d

/** Defines all non-mutable methods of the four dimensional double vector. */
interface Vec4dAccessor : BufferStorable {

    /** The first component (x) of this vector. */
    val x: Double

    /** The second component (y) of this vector. */
    val y: Double

    /** The third component (z) of this vector. */
    val z: Double

    /** The fourth component (w) of this vector. */
    val w: Double

    /** Returns [x], [y] and [z] in a new [Vec3d]. */
    fun xyz(): Vec3d

    /** Returns [x], [y] and [w] in a new [Vec3d]. */
    fun xyw(): Vec3d

    /** Returns [x], [z] and [w] in a new [Vec3d]. */
    fun xzw(): Vec3d

    /** Returns [y], [z] and [w] in a new [Vec3d]. */
    fun yzw(): Vec3d

    /** Returns true if at least one component is zero. */
    fun hasZeroComponent(): Boolean

    /** Returns the length of this vector. */
    fun length(): Double

    /** Returns the squared length of this vector. Less computationally expensive then [length]. */
    fun squaredLength(): Double

    /** Returns the lowest component of this vector. */
    fun min(): Double

    /** Returns the highest component of this vector. */
    fun max(): Double

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Use values in the [0,...,1] (both inclusive) range.
     * @return The interpolated vector.
     * @throws IllegalArgumentException If the specified [lerpFactor] is either negative or greater than 1.
     */
    fun lerp(target: Vec4dAccessor, lerpFactor: Double): Vec4d

    /**
     * Performs a linear interpolation between this and the target vector.
     *
     * @param target The target vector.
     * @param lerpFactor Amount of interpolation. Any value can be used.
     * @return The interpolated vector.
     */
    fun lerpFree(target: Vec4dAccessor, lerpFactor: Double): Vec4d

    /**
     * Returns a byte array which represents this vector.
     *
     * @return A byte array. Its length is equal to the dimension of this vector. The vectors components are stored in
     *     the ordinary ordering of the vectors components (first to last).
     */
    fun bytes(): ByteArray

    /**
     * Provides a shortened string representation of this vector. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this vector which contains its components.
     */
    fun toFormattedString(): String

}
