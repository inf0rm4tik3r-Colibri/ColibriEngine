package de.colibriengine.math.vector.vec3l

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2l.Vec2lAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3i.Vec3iAccessor

/**
 * Provides the general functionality of a three dimensional vector in long precision.
 *
 * 
 */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec3l : Vec3lAccessor, Vec3lMutator, Viewable<Vec3lView>, Copyable<Vec3l>, AsImmutable<ImmutableVec3l> {

    override var x: Long
    override var y: Long
    override var z: Long

    /*
    override fun setX(x: Long): Vec3l
    override fun setY(y: Long): Vec3l
    override fun setZ(z: Long): Vec3l
    */

    override fun set(value: Long): Vec3l
    override fun set(x: Long, y: Long, z: Long): Vec3l
    override fun set(other: Vec3fAccessor): Vec3l
    override fun set(other: Vec3dAccessor): Vec3l
    override fun set(other: Vec3iAccessor): Vec3l
    override fun set(other: Vec3lAccessor): Vec3l
    override fun set(other: Vec2lAccessor, z: Long): Vec3l

    override fun initZero(): Vec3l

    override operator fun plus(addend: Long): Vec3l
    override operator fun plus(addends: Vec3lAccessor): Vec3l
    override fun plus(addendX: Long, addendY: Long, addendZ: Long): Vec3l

    override operator fun minus(subtrahend: Long): Vec3l
    override operator fun minus(subtrahends: Vec3lAccessor): Vec3l
    override fun minus(subtrahendX: Long, subtrahendY: Long, subtrahendZ: Long): Vec3l

    override operator fun times(factor: Long): Vec3l
    override operator fun times(factors: Vec3lAccessor): Vec3l
    override fun times(factorX: Long, factorY: Long, factorZ: Long): Vec3l

    override operator fun div(divisor: Long): Vec3l
    override operator fun div(divisors: Vec3lAccessor): Vec3l
    override fun div(divisorX: Long, divisorY: Long, divisorZ: Long): Vec3l

    override fun normalize(): Vec3l
    override fun setLengthTo(targetValue: Double): Vec3l
    override fun invert(): Vec3l
    override fun abs(): Vec3l

    override fun cross(other: Vec3lAccessor): Vec3l

    companion object {
        /**
         * The number of long values used to represent this vector.
         */
        const val LONGS = 3

        /**
         * The number of bytes used to represent this vector object.
         */
        const val BYTES = LONGS * java.lang.Long.BYTES
    }

}
