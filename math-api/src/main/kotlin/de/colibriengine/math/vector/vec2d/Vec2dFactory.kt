package de.colibriengine.math.vector.vec2d

/** Every [Vec2d] factory implementation should expose these methods. */
interface Vec2dFactory {

    fun acquire(): Vec2d

    fun acquireDirty(): Vec2d

    fun free(vec2d: Vec2d)

    fun immutableVec2dBuilder(): ImmutableVec2d.Builder

    fun zero(): ImmutableVec2d

    fun one(): ImmutableVec2d

}
