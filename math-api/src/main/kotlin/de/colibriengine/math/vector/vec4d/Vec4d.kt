package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec4f.Vec4fAccessor
import de.colibriengine.math.vector.vec4i.Vec4iAccessor
import de.colibriengine.math.vector.vec4l.Vec4lAccessor

/** Provides the general functionality of a four dimensional vector in double precision. */
@Suppress("MemberVisibilityCanBePrivate")
interface Vec4d : Vec4dAccessor, Vec4dMutator, Viewable<Vec4dView>, Copyable<Vec4d>, AsImmutable<ImmutableVec4d> {

    override var x: Double
    override var y: Double
    override var z: Double
    override var w: Double

    /*
    override fun setX(x: Double): Vec4d
    override fun setY(y: Double): Vec4d
    override fun setZ(z: Double): Vec4d
    override fun setW(w: Double): Vec4d
    */

    override fun set(value: Double): Vec4d
    override fun set(x: Double, y: Double, z: Double, w: Double): Vec4d
    override fun set(other: Vec4fAccessor): Vec4d
    override fun set(other: Vec4dAccessor): Vec4d
    override fun set(other: Vec4iAccessor): Vec4d
    override fun set(other: Vec4lAccessor): Vec4d
    override fun set(other: Vec3dAccessor, w: Double): Vec4d

    override fun initZero(): Vec4d

    override operator fun plus(addend: Double): Vec4d
    override operator fun plus(addends: Vec4dAccessor): Vec4d
    override fun plus(addendX: Double, addendY: Double, addendZ: Double, addendW: Double): Vec4d

    override operator fun minus(subtrahend: Double): Vec4d
    override operator fun minus(subtrahends: Vec4dAccessor): Vec4d
    override fun minus(subtrahendX: Double, subtrahendY: Double, subtrahendZ: Double, subtrahendW: Double): Vec4d

    override operator fun times(factor: Double): Vec4d
    override operator fun times(factors: Vec4dAccessor): Vec4d
    override fun times(factorX: Double, factorY: Double, factorZ: Double, factorW: Double): Vec4d

    override operator fun div(divisor: Double): Vec4d
    override operator fun div(divisors: Vec4dAccessor): Vec4d
    override fun div(divisorX: Double, divisorY: Double, divisorZ: Double, divisorW: Double): Vec4d

    override fun normalize(): Vec4d
    override fun setLengthTo(targetValue: Double): Vec4d
    override fun invert(): Vec4d
    override fun abs(): Vec4d
    override fun shorten(): Vec4d

    companion object {
        /** The number of double values used to represent this vector. */
        const val DOUBLES = 4

        /** The number of bytes used to represent this vector object. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES
    }

}
