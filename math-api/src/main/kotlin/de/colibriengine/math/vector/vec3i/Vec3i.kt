package de.colibriengine.math.vector.vec3i

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.Copyable
import de.colibriengine.math.Viewable
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.math.vector.vec3l.Vec3lAccessor

/** Provides the general functionality of a three dimensional vector in int precision. */
interface Vec3i : Vec3iAccessor, Vec3iMutator, Viewable<Vec3iView>, Copyable<Vec3i>, AsImmutable<ImmutableVec3i> {

    override var x: Int
    override var y: Int
    override var z: Int

    /*
    override fun setX(x: Int): Vec3i
    override fun setY(y: Int): Vec3i
    override fun setZ(z: Int): Vec3i
    */

    override fun set(value: Int): Vec3i
    override fun set(x: Int, y: Int, z: Int): Vec3i
    override fun set(other: Vec3fAccessor): Vec3i
    override fun set(other: Vec3dAccessor): Vec3i
    override fun set(other: Vec3iAccessor): Vec3i
    override fun set(other: Vec3lAccessor): Vec3i
    override fun set(other: Vec2iAccessor, z: Int): Vec3i

    override fun initZero(): Vec3i

    override operator fun plus(addend: Int): Vec3i
    override operator fun plus(addends: Vec3iAccessor): Vec3i
    override fun plus(addendX: Int, addendY: Int, addendZ: Int): Vec3i

    override operator fun minus(subtrahend: Int): Vec3i
    override operator fun minus(subtrahends: Vec3iAccessor): Vec3i
    override fun minus(subtrahendX: Int, subtrahendY: Int, subtrahendZ: Int): Vec3i

    override operator fun times(factor: Int): Vec3i
    override operator fun times(factors: Vec3iAccessor): Vec3i
    override fun times(factorX: Int, factorY: Int, factorZ: Int): Vec3i

    override operator fun div(divisor: Int): Vec3i
    override operator fun div(divisors: Vec3iAccessor): Vec3i
    override fun div(divisorX: Int, divisorY: Int, divisorZ: Int): Vec3i

    override fun normalize(): Vec3i
    override fun setLengthTo(targetValue: Double): Vec3i
    override fun invert(): Vec3i
    override fun abs(): Vec3i

    override fun cross(other: Vec3iAccessor): Vec3i

    companion object {
        /** The number of int values used to represent this vector. */
        const val INTS = 3

        /** The number of bytes used to represent this vector object. */
        const val BYTES = INTS * Integer.BYTES
    }

}
