package de.colibriengine.math.vector.vec4d

import de.colibriengine.math.AsMutable

/** Immutable version of the [Vec4d]. */
interface ImmutableVec4d : Vec4dAccessor, AsMutable<Vec4d> {

    interface Builder {
        fun of(value: Double): ImmutableVec4d
        fun of(x: Double, y: Double, z: Double, w: Double): ImmutableVec4d
        fun of(vec4dAccessor: Vec4dAccessor): ImmutableVec4d

        fun setX(x: Double): Builder
        fun setY(y: Double): Builder
        fun setZ(z: Double): Builder
        fun setW(w: Double): Builder

        fun set(value: Double): Builder
        fun set(x: Double, y: Double, z: Double, w: Double): Builder
        fun set(vec4dAccessor: Vec4dAccessor): Builder

        fun build(): ImmutableVec4d
    }

}
