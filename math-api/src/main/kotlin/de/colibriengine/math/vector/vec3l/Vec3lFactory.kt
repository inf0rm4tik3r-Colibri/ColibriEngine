package de.colibriengine.math.vector.vec3l

/**
 * Every [Vec3l] factory implementation should expose these methods.
 *
 *
 */
interface Vec3lFactory {

    fun acquire(): Vec3l

    fun acquireDirty(): Vec3l

    fun free(vec3l: Vec3l)

    fun immutableVec3lBuilder(): ImmutableVec3l.Builder

    fun zero(): ImmutableVec3l

    fun one(): ImmutableVec3l

    fun unitXAxis(): ImmutableVec3l

    fun unitYAxis(): ImmutableVec3l

    fun unitZAxis(): ImmutableVec3l

    fun negativeUnitXAxis(): ImmutableVec3l

    fun negativeUnitYAxis(): ImmutableVec3l

    fun negativeUnitZAxis(): ImmutableVec3l

}
