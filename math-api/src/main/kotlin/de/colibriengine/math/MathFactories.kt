package de.colibriengine.math

import de.colibriengine.math.matrix.MatrixFactories
import de.colibriengine.math.quaternion.QuaternionFactories
import de.colibriengine.math.vector.VectorFactories

interface MathFactories : QuaternionFactories, VectorFactories, MatrixFactories
