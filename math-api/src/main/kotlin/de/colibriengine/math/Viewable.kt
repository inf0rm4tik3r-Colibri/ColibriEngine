package de.colibriengine.math

interface Viewable<ViewType> {

    /**
     * Returns an object with which the current object can be viewed. The returned object allows access to this object
     * whilst assuring that no state of this object is mutated.
     */
    val view: ViewType

}
