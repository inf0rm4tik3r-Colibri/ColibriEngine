package de.colibriengine.math

interface AsImmutable<ImmutableType> {

    /**
     * Returns an immutable version of this object.
     *
     * @return A copy of this object which can not be mutated.
     */
    fun asImmutable(): ImmutableType

}
