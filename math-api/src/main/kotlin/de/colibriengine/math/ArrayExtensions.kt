package de.colibriengine.math

fun FloatArray.insert(index: Int, value1: Float, value2: Float) {
    this[index + 0] = value1
    this[index + 1] = value2
}

fun FloatArray.insert(index: Int, value1: Float, value2: Float, value3: Float) {
    this[index + 0] = value1
    this[index + 1] = value2
    this[index + 2] = value3
}

fun FloatArray.insert(index: Int, value1: Float, value2: Float, value3: Float, value4: Float) {
    this[index + 0] = value1
    this[index + 1] = value2
    this[index + 2] = value3
    this[index + 3] = value4
}

fun FloatArray.insertInverted(index: Int, value1: Float, value2: Float) {
    this[index + 0] = -value1
    this[index + 1] = -value2
}

fun FloatArray.insertInverted(index: Int, value1: Float, value2: Float, value3: Float) {
    this[index + 0] = -value1
    this[index + 1] = -value2
    this[index + 2] = -value3
}

fun FloatArray.insertInverted(index: Int, value1: Float, value2: Float, value3: Float, value4: Float) {
    this[index + 0] = -value1
    this[index + 1] = -value2
    this[index + 2] = -value3
    this[index + 3] = -value4
}

fun DoubleArray.insert(index: Int, value1: Double, value2: Double) {
    this[index + 0] = value1
    this[index + 1] = value2
}

fun DoubleArray.insert(index: Int, value1: Double, value2: Double, value3: Double) {
    this[index + 0] = value1
    this[index + 1] = value2
    this[index + 2] = value3
}

fun DoubleArray.insert(index: Int, value1: Double, value2: Double, value3: Double, value4: Double) {
    this[index + 0] = value1
    this[index + 1] = value2
    this[index + 2] = value3
    this[index + 3] = value4
}

fun DoubleArray.insertInverted(index: Int, value1: Double, value2: Double) {
    this[index + 0] = -value1
    this[index + 1] = -value2
}

fun DoubleArray.insertInverted(index: Int, value1: Double, value2: Double, value3: Double) {
    this[index + 0] = -value1
    this[index + 1] = -value2
    this[index + 2] = -value3
}

fun DoubleArray.insertInverted(index: Int, value1: Double, value2: Double, value3: Double, value4: Double) {
    this[index + 0] = -value1
    this[index + 1] = -value2
    this[index + 2] = -value3
    this[index + 3] = -value4
}
