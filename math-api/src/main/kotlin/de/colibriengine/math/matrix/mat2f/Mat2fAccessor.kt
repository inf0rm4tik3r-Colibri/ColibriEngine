package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.buffers.BufferStorable

/** Defines all non-mutable methods of the 2x2 float matrix. */
interface Mat2fAccessor : BufferStorable {

    /** First row, first column. */
    val m00: Float

    /** First row, second column. */
    val m01: Float

    /** Second row, first column. */
    val m10: Float

    /** Second row, second column. */
    val m11: Float

    /**
     * Returns the value at the cell defined by the intersection of [row] and [column].
     *
     * @param row The row to look in.
     * @param column The column to look in.
     * @return The value at cell ([row], [column]).
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    operator fun get(row: Int, column: Int): Float

    /**
     * Returns the values of the specified [row] in the given *storeIn* vector instance.
     *
     * @param row Index of the row to obtain.
     * @param storeIn The target vector in which the data is stored.
     * @return The given storeIn vector.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    fun getRow(row: Int, storeIn: Vec2f): Vec2f

    /**
     * Returns the values of the first row in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getRow1(storeIn: Vec2f): Vec2f

    /**
     * Returns the values of the second row in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getRow2(storeIn: Vec2f): Vec2f

    /**
     * Returns the values of the specified [column] in the given storeIn vector instance.
     *
     * @param column Index of the column to obtain.
     * @param storeIn The target vector in which the data is stored.
     * @return The given storeIn vector.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    fun getColumn(column: Int, storeIn: Vec2f): Vec2f

    /**
     * Returns the values of the first column in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getColumn1(storeIn: Vec2f): Vec2f

    /**
     * Returns the values of the second column in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getColumn2(storeIn: Vec2f): Vec2f

    /**
     * Returns the elements of the major diagonal of this matrix in the given vector instance.
     *
     * @param storeIn The vector in which the major diagonal gets stored.
     * @return The given storeIn vector.
     */
    fun getMajorDiagonal(storeIn: Vec2f): Vec2f

    /**
     * Return the contents of this matrix in a new two dimension array of size 2x2.
     *
     * @return A new array holding the data of this matrix.
     */
    fun asArray(): Array<out FloatArray>

    /**
     * Returns the contents of this matrix in the given array structure.
     *
     * @param storeIn The array in which the data should be stored. Must be of size 2x2!
     * @return The given storeIn array.
     * @throws IllegalArgumentException If the given matrix is not of size 2x2.
     */
    fun asArray(storeIn: Array<out FloatArray>): Array<out FloatArray>

    /**
     * Returns a byte array which represents this matrix.
     *
     * @return A byte array. Its length is 4 (2*2). The matrices components are stored "row by row".
     */
    fun bytes(): ByteArray

    /**
     * Transforms the given vector by multiplying it with this matrix.
     *
     * @param vector The vector to transform.
     * @return The transformed [vector].
     */
    fun transform(vector: Vec2f): Vec2f

    /**
     * Calculates the determinant of this 2x2 matrix.
     *
     * @return The determinant of this 2x2 matrix.
     */
    fun determinant(): Float

    /**
     * Returns true if at least one component is zero.
     *
     * @return True if at least one zero-component exists. False otherwise.
     */
    fun hasZeroComponent(): Boolean

    /**
     * Provides a shortened string representation of this matrix. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this matrix which contains its components.
     */
    fun toFormattedString(): String

}
