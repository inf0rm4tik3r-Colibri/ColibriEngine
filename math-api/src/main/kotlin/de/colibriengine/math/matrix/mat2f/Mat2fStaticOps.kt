package de.colibriengine.math.matrix.mat2f

interface Mat2fStaticOps {

    fun determinantOf(
        m00: Float, m01: Float,
        m10: Float, m11: Float
    ): Float

}
