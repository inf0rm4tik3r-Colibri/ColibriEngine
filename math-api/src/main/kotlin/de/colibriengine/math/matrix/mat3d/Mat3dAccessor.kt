package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.matrix.mat2d.Mat2d
import de.colibriengine.math.quaternion.quaternionD.QuaternionD
import de.colibriengine.math.vector.vec3d.Vec3d
import de.colibriengine.buffers.BufferStorable

/** Defines all non-mutable methods of the 3x3 double matrix. */
interface Mat3dAccessor : BufferStorable {

    /** First row, first column. */
    val m00: Double

    /** First row, second column. */
    val m01: Double

    /** First row, third column. */
    val m02: Double

    /** Second row, first column. */
    val m10: Double

    /** Second row, second column. */
    val m11: Double

    /** Second row, third column. */
    val m12: Double

    /** Third row, first column. */
    val m20: Double

    /** Third row, second column. */
    val m21: Double

    /** Third row, third column. */
    val m22: Double

    /**
     * Returns the value at the cell defined by the intersection of [row] and [column] in the graphical layout of this
     * 3x3 matrix.
     *
     * @param row The row to look in.
     * @param column The column to look in.
     * @return The value at cell ([row], [column]).
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    operator fun get(row: Int, column: Int): Double

    /**
     * Returns the values of the specified [row] in the given storeIn vector instance.
     *
     * @param row Index of the row to obtain.
     * @param storeIn The quaternion in which the result should be stored.
     * @return The given storeIn vector.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    fun getRow(row: Int, storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the first row in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getRow1(storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the second row in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getRow2(storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the third row in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getRow3(storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the specified [column] in the given storeIn vector instance.
     *
     * @param column Index of the column to obtain.
     * @param storeIn The quaternion in which the result should be stored.
     * @return The given storeIn vector.
     * @throws IllegalArgumentException If the row or the column index are not in the `[0..1]` range.
     */
    fun getColumn(column: Int, storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the first column in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getColumn1(storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the second column in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getColumn2(storeIn: Vec3d): Vec3d

    /**
     * Returns the values of the third column in the given [storeIn] vector instance.
     *
     * @param storeIn The target vector in which the data is stored.
     * @return The given [storeIn] vector.
     */
    fun getColumn3(storeIn: Vec3d): Vec3d

    /**
     * Returns the elements of the major diagonal of this matrix in the given [storeIn] vector.
     *
     * @param storeIn The vector in which the major diagonal gets stored.
     * @return The given storeIn vector.
     */
    fun getMajorDiagonal(storeIn: Vec3d): Vec3d

    /**
     * Extracts a right direction vector from this matrix and returns the result in the given [storeIn] vector.
     *
     * @param storeIn The three dimensional vector in which the result gets stored.
     * @return The given storeIn vector.
     */
    fun getRight(storeIn: Vec3d): Vec3d

    /**
     * Extracts an up direction vector from this matrix and returns the result in the given [storeIn] vector.
     *
     * @param storeIn The three dimensional vector in which the result gets stored.
     * @return The given storeIn vector.
     */
    fun getUp(storeIn: Vec3d): Vec3d

    /**
     * Extracts a forward direction vector from this matrix and returns the result in the given [storeIn] vector.
     *
     * @param storeIn The three dimensional vector in which the result gets stored.
     * @return The given storeIn vector.
     */
    fun getForward(storeIn: Vec3d): Vec3d

    /**
     * Converts this matrix to a quaternion and returns the result in the given [storeIn] quaternion.
     *
     * @param storeIn The quaternion in which the result should be stored.
     * @return The given storeIn quaternion.
     */
    fun asQuaternion(storeIn: QuaternionD): QuaternionD

    /**
     * Return the contents of this matrix in a new two dimension array of size 3x3.
     *
     * @return A new array holding the data of this matrix.
     */
    fun asArray(): Array<out DoubleArray>

    /**
     * Returns the contents of this matrix in the given array structure.
     *
     * @param storeIn The array in which the data should be stored. Must be of size 3x3!
     * @return The given storeIn array.
     * @throws IllegalArgumentException If the given matrix is not of size 3x3.
     */
    fun asArray(storeIn: Array<out DoubleArray>): Array<out DoubleArray>

    /**
     * Calculates a 2x2 submatrix by excluding / omitting [row] and [column] of this matrix. The 4 values of the
     * resulting matrix will be stored in the given [storeIn] matrix instance.
     *
     * @param row The index of the row to exclude / ignore. `[0..1]`
     * @param column The index of the column to exclude / ignore. `[0..1]`
     * @param storeIn The 2x2 matrix which gets filled with the resulting submatrix.
     * @return The given storeIn matrix instance.
     */
    fun getSubmatrix(row: Int, column: Int, storeIn: Mat2d): Mat2d

    /**
     * Transforms the given vector by multiplying it with this matrix.
     *
     * @param vector The vector to transform.
     * @return The transformed [vector].
     */
    fun transform(vector: Vec3d): Vec3d

    /**
     * Calculates the determinant of this 3x3 matrix.
     *
     * @return The determinant of this 3x3 matrix.
     */
    fun determinant(): Double

    /**
     * Returns true if at least one component is zero.
     *
     * @return True if at least one zero-component exists. False otherwise.
     */
    fun hasZeroComponent(): Boolean

    /**
     * Returns a byte array which represents this matrix.
     *
     * @return A byte array. Its length is 9 (3*3). The matrices components are stored "row by row".
     */
    fun bytes(): ByteArray

    /**
     * Provides a shortened string representation of this matrix. Decimal places of each component are restricted. Maybe
     * more reader-friendly.
     *
     * @return A descriptive string of this matrix which contains its components.
     */
    fun toFormattedString(): String
}
