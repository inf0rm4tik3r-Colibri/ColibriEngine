package de.colibriengine.math.matrix.mat2d

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat2f.Mat2fAccessor
import de.colibriengine.math.vector.vec2d.Vec2dAccessor

/** Provides the general functionality of a 2x2 matrix in double precision. */
interface Mat2d : Mat2dAccessor, Mat2dMutator, Viewable<Mat2dView> {

    override var m00: Double
    override var m01: Double
    override var m10: Double
    override var m11: Double

    override fun setM00(value: Double): Mat2d
    override fun setM01(value: Double): Mat2d
    override fun setM10(value: Double): Mat2d
    override fun setM11(value: Double): Mat2d

    override fun set(row: Int, column: Int, value: Double): Mat2d

    override fun setRow(row: Int, x: Double, y: Double): Mat2d
    override fun setRow1(x: Double, y: Double): Mat2d
    override fun setRow2(x: Double, y: Double): Mat2d
    override fun setRow(row: Int, vector: Vec2dAccessor): Mat2d
    override fun setRow1(vector: Vec2dAccessor): Mat2d
    override fun setRow2(vector: Vec2dAccessor): Mat2d

    override fun setColumn(column: Int, x: Double, y: Double): Mat2d
    override fun setColumn1(x: Double, y: Double): Mat2d
    override fun setColumn2(x: Double, y: Double): Mat2d
    override fun setColumn(column: Int, vector: Vec2dAccessor): Mat2d
    override fun setColumn1(vector: Vec2dAccessor): Mat2d
    override fun setColumn2(vector: Vec2dAccessor): Mat2d

    override fun set(value: Double): Mat2d
    override fun set(m00: Double, m01: Double, m10: Double, m11: Double): Mat2d
    override fun set(other: Mat2fAccessor): Mat2d
    override fun set(other: Mat2dAccessor): Mat2d
    override fun set(matrixData: Array<DoubleArray>): Mat2d

    override fun initZero(): Mat2d
    override fun initIdentity(): Mat2d

    override fun plus(value: Double): Mat2d
    override fun plus(other: Mat2dAccessor): Mat2d

    override fun minus(value: Double): Mat2d
    override fun minus(other: Mat2dAccessor): Mat2d

    override fun times(value: Double): Mat2d
    override fun times(other: Mat2dAccessor): Mat2d
    override fun mulSelf(): Mat2d

    override fun inverse(): Mat2d
    override fun transpose(): Mat2d
    override fun pow(exponent: Int): Mat2d
    override fun abs(): Mat2d
    override fun shorten(): Mat2d

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of double values used to represent this matrix. */
        const val DOUBLES = 4

        /** The number of bytes used to represent this matrix object. 4 double values of 8 bytes each => 32 bytes. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 2

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 2

        /** Index of the first row. */
        const val FIRST_ROW = 0

        /** Index of the second row. */
        const val SECOND_ROW = 1

        /** Index of the first row. */
        const val FIRST_COLUMN = 0

        /** Index of the second row. */
        const val SECOND_COLUMN = 1
    }

}
