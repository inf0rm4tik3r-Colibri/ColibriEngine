package de.colibriengine.math.matrix.mat2f

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat2d.Mat2dAccessor
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor

/** Provides the general functionality of a 2x2 matrix in float precision. */
interface Mat2f : Mat2fAccessor, Mat2fMutator, Viewable<Mat2fView> {

    override var m00: Float
    override var m01: Float
    override var m10: Float
    override var m11: Float

    override fun setM00(value: Float): Mat2f
    override fun setM01(value: Float): Mat2f
    override fun setM10(value: Float): Mat2f
    override fun setM11(value: Float): Mat2f

    override fun set(row: Int, column: Int, value: Float): Mat2f

    override fun setRow(row: Int, x: Float, y: Float): Mat2f
    override fun setRow(row: Int, vector: Vec2fAccessor): Mat2f
    override fun setRow1(x: Float, y: Float): Mat2f
    override fun setRow2(x: Float, y: Float): Mat2f
    override fun setRow1(vector: Vec2fAccessor): Mat2f
    override fun setRow2(vector: Vec2fAccessor): Mat2f

    override fun setColumn(column: Int, x: Float, y: Float): Mat2f
    override fun setColumn(column: Int, vector: Vec2fAccessor): Mat2f
    override fun setColumn1(x: Float, y: Float): Mat2f
    override fun setColumn2(x: Float, y: Float): Mat2f
    override fun setColumn1(vector: Vec2fAccessor): Mat2f
    override fun setColumn2(vector: Vec2fAccessor): Mat2f

    override fun set(value: Float): Mat2f
    override fun set(m00: Float, m01: Float, m10: Float, m11: Float): Mat2f
    override fun set(other: Mat2fAccessor): Mat2f
    override fun set(other: Mat2dAccessor): Mat2f
    override fun set(matrixData: Array<FloatArray>): Mat2f

    override fun initZero(): Mat2f
    override fun initIdentity(): Mat2f

    override fun plus(value: Float): Mat2f
    override fun plus(other: Mat2fAccessor): Mat2f

    override fun minus(value: Float): Mat2f
    override fun minus(other: Mat2fAccessor): Mat2f

    override fun times(value: Float): Mat2f
    override fun times(other: Mat2fAccessor): Mat2f
    override fun mulSelf(): Mat2f

    override fun inverse(): Mat2f
    override fun transpose(): Mat2f
    override fun pow(exponent: Int): Mat2f
    override fun abs(): Mat2f
    override fun shorten(): Mat2f
    override fun transform(vector: Vec2f): Vec2f

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of float values used to represent this matrix. */
        const val FLOATS = 4

        /** The number of bytes used to represent this matrix object. 4 float values of 4 bytes each => 16 bytes. */
        const val BYTES = FLOATS * java.lang.Float.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 2

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 2

        /** Index of the first row. */
        const val FIRST_ROW = 0

        /** Index of the second row. */
        const val SECOND_ROW = 1

        /** Index of the first row. */
        const val FIRST_COLUMN = 0

        /** Index of the second row. */
        const val SECOND_COLUMN = 1
    }

}
