package de.colibriengine.math.matrix.mat3d

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat3f.Mat3fAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor

/** Provides the general functionality of a 3x3 matrix in double precision. */
interface Mat3d : Mat3dAccessor, Mat3dMutator, Viewable<Mat3dView> {

    override var m00: Double
    override var m01: Double
    override var m02: Double
    override var m10: Double
    override var m11: Double
    override var m12: Double
    override var m20: Double
    override var m21: Double
    override var m22: Double

    override fun setM00(value: Double): Mat3dMutator;
    override fun setM01(value: Double): Mat3dMutator;
    override fun setM02(value: Double): Mat3dMutator;
    override fun setM10(value: Double): Mat3dMutator;
    override fun setM11(value: Double): Mat3dMutator;
    override fun setM12(value: Double): Mat3dMutator;
    override fun setM20(value: Double): Mat3dMutator;
    override fun setM21(value: Double): Mat3dMutator;
    override fun setM22(value: Double): Mat3dMutator;

    override fun set(row: Int, column: Int, value: Double): Mat3d

    override fun setRow(row: Int, x: Double, y: Double, z: Double): Mat3d
    override fun setRow1(x: Double, y: Double, z: Double): Mat3d
    override fun setRow2(x: Double, y: Double, z: Double): Mat3d
    override fun setRow3(x: Double, y: Double, z: Double): Mat3d
    override fun setRow(row: Int, vector: Vec3dAccessor): Mat3d
    override fun setRow1(vector: Vec3dAccessor): Mat3d
    override fun setRow2(vector: Vec3dAccessor): Mat3d
    override fun setRow3(vector: Vec3dAccessor): Mat3d

    override fun setColumn(column: Int, x: Double, y: Double, z: Double): Mat3d
    override fun setColumn1(x: Double, y: Double, z: Double): Mat3d
    override fun setColumn2(x: Double, y: Double, z: Double): Mat3d
    override fun setColumn3(x: Double, y: Double, z: Double): Mat3d
    override fun setColumn(column: Int, vector: Vec3dAccessor): Mat3d
    override fun setColumn1(vector: Vec3dAccessor): Mat3d
    override fun setColumn2(vector: Vec3dAccessor): Mat3d
    override fun setColumn3(vector: Vec3dAccessor): Mat3d

    override fun set(value: Double): Mat3d
    override fun set(
        m00: Double, m01: Double, m02: Double,
        m10: Double, m11: Double, m12: Double,
        m20: Double, m21: Double, m22: Double
    ): Mat3d

    override fun set(other: Mat3fAccessor): Mat3d
    override fun set(other: Mat3dAccessor): Mat3d
    override fun set(matrixData: Array<DoubleArray>): Mat3d

    override fun initZero(): Mat3d
    override fun initIdentity(): Mat3d
    override fun initRotationX(angleAroundX: Double): Mat3d
    override fun initRotationY(angleAroundY: Double): Mat3d
    override fun initRotationZ(angleAroundZ: Double): Mat3d
    override fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): Mat3d
    override fun initRotation(angles: Vec3dAccessor): Mat3d
    override fun initRotation(axis: Vec3dAccessor, angle: Double): Mat3d

    override fun plus(value: Double): Mat3d
    override fun plus(other: Mat3dAccessor): Mat3d

    override fun minus(value: Double): Mat3d
    override fun minus(other: Mat3dAccessor): Mat3d

    override fun times(value: Double): Mat3d
    override fun times(other: Mat3dAccessor): Mat3d
    override fun mulSelf(): Mat3d

    override fun inverse(): Mat3d
    override fun transpose(): Mat3d
    override fun pow(exponent: Int): Mat3d
    override fun abs(): Mat3d
    override fun shorten(): Mat3d

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of double values used to represent this matrix. */
        const val DOUBLES = 9

        /** The number of bytes used to represent this matrix object. 9 double values of 8 bytes each => 72 bytes. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 3

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 3

        /** Index of the first row or column. */
        const val FIRST = 0

        /** Index of the second row or column. */
        const val SECOND = 1

        /** Index of the third row or column. */
        const val THIRD = 2
    }

}
