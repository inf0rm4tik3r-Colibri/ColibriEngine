package de.colibriengine.math.matrix.mat4d

import de.colibriengine.math.Viewable
import de.colibriengine.math.matrix.mat3d.Mat3dAccessor
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor
import de.colibriengine.math.vector.vec4d.Vec4dAccessor

/** Provides the general functionality of a 4x4 matrix in double precision. */
interface Mat4d : Mat4dAccessor, Mat4dMutator, Viewable<Mat4dView> {

    override var m00: Double
    override var m01: Double
    override var m02: Double
    override var m03: Double
    override var m10: Double
    override var m11: Double
    override var m12: Double
    override var m13: Double
    override var m20: Double
    override var m21: Double
    override var m22: Double
    override var m23: Double
    override var m30: Double
    override var m31: Double
    override var m32: Double
    override var m33: Double

    override fun setM00(value: Double): Mat4d
    override fun setM01(value: Double): Mat4d
    override fun setM02(value: Double): Mat4d
    override fun setM03(value: Double): Mat4d
    override fun setM10(value: Double): Mat4d
    override fun setM11(value: Double): Mat4d
    override fun setM12(value: Double): Mat4d
    override fun setM13(value: Double): Mat4d
    override fun setM20(value: Double): Mat4d
    override fun setM21(value: Double): Mat4d
    override fun setM22(value: Double): Mat4d
    override fun setM23(value: Double): Mat4d
    override fun setM30(value: Double): Mat4d
    override fun setM31(value: Double): Mat4d
    override fun setM32(value: Double): Mat4d
    override fun setM33(value: Double): Mat4d

    override fun set(row: Int, column: Int, value: Double): Mat4d

    override fun setRow(row: Int, x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setRow1(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setRow2(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setRow3(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setRow4(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setRow(row: Int, vector: Vec4dAccessor): Mat4d
    override fun setRow1(vector: Vec4dAccessor): Mat4d
    override fun setRow2(vector: Vec4dAccessor): Mat4d
    override fun setRow3(vector: Vec4dAccessor): Mat4d
    override fun setRow4(vector: Vec4dAccessor): Mat4d

    override fun setColumn(column: Int, x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setColumn1(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setColumn2(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setColumn3(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setColumn4(x: Double, y: Double, z: Double, w: Double): Mat4d
    override fun setColumn(column: Int, vector: Vec4dAccessor): Mat4d
    override fun setColumn1(vector: Vec4dAccessor): Mat4d
    override fun setColumn2(vector: Vec4dAccessor): Mat4d
    override fun setColumn3(vector: Vec4dAccessor): Mat4d
    override fun setColumn4(vector: Vec4dAccessor): Mat4d

    override fun set(value: Double): Mat4d
    override fun set(
        m00: Double, m01: Double, m02: Double, m03: Double,
        m10: Double, m11: Double, m12: Double, m13: Double,
        m20: Double, m21: Double, m22: Double, m23: Double,
        m30: Double, m31: Double, m32: Double, m33: Double
    ): Mat4d

    override fun set(other: Mat4fAccessor): Mat4d
    override fun set(other: Mat4dAccessor): Mat4d
    override fun set(other: Mat3dAccessor): Mat4d
    override fun set(other: Mat3dAccessor, fillWith: Double): Mat4d
    override fun set(
        other: Mat3dAccessor,
        fillWith03: Double, fillWith13: Double, fillWith23: Double,
        fillWith30: Double, fillWith31: Double, fillWith32: Double,
        fillWith33: Double
    ): Mat4d

    override fun set(matrixData: Array<DoubleArray>): Mat4d

    override fun initZero(): Mat4d
    override fun initIdentity(): Mat4d
    override fun initBias(): Mat4d
    override fun initTranslation(x: Double, y: Double, z: Double): Mat4d
    override fun initTranslation(translation: Vec3dAccessor): Mat4d
    override fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): Mat4d
    override fun initRotation(angles: Vec3dAccessor): Mat4d
    override fun initRotation(axis: Vec3dAccessor, angle: Double): Mat4d
    override fun initRotation(forward: Vec3dAccessor, up: Vec3dAccessor, right: Vec3dAccessor): Mat4d
    override fun initRotation(forward: Vec3dAccessor, up: Vec3dAccessor): Mat4d
    override fun initRotation(rotation: QuaternionDAccessor): Mat4d
    override fun initScale(scaleX: Double, scaleY: Double, scaleZ: Double): Mat4d
    override fun initScale(scale: Double): Mat4d
    override fun initScale(scaleView: Vec3dAccessor): Mat4d

    override fun initFrustumProjection(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d

    override fun initPerspectiveProjectionLH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4d

    override fun initPerspectiveProjectionRH(
        fov: Double, aspectRatio: Double,
        zNear: Double, zFar: Double
    ): Mat4d

    override fun initOrthographicProjectionLH(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d

    override fun initOrthographicProjectionRH(
        left: Double, right: Double, bottom: Double, top: Double,
        zNear: Double, zFar: Double
    ): Mat4d

    override fun initLookAtRH(
        source: Vec3dAccessor,
        targetUp: Vec3dAccessor,
        pointOfInterest: Vec3dAccessor
    ): Mat4d

    override fun plus(value: Double): Mat4d
    override fun plus(other: Mat4dAccessor): Mat4d

    override fun minus(value: Double): Mat4d
    override fun minus(other: Mat4dAccessor): Mat4d

    override fun times(value: Double): Mat4d
    override fun mulSelf(): Mat4d
    override fun times(other: Mat4dAccessor): Mat4d

    override fun inverse(): Mat4d
    override fun transpose(): Mat4d
    override fun pow(exponent: Int): Mat4d
    override fun abs(): Mat4d
    override fun shorten(): Mat4d

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of double values used to represent this matrix. */
        const val DOUBLES = 16

        /** The number of bytes used to represent this matrix object. 16 double values of 8 bytes each => 128 bytes. */
        const val BYTES = DOUBLES * java.lang.Double.BYTES

        /** The amount of rows this matrix contains. */
        const val ROW_AMT = 4

        /** The amount of columns this matrix contains. */
        const val COLUMN_AMT = 4

        /** Index of the first row. */
        const val FIRST_ROW = 0

        /** Index of the second row. */
        const val SECOND_ROW = 1

        /** Index of the third row. */
        const val THIRD_ROW = 2

        /** Index of the fourth row. */
        const val FOURTH_ROW = 3

        /** Index of the first column. */
        const val FIRST_COLUMN = 0

        /** Index of the second column. */
        const val SECOND_COLUMN = 1

        /** Index of the third column. */
        const val THIRD_COLUMN = 2

        /** Index of the fourth column. */
        const val FOURTH_COLUMN = 3
    }

}
