package de.colibriengine.math.matrix.mat2f

/** Every [Mat2f] factory implementation should expose these methods. */
interface Mat2fFactory {

    fun acquire(): Mat2f

    fun acquireDirty(): Mat2f

    fun free(mat2f: Mat2f)

    fun immutableMat2fBuilder(): ImmutableMat2f.Builder

    fun zero(): ImmutableMat2f

    fun one(): ImmutableMat2f

    fun identity(): ImmutableMat2f

}
