package de.colibriengine.math

import kotlin.math.abs

/** Provides mathematical helper functions. */
@Suppress("MemberVisibilityCanBePrivate")
object FEMath {

    const val PI = Math.PI
    const val PI_F = PI.toFloat()

    const val GOLDEN_RATIO = (1.0 + 2.23606797749979) / 2.0 // sqrt(5) = 2.23606797749979
    const val GOLDEN_RATIO_F = GOLDEN_RATIO.toFloat()

    /**
     * Clamps the value specified in the [value] parameter to be in range of [min] and [max].
     *
     * @param value The value which is to be checked.
     * @param min Minimum bound.
     * @param max Maximum bound.
     * @return Either: min, value, or max. If [value] is greater than [min] and smaller than [max] respective to their
     *     implementation of the [Comparable] interface, [value] is returned. If [value]
     *     is smaller than [min], the specified minimum value is returned. In case
     *     [value] is greater than [max], the specified maximum value is returned.
     */
    fun <T : Comparable<T>> clamp(value: T, min: T, max: T): T {
        return when {
            value < min -> min
            value > max -> max
            else -> value
        }
    }

    /**
     * Converts the given (base 10) [number] into the given base. For example:
     *
     *      rebase(10, 2) -> "1010"
     *      rebase(123, 16) -> "B7"
     *      ...
     *
     * @param number The number to convert. Negative and positive numbers will be treated equally, but specifying a
     *     negative number results in a '-' prepending the output.
     * @param base The target base in range [0..36]. Negative values get converted to their positive equivalents.
     * @return The [number] in base [base].
     * @throws IllegalArgumentException If the given [base] is not in the range [2..36].
     */
    fun rebase(number: Int, base: Int): String {
        @Suppress("NAME_SHADOWING") var number = number
        @Suppress("NAME_SHADOWING") var base = base
        // Perform argument checks.
        base = abs(base)
        require(base in 2..36) { "Rebasing is only supported for bases in the range [2..36]. Given base: $base" }

        // This StringBuilder instance will hold the rebased value as a sequence of characters.
        val rebased = StringBuilder()

        // Treat negative numbers.
        val negative = number < 0
        number = abs(number)

        // Accumulated remnants of consecutive divisions by the target base value.
        do {
            val rest = number % base
            // Is "rest" in the [0..9] range?
            when {
                rest <= 9 ->
                    // Append the character of the number (ASCII 0 is at int[48]).
                    // 48+rest will result int the ASCII code of the number stored in rest.
                    rebased.append((48 + rest).toChar())
                else ->
                    // Append a letter instead. ASCII 'A' is at int[65], so if 10 is the first number which must be
                    // replaced by a letter, 55+10 will result in the first character of the alphabet ('A').
                    // 55+rest will lead to all characters.
                    rebased.append((55 + rest).toChar())
            }
            // Perform the next division, so we can abort or calculate the next rest->literal.
            number /= base
        } while (number != 0)

        // If a negative number is processed. Add a '-' sign. It will be the first character after reversing the
        // StringBuilder!.
        if (negative) {
            rebased.append('-')
        }

        // We have to reverse the data in the StringBuilder as we calculated the least significant literals first.
        return rebased.reverse().toString()
    }

    /**
     * Performs a linear interpolation form [low] to [high] using the supplied [factor] value as a stepping mechanism.
     *
     * @param low The lower bound.
     * @param high The upper bound.
     * @param factor The amount of interpolation to apply. Must be in range [0,..,1]!
     * @return If [factor] is 0, [low] will be returned. If [factor] is 1, [high] will be returned. The returned value
     *     is otherwise linearly interpolated between [low] and [high].
     * @throws IllegalArgumentException If the given [factor] was out of the allowed range (0,..,1).
     */
    fun lerp(low: Float, high: Float, factor: Float): Float {
        require(factor in 0.0..1.0) {
            "Interpolation with a factor value outside [0,..,1] is not allowed."
        }
        return lerpFree(low, high, factor);
    }

    /**
     * Performs a linear interpolation form [low] to [high] by using the supplied [factor] value as a stepping
     * mechanism. The [factor] can thereby be arbitrarily low or high!
     *
     * @param low The lower bound.
     * @param high The upper bound.
     * @param factor The amount of interpolation to apply. Must NOT be in range [0,..,1]!
     * @return If [factor] is 0, [low] will be returned. If [factor] is 1, [high] will be returned. The returned value
     *     is otherwise linearly interpolated between [low] and [high].
     */
    fun lerpFree(low: Float, high: Float, factor: Float): Float {
        return low + factor * (high - low)
    }

}

