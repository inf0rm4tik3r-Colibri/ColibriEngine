package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.math.AsImmutable
import de.colibriengine.math.AsMutable

/** QuaternionDView. */
interface QuaternionDView : QuaternionDAccessor
