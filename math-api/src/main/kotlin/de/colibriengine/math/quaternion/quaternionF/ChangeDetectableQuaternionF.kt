package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor
import de.colibriengine.reactive.Subject

class ChangeDetectableQuaternionF(private val quaternion: QuaternionF) : QuaternionF by quaternion {

    val onChange: Subject<Boolean> = Subject()

    var changed: Boolean = false
        set(value) {
            field = value
            onChange.next(value)
        }

    override var x: Float
        get() = quaternion.x
        set(value) {
            quaternion.x = value
            changed = true
        }

    override var y: Float
        get() = quaternion.y
        set(value) {
            quaternion.y = value
            changed = true
        }

    override var z: Float
        get() = quaternion.z
        set(value) {
            quaternion.z = value
            changed = true
        }

    override var w: Float
        get() = quaternion.w
        set(value) {
            quaternion.w = value
            changed = true
        }

    override fun setX(x: Float): QuaternionF {
        quaternion.setX(x)
        changed = true
        return quaternion
    }

    override fun setY(y: Float): QuaternionF {
        quaternion.setY(y)
        changed = true
        return quaternion
    }

    override fun setZ(z: Float): QuaternionF {
        quaternion.setZ(z)
        changed = true
        return quaternion
    }

    override fun setW(w: Float): QuaternionF {
        quaternion.setW(w)
        changed = true
        return quaternion
    }

    override fun set(x: Float, y: Float, z: Float, w: Float): QuaternionF {
        quaternion.set(x, y, z, w)
        changed = true
        return quaternion
    }

    override fun set(other: QuaternionFAccessor): QuaternionF {
        quaternion.set(other)
        changed = true
        return quaternion
    }

    override fun set(other: QuaternionDAccessor): QuaternionF {
        quaternion.set(other)
        changed = true
        return quaternion
    }

    override fun initIdentity(): QuaternionF {
        quaternion.initIdentity()
        changed = true
        return quaternion
    }

    override fun initRotation(axisX: Float, axisY: Float, axisZ: Float, angle: Float): QuaternionF {
        quaternion.initRotation(axisX, axisY, axisZ, angle)
        changed = true
        return quaternion
    }

    override fun initRotation(axis: Vec3fAccessor, angle: Float): QuaternionF {
        quaternion.initRotation(axis, angle)
        changed = true
        return quaternion
    }

    override fun initXRotation(angle: Float): QuaternionF {
        quaternion.initXRotation(angle)
        changed = true
        return quaternion
    }

    override fun initYRotation(angle: Float): QuaternionF {
        quaternion.initYRotation(angle)
        changed = true
        return quaternion
    }

    override fun initZRotation(angle: Float): QuaternionF {
        quaternion.initZRotation(angle)
        changed = true
        return quaternion
    }

    override fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): QuaternionF {
        quaternion.initRotation(angleAroundX, angleAroundY, angleAroundZ)
        changed = true
        return quaternion
    }

    override fun initRotation(angles: Vec3fAccessor): QuaternionF {
        quaternion.initRotation(angles)
        changed = true
        return quaternion
    }

    override fun initLookAt(
        sourcePosition: Vec3fAccessor,
        targetPosition: Vec3fAccessor,
        upDirection: Vec3fAccessor
    ): QuaternionF {
        quaternion.initLookAt(sourcePosition, targetPosition, upDirection)
        changed = true
        return quaternion
    }

    override fun initLookRotation(forward: Vec3fAccessor, up: Vec3fAccessor): QuaternionF {
        quaternion.initLookRotation(forward, up)
        changed = true
        return quaternion
    }

    override fun normalize(): QuaternionF {
        quaternion.normalize()
        changed = true
        return quaternion
    }

    override fun invert(): QuaternionF {
        quaternion.invert()
        changed = true
        return quaternion
    }

    override fun conjugate(): QuaternionF {
        quaternion.conjugate()
        changed = true
        return quaternion
    }

    override fun times(scalar: Float): QuaternionF {
        quaternion.times(scalar)
        changed = true
        return quaternion
    }

    override fun times(vector: Vec3fAccessor): QuaternionF {
        quaternion.times(vector)
        changed = true
        return quaternion
    }

    override fun times(other: QuaternionFAccessor): QuaternionF {
        quaternion.times(other)
        changed = true
        return quaternion
    }

    override fun div(other: QuaternionFAccessor): QuaternionF {
        quaternion.div(other)
        changed = true
        return quaternion
    }

    override fun lerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF {
        quaternion.lerp(target, lerpFactor)
        changed = true
        return quaternion
    }

    override fun slerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionF {
        quaternion.slerp(target, lerpFactor)
        changed = true
        return quaternion
    }

    override fun toString(): String {
        return "$quaternion - changed: $changed"
    }

}
