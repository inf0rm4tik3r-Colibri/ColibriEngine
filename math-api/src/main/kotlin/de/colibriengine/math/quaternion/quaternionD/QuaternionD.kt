package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.math.Viewable
import de.colibriengine.math.quaternion.quaternionF.QuaternionFAccessor
import de.colibriengine.math.vector.vec3d.Vec3dAccessor

/** Provides the general functionality of a quaternion in double precision. */
interface QuaternionD : QuaternionDAccessor, QuaternionDMutator, Viewable<QuaternionDView> {

    override var x: Double
    override var y: Double
    override var z: Double
    override var w: Double

    override fun setX(x: Double): QuaternionD
    override fun setY(y: Double): QuaternionD
    override fun setZ(z: Double): QuaternionD
    override fun setW(w: Double): QuaternionD

    override fun set(x: Double, y: Double, z: Double, w: Double): QuaternionD
    override fun set(other: QuaternionFAccessor): QuaternionD
    override fun set(other: QuaternionDAccessor): QuaternionD

    override fun initIdentity(): QuaternionD
    override fun initRotation(axisX: Double, axisY: Double, axisZ: Double, angle: Double): QuaternionD
    override fun initRotation(axis: Vec3dAccessor, angle: Double): QuaternionD
    override fun initXRotation(angle: Double): QuaternionD
    override fun initYRotation(angle: Double): QuaternionD
    override fun initZRotation(angle: Double): QuaternionD
    override fun initRotation(angleAroundX: Double, angleAroundY: Double, angleAroundZ: Double): QuaternionD
    override fun initRotation(angles: Vec3dAccessor): QuaternionD
    override fun initLookAt(
        sourcePosition: Vec3dAccessor, targetPosition: Vec3dAccessor,
        upDirection: Vec3dAccessor
    ): QuaternionD

    override fun initLookRotation(forward: Vec3dAccessor, up: Vec3dAccessor): QuaternionD

    override fun normalize(): QuaternionD
    override fun invert(): QuaternionD
    override fun conjugate(): QuaternionD

    override operator fun times(scalar: Double): QuaternionD
    override operator fun times(vector: Vec3dAccessor): QuaternionD
    override operator fun times(other: QuaternionDAccessor): QuaternionD

    override operator fun div(other: QuaternionDAccessor): QuaternionD

    override fun lerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionD
    override fun slerp(target: QuaternionDAccessor, lerpFactor: Double): QuaternionD

    @Suppress("MemberVisibilityCanBePrivate", "unused")
    companion object {
        /** The number of double values used to represent this quaternion. */
        const val DOUBLES = 4

        /**
         * The number of bytes used to represent this quaternion object. 4 double values of 8 bytes each => 32 bytes.
         */
        const val BYTES = DOUBLES * java.lang.Double.BYTES
    }

}
