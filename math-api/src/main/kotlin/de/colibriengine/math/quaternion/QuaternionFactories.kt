package de.colibriengine.math.quaternion

import de.colibriengine.math.quaternion.quaternionD.QuaternionDFactory
import de.colibriengine.math.quaternion.quaternionF.QuaternionFFactory

/**
 * QuaternionFactories.
 *
 * 
 */
interface QuaternionFactories {

    fun quaternionF(): QuaternionFFactory
    fun quaternionD(): QuaternionDFactory

}
