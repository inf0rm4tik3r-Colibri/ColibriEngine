package de.colibriengine.math.quaternion.quaternionF

import de.colibriengine.math.quaternion.quaternionD.QuaternionDAccessor
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

/** Defines all mutating methods of the float quaternion. */
interface QuaternionFMutator {

    /** The first vector component (x) of this quaternion. */
    var x: Float

    /** The second vector component (y) of this quaternion. */
    var y: Float

    /** The third vector component (z) of this quaternion. */
    var z: Float

    /** The scalar part (w) of this quaternion. */
    var w: Float

    /**
     * Sets the x component of this quaternion to the specified value.
     *
     * @param x The new value for the x component.
     * @return This instance for method chaining.
     */
    fun setX(x: Float): QuaternionFMutator

    /**
     * Sets the y component of this quaternion to the specified value.
     *
     * @param y The new value for the y component.
     * @return This instance for method chaining.
     */
    fun setY(y: Float): QuaternionFMutator

    /**
     * Sets the z component of this quaternion to the specified value.
     *
     * @param z The new value for the z component.
     * @return This instance for method chaining.
     */
    fun setZ(z: Float): QuaternionFMutator

    /**
     * Sets the w component of this quaternion to the specified value.
     *
     * @param w The new value for the w component.
     * @return This instance for method chaining.
     */
    fun setW(w: Float): QuaternionFMutator

    /**
     * Sets the vector part of this quaternion to the vector specified by `(x y z)` and the scalar part to [w].
     *
     * @param x The x component of the vector part.
     * @param y The y component of the vector part.
     * @param z The z component of the vector part.
     * @param w The value of / for the scalar part.
     * @return This instance for method chaining.
     */
    fun set(x: Float, y: Float, z: Float, w: Float): QuaternionFMutator

    /**
     * Sets the vector and scalar parts of this quaternion to the vector and scalar parts of [other] respectively.
     *
     * @param other The quaternion which vector and scalar parts should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: QuaternionFAccessor): QuaternionFMutator

    /**
     * Sets the vector and scalar parts of this quaternion to the vector and scalar parts of [other] respectively.
     *
     * @param other The quaternion which vector and scalar parts should be taken / copied.
     * @return This instance for method chaining.
     */
    fun set(other: QuaternionDAccessor): QuaternionFMutator

    /**
     * Initializes this quaternion to the unit quaternion. Its vector part is set to 0 and its scalar part is set to 1.
     * It then looks like this: [0, 0, 0, 1]. The identity quaternion represents no rotation. Multiplying a quaternion
     * with this identity leaves the quaternion unchanged!
     *
     * @return This instance for method chaining.
     */
    fun initIdentity(): QuaternionFMutator

    /**
     * Initializes this quaternion using the specified rotation axis and rotation angle. It becomes a normalized unit
     * quaternion representing an orientation rotated angle degrees around the specified axis vector. The rotation axis
     * must not be normalized!
     *
     * @param axisX The x component of the axis to rotate around.
     * @param axisY The y component of the axis to rotate around.
     * @param axisZ The z component of the axis to rotate around.
     * @param angle The amount of rotation around the specified axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axisX: Float, axisY: Float, axisZ: Float, angle: Float): QuaternionFMutator

    /**
     * Initializes this quaternion using the specified rotation axis and rotation angle. It becomes a normalized, unit
     * quaternion representing an orientation rotated angle degrees around the specified axis vector. The rotation axis
     * must not be normalized!
     *
     * @param axis The axis to rotate around.
     * @param angle The amount of rotation around the specified axis in degrees.
     * @return This instance for method chaining.
     */
    fun initRotation(axis: Vec3fAccessor, angle: Float): QuaternionFMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit x axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_X_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit x axis.
     * @return This instance for method chaining.
     */
    fun initXRotation(angle: Float): QuaternionFMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit y axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_Y_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit y axis.
     * @return This instance for method chaining.
     */
    fun initYRotation(angle: Float): QuaternionFMutator

    /**
     * Initializes this quaternion to represent a rotation around the unit z axis of [angle] degrees. The same effect
     * can be obtained by calling `initRotation(UNIT_Z_AXIS, angle)`.
     *
     * @param angle The degrees to rotate around the unit z axis.
     * @return This instance for method chaining.
     */
    fun initZRotation(angle: Float): QuaternionFMutator

    /**
     * Initializes this quaternion to represent a rotation specified by the 3 angles (around the unit axes). Try to
     * avoid this method as it might be slower than the alternatives!
     *
     * @param angleAroundX Amount of degrees to rotate around the x-Axis.
     * @param angleAroundY Amount of degrees to rotate around the y-Axis.
     * @param angleAroundZ Amount of degrees to rotate around the z-Axis.
     * @return This instance for method chaining.
     */
    fun initRotation(angleAroundX: Float, angleAroundY: Float, angleAroundZ: Float): QuaternionFMutator

    /**
     * Initializes this quaternion to represent a rotation specified by the 3 angles (around the unit axes) which
     * are stored in the vector accessible by [angles]. Try to avoid this method as it might be slower than the
     * alternatives!
     *
     * @param angles The vector which stores the axis-angle information.
     * @return This instance for method chaining.
     */
    fun initRotation(angles: Vec3fAccessor): QuaternionFMutator

    /**
     * Initializes this quaternion so that its defined rotation lets us rotate in a way that the forward direction of
     * this quaternion goes from the source to the target position while ensuring a specific up direction.
     *
     * @param sourcePosition The position from which to look.
     * @param targetPosition The position to look at.
     * @param upDirection The direction to the top when looking from the source to the target position.
     * @return This instance for method chaining.
     */
    fun initLookAt(
        sourcePosition: Vec3fAccessor,
        targetPosition: Vec3fAccessor,
        upDirection: Vec3fAccessor
    ): QuaternionFMutator

    /**
     * See:[what-is-the-source-code-of-quaternionlookrotation (unity)]
     * (https://answers.unity.com/questions/467614/what-is-the-source-code-of-quaternionlookrotation.html)
     *
     * @param forward The direction in which we want to look.
     * @param up The up direction of the viewer.
     * @return This instance for method chaining.
     */
    fun initLookRotation(forward: Vec3fAccessor, up: Vec3fAccessor): QuaternionFMutator

    /**
     * Normalizes this quaternion. Same operation as normalizing a vector.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the quaternion is of length 0.
     */
    fun normalize(): QuaternionFMutator

    /**
     * Inverts this quaternion.
     *
     * @return This instance for method chaining.
     * @throws IllegalStateException If the quaternion is of length 0.
     */
    fun invert(): QuaternionFMutator

    /**
     * Inverts the x, y and z components (vector part) of this quaternion (the real / scalar part remains unchanged).
     *
     * @return This instance for method chaining.
     */
    fun conjugate(): QuaternionFMutator

    /**
     * Multiplies all components of this quaternion with the given scalar value.
     *
     * @param scalar The value to mul with.
     * @return This instance for method chaining.
     */
    operator fun times(scalar: Float): QuaternionFMutator

    /**
     * Multiplies this quaternion with the given vector.
     *
     * @param vector The vector to multiply with.
     * @return This instance for method chaining.
     */
    operator fun times(vector: Vec3fAccessor): QuaternionFMutator

    /**
     * Multiplies this quaternion with the specified quaternion and stores the result in this object.
     *
     * @param other The other quaternion to multiply with.
     * @return This instance for method chaining.
     * @see "Grundlagen der 3D-Programmierung" S.517 -> 7.96
     */
    operator fun times(other: QuaternionFAccessor): QuaternionFMutator

    /**
     * Divides this quaternion by the given [other] quaternion.
     *
     * @param other The quaternion to divide by.
     * @return This instance for method chaining.
     */
    operator fun div(other: QuaternionFAccessor): QuaternionFMutator

    /**
     * Performs a linear interpolation to the [target] quaternion. Distance between each step is equal across the entire
     * interpolation.
     *
     * @param target The target quaternion.
     * @param lerpFactor The step of interpolation.
     * @return This instance for method chaining.
     */
    fun lerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionFMutator

    /**
     * Performs a spherical linear interpolation to the [target] quaternion. The interpolation is mapped as though on a
     * quarter segment of a circle. The distant between each step is not equidistant.
     *
     * @param target The target quaternion.
     * @param lerpFactor The step of interpolation.
     * @return This instance for method chaining.
     */
    fun slerp(target: QuaternionFAccessor, lerpFactor: Float): QuaternionFMutator

}
