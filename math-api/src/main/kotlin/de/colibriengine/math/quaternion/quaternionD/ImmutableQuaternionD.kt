package de.colibriengine.math.quaternion.quaternionD

/** Immutable version of the [QuaternionD]. */
interface ImmutableQuaternionD : QuaternionDAccessor {

    interface Builder {
        fun of(x: Double, y: Double, z: Double, w: Double): ImmutableQuaternionD

        fun of(quaternionDAccessor: QuaternionDAccessor): ImmutableQuaternionD

        fun setX(x: Double): Builder

        fun setY(y: Double): Builder

        fun setZ(z: Double): Builder

        fun setW(w: Double): Builder

        fun set(x: Double, y: Double, z: Double, w: Double): Builder

        fun set(quaternionDAccessor: QuaternionDAccessor): Builder

        fun build(): ImmutableQuaternionD
    }

}
