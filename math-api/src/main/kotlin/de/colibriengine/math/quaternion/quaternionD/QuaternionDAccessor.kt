package de.colibriengine.math.quaternion.quaternionD

import de.colibriengine.buffers.BufferStorable
import de.colibriengine.math.matrix.mat4d.Mat4d
import de.colibriengine.math.vector.vec3d.Vec3d

/** Defines all non-mutable methods of the double quaternion. */
interface QuaternionDAccessor : BufferStorable {

    /** The first vector component (x) of this quaternion. */
    val x: Double

    /** The second vector component (y) of this quaternion. */
    val y: Double

    /** The third vector component (z) of this quaternion. */
    val z: Double

    /** The scalar part (w) of this quaternion. */
    val w: Double

    /** Stores the vector part of this quaternion in [storeIn] and returns [storeIn]. */
    fun xyz(storeIn: Vec3d): Vec3d

    /** Stores the vector part of this quaternion in [storeIn] and returns [storeIn]. Alias for [xyz]. */
    fun vectorPart(storeIn: Vec3d): Vec3d

    /** The scalar part of this quaternion. Alias for [w]. */
    fun scalarPart(): Double

    /** Returns true if the vector part is 0 and the scalar part is 1. Quaternion must look like: [0, 0, 0, 1] */
    fun isIdentity(): Boolean

    /** Calculates the squared length of this quaternion. */
    fun lengthSquared(): Double

    /** Calculates the length of this quaternion. */
    fun length(): Double

    /** Stores the forward direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun forward(storeIn: Vec3d): Vec3d

    /** Stores the forward direction of this quaternion in [storeIn] starting at index [index]. */
    fun forward(storeIn: DoubleArray, index: Int = 0)

    /** Stores the backward direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun backward(storeIn: Vec3d): Vec3d

    /** Stores the backward direction of this quaternion in [storeIn] starting at index [index]. */
    fun backward(storeIn: DoubleArray, index: Int = 0)

    /** Stores the up direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun up(storeIn: Vec3d): Vec3d

    /** Stores the up direction of this quaternion in [storeIn] starting at index [index]. */
    fun up(storeIn: DoubleArray, index: Int = 0)

    /** Stores the down direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun down(storeIn: Vec3d): Vec3d

    /** Stores the down direction of this quaternion in [storeIn] starting at index [index]. */
    fun down(storeIn: DoubleArray, index: Int = 0)

    /** Stores the right direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun right(storeIn: Vec3d): Vec3d

    /** Stores the right direction of this quaternion in [storeIn] starting at index [index]. */
    fun right(storeIn: DoubleArray, index: Int = 0)

    /** Stores the left direction of this quaternion in [storeIn] and returns [storeIn]. */
    fun left(storeIn: Vec3d): Vec3d

    /** Stores the left direction of this quaternion in [storeIn] starting at index [index]. */
    fun left(storeIn: DoubleArray, index: Int = 0)

    /** Stores the matrix representation of this quaternion in [storeIn] and returns [storeIn]. */
    fun asMatrix(storeIn: Mat4d): Mat4d

    /** Returns a byte array which contains the [x] [y] [z] and [w] components in this order. */
    fun bytes(): ByteArray

    /** Provides a shortened string representation by restricting the decimal places of each component. */
    fun toFormattedString(): String

}
