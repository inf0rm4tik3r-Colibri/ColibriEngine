package de.colibriengine.datastructures.list;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a node of a {@link DoublyLinkedList}.
 *
 * @version 0.0.0.2
 * @time 02.08.2018 21:39
 */
@SuppressWarnings("WeakerAccess")
public class DoublyLinkedListNode<T> {
    
    private @Nullable DoublyLinkedListNode<T> previous = null;
    private @Nullable DoublyLinkedListNode<T> next     = null;
    private @Nullable T                       data;
    
    public DoublyLinkedListNode(final @NotNull T data,
                                final @Nullable DoublyLinkedListNode<T> previous,
                                final @Nullable DoublyLinkedListNode<T> next) {
        this.data = data;
        this.previous = previous;
        this.next = next;
    }
    
    public DoublyLinkedListNode(final @NotNull T data) {
        this.data = data;
    }
    
    public void reset() {
        previous = null;
        next = null;
        data = null;
    }
    
    protected void setPrevious(final @Nullable DoublyLinkedListNode<T> previous) {
        this.previous = previous;
    }
    
    protected void setNext(final @Nullable DoublyLinkedListNode<T> next) {
        this.next = next;
    }
    
    protected void nullData() {
        data = null;
    }
    
    public void setData(final @NotNull T data) {
        this.data = data;
    }
    
    public boolean hasPrevious() {
        return previous != null;
    }
    
    public @Nullable DoublyLinkedListNode<T> getPrevious() {
        return previous;
    }
    
    public boolean hasNext() {
        return next != null;
    }
    
    public @Nullable DoublyLinkedListNode<T> getNext() {
        return next;
    }
    
    public @NotNull T getData() throws RuntimeException {
        // TODO: Replace with assertion...
        if (data == null) {
            throw new RuntimeException("Internal error: Data is not assigned to list node!");
        }
        return data;
    }
    
    @Override
    public @NotNull String toString() {
        return data != null ? data.toString() : "null";
    }
    
}
