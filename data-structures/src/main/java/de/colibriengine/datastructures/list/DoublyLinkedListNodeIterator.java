package de.colibriengine.datastructures.list;

import de.colibriengine.lambda.NullaryProducerLambda;
import de.colibriengine.lambda.UnaryConsumerLambda;
import de.colibriengine.lambda.UnaryProducerLambda;

import java.util.NoSuchElementException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * DoublyLinkedListNodeIterator
 */
class DoublyLinkedListNodeIterator<NodeType extends DoublyLinkedListNode<?>>
    implements DoublyLinkedListIterator<NodeType, NodeType> {
    
    private @Nullable NodeType current;
    
    private @Nullable NodeType last;
    
    private final @NotNull UnaryProducerLambda<NodeType, NodeType> forwardFunction;
    
    private final @NotNull UnaryProducerLambda<NodeType, NodeType> backwardFunction;
    
    private final @NotNull NullaryProducerLambda<NodeType> initializationFunction;
    
    private final @NotNull UnaryConsumerLambda<NodeType> removeFunction;
    
    DoublyLinkedListNodeIterator(
        final @NotNull UnaryProducerLambda<NodeType, NodeType> forwardFunction,
        final @NotNull UnaryProducerLambda<NodeType, NodeType> backwardFunction,
        final @NotNull NullaryProducerLambda<NodeType> initializationFunction,
        final @NotNull UnaryConsumerLambda<NodeType> removeFunction
    ) {
        current = initializationFunction.call();
        last = null;
        this.forwardFunction = forwardFunction;
        this.backwardFunction = backwardFunction;
        this.initializationFunction = initializationFunction;
        this.removeFunction = removeFunction;
    }
    
    @Override
    public @NotNull DoublyLinkedListNodeIterator<NodeType> reinit() {
        current = initializationFunction.call();
        return this;
    }
    
    @Override
    public boolean hasNext() {
        return current != null;
    }
    
    @Override
    public @NotNull NodeType next() throws NoSuchElementException {
        if (current == null) {
            throw new NoSuchElementException("Iterator has no more elements.");
        }
        last = current;
        current = forwardFunction.call(current);
        return last;
    }
    
    @Override
    public boolean hasPrevious() {
        return current != null;
    }
    
    @Override
    public @NotNull NodeType previous() {
        if (current == null) {
            throw new NoSuchElementException("Iterator has no more elements.");
        }
        last = current;
        current = backwardFunction.call(current);
        return last;
    }
    
    @Override
    public int nextIndex() {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public int previousIndex() {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public void remove() throws IllegalStateException {
        if (last == null) {
            throw new IllegalStateException("next() must be called before remove()!");
        }
        removeFunction.call(last);
        last = null;
    }
    
    @Override
    public void set(final NodeType t) {
        throw new UnsupportedOperationException("Unsupported");
    }
    
    @Override
    public void add(final NodeType t) {
        throw new UnsupportedOperationException("Unsupported");
    }
    
}
