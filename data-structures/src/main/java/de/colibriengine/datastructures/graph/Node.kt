package de.colibriengine.datastructures.graph

open class Node<T>(val data: T) {

    val outEdges: MutableList<Edge<T>> = mutableListOf()
    val inEdges: MutableList<Edge<T>> = mutableListOf()

    override fun toString(): String {
        return "Node(data=$data, #outEdges=${outEdges.size}, #inEdges=${inEdges.size})"
    }

}
