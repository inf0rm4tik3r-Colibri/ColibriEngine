package de.colibriengine.datastructures.graph

open class Graph<T, EDGE : Edge<T>>(private val allowDuplicates: Boolean = false) {

    val nodes: MutableList<Node<T>> = mutableListOf()
    val edges: MutableList<EDGE> = mutableListOf()

    private val nodeRegistry: MutableMap<T, Node<T>> = mutableMapOf()
    private val nodeRegistryForDuplicates: MutableMap<T, MutableList<Node<T>>> = mutableMapOf()

    private fun registerNode(node: Node<T>) {
        if (allowDuplicates) {
            nodeRegistryForDuplicates.computeIfAbsent(node.data) { mutableListOf() }.add(node)
        } else {
            nodeRegistry[node.data] = node
        }
    }

    private fun unregisterNode(node: Node<T>) {
        if (allowDuplicates) {
            val list = nodeRegistryForDuplicates.getValue(node.data)
            val removed = list.remove(node)
            require(removed) { "Node $node was not present in node registry." }
        } else {
            val removed = nodeRegistry.remove(node.data)
            require(removed != null) { "Node $node was not present in node registry." }
        }
    }

    fun addNode(data: T): Node<T> {
        val node: Node<T> = Node(data)
        nodes.add(node)
        registerNode(node)
        return node
    }

    fun removeNode(node: Node<T>) {
        val removed = nodes.remove(node)
        require(removed) { "Node $node was not present." }
        removeEdgesToAndFrom(node)
        unregisterNode(node)
    }

    inline fun addEdge(
        start: Node<T>,
        end: Node<T>,
        edgeFactory: (start: Node<T>, end: Node<T>) -> EDGE
    ): EDGE {
        val edge = edgeFactory(start, end)
        edges.add(edge)
        edge.start.outEdges.add(edge)
        edge.end.inEdges.add(edge)
        return edge
    }

    fun removeEdgesToAndFrom(node: Node<T>) {
        node.inEdges.forEach {
            edges.remove(it)
            it.start.outEdges.remove(it)
        }
        node.inEdges.clear()
        node.outEdges.forEach {
            edges.remove(it)
            it.end.inEdges.remove(it)
        }
        node.outEdges.clear()
    }

    fun find(data: T): Node<T>? = when {
        allowDuplicates -> nodeRegistryForDuplicates[data]?.get(0) // TODO: multiples?
        else -> nodeRegistry[data]
    }

    fun traverse(node: Node<T>, action: (T) -> Unit) {
        _traverse(node, node, action)
    }

    fun traverse(
        node: Node<T>,
        checkEdge: (EDGE) -> Boolean,
        action: (T) -> Unit
    ) {
        _traverse(true, node, node, checkEdge, action)
    }

    fun _traverse(node: Node<T>, last: Node<T>, action: (T) -> Unit) {
        _traverse(true, node, last, { true }, action)
    }

    private fun _traverse(
        initialCall: Boolean,
        node: Node<T>,
        last: Node<T>,
        checkEdge: (EDGE) -> Boolean,
        action: (T) -> Unit
    ) {
        if (!initialCall) {
            action(node.data)
        }
        for (out in node.outEdges) {
            if (out.end != last && checkEdge(out as EDGE)) {
                _traverse(false, out.end, node, checkEdge, action)
            }
        }
    }

    override fun toString(): String {
        return "Graph(\n" +
            "  #nodes=${nodes.size},\n" +
            "  nodes=${nodes.joinToString(prefix = "\n    ", separator = ",\n    ", postfix = ",\n")}" +
            "  nodesSimple=${
                nodes.joinToString(prefix = "\n    ", separator = ",\n    ", postfix = ",\n") { it.data.toString() }
            }" +
            "  #edges=${edges.size},\n" +
            "  edges=${edges.joinToString(prefix = "\n    ", separator = ",\n    ", postfix = ",\n")}" +
            "  edgesSimple=${
                edges.joinToString(prefix = "\n    ", separator = ",\n    ", postfix = ",\n") { it.toSimpleString() }
            }" +
            ")\n"
    }

}
