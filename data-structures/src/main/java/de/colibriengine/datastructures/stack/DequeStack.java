package de.colibriengine.datastructures.stack;

import de.colibriengine.logging.LogUtil;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Proper implementation of an efficient stack in java using an ArrayDeque. Uses the FIFO principle.
 *
 * @since ColibriEngine 0.0.6.6
 */
public class DequeStack<T> implements Stack<T> {
    
    private static final @NotNull Logger LOG = LogUtil.getLogger(DequeStack.class);
    
    private static final int DEFAULT_INITIAL_STACK_SIZE = 16;
    
    private final @NotNull Deque<T> deque;
    
    /**
     * Constructs a new stack using an ArrayDeque object that can initially hold 16 elements.
     */
    public DequeStack() {
        this(DEFAULT_INITIAL_STACK_SIZE);
    }
    
    /**
     * Constructs a new stack using an ArrayDeque object. Use the initialCapacity to determine how many objects this
     * stack can hold before the next call to push() leads to the stack reallocating its array of twice of its old
     * size.
     *
     * @param initialCapacity
     *     The initial capacity of the stack.
     */
    public DequeStack(final int initialCapacity) {
        deque = new ArrayDeque<>(initialCapacity);
    }
    
    /**
     * Pushes the given object on top of the stack.
     * Runtime: O(1)
     *
     * @param instance
     *     The object to push.
     */
    @Override
    public void push(final @NotNull T instance) {
        deque.addFirst(instance);
    }
    
    /**
     * Retrieves and removes the top of the stack.
     * NOTE: Check whether the pool has remaining elements at first.
     * Runtime: O(1)
     *
     * @return The removed element from the top of the stack.
     *
     * @throws NoSuchElementException
     *     If there is no element to pop.
     */
    @Override
    public @NotNull T pop() throws NoSuchElementException {
        final @Nullable T object = deque.pollFirst();
        if (object == null) {
            throw new NoSuchElementException("This stack is empty!");
        }
        return object;
    }
    
    /**
     * Checks whether there are still elements on the stack.
     * Runtime: O(1)
     *
     * @return True if an elements remaining. -> The next call to pop() is then guaranteed to not return null.
     */
    @Override
    public boolean isEmpty() {
        return deque.isEmpty();
    }
    
    /**
     * Checks if the specified {@code object} is part of this {@link DequeStack}.
     * NOTE: Objects in this stack will be checked against the given object using {@link Object#equals(Object)}!
     * Runtime: O(n)
     *
     * @param element
     *     The object to check.
     *
     * @return True if {@code object} is (logically) already contained somewhere in this stack.
     */
    @Override
    public boolean containsEqual(T element) {
        return deque.contains(element);
    }
    
    /**
     * Checks if the specified {@code object} is part of this {@link DequeStack}.
     * NOTE: Objects in this stack will be checked against the given object using {@code '=='}!
     * Runtime: O(n)
     *
     * @param object
     *     The object to check.
     *
     * @return True if the {@code object}'s reference is already contained somewhere in this stack.
     */
    @Override
    public boolean containsReference(final @NotNull T object) {
        for (final @NotNull T underTest : deque) {
            if (underTest == object) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Clears the stack by losing all references to stored objects. The stack can then considered to be empty.
     * Runtime: O(1)
     */
    @Override
    public void clear() {
        deque.clear();
    }

    @Override
    public int size() {
        return deque.size();
    }

}
