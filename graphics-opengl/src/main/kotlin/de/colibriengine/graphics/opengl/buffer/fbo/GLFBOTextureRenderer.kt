package de.colibriengine.graphics.opengl.buffer.fbo

import de.colibriengine.components.TransformComponent
import de.colibriengine.ecs.ECS
import de.colibriengine.ecs.remove
import de.colibriengine.ecs.useOrThrow
import de.colibriengine.graphics.camera.Camera
import de.colibriengine.graphics.components.InternalRenderComponent
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.fbo.FBO
import de.colibriengine.graphics.fbo.FBOTextureRenderer
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.model.concrete.QuadAlignment
import de.colibriengine.graphics.model.concrete.QuadDirection
import de.colibriengine.graphics.model.concrete.QuadImpl
import de.colibriengine.graphics.model.sceneentities.AlignedQuad
import de.colibriengine.graphics.opengl.GLSet
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.shader.Shader
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.util.IntList

/**
 * This class provides the functionality of rendering with input textures of one [FBO] and output textures of another
 * [FBO].
 */
class GLFBOTextureRenderer(
    override val ecs: ECS,
    override val meshFactory: MeshFactory,
    /* SRC */
    override var srcFBO: FBO? = null,
    override val srcInputTextureIndices: IntList,
    override val srcSamplePos: Vec2f,
    override val srcSampleDimension: Vec2f,
    /* DEST */
    override var destFBO: FBO? = null,
    override val destOutputTextureIndices: IntList,
    override val destWritePos: Vec2f,
    override val destWriteDimension: Vec2f,
    /* SHADER */
    override var shader: Shader,
    override var shaderSetup: (GLFBOTextureRenderer) -> Unit
) : FBOTextureRenderer<GLFBOTextureRenderer> {

    private val alignedQuad: AlignedQuad
    private val tmpVec3f: Vec3f = StdVec3f()

    init {
        checkFBOTextureIndices(srcFBO, srcInputTextureIndices)
        checkFBOTextureIndices(destFBO, destOutputTextureIndices)

        // Check for possibly bad input.
        checkPosAndDimension(srcSamplePos, srcSampleDimension)

        /*
         * We create the quad with specialized UV coordinates.
         * This fulfills the requirement that the sampled area of the source textures must be dynamic.
         */
        val specialUVMappedQuad = QuadImpl.newInstance(
            QuadDirection.XY, QuadAlignment.BOTTOM_LEFT_ALIGNED,
            StdVec2f().set(srcSamplePos.x + srcSampleDimension.y, srcSamplePos.y), // uvBottomRight
            StdVec2f().set(srcSamplePos.x + srcSampleDimension.y, srcSamplePos.y + srcSampleDimension.x), // uvTopRight
            StdVec2f().set(srcSamplePos.x, srcSamplePos.y + srcSampleDimension.x), // uvTopLeft
            StdVec2f().set(srcSamplePos.x, srcSamplePos.y), // uvBottomLeft
            meshFactory
        )

        // We align the renderable quad entity to match the requested destination write position and dimension.
        alignedQuad = AlignedQuad(ecs.createEntity(), specialUVMappedQuad)
        alignedQuad.entity.useOrThrow<RenderComponent> {
            alignedQuad.entity.add(InternalRenderComponent(model).also {
                shadowCaster = false
            })
        }
        alignedQuad.entity.remove<RenderComponent>()

        updateDestWritePosAndDimension(destWritePos, destWriteDimension)
    }

    private fun checkFBOTextureIndices(fbo: FBO?, textureIndices: IntList) {
        fbo?.let {
            val fboTextureAmt = fbo.textures.size
            textureIndices.forEach { index ->
                require(index >= 0) { "Negative texture index ($index) is not allowed." }
                require(index < fboTextureAmt || index == fbo.depthTextureIndex) {
                    "Index: $index can not correspond to any texture. FBO only has $fboTextureAmt textures."
                }
            }
        }
    }

    private fun checkPosAndDimension(pos: Vec2fAccessor, dimension: Vec2fAccessor) {
        if (pos.min() < 0f) {
            LOG.warn("Negative position given.")
        }
        require(dimension.min() >= 0f) { "Negative dimension given." }
    }

    private fun checkPosAndDimension(pos: Vec2iAccessor, dimension: Vec2iAccessor) {
        if (pos.min() < 0) {
            LOG.warn("Negative position given.")
        }
        require(dimension.min() >= 0) { "Negative dimension given." }
    }

    fun updateSrcFBO(srcFBO: FBO?, srcInputTextureIndex: Int) {
        this.srcFBO = srcFBO
        this.srcInputTextureIndices.clear()
        this.srcInputTextureIndices.add(srcInputTextureIndex)
        checkFBOTextureIndices(this.srcFBO, this.srcInputTextureIndices)
    }

    fun updateSrcFBO(srcFBO: FBO?, textureIndicesProvider: IntList.() -> Unit) {
        this.srcFBO = srcFBO
        this.srcInputTextureIndices.clear()
        this.srcInputTextureIndices.textureIndicesProvider()
        checkFBOTextureIndices(this.srcFBO, this.srcInputTextureIndices)
    }

    fun updateDestFBO(destFBO: FBO?, destOutputTextureIndex: Int) {
        this.destFBO = destFBO
        this.destOutputTextureIndices.clear()
        this.destOutputTextureIndices.add(destOutputTextureIndex)
        checkFBOTextureIndices(this.destFBO, this.destOutputTextureIndices)
    }

    fun updateDstFBO(destFBO: FBO?, textureIndicesProvider: IntList.() -> Unit) {
        this.destFBO = destFBO
        this.destOutputTextureIndices.clear()
        this.destOutputTextureIndices.textureIndicesProvider()
        checkFBOTextureIndices(this.destFBO, this.destOutputTextureIndices)
    }

    private fun updateDestWritePosAndDimension(
        destWritePos: Vec2fAccessor,
        destWriteDimension: Vec2fAccessor
    ) {
        checkPosAndDimension(destWritePos, destWriteDimension)
        alignedQuad.alignSimple(destWritePos, destWriteDimension)
    }

    fun updateDestWritePosAndDimension(
        destWritePos: Vec2iAccessor,
        destWriteDimension: Vec2iAccessor
    ) {
        checkPosAndDimension(destWritePos, destWriteDimension)
        alignedQuad.alignSimple(destWritePos, destWriteDimension)
    }

    private fun updateDestWritePosAndDimensionStretchAligned(
        destWidth: Float,
        destHeight: Float,
        srcWidth: Float,
        srcHeight: Float
    ) {
        require(destWidth >= 0) { "Negative destWidth given." }
        require(destHeight >= 0) { "Negative destHeight given." }
        require(srcWidth >= 0) { "Negative srcWidth given." }
        require(srcHeight >= 0) { "Negative srcHeight given." }
        alignedQuad.align(destWidth, destHeight, srcWidth, srcHeight)
    }

    fun updateDestWritePosAndDimensionStretchAligned(
        destDimension: Vec2fAccessor,
        srcDimension: Vec2fAccessor
    ) {
        updateDestWritePosAndDimensionStretchAligned(
            destDimension.x, destDimension.y,
            srcDimension.x, srcDimension.y
        )
    }

    fun updateDestWritePosAndDimensionStretchAligned(destDimension: Vec2iAccessor, srcDimension: Vec2iAccessor) {
        updateDestWritePosAndDimensionStretchAligned(
            destDimension.x.toFloat(),
            destDimension.y.toFloat(),
            srcDimension.x.toFloat(),
            srcDimension.y.toFloat()
        )
    }

    /**
     * NOTE: This method does NOT manipulate the GL viewport!
     */
    fun render(withCamera: Camera, specialViewportSetup: (() -> Unit)? = null) {
        require(!(srcFBO != null && !srcFBO!!.isCreated)) { "The source FBO is not created!" }
        require(!(destFBO != null && !destFBO!!.isCreated)) { "The destination FBO is not created!" }

        if (srcFBO != null) {
            srcFBO!!.bindForReading()
            srcFBO!!.bindTextures(srcInputTextureIndices)
        } else {
            // TODO: Is this really necessary?
            GLFBO.bindDefaultFBOForReading()
        }
        if (destFBO != null) {
            destFBO!!.bindForDrawing()
            destFBO!!.setDrawBuffers(destOutputTextureIndices)
        } else {
            GLFBO.bindDefaultFBOForDrawing()
        }

        // We obtain the current viewport configuration.
        val viewportXBackup = GLUtil.getCST().viewportX
        val viewportYBackup = GLUtil.getCST().viewportY
        val viewportWidthBackup = GLUtil.getCST().viewportWidth
        val viewportHeightBackup = GLUtil.getCST().viewportHeight

        // We set the viewport to match the position and dimension of our quad.
        // Offsetting the viewport by the quads translation will lead to an offsetted draw in the destination textures.
        if (specialViewportSetup == null) {
            GLSet.viewport(
                alignedQuad.transform.translation.x.toInt(),
                alignedQuad.transform.translation.y.toInt(),
                alignedQuad.transform.scale.x.toInt(),
                alignedQuad.transform.scale.y.toInt()
            )
        } else {
            specialViewportSetup.invoke()
        }

        // SAVE backup
        val backup = tmpVec3f.set(alignedQuad.transform.translation)
        alignedQuad.transform.setTranslation(0f, 0f, 0f)
        withCamera.initProjectionMatrices(
            alignedQuad.transform.scale.x.toInt(),
            alignedQuad.transform.scale.y.toInt()
        )
        withCamera.calculateViewProjectionMatrices()
        alignedQuad.computeFinalOrthographicTransformation(withCamera.orthographicViewProjectionMatrix)
        shaderSetup.invoke(this)

        // TODO: This should probably be done differently!
        GLUtil.enableDepthClamp() // TODO: Can we render this without depth clamping?
        alignedQuad.entity.useOrThrow<InternalRenderComponent> {
            model.render()
        }
        GLUtil.disableDepthClamp()

        // RESTORE backup
        alignedQuad.transform.translation.set(backup)

        // Reset the viewport, so that the calling program does not have to deal with the viewport change.
        GLSet.viewport(viewportXBackup, viewportYBackup, viewportWidthBackup, viewportHeightBackup)
    }

    fun getQuadTransform(): TransformComponent {
        return alignedQuad.transform
    }

    fun getQuadOrthographicMVP(): Mat4fAccessor {
        return alignedQuad.transformCalculation.orthographicTransformation
    }

    companion object {
        private val LOG = getLogger<GLFBOTextureRenderer>()
    }

}
