package de.colibriengine.graphics.opengl.shader;

import de.colibriengine.buffers.BufferTools;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.graphics.shader.Shader;
import de.colibriengine.graphics.shader.ShaderManagerImpl;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.matrix.mat3d.Mat3d;
import de.colibriengine.math.matrix.mat3f.Mat3f;
import de.colibriengine.math.matrix.mat4d.Mat4d;
import de.colibriengine.math.matrix.mat4d.Mat4dAccessor;
import de.colibriengine.math.matrix.mat4f.Mat4f;
import de.colibriengine.math.matrix.mat4f.Mat4fAccessor;
import de.colibriengine.math.vector.vec2f.Vec2fAccessor;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec3d.Vec3dAccessor;
import de.colibriengine.math.vector.vec3f.Vec3f;
import de.colibriengine.math.vector.vec3f.Vec3fAccessor;
import de.colibriengine.math.vector.vec3i.Vec3iAccessor;
import de.colibriengine.math.vector.vec4f.Vec4fAccessor;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.BufferUtils;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL41;
import org.lwjgl.system.MemoryStack;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL31.glGetUniformIndices;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * Provides methods to load shader source files and create shader programs.
 * TODO: Implement more code paths
 * Add missing vector uniform setters.
 */
public abstract class AbstractOpenGLShader implements Shader {

    public static final int NO_BOND = 0;

    /**
     * OpenGL ID of the shade program which this class represents.
     */
    private final int program;

    private final List<Integer> availableShaderTypes = Arrays.asList(GL_VERTEX_SHADER, GL_FRAGMENT_SHADER);

    /**
     * Temporarily holds shader objects, created through {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)},
     * until this shader program object got created through {@link AbstractOpenGLShader#linkProgram()}.
     */
    private final ArrayList<Integer> boundShaders;
    private static final int BOUND_SHADERS_INITIAL_CAPACITY = 8;

    /**
     * Holds information on which uniform name maps to which uniform id in the shader. Values from this HashMap are
     * used to conveniently access and update the uniform variables of this shader.
     */
    private final HashMap<String, Integer> uniforms;
    private static final int UNIFORMS_INITIAL_CAPACITY = 8;

    private int[] uniformIndices = null;
    private int[][] uniformInformation = null;

    private static final int MAX_ERROR_BUFFER_SIZE = 2048;

    private static final Logger LOG = LogUtil.getLogger(AbstractOpenGLShader.class);

    /**
     * Creates a new program object that is ready to accept shader objects.
     */
    public AbstractOpenGLShader() {
        // Creates the program object to which shader objects can be attached.
        program = glCreateProgram();

        if (program == GL_FALSE) {
            throw new RuntimeException("Could not create a shader program!");
        }

        boundShaders = new ArrayList<>(BOUND_SHADERS_INITIAL_CAPACITY);
        uniforms = new HashMap<>(UNIFORMS_INITIAL_CAPACITY);
    }

    /**
     * Wrapper for the {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)} method.
     * Passes ShaderType.FRAGMENT_SHADER.
     */
    protected void attachVertexShader(final @NotNull String... sourceCodes) {
        attachShader(GLShaderType.VERTEX_SHADER, sourceCodes);
    }

    /**
     * Wrapper for the {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)} method.
     * Passes ShaderType.TESS_CONTROL_SHADER.
     */
    protected void attachTessControlShader(final @NotNull String... sourceCodes) {
        attachShader(GLShaderType.TESS_CONTROL_SHADER, sourceCodes);
    }

    /**
     * Wrapper for the {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)} method.
     * Passes ShaderType.TESS_EVALUATION_SHADER.
     */
    protected void attachTessEvaluationShader(final @NotNull String... sourceCodes) {
        attachShader(GLShaderType.TESS_EVALUATION_SHADER, sourceCodes);
    }

    /**
     * Wrapper for the {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)} method.
     * Passes ShaderType.GEOMETRY_SHADER.
     */
    protected void attachGeometryShader(final @NotNull String... sourceCodes) {
        attachShader(GLShaderType.GEOMETRY_SHADER, sourceCodes);
    }

    /**
     * Wrapper for the {@link AbstractOpenGLShader#attachShader(GLShaderType, String...)} method.
     * Passes ShaderType.FRAGMENT_SHADER.
     */
    protected void attachFragmentShader(final @NotNull String... sourceCodes) {
        attachShader(GLShaderType.FRAGMENT_SHADER, sourceCodes);
    }

    /**
     * Creates a shader of the specified type, attaches the given shader sources to it, compiles the shader and finally
     * adds it to the program object.
     *
     * @param type        The type of the shader source. One of the available enum vectors.
     * @param sourceCodes The source codes for the shader program. Use loadShaderSource(String) to load a source file.
     * @minContextRequired 2.0
     * @see ShaderManagerImpl#requestShaderSource(String)
     */
    private void attachShader(final GLShaderType type, final @NotNull String... sourceCodes) {
        // Check that there is some source code provided.
        if (sourceCodes.length == 0) {
            throw new RuntimeException("You can not attach an empty shader source. Using type: " + type.name());
        }

        // Create an empty shader object, ready to accept source code.
        final int shader = GL20.glCreateShader(type.getGlId());

        // Hand the given shader source codes to the shader object so that it can keep a copy of it.
        for (final @NotNull String sourceCode : sourceCodes) {
            LOG.debug("Attaching shader code: \n" + sourceCode);
            GL20.glShaderSource(shader, sourceCode);
        }

        // Compile the source code that the shader object contains. Automatic optimization will take place.
        GL20.glCompileShader(shader);

        // And check if the compilation process succeeded.
        if (GL20.glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE) {
            throw new RuntimeException(glGetShaderInfoLog(shader, MAX_ERROR_BUFFER_SIZE));
        }

        // Attach the created shader object to the program
        GL20.glAttachShader(program, shader);

        boundShaders.add(shader);
    }

    /**
     * Links this shader program. Call this method after adding all shader sources to the program.
     */
    protected void linkProgram() {
        // Link all previously attached shader objects attached to the program object together.
        GL20.glLinkProgram(program);
        if (GL20.glGetProgrami(program, GL_LINK_STATUS) == GL_FALSE) {
            final @NotNull String err = "Shader program could not be linked. \n";
            throw new RuntimeException(err + GL20.glGetProgramInfoLog(program, MAX_ERROR_BUFFER_SIZE));
        }

        // Validate whether the compiled program is able to execute
        GL20.glValidateProgram(program);
        if (GL20.glGetProgrami(program, GL_VALIDATE_STATUS) == GL_FALSE) {
            final @NotNull String err = "Shader program could not be validated. \n";
            throw new RuntimeException(err + GL20.glGetProgramInfoLog(program, MAX_ERROR_BUFFER_SIZE));
        }

        // Delete the shader objects as they are now part of the program object.
        boundShaders.forEach(GL20::glDeleteShader);
        boundShaders.clear();
    }

    /**
     * Set this program to be used in the pipeline.
     *
     * @minContextRequired 2.0
     */
    public void bind() {
        if (GLUtil.getCST().getActiveShaderProgram() != program) {
            GL20.glUseProgram(program);
            GLUtil.getCST().setActiveShaderProgram(program);
        }
    }

    /**
     * Releases all stored shader and the program.
     *
     * @minContextRequired 2.0
     */
    public void release() {
        // Delete all remaining shader objects. Method will flag the objects for deletion if they are
        // currently attached to a program object.
        boundShaders.forEach(GL20::glDeleteShader);

        // Delete the program object to release the attached shader sources.
        GL20.glDeleteProgram(program);
    }

    /* ATTRIBUTES */

    /**
     * Returns the location of the specified attribute.
     *
     * @param attribute
     * @return
     * @minContextRequired 2.0
     */
    public int getAttributeLocation(final @NotNull String attribute) {
        final int attributeLocation = GL20.glGetAttribLocation(program, attribute);
        if (attributeLocation == -1) {
            throw new RuntimeException("Could not find attribute location: " + attribute);
        }

        return attributeLocation;
    }

    /* BUFFER UNIFORMS */

    /**
     * Asks OpenGL for the indices of the specified uniforms. All uniform
     *
     * @param uniformNames The uniform to query.
     */
    public void getUniformIndices(final @NotNull String... uniformNames) {
        uniformIndices = new int[uniformNames.length];
        // TODO rewrite using MemoryStack!
        final PointerBuffer pointer = BufferUtils
                .createPointerBuffer(1)
                .put(BufferTools.toByteBuffer(uniformNames));

        GL31.glGetUniformIndices(program, pointer, uniformIndices);
    }

    /* DEFAULT BLOCK UNIFORMS */

    public void addUniformLocations(final @NotNull String... uniforms) {
        for (final @NotNull String uniform : uniforms) {
            addUniformLocation(uniform);
        }
    }

    /**
     * @param uniformName
     * @throws RuntimeException
     */
    public int addUniformLocation(final @NotNull String uniformName) throws RuntimeException {
        final int uniformLocation = GL20.glGetUniformLocation(program, uniformName);
        if (uniformLocation == -1) {
            //throw new RuntimeException("Could not find uniform: " + uniformName); // TODO: COMMENT IN
        }
        uniforms.put(uniformName, uniformLocation);
        return uniformLocation;
    }

    public void setUniform1i(final int uniformLocation, final int value) {
        GL41.glProgramUniform1i(program, uniformLocation, value);
    }

    public void setUniform1ui(final int uniformLocation, final int value) {
        GL41.glProgramUniform1ui(program, uniformLocation, value);
    }

    public void setUniform1i(final @NotNull String uniform, final int value) {
        setUniform1i(uniforms.get(uniform), value);
    }

    public void setUniform1f(final int uniformLocation, final float value) {
        GL41.glProgramUniform1f(program, uniformLocation, value);
    }

    public void setUniform1d(final int uniformLocation, final double value) {
        GL41.glProgramUniform1d(program, uniformLocation, value);
    }

    public void setUniform1f(final @NotNull String uniform, final float value) {
        setUniform1f(uniforms.get(uniform), value);
    }

    public void setUniform2i(final int uniformLocation, final int a, final int b) {
        GL41.glProgramUniform2i(program, uniformLocation, a, b);
    }

    public void setUniform2i(final @NotNull String uniform, final int a, final int b) {
        setUniform2i(uniforms.get(uniform), a, b);
    }

    public void setUniform2i(final int uniformLocation, final @NotNull Vec2iAccessor vector) {
        setUniform2i(uniformLocation, vector.getX(), vector.getY());
    }

    public void setUniform2i(final @NotNull String uniform, final @NotNull Vec2iAccessor vector) {
        setUniform2i(uniforms.get(uniform), vector.getX(), vector.getY());
    }

    public void setUniform2f(final int uniformLocation, final float first, final float second) {
        GL41.glProgramUniform2f(program, uniformLocation, first, second);
    }

    public void setUniform2f(final @NotNull String uniformName, final float first, final float second) {
        setUniform2f(uniforms.get(uniformName), first, second);
    }

    public void setUniform2f(final int uniformLocation, final @NotNull Vec2fAccessor vector) {
        setUniform2f(uniformLocation, vector.getX(), vector.getY());
    }

    public void setUniform2f(final @NotNull String uniformName, final @NotNull Vec2fAccessor vector) {
        setUniform2f(uniforms.get(uniformName), vector.getX(), vector.getY());
    }

    public void setUniform3i(final int uniformLocation, final @NotNull Vec3iAccessor vector) {
        GL41.glProgramUniform3i(program, uniformLocation, vector.getX(), vector.getY(), vector.getZ());
    }

    public void setUniform3i(final @NotNull String uniform, final @NotNull Vec3iAccessor vector) {
        setUniform3i(uniforms.get(uniform), vector);
    }

    public void setUniform3f(final int uniformLocation, final @NotNull Vec3fAccessor vector) {
        GL41.glProgramUniform3f(program, uniformLocation, vector.getX(), vector.getY(), vector.getZ());
    }

    public void setUniform3d(final int uniformLocation, final @NotNull Vec3dAccessor vector) {
        GL41.glProgramUniform3d(program, uniformLocation, vector.getX(), vector.getY(), vector.getZ());
    }

    public void setUniform3f(final @NotNull String uniform, final @NotNull Vec3fAccessor vector) {
        setUniform3f(uniforms.get(uniform), vector);
    }

    public void setUniform3d(final @NotNull String uniform, final @NotNull Vec3dAccessor vector) {
        setUniform3d(uniforms.get(uniform), vector);
    }

    public void setUniform3fv(final int uniformLocation, final @NotNull FloatBuffer data) {
        GL41.glProgramUniform3fv(program, uniformLocation, data);
    }

    public void setUniform3fv(final @NotNull String uniform, final @NotNull FloatBuffer data) {
        setUniform3fv(uniforms.get(uniform), data);
    }

    public void setUniform3fv(final int uniformLocation, final @NotNull Vec3fAccessor[] data) {
        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final FloatBuffer floatBuffer = stack.mallocFloat(data.length * Vec3f.FLOATS);
            for (final Vec3fAccessor vec3f : data) {
                vec3f.storeIn(floatBuffer);
            }
            floatBuffer.rewind();
            setUniform3fv(uniformLocation, floatBuffer);
        }
    }

    public void setUniform3fv(final @NotNull String uniform, final @NotNull Vec3fAccessor[] data) {
        setUniform3fv(uniforms.get(uniform), data);
    }

    public void setUniform4f(final int uniformLocation, final @NotNull Vec4fAccessor vector) {
        GL41.glProgramUniform4f(program, uniformLocation, vector.getX(), vector.getY(), vector.getZ(), vector.getW());
    }

    public void setUniform4f(final int uniformLocation, float x, float y, float z, float w) {
        GL41.glProgramUniform4f(program, uniformLocation, x, y, z, w);
    }

    public void setUniform4f(final @NotNull String uniform, final @NotNull Vec4fAccessor vector) {
        setUniform4f(uniforms.get(uniform), vector);
    }

    public void setUniformM3f(final int uniformLocation, final @NotNull Mat3f matrix) {
        try (final MemoryStack stack = stackPush()) {
            final FloatBuffer buffer = stack.mallocFloat(Mat3f.FLOATS);
            matrix.storeIn(buffer);
            buffer.flip();
            GL41.glProgramUniformMatrix3fv(program, uniformLocation, true, buffer);
        }
    }

    public void setUniformM3f(final @NotNull String uniformName, final @NotNull Mat3f matrix) {
        setUniformM3f(uniforms.get(uniformName), matrix);
    }

    public void setUniformM3d(final int uniformLocation, final @NotNull Mat3d vector) {
        try (final MemoryStack stack = stackPush()) {
            final DoubleBuffer buffer = stack.mallocDouble(Mat3d.DOUBLES);
            vector.storeIn(buffer);
            buffer.flip();
            GL41.glProgramUniformMatrix3dv(program, uniformLocation, true, buffer);
        }
    }

    public void setUniformM3d(final @NotNull String uniformName, final @NotNull Mat3d matrix) {
        setUniformM3d(uniforms.get(uniformName), matrix);
    }

    public void setUniformM4f(final int uniformLocation, final @NotNull Mat4fAccessor matrix) {
        try (final MemoryStack stack = stackPush()) {
            final FloatBuffer buffer = stack.mallocFloat(Mat4f.FLOATS);
            matrix.storeIn(buffer);
            buffer.flip();
            GL41.glProgramUniformMatrix4fv(program, uniformLocation, true, buffer);
        }
    }

    public void setUniformM4f(final @NotNull String uniformName, final @NotNull Mat4fAccessor matrix) {
        setUniformM4f(uniforms.get(uniformName), matrix);
    }

    public void setUniformM4d(final int uniformLocation, final @NotNull Mat4dAccessor matrix) {
        try (final MemoryStack stack = stackPush()) {
            final DoubleBuffer buffer = stack.mallocDouble(Mat4d.DOUBLES);
            matrix.storeIn(buffer);
            buffer.flip();
            GL41.glProgramUniformMatrix4dv(program, uniformLocation, true, buffer);
        }
    }

    public void setUniformM4d(final @NotNull String uniformName, final @NotNull Mat4dAccessor matrix) {
        setUniformM4d(uniforms.get(uniformName), matrix);
    }

    /**
     * Returns the shader program that represents this shader.
     *
     * @return the shader program.
     */
    public int getProgram() {
        return program;
    }

}
