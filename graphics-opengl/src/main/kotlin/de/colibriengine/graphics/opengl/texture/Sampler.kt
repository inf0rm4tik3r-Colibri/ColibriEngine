package de.colibriengine.graphics.opengl.texture

import de.colibriengine.graphics.Bindable
import de.colibriengine.graphics.Creatable
import de.colibriengine.graphics.opengl.GLUtil
import org.lwjgl.opengl.ARBDirectStateAccess
import org.lwjgl.opengl.ARBSamplerObjects
import org.lwjgl.opengl.GL33
import org.lwjgl.opengl.GL45

class Sampler : Creatable, Bindable {

    private var sampler = 0
    private var created = false

    override fun setCreated(created: Boolean) {
        this.created = created
    }

    override fun isCreated(): Boolean {
        return created
    }

    fun create() {
        when {
            GLUtil.isContextAtLeast(4, 5) -> sampler = GL45.glCreateSamplers()
            GLUtil.getCaps().GL_ARB_direct_state_access -> sampler = ARBDirectStateAccess.glCreateSamplers()
            else -> {
                sampler = GL33.glGenSamplers()
                bind()
            }
        }
    }

    fun release() {
        GL33.glDeleteSamplers(sampler)
    }

    override fun bind() {}
    override fun isBound(): Boolean {
        return false
    }

    override fun ensureBound() {
    }

    override fun ensureNotBound() {
    }

    override fun unbind() {
    }

    override fun unbindIfBound() {}

    /**
     * Sets the specified sampler parameter.
     *
     * @param parameterName  The parameter to set/modify.
     * @param parameterValue The new value to use.
     */
    private fun setSamplerParameterUnsafe(parameterName: Int, parameterValue: Int) {
        // Use the standard 3.3 function.
        when {
            GLUtil.isContextAtLeast(3, 3) ->
                GL33.glSamplerParameteri(sampler, parameterName, parameterValue)
            GLUtil.getCaps().GL_ARB_sampler_objects ->
                ARBSamplerObjects.glSamplerParameteri(sampler, parameterName, parameterValue)
            else -> GLUtil.throwNoPathError()
        }
    }

    /**
     * Sets the specified sampler parameter
     *
     * @param samplerParameter The parameter to set/modify.
     * @param parameterValue   The new value to use.
     */
    protected fun setSamplerParameter(samplerParameter: GLSamplerParameter, parameterValue: Int) {
        setSamplerParameterUnsafe(samplerParameter.id, parameterValue)
    }

}
