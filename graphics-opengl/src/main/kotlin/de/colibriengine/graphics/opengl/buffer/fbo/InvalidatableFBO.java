package de.colibriengine.graphics.opengl.buffer.fbo;

import de.colibriengine.util.SystemKt;

// TODO: ensureBound() calls for (mostly) every overloadable method?
@SuppressWarnings("WeakerAccess")
public abstract class InvalidatableFBO extends GLFBO {
    
    /**
     * Message to be displayed if the user tries to call a method which requires this FBO to NOT be invalidated when it
     * currently is. Like the {@link GLFBO#bind()} method.
     */
    private static final String EXCEPTION_MSG_IS_INVALIDATED =
        "Operation \"%s\" is not supported when this object is invalidated!";
    
    /**
     * Stores whether this object is currently to be considered invalidated.
     * An invalidated {@code FBO} should not be used in any way and should be reinitialized!
     *
     * @see InvalidatableFBO#invalidate()
     */
    private boolean invalidated;
    
    /**
     * Invalidates this object. An invalidated {@code FBO} should not be used in any way and should be reinitialized!
     */
    protected void invalidate() {
        invalidated = true;
    }
    
    /**
     * Makes this object valid again.
     */
    protected void makeValid() {
        invalidated = false;
    }
    
    /**
     * @return Whether this object is currently invalidated.
     */
    protected boolean isInvalidated() {
        return invalidated;
    }
    
    /**
     * Tests if this object is currently invalidated. Throws an exception if that test returns true.
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    protected void ensureNotInvalidated() throws UnsupportedOperationException {
        if (invalidated) {
            throw new UnsupportedOperationException(
                String.format(
                    EXCEPTION_MSG_IS_INVALIDATED,
                    SystemKt.getCallerMethodName(Thread.currentThread().getStackTrace())
                )
            );
        }
    }
    
    /**
     *
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    @Override
    public void bind() throws UnsupportedOperationException {
        ensureNotInvalidated();
        super.bind();
    }
    
    /**
     *
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    @Override
    public void bindForDrawing() throws UnsupportedOperationException {
        ensureNotInvalidated();
        super.bindForDrawing();
    }
    
    /**
     *
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    @Override
    public void bindForReading() throws UnsupportedOperationException {
        ensureNotInvalidated();
        super.bindForReading();
    }
    
    /**
     *
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    @Override
    public void bindColorTextures(final int... colorTextures) throws UnsupportedOperationException {
        ensureNotInvalidated();
        super.bindColorTextures(colorTextures);
    }
    
    /**
     *
     *
     * @throws UnsupportedOperationException
     *     If this object is currently invalidated.
     */
    @Override
    public void bindDepthTexture() throws UnsupportedOperationException {
        ensureNotInvalidated();
        super.bindDepthTexture();
    }
    
}
