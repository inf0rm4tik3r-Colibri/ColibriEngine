package de.colibriengine.graphics.opengl.window

import de.colibriengine.asset.ResourceLoader
import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.image.Image
import de.colibriengine.asset.image.ImageLoadOptions
import de.colibriengine.asset.load
import de.colibriengine.graphics.Creatable
import de.colibriengine.graphics.camera.CameraManager
import de.colibriengine.graphics.material.MaterialManager
import de.colibriengine.graphics.model.ModelManager
import de.colibriengine.graphics.opengl.GLSet
import de.colibriengine.graphics.opengl.GLUtil
import de.colibriengine.graphics.opengl.context.GLContextInformation
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.graphics.window.*
import de.colibriengine.graphics.window.WindowHelper.calculateDesktopVideoMode
import de.colibriengine.graphics.window.WindowHelper.calculateMonitorToUse
import de.colibriengine.input.InputManager
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec2f.Vec2fAccessor
import de.colibriengine.math.vector.vec2i.StdVec2i
import de.colibriengine.math.vector.vec2i.Vec2i
import de.colibriengine.math.vector.vec2i.Vec2iAccessor
import de.colibriengine.math.vector.vec4f.StdVec4f
import de.colibriengine.math.vector.vec4f.Vec4f
import de.colibriengine.math.vector.vec4f.buildImmutableVec4f
import org.koin.core.context.GlobalContext.get
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWImage
import org.lwjgl.glfw.GLFWVidMode
import org.lwjgl.opengl.GL11
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import kotlin.math.floor

abstract class AbstractOpenGLWindow(
    /** A reference to the window manager in which this window lies. */
    val windowManager: WindowManager,
    /** A reference to the input manager to which inputs must be forwarded. */
    val inputManager: InputManager,
    val resourceLoader: ResourceLoader
) : Window, WindowInternals, Creatable {

    /** The GLFW window handle which is used to access (and communicate with) the window. */
    protected var windowHandle: Long = 0

    override val id: Long
        get() = windowHandle

    val contextInformation: GLContextInformation = GLContextInformation()

    // TODO: Use other injection method..
    override val cameraManager: CameraManager = get().get()
    override val modelManager: ModelManager = get().get()
    override val materialManager: MaterialManager = get().get()
    override val textureManager: TextureManager = get().get()

    override val clearColor: Vec4f = StdVec4f().set(DEFAULT_CLEAR_COLOR)

    override var title: String = ""
        set(value) {
            ensureCreated()
            field = value
            GLFW.glfwSetWindowTitle(windowHandle, value)
        }

    override var icon: List<ResourceName> = listOf()
        set(resourceNames) {
            if (resourceNames.isNotEmpty()) {
                val glfwImages: Array<GLFWImage> = Array(resourceNames.size) { GLFWImage.malloc() }
                val glfwImageBuffer = GLFWImage.malloc(resourceNames.size)
                val imageLoadOptions = ImageLoadOptions(flipVertically = false)
                resourceNames.forEachIndexed { index, resourceName ->
                    val image: Image = resourceLoader.load(resourceName, imageLoadOptions)
                    glfwImages[index].set(image.width.toInt(), image.height.toInt(), image.data)
                    glfwImageBuffer.put(index, glfwImages[index])
                }
                // Pass icon dat to GLFW.
                GLFW.glfwSetWindowIcon(windowHandle, glfwImageBuffer)
                glfwImages.forEach { it.free() }
                glfwImageBuffer.free()
            } else {
                GLFW.glfwSetWindowIcon(windowHandle, null)
            }
            field = resourceNames
        }

    private val callbacks: WindowCallbacks = WindowCallbacks(this, this)

    private val cachedWindowPosition: Vec2i = StdVec2i().initZero()
    private val cachedWindowSize: Vec2i = StdVec2i().initZero()
    private val cachedFrameBufferSize: Vec2i = StdVec2i().initZero()
    private val cachedWindowContentScale: Vec2f = StdVec2f().initZero()
    private val cachedMonitorContentScale: Vec2f = StdVec2f().initZero()
    private val cachedCursorPosition: Vec2i = StdVec2i().initZero()

    override val size: Vec2iAccessor
        get() = cachedWindowSize

    override val position: Vec2iAccessor
        get() = cachedWindowPosition

    override val framebufferSize: Vec2iAccessor
        get() = cachedFrameBufferSize

    override val contentScale: Vec2fAccessor
        get() = cachedWindowContentScale

    override val monitorContentScale: Vec2fAccessor
        get() = cachedMonitorContentScale

    val cursorPosition: Vec2iAccessor
        get() = cachedCursorPosition

    override var cursorMode: CursorMode
        get() = when (GLFW.glfwGetInputMode(windowHandle, GLFW.GLFW_CURSOR)) {
            GLFW.GLFW_CURSOR_NORMAL -> CursorMode.NORMAL
            GLFW.GLFW_CURSOR_HIDDEN -> CursorMode.HIDDEN
            GLFW.GLFW_CURSOR_DISABLED -> CursorMode.LOCKED
            else -> throw IllegalArgumentException("Unknown cursor mode.")
        }
        set(value) = when (value) {
            CursorMode.NORMAL -> GLFW.glfwSetInputMode(windowHandle, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_NORMAL)
            CursorMode.HIDDEN -> GLFW.glfwSetInputMode(windowHandle, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_HIDDEN)
            CursorMode.LOCKED -> GLFW.glfwSetInputMode(windowHandle, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED)
            else -> throw IllegalArgumentException("Unknown enum constant.")
        }

    override var visible: Boolean
        get() = GLFW.glfwGetWindowAttrib(windowHandle, GLFW.GLFW_VISIBLE) == GL11.GL_TRUE
        set(value) = when (value) {
            true -> GLFW.glfwShowWindow(windowHandle)
            false -> GLFW.glfwHideWindow(windowHandle)
        }

    override var shouldClose: Boolean
        get() = GLFW.glfwWindowShouldClose(windowHandle)
        set(value) = GLFW.glfwSetWindowShouldClose(windowHandle, value)


    override var fullscreen: Boolean
        get() = GLFW.glfwGetWindowMonitor(windowHandle) != MemoryUtil.NULL
        set(value) = when (value) {
            true -> setFullscreenDesiredMonitor(GLFW.GLFW_DONT_CARE.toLong()) // TODO: Fix this. Store the monitor.
            false -> unsetFullscreen()
        }

    // We have "monitor" and "fullscreenMonitor". Can we make this one variable?
    var monitor: Long = 0
    var videoMode: GLFWVidMode? = null
    override var fullscreenMonitor: Long
        get() {
            val monitor = GLFW.glfwGetWindowMonitor(windowHandle)
            if (monitor == MemoryUtil.NULL) {
                throw RuntimeException("The windows is not in fullscreen mode!")
            }
            return monitor
        }
        set(value) {
            GLFW.glfwSetWindowMonitor(
                windowHandle, value,
                0, 0,
                videoMode!!.width(), videoMode!!.height(), videoMode!!.refreshRate()
            )
            // TODO: enabling fullscreen should not automatically enable vsync!
            GLFW.glfwSwapInterval(1)
            LOG.debug("enabling fullscreen: (HZ: )" + videoMode!!.refreshRate())
        }

    /** Stores whether or not this window is currently created. */
    private var created = false

    override fun setCreated(created: Boolean) {
        this.created = created
    }

    override fun isCreated(): Boolean {
        return created
    }

    override fun activate() {
        makeContextCurrent()
        visible = true
    }

    /**
     * Initializes the callbacks of this window. This function gets automatically called when the window is created.
     *
     * @throws IllegalStateException If the window is not jet created.
     */
    fun createCallbacks() {
        ensureCreated()
        callbacks.initialize()
    }

    /** Output information about the final window state. */
    fun printInfo() {
        LOG.info("Created (ID: $windowHandle)")
        LOG.info("Position: (${position.x}, ${position.y})")
        LOG.info("WindowSize: (${size.x}, ${size.y})")
        LOG.info("FramebufferSize: (${framebufferSize.x}, ${framebufferSize.y})")
        LOG.info("WindowContentScale: (${contentScale.x}, ${contentScale.y})")
        LOG.info("MonitorContentScale: (${monitorContentScale.x}, ${monitorContentScale.y})")
        LOG.info("Visible: $visible")
        LOG.info("ContextVersion: ${contextInformation.contextVersion.x}.${contextInformation.contextVersion.y}")
    }

    /**
     * Decodes the names (path to each file) of the dropped object and calls the abstract dropCallback method to inform
     * the specific window class.
     *
     * @param count The amount of objects dropped.
     * @param names Memory address of the name of the first object.
     */
    override fun dropCallbackDecode(count: Int, names: Long) {
        val pointerBuffer = MemoryUtil.memPointerBuffer(names, count)
        dropCallback(Array(count) {
            MemoryUtil.memUTF8(MemoryUtil.memByteBufferNT1(pointerBuffer[it]))
        })
    }

    override fun clear() {
        // Bind the default framebuffer. We do not know which buffer is currently bound.
        // TODO: let the engine keep track of the currently bound buffer.
        //glBindFramebuffer(GL_FRAMEBUFFER, 0)

        // Clear the color, depth and stencil buffer. Each with their specified default values.
        // TODO remove GLUtil dep.
        GLUtil.clearColorAndDepthBuffer(clearColor, DEFAULT_DEPTH_VALUE, DEFAULT_STENCIL_VALUE)
    }

    override fun swap() {
        GLFW.glfwSwapBuffers(windowHandle)
    }

    override fun makeContextCurrent() {
        if (GLFW.glfwGetCurrentContext() != windowHandle) {
            GLFW.glfwMakeContextCurrent(windowHandle)
            GLSet.setContextInformation(contextInformation)
        }
    }

    override fun release() {
        beforeDestruction()
        modelManager.releaseAllModels()
        callbacks.destroy()
        GLFW.glfwDestroyWindow(windowHandle)
        created = false
        afterDestruction()
    }

    protected fun cacheWindowSize() {
        MemoryStack.stackPush().use { stack ->
            val width = stack.mallocInt(1)
            val height = stack.mallocInt(1)
            GLFW.glfwGetWindowSize(windowHandle, width, height)
            cacheWindowSize(width.get(), height.get())
        }
    }

    override fun cacheWindowSize(width: Int, height: Int) {
        cachedWindowSize.set(width, height)
    }

    protected fun cacheWindowPosition() {
        MemoryStack.stackPush().use { stack ->
            val xPos = stack.mallocInt(1)
            val yPos = stack.mallocInt(1)
            GLFW.glfwGetWindowPos(windowHandle, xPos, yPos)
            cacheWindowPosition(xPos.get(), yPos.get())
        }
    }

    override fun cacheWindowPosition(xPos: Int, yPos: Int) {
        cachedWindowPosition.set(xPos, yPos)
    }

    protected fun cacheFramebufferSize() {
        MemoryStack.stackPush().use { stack ->
            val width = stack.mallocInt(1)
            val height = stack.mallocInt(1)
            GLFW.glfwGetFramebufferSize(windowHandle, width, height)
            cacheFramebufferSize(width.get(), height.get())
        }
    }

    override fun cacheFramebufferSize(width: Int, height: Int) {
        cachedFrameBufferSize.set(width, height)
    }

    protected fun cacheWindowContentScale() {
        MemoryStack.stackPush().use { stack ->
            val xScale = stack.mallocFloat(1)
            val yScale = stack.mallocFloat(1)
            GLFW.glfwGetWindowContentScale(windowHandle, xScale, yScale)
            cacheWindowContentScale(xScale.get(), yScale.get())
        }
    }

    override fun cacheWindowContentScale(xScale: Float, yScale: Float) {
        cachedWindowContentScale.set(xScale, yScale)
    }

    fun cacheMonitorContentScale() {
        MemoryStack.stackPush().use { stack ->
            val xScale = stack.mallocFloat(1)
            val yScale = stack.mallocFloat(1)
            // TODO: This is not correct! The windowHandle is not a monitor index.
            GLFW.glfwGetMonitorContentScale(windowHandle, xScale, yScale)
            cacheMonitorContentScale(xScale.get(), yScale.get())
        }
    }

    private fun cacheMonitorContentScale(xScale: Float, yScale: Float) {
        cachedMonitorContentScale.set(xScale, yScale)
    }

    fun cacheCursorPosition() {
        MemoryStack.stackPush().use { stack ->
            val x = stack.mallocDouble(1)
            val y = stack.mallocDouble(1)
            GLFW.glfwGetCursorPos(windowHandle, x, y)
            cacheCursorPosition(x.get(), y.get())
        }
    }

    override fun cacheCursorPosition(xPos: Double, yPos: Double) {
        cachedCursorPosition.set(floor(xPos).toInt(), floor(yPos).toInt())
    }

    // TODO: Invalidate cachedPosition in the upcoming functions ???

    // TODO: Invalidate cachedPosition in the upcoming functions ???
    override fun center() {
        // TODO: Do not calculate all video modes every time.
        val desktopVideoMode = calculateDesktopVideoMode(monitor)
        val posX = (desktopVideoMode.width() - videoMode!!.width()) / 2
        val posY = (desktopVideoMode.height() - videoMode!!.height()) / 2
        GLFW.glfwSetWindowPos(windowHandle, posX, posY)
    }

    private fun setFullscreenDesiredMonitor(desiredMonitor: Long) {
        fullscreenMonitor = calculateMonitorToUse(desiredMonitor)
    }

    private fun unsetFullscreen() {
        GLFW.glfwSetWindowMonitor(
            windowHandle, MemoryUtil.NULL,
            cachedWindowPosition.x, cachedWindowPosition.y,
            cachedWindowSize.x, cachedWindowSize.y,
            videoMode!!.refreshRate()
        )
    }

    companion object {
        private val LOG = getLogger<AbstractOpenGLWindow>()

        /**
         * The depth component of this windows default framebuffer will be cleared to this depth value whenever
         * [AbstractOpenGLWindow.clear] gets invoked.
         */
        private const val DEFAULT_DEPTH_VALUE = 1.0f

        /**
         * The stencil component of this windows default framebuffer will be cleared to this stencil value whenever
         * [AbstractOpenGLWindow.clear] gets invoked.
         */
        private const val DEFAULT_STENCIL_VALUE = 0

        /**
         * The color component of this windows default framebuffer will be cleared to this color whenever
         * [AbstractOpenGLWindow.clear] gets invoked. Note: This color can neither be changed nor freed.
         */
        private val DEFAULT_CLEAR_COLOR = buildImmutableVec4f(0.9f, 0.9f, 0.9f, 1.0f)
    }

}
