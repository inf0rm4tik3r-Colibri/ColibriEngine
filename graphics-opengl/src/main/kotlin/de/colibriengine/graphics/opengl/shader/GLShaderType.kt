package de.colibriengine.graphics.opengl.shader

import de.colibriengine.graphics.shader.ShaderType
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL32
import org.lwjgl.opengl.GL40

enum class GLShaderType(
    /** The OpenGL ID for this particular shader type. Accepted by the type parameter of CreateShader. */
    val glId: Int
) {

    VERTEX_SHADER(GL20.GL_VERTEX_SHADER),
    TESS_CONTROL_SHADER(GL40.GL_TESS_CONTROL_SHADER),
    TESS_EVALUATION_SHADER(GL40.GL_TESS_EVALUATION_SHADER),
    GEOMETRY_SHADER(GL32.GL_GEOMETRY_SHADER),
    FRAGMENT_SHADER(GL20.GL_FRAGMENT_SHADER);

    companion object {

        fun from(shaderType: ShaderType): GLShaderType {
            return when (shaderType) {
                ShaderType.VERTEX_SHADER -> VERTEX_SHADER
                ShaderType.TESS_CONTROL_SHADER -> TESS_CONTROL_SHADER
                ShaderType.TESS_EVALUATION_SHADER -> TESS_EVALUATION_SHADER
                ShaderType.GEOMETRY_SHADER -> GEOMETRY_SHADER
                ShaderType.FRAGMENT_SHADER -> FRAGMENT_SHADER
            }
        }

    }

}
