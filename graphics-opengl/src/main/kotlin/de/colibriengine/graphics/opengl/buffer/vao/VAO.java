package de.colibriengine.graphics.opengl.buffer.vao;

import de.colibriengine.graphics.opengl.buffer.GLDataType;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.graphics.Bindable;
import de.colibriengine.graphics.Creatable;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.graphics.opengl.buffer.GLBuffer;
import de.colibriengine.graphics.opengl.buffer.GLBufferTarget;
import de.colibriengine.graphics.opengl.context.ContextStateTracker;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL43.glBindVertexBuffer;
import static org.lwjgl.opengl.GL43.glVertexAttribBinding;
import static org.lwjgl.opengl.GL43.glVertexAttribFormat;
import static org.lwjgl.opengl.GL45.glCreateVertexArrays;
import static org.lwjgl.opengl.GL45.glDisableVertexArrayAttrib;
import static org.lwjgl.opengl.GL45.glEnableVertexArrayAttrib;
import static org.lwjgl.opengl.GL45.glVertexArrayAttribBinding;
import static org.lwjgl.opengl.GL45.glVertexArrayAttribFormat;
import static org.lwjgl.opengl.GL45.glVertexArrayElementBuffer;
import static org.lwjgl.opengl.GL45.glVertexArrayVertexBuffer;

/**
 * VAO - Encapsulation of OpenGL vertex array objects (VAO's).
 * A VAO is considered to be active after its {@link VAO#create()} method got called. After {@link VAO#release()} got
 * called, a VAO is no longer "active"!
 */
@SuppressWarnings("WeakerAccess")
public class VAO implements Creatable, Bindable {
    
    public static final int ONE = 1, R = 1;
    public static final int TWO = 2, RG = 2;
    public static final int THREE = 3, RGB = 3;
    public static final int FOUR = 4, RGBA = 4;
    
    /**
     * Used in {@link VAO#unbind()}. Specifies the value which must be used to break the existing vertex array object
     * binding.
     */
    public static final int NO_BOND = 0;
    
    /**
     * The vao handle must have this value assigned to it if it is currently not initialized by OpenGL.
     * (Does not represent an OpenGL id/handle.)
     */
    private static final int VAO_UNSET = -1;
    
    /**
     * The OpenGL handle of this vertex array object.
     */
    private int vao = VAO_UNSET;
    
    /**
     * Stores a reference to the index buffer which this VAO currently uses.
     * A call to {@link VAO#bind()} will also bind this index buffer if it is not null!
     * This must be reflected in the {@link ContextStateTracker}. We therefor need this reference.
     */
    private @Nullable GLBuffer indexBuffer;
    
    /**
     * Stores whether or not the VAO is currently "created".
     */
    private boolean created;
    
    private static final Logger LOG = LogUtil.getLogger(VAO.class);
    
    /**
     * Must not be called from outside this class!
     *
     * @param created
     *     If this VAO is created.
     */
    @Override
    public void setCreated(final boolean created) {
        this.created = created;
    }
    
    @Override
    public boolean isCreated() {
        return created;
    }
    
    /**
     * Creates the vao in the current OpenGL context.
     * Each consecutive call to this method will trigger a call of {@link VAO#release()}, if it was not called
     * before by the user. This allows a reusage of this object and must be done to free old GPU/driver resources!
     *
     * OpenGL context >4.5: The created vao will not be bound. The context remains unchanged.
     * OpenGL context <4.5: The created vao will be bound.
     *
     * @minContextRequired 4.3
     */
    public void create() {
        // We must release the VAO first, before it can be reassigned / recreated!
        if (created) {
            release();
        }
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            vao = glCreateVertexArrays();
        } else {
            // Traditionally bind this VAO to be able to manipulate it.
            vao = glGenVertexArrays();
            bind();
        }
        
        this.created = true;
    }
    
    /**
     * Should be called after this VAO got created / initialized.
     * Basically unbinds this VAO if it must (if it was bound in the creation process).
     *
     * @minContextRequired 3.0
     */
    public void finish() {
        unbindIfBound();
    }
    
    /**
     * Releases this vao. This VAO must be active!
     * No VAO will be bound to the context if this method gets called on the currently bound VAO.
     *
     * @throws IllegalStateException
     *     If this VAO is not active.
     * @minContextRequired 3.0
     */
    public void release() throws IllegalStateException {
        ensureCreated();
        
        // Store whether or not this VAO is bound before deleting it.
        final boolean wasBound = isBound();
        
        // Let OpenGL delete this VAO.
        // NOTE: This will unbind the VAO itself (and its possibly defined index buffer), if it was currently bound.
        glDeleteVertexArrays(vao);
        
        // Inform the CST that this VAO is no longer bound, if it was bound before its destruction.
        if (wasBound) {
            // OpenGL will not change the currently bound GL_ELEMENT_ARRAY_BUFFER when unbinding a VAO!
            /*
            if (indexBuffer != null) {
                GLUtil.getCST().setBoundBufferAt(
                    GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER, GLBuffer.NO_BOND, false
                );
            }
            */
            GLUtil.getCST().setBoundVAO(NO_BOND);
        }
        
        this.created = false;
    }

    public void releaseIfCreated() {
        if (isCreated()) {
            release();
        }
    }
    
    /**
     * Binds this vao.
     * Must be called once before a drawing command is issued.
     *
     * @minContextRequired 3.0
     */
    @Override
    public void bind() {
        // Only bind this VAO if it isn't already bound.
        if (GLUtil.getCST().getBoundVAO() != vao) {
            GL30.glBindVertexArray(vao);
            // This VAO got bound. If it uses an index buffer, this index buffer will now be bound!
            GLUtil.getCST().setBoundBufferAt(
                GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER,
                indexBuffer != null ? indexBuffer.getHandle() : GLBuffer.NO_BOND,
                false
            );
            GLUtil.getCST().setBoundVAO(vao);
        }
    }
    
    /**
     * @return Whether or not this VAO is currently bound to the current context.
     */
    @Override
    public boolean isBound() {
        return GLUtil.getCST().getBoundVAO() == vao;
    }
    
    /**
     * Erases the current VAO binding from the context.
     * After a call to this method returns, no VAO will be bound to the context.
     *
     * @throws IllegalStateException
     *     If this VAO is not currently bound.
     * @minContextRequired 3.0
     */
    @Override
    public void unbind() throws IllegalStateException {
        ensureBound();
        
        GL30.glBindVertexArray(NO_BOND);
        GL30.glBindBuffer(GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER.id, GLBuffer.NO_BOND);
        
        GLUtil.getCST().setBoundBufferAt(
            GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER, GLBuffer.NO_BOND, false
        );
        GLUtil.getCST().setBoundVAO(NO_BOND);
    }
    
    /**
     * Erases the current VAO binding from the context if (and only if) THIS VAO is the currently bound VAO.
     *
     * @minContextRequired 3.0
     */
    @Override
    public void unbindIfBound() {
        if (isBound()) {
            unbind();
        }
    }
    
    /**
     * Enables a vertex attribute location for this vertex array object.
     * The VAO must be active!
     *
     * OpenGL context >4.5: The vao must not be bound.
     * OpenGL context <4.5: The vao must be bound!
     *
     * @param attribIndex
     *     The index of the vertex attribute specified in a vertex shader:
     *     <pre>layout (location = {index}) in {type} {name};</pre>
     *
     * @minContextRequired 2.0
     */
    public void enableVertexAttrib(final int attribIndex) {
        if (attribIndex < 0) {
            throw new IllegalArgumentException("attribIndex must be positive!");
        }
        
        ensureCreated();
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glEnableVertexArrayAttrib(vao, attribIndex);
        } else {
            ensureBound();
            glEnableVertexAttribArray(attribIndex);
        }
    }
    
    /**
     * Disables a vertex attribute location for this vertex array object.
     * The VAO must be active!
     *
     * OpenGL context >4.5: The vao must not be bound.
     * OpenGL context <4.5: The vao must be bound!
     *
     * @param attribIndex
     *     The index specified in a vertex shader:
     *     <pre>layout (location = {index}) in {type} {name};</pre>
     *
     * @minContextRequired 2.0
     */
    public void disableVertexAttrib(final int attribIndex) {
        if (attribIndex < 0) {
            throw new IllegalArgumentException("attribIndex must be positive!");
        }
        
        ensureCreated();
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glDisableVertexArrayAttrib(vao, attribIndex);
        } else {
            ensureBound();
            glDisableVertexAttribArray(attribIndex);
        }
    }
    
    /**
     * Vertex data gets accessed (in a vertex shader) through "attribute indices" (layout (location = index))..
     * This data gets fetched from this VAO's "buffer binding points".
     * This method defines which buffer is getting accessed when fetching vertex data for a specific buffer binding
     * point.
     *
     * The {@link VAO#setAttribBinding(int, int)} method must be called to tell OpenGL which "attribute index" should
     * fetch its data from which "buffer binding point"!
     *
     * @param buffer
     *     The buffer which holds the vertex data.
     * @param bindingPointIndex
     *     One of this VAO's buffer binding points [0,...] to which the buffer should be bound.
     * @param offset
     *     Byte-offset at which the data starts in the given buffer (first element).
     * @param stride
     *     Distance between elements in the given buffer.
     *
     * @minContextRequired 4.3
     */
    public void setVertexBuffer(
            final @NotNull GLBuffer buffer, final int bindingPointIndex,
            final int offset, final int stride
    ) {
        if (bindingPointIndex < 0) {
            throw new IllegalArgumentException("bindingPointIndex must be positive!");
        }
        
        ensureCreated();
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glVertexArrayVertexBuffer(vao, bindingPointIndex, buffer.getHandle(), offset, stride);
        } else {
            ensureBound();
            glBindVertexBuffer(bindingPointIndex, buffer.getHandle(), offset, stride);
        }
    }
    
    /**
     * Sets the index buffer for this vao.
     *
     * OpenGL context >4.5: The index buffer gets attached to this vbo without changing the context state.
     * OpenGL context <4.5: The index buffer gets bound in order to let this vao store the binding.
     *
     * @param indexBuffer
     *     The buffer which should be used to supply drawing indices.
     *     The buffer must have been created using the {@link GLBufferTarget#GL_ELEMENT_ARRAY_BUFFER} target.
     *
     * @minContextRequired 1.5
     */
    public void setIndexBuffer(final @NotNull GLBuffer indexBuffer) {
        ensureCreated();
        
        if (indexBuffer.getTarget() != GLBufferTarget.GL_ELEMENT_ARRAY_BUFFER) {
            throw new IllegalArgumentException(
                "The index buffer must have been created using the GL_ELEMENT_ARRAY_BUFFER target!"
            );
        }
        
        // Remember which index buffer is used for this VAO.
        this.indexBuffer = indexBuffer;
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glVertexArrayElementBuffer(vao, indexBuffer.getHandle());
        } else {
            ensureBound();
            indexBuffer.bind();
        }
    }
    
    /**
     * This VAO must be active!
     *
     * @param attribIndex
     *     The index of the vertex attribute for which the format should be specified.
     * @param size
     *     One of:
     *     {@link VAO#ONE}, {@link VAO#R}
     *     {@link VAO#TWO}, {@link VAO#RG
     *     {@link VAO#THREE}, {@link VAO#RGB}
     *     {@link VAO#FOUR}, {@link VAO#RGBA}
     * @param type
     *     One of:
     *     {@link GL11#GL_BYTE}
     *     {@link GL11#GL_UNSIGNED_BYTE}
     *     {@link GL11#GL_SHORT}
     *     {@link GL11#GL_UNSIGNED_SHORT}
     *     {@link GL11#GL_INT}
     *     {@link GL11#GL_UNSIGNED_INT}
     *     {@link GL11#GL_FLOAT}
     *     {@link GL11#GL_2_BYTES}
     *     {@link GL11#GL_3_BYTES}
     *     {@link GL11#GL_4_BYTES}
     *     {@link GL11#GL_DOUBLE}
     * @param normalized
     *     If true, integer data is normalized to the range [-1, 1] or [0, 1] if it is signed or unsigned,
     *     respectively.
     *     If false, integer data is directly converted to floating point.
     * @param relativeOffset
     *     The offset, measured in basic machine units, of the first element relative to the start of the vertex
     *     buffer binding this attribute fetches from.
     *
     * @minContextRequired 4.3
     */
    public void setAttribFormat(
        final int attribIndex, final int size, final GLDataType dataType,
        final boolean normalized, final int relativeOffset
    ) {
        if (attribIndex < 0) {
            throw new IllegalArgumentException("attribIndex must be positive!");
        }
        
        ensureCreated();
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glVertexArrayAttribFormat(vao, attribIndex, size, dataType.getId(), normalized, relativeOffset);
        } else {
            ensureBound();
            glVertexAttribFormat(attribIndex, size, dataType.getId(), normalized, relativeOffset);
        }
    }
    
    /**
     * Says that data for attribute index {@code attribIndex} should be fetched from the buffer bound to binding
     * position {@code bindingPointIndex}.
     * This VAO must be active!
     *
     * @param attribIndex
     *     The vertex attribute index for which the binding should be specified.
     * @param bindingPointIndex
     *     The VAO's buffer binding point from which data should be fetched.
     *
     * @minContextRequired 4.3
     */
    public void setAttribBinding(final int attribIndex, final int bindingPointIndex) {
        if (attribIndex < 0) {
            throw new IllegalArgumentException("attribIndex must be positive!");
        }
        
        if (bindingPointIndex < 0) {
            throw new IllegalArgumentException("bindingPointIndex must be positive!");
        }
        
        ensureCreated();
        
        if (GLUtil.isContextAtLeast(4, 5)) {
            glVertexArrayAttribBinding(vao, attribIndex, bindingPointIndex);
        } else {
            ensureBound();
            glVertexAttribBinding(attribIndex, bindingPointIndex);
        }
    }
    
}
