package de.colibriengine.graphics.opengl.texture;

import java.util.List;

import de.colibriengine.graphics.texture.TextureTarget;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL40;

public enum GLTextureTarget {
    
    GL_TEXTURE_1D(GL11.GL_TEXTURE_1D,
            true, false, false),
    GL_TEXTURE_1D_ARRAY(GL30.GL_TEXTURE_1D_ARRAY,
            true, true, false),
    GL_TEXTURE_2D(GL11.GL_TEXTURE_2D,
            true, false, false),
    GL_TEXTURE_2D_ARRAY(GL30.GL_TEXTURE_2D_ARRAY,
            true, true, false),
    GL_TEXTURE_2D_MULTISAMPLE(GL32.GL_TEXTURE_2D_MULTISAMPLE,
            false, false, false),
    GL_TEXTURE_2D_MULTISAMPLE_ARRAY(GL32.GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
            false, true, false),
    GL_TEXTURE_3D(GL12.GL_TEXTURE_3D,
            true, false, false),
    GL_TEXTURE_CUBE_MAP(GL13.GL_TEXTURE_CUBE_MAP,
            true, false, true),
    GL_TEXTURE_CUBE_MAP_ARRAY(GL40.GL_TEXTURE_CUBE_MAP_ARRAY,
            true, true, true),
    GL_TEXTURE_RECTANGLE(GL31.GL_TEXTURE_RECTANGLE,
            false, false, false),
    GL_TEXTURE_BUFFER(GL31.GL_TEXTURE_BUFFER,
            false, false, false);
    
    /**
     * Immutable list view of the constants of this enum.
     */
    public static final List<GLTextureTarget> values = List.of(GLTextureTarget.values());
    
    /**
     * Stores the amount of available OpenGL buffer targets.
     */
    public static final int SIZE = GLTextureTarget.values().length;
    
    /**
     * The value of this constant in OpenGL.
     */
    public final int id;
    
    public final boolean allowsMipmaps;
    
    public final boolean allowsArrayLayers;
    
    public final boolean allowsCubeMapFaces;
    
    /**
     * Constructs a new TextureTarget enum value.
     *
     * @param id
     *     The OpenGL constant value of this target.
     * @param allowsMipmaps
     *     Whether or not this target allows mipmaps.
     * @param allowsArrayLayers
     *     Whether or not this target allows layers.
     * @param allowsCubeMapFaces
     *     Whether or not this target allows cube map faces.
     */
    GLTextureTarget(
        final int id, final boolean allowsMipmaps, final boolean allowsArrayLayers, final boolean allowsCubeMapFaces
    ) {
        this.id = id;
        this.allowsMipmaps = allowsMipmaps;
        this.allowsArrayLayers = allowsArrayLayers;
        this.allowsCubeMapFaces = allowsCubeMapFaces;
    }
    
    /**
     * Get the enum which corresponds to the specified ordinal.
     * Time complexity: O(1)
     *
     * @param ordinal
     *     The ordinal value of which the corresponding enum value should be returned.
     *
     * @return The TextureTarget of ordinal {@code ordinal}.
     */
    public static GLTextureTarget fromOrdinal(final int ordinal) {
        if (ordinal < 0 || ordinal >= values.size()) {
            throw new IllegalArgumentException("There is no enum with ordinal: " + ordinal);
        }
        return values.get(ordinal);
    }
    
    /**
     * Get the enum which corresponds to the specified id.
     * Time complexity: O(n), where n := Amount of values in this enum.
     *
     * @param id
     *     The id of which the corresponding enum value should be returned.
     *
     * @return The TextureTarget with {@code id}.
     */
    public static GLTextureTarget fromID(final int id) {
        for (final GLTextureTarget current : values) {
            if (current.id == id) {
                return current;
            }
        }
        throw new IllegalArgumentException("There is no enum with ID: " + id);
    }

    public static GLTextureTarget fromTextureTarget(TextureTarget target) {
        return switch (target) {
            case TEXTURE_2D -> GL_TEXTURE_2D;
        };
    }

}
