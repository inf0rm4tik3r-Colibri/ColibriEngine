package de.colibriengine.graphics.opengl.context

import de.colibriengine.graphics.Extension
import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL30

/** Provides information about OpenGL extensions. */
class GLExtensions {

    /** The amount of extensions which are supported on the current systems. */
    val amountOfAvailableExtensions: Int = GL11.glGetInteger(GL30.GL_NUM_EXTENSIONS)

    /** All extension names in ascending order. */
    private val extensions: Array<Extension>

    /**
     * Map of extension name hashes to their actual names. Allows for a performant check if a specific extension is
     * supported.
     */
    private val map: Map<Int, Extension>

    /**
     * Load all available extensions into `extensions` and sorts the array for faster access times.
     * The method must (and will) be called by the engines startup routine.
     */
    init {
        Array(amountOfAvailableExtensions) {
            getExtension(it)
        }.also {
            it.sort()
            extensions = it
        }
        map = mutableMapOf()
        extensions.forEach {
            map[it.hashCode()] = it
        }
    }

    private fun getExtension(atIndex: Int): Extension =
        Extension(
            GL30.glGetStringi(GL11.GL_EXTENSIONS, atIndex)
                ?: throw IllegalArgumentException("No extension at index $atIndex.")
        )

    /** Returns true if [extensionName] is the name of a supported extension. */
    fun isSupported(extensionName: String): Boolean = map.containsKey(extensionName.hashCode())

    fun logAvailableExtensions() = extensions.forEach { LOG.info(it) }

    companion object {
        private val LOG = getLogger<GLExtensions>()
    }

}
