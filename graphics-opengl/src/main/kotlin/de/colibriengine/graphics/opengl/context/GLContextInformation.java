package de.colibriengine.graphics.opengl.context;

import de.colibriengine.graphics.opengl.GLGet;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2i.StdVec2i;
import de.colibriengine.math.vector.vec2i.Vec2i;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.util.StringUtil;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;

// TODO: Add OpenAL context information..
@SuppressWarnings("WeakerAccess")
public class GLContextInformation {

    private static final Logger LOG = LogUtil.getLogger(GLContextInformation.class);

    private @NotNull Vec2i contextVersion;

    private @NotNull GLExtensions glExtensions;

    private @NotNull String glVersion = "";
    private @NotNull String glShadingLanguageVersion = "";
    private @NotNull String glVendor = "";
    private @NotNull String glRenderer = "";

    /**
     * Implementation defined constants.
     */
    private int
            maxTextureImageUnits,
            maxCombinedTextureImageUnits,
            maxVertexAttribBindings,
            maxRenderbufferSize;

    private @Nullable GLCapabilities caps;

    /**
     * Used to keep track of the current state of this context inside the Java application.
     * Removes the need to constantly bother the driver whith questions like: "What' currently bound there?".
     * Enables greater debugging capabilities.
     */
    private final @NotNull ContextStateTracker cst;

    /**
     * CONTEXT NOT JET PRESENT WHEN EXECUTED!
     */
    public GLContextInformation() {
        cst = new ContextStateTracker();
    }

    /**
     * Load the information. A GL context must be present!
     */
    public void load(final long windowId) {
        contextVersion = new StdVec2i(
                GLFW.glfwGetWindowAttrib(windowId, GLFW.GLFW_CONTEXT_VERSION_MAJOR),
                GLFW.glfwGetWindowAttrib(windowId, GLFW.GLFW_CONTEXT_VERSION_MINOR)
        );

        glVersion = GLGet.version();
        glShadingLanguageVersion = GLGet.shadingLanguageVersion();
        glVendor = GLGet.vendor();
        glRenderer = GLGet.renderer();

        maxTextureImageUnits = GLGet.maxTextureImageUnits();
        maxCombinedTextureImageUnits = GLGet.maxCombinedTextureImageUnits();
        maxVertexAttribBindings = GLGet
                .maxVertexAttribBindings(); // TODO: What if context version is below 4.3 ??? -> Error?
        maxRenderbufferSize = GLGet.maxRenderbufferSize();

        // All available extensions should be known,
        // so that the engine can query the availability of certain extensions.
        glExtensions = new GLExtensions();

        // The ContextStateTracker must get initialized with an OpenGL context being active.
        // It must access some information provided by OpenGL.
        cst.init(this);

        caps = GL.createCapabilities(true);
    }

    /**
     * Prints the information provided through {@link GLContextInformation#getInfo()} line by line into the log.
     */
    public void printInfo() {
        final String info = getInfo();
        for (final String part : info.split(StringUtil.LINE_SEPARATOR)) {
            LOG.info(part);
        }
    }

    /**
     * Collects OpenGL information, containing:
     * <ul>
     * <li>OpenGL version</li>
     * <li>GPU vendor</li>
     * <li>GPU name</li>
     * <li>Highest supported shader version</li>
     * <li>Available amount of extensions</li>
     * <li>... GL - constants</li>
     * </ul>
     *
     * @return A formatted String containing the information mentioned above. Each in a separate line.
     */
    public String getInfo() {
        return
                "OpenGL Version    :  " + glVersion + StringUtil.LINE_SEPARATOR +
                        "OpenGL Vendor     :  " + glVendor + StringUtil.LINE_SEPARATOR +
                        "OpenGL Renderer   :  " + glRenderer + StringUtil.LINE_SEPARATOR +
                        "OpenGL ShadLangVer:  " + glShadingLanguageVersion + StringUtil.LINE_SEPARATOR +
                        "OpenGL Extensions :  " + glExtensions.getAmountOfAvailableExtensions() + " supported extensions" +
                        StringUtil.LINE_SEPARATOR +
                        "GL_MAX_TEXTURE_IMAGE_UNITS = " + maxTextureImageUnits + StringUtil.LINE_SEPARATOR +
                        "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = " + maxCombinedTextureImageUnits + StringUtil.LINE_SEPARATOR +
                        "GL_MAX_VERTEX_ATTRIB_BINDINGS = " + maxVertexAttribBindings + StringUtil.LINE_SEPARATOR;
    }

    /**
     * Checks if the specified texture unit is supported by / available in the current system.
     *
     * @param textureUnit The texture-unit-index to check.
     * @return true / false
     */
    public boolean isTextureUnitSupported(final int textureUnit) {
        return textureUnit < maxTextureImageUnits;
    }

    /**
     * Ensures that the specified texture unit is supported by the current system.
     *
     * @param textureUnit The texture unit to check.
     * @throws IllegalArgumentException If the specified texture unit is not supported.
     * @see GLContextInformation#isTextureUnitSupported(int)
     */
    public void ensureTextureUnitSupported(final int textureUnit) throws IllegalArgumentException {
        if (!isTextureUnitSupported(textureUnit)) {
            throw new IllegalArgumentException("This texture unit is not supported by this system: " + textureUnit);
        }
    }

    public @Nullable GLCapabilities getCaps() {
        return caps;
    }

    public @NotNull ContextStateTracker getCST() {
        return cst;
    }

    /**
     * Checks whether the context is greater then or equal to the given context version.
     *
     * @param major The major OpenGL version (e.g. the 4 of version 4.5)
     * @param minor The minor OpenGL version (e.g. the 5 of version 4.5)
     * @return true, if the context version is higher then major.minor (e.g. context: 4.5 >= (major.minor: 3.3))
     */
    public boolean isContextAtLeast(final int major, final int minor) {
        return major < contextVersion.getX() ||
                major == contextVersion.getX() &&
                        minor <= contextVersion.getY();
    }

    /**
     * Returns the version of the context.
     *
     * @return A vector. Major version is stored as x, minor is stored as y.
     */
    public @NotNull Vec2iAccessor getContextVersion() {
        return contextVersion;
    }

    public GLExtensions getGLExtensions() {
        return glExtensions;
    }

    public @NotNull String getGlVersion() {
        return glVersion;
    }

    public @NotNull String getGlVendor() {
        return glVendor;
    }

    public @NotNull String getGlRenderer() {
        return glRenderer;
    }

    public @NotNull String getGlShadingLanguageVersion() {
        return glShadingLanguageVersion;
    }

    public int getMaxTextureImageUnits() {
        return maxTextureImageUnits;
    }

    public int getMaxCombinedTextureImageUnits() {
        return maxCombinedTextureImageUnits;
    }

    public int getMaxVertexAttribBindings() {
        return maxVertexAttribBindings;
    }

    public int getMaxRenderbufferSize() {
        return maxRenderbufferSize;
    }

}
