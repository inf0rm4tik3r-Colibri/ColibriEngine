package de.colibriengine.graphics.opengl.texture

import de.colibriengine.asset.ResourceName
import de.colibriengine.asset.image.Image
import de.colibriengine.graphics.texture.ImageFormat
import de.colibriengine.graphics.texture.TextureFactory
import de.colibriengine.graphics.texture.TextureOptions

// TODO: Replace with Builder-Pattern
object OpenGLTextureFactory : TextureFactory {

    @Deprecated("Only for Java-Interop")
    fun createWithString(
        resourceName: String,
        options: TextureOptions,
        format: ImageFormat,
        width: Int,
        height: Int
    ): OpenGlTexture {
        return create(ResourceName(resourceName), options, format, width.toUInt(), height.toUInt())
    }

    override fun create(
        resourceName: ResourceName,
        options: TextureOptions,
        format: ImageFormat,
        width: UInt,
        height: UInt
    ): OpenGlTexture {
        val texture = OpenGlTexture(resourceName.s, options)
        texture.create()
        texture.setWrapType(GLTextureWrapType.from(options.wrapType))
        texture.setMinFilterType(GLTextureFilterType.from(options.minFilter))
        texture.setMagFilterType(GLTextureFilterType.from(options.magFilter))
        texture.levels = if (options.generateMipmaps) 5 else 1
        texture.setDimensions(width.toInt(), height.toInt()) // TODO: Use as UInt
        texture.format = GLImageFormat.fromImageFormat(format)
        texture.createDataStore()
        if (options.generateMipmaps) {
            texture.generateMipMaps()
        }
        texture.finish()
        return texture
    }

    override fun createFromImage(
        image: Image,
        options: TextureOptions,
        format: ImageFormat
    ): OpenGlTexture {
        val texture = OpenGlTexture(image.resourceName.s, options)
        texture.create()
        texture.setWrapType(GLTextureWrapType.from(options.wrapType))
        texture.setMinFilterType(GLTextureFilterType.from(options.minFilter))
        texture.setMagFilterType(GLTextureFilterType.from(options.magFilter))
        texture.levels = if (options.generateMipmaps) 5 else 1
        texture.setDimensions(image.width.toInt(), image.height.toInt())
        texture.format = GLImageFormat.fromImageFormat(format)
        texture.createDataStore()
        texture.fill(1, texelDataFormat(image.components.toInt()), GLTexelDataType.GL_UNSIGNED_BYTE, image.data)
        if (options.generateMipmaps) {
            texture.generateMipMaps()
        }
        texture.finish()
        return texture
    }

    override fun createFromImageSRBG(
        image: Image,
        options: TextureOptions
    ): OpenGlTexture {
        val texture = OpenGlTexture(image.resourceName.s, options)
        texture.create()
        texture.setWrapType(GLTextureWrapType.from(options.wrapType))
        texture.setMinFilterType(GLTextureFilterType.from(options.minFilter))
        texture.setMagFilterType(GLTextureFilterType.from(options.magFilter))
        texture.levels = if (options.generateMipmaps) 5 else 1
        texture.setDimensions(image.width.toInt(), image.height.toInt())
        texture.format = imageFormat(image.components.toInt())
        texture.createDataStore()
        texture.fill(1, texelDataFormat(image.components.toInt()), GLTexelDataType.GL_UNSIGNED_BYTE, image.data)
        if (options.generateMipmaps) {
            texture.generateMipMaps()
        }
        texture.finish()
        return texture
    }

    private fun imageFormat(components: Int): GLImageFormat {
        return when (components) {
            1 -> GLImageFormat.GL_SRGB8 // GL_R8
            2 -> GLImageFormat.GL_SRGB8 // GL_RG8
            3 -> GLImageFormat.GL_SRGB8
            4 -> GLImageFormat.GL_SRGB8_ALPHA8
            else -> throw IllegalArgumentException("Unexpected amount ot components.")
        }
    }

    private fun texelDataFormat(components: Int): GLTexelDataFormat {
        return when (components) {
            1 -> GLTexelDataFormat.GL_RED
            2 -> GLTexelDataFormat.GL_RG
            3 -> GLTexelDataFormat.GL_RGB
            4 -> GLTexelDataFormat.GL_RGBA
            else -> throw IllegalArgumentException("Unexpected amount ot components.")
        }
    }

}
