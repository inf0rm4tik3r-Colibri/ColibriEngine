package de.colibriengine.graphics.opengl.rendering

import de.colibriengine.graphics.rendering.PrimitiveMode
import org.lwjgl.opengl.GL11

enum class GLPrimitiveMode(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    POINTS(GL11.GL_POINTS),
    LINES(GL11.GL_LINES),
    TRIANGLES(GL11.GL_TRIANGLES);

    fun toPrimitiveMode(): PrimitiveMode {
        return when (this) {
            POINTS -> PrimitiveMode.POINTS
            LINES -> PrimitiveMode.LINES
            TRIANGLES -> PrimitiveMode.TRIANGLES
        }
    }

    companion object {

        fun from(primitiveMode: PrimitiveMode): GLPrimitiveMode {
            return when (primitiveMode) {
                PrimitiveMode.POINTS -> POINTS
                PrimitiveMode.LINES -> LINES
                PrimitiveMode.TRIANGLES -> TRIANGLES
            }
        }

    }

}
