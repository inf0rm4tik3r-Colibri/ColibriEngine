package de.colibriengine.graphics.opengl.buffer.fbo;

import de.colibriengine.graphics.fbo.FBOBlitType;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.graphics.opengl.texture.GLTextureFilterType;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import static org.lwjgl.opengl.GL11.GL_BACK;

/**
 * FBOUtil - Utility functions for frame buffer objects (FBO's).
 */
public class FBOUtil {
    
    // TODO: CST conform ?
    
    private static final int ZERO_I = 0;
    
    /**
     * Copies (blits) the {@code sourceTextureIndex} from the {@code source} FBO to the {@code targetTextureIndex}
     * from the {@code target} FBO.
     *
     * @param source
     *     The FBO from which to copy.
     * @param sourceTextureIndex
     *     The texture of the {@code source} FBO to copy from.
     * @param sourceTextureSize
     *     The size / dimensions of the {@code source} texture.
     * @param target
     *     The target FBO to blit into or {@code null} to blit into the default frame buffer.
     * @param targetTextureIndex
     *     The texture of the {@code target} FBO which should be filled with data. Falls back to GL_BACK if
     *     {@code target} is set to {@code null}.
     * @param targetTextureSize
     *     The size / dimensions of the {@code target} texture.
     * @param blitType
     *     How the blit should be performed:
     *     <ul>
     *     <li>
     *     - {@link FBOBlitType#BLIT_NORMAL}:
     *     Centers the source image in its original size in the target image.
     *     </li>
     *     <li>
     *     - {@link FBOBlitType#BLIT_STRETCHED}:
     *     Stretches the source image on x and y so that it fills up the target image. May produce distortion!
     *     </li>
     *     <li>
     *     - {@link FBOBlitType#BLIT_FILL}:
     *     Stretches the source image on x or y (depending on its space left to the edges of the texture) to fill the
     *     target texture. The stretch will neither crop nor distort the source image.
     *     </li>
     *     </ul>
     * @param mask
     *     The bitwise OR of the flags indicating which buffers are to be copied. One of:
     *     {@link GL11#GL_COLOR_BUFFER_BIT},
     *     {@link GL11#GL_DEPTH_BUFFER_BIT},
     *     {@link GL11#GL_STENCIL_BUFFER_BIT}
     */
    public static void blitTo(
            final @NotNull GLFBO source, final int sourceTextureIndex,
            final @NotNull Vec2iAccessor sourceTextureSize,
            final @Nullable GLFBO target, final int targetTextureIndex,
            final @NotNull Vec2iAccessor targetTextureSize,
            final @NotNull FBOBlitType blitType, final int mask, final @NotNull GLTextureFilterType filterType
    ) {
        final int finalTargetTextureIndex = target != null ? targetTextureIndex : GL_BACK;
        
        switch (blitType) {
            // Centers the source image in its original size in the target image.
            case BLIT_NORMAL: {
                final int xOffset = (targetTextureSize.getX() - sourceTextureSize.getX()) / 2;
                final int yOffset = (targetTextureSize.getY() - sourceTextureSize.getY()) / 2;
                
                blitTexture(
                    // SRC
                    source, sourceTextureIndex,
                    ZERO_I, ZERO_I,
                    sourceTextureSize.getX(), sourceTextureSize.getY(),
                    // DEST
                    target, finalTargetTextureIndex,
                    xOffset, yOffset,
                    sourceTextureSize.getX(), sourceTextureSize.getY(),
                    // BLIT PARAMETERS
                    mask, filterType
                );
            }
            break;
            
            // Stretches the source image on x and y so that it fills up the target image. May produce distortion!
            case BLIT_STRETCHED: {
                blitTexture(
                    // SRC
                    source, sourceTextureIndex,
                    ZERO_I, ZERO_I,
                    sourceTextureSize.getX(), sourceTextureSize.getY(),
                    // DEST
                    target, finalTargetTextureIndex,
                    ZERO_I, ZERO_I,
                    targetTextureSize.getX(), targetTextureSize.getY(),
                    // BLIT PARAMETERS
                    mask, filterType
                );
            }
            break;
            
            // Stretches the source image on x or y (depending on its space left to the edges of the texture) to
            // fill the target texture. The stretch will neither crop nor distort the source image.
            case BLIT_FILL: {
                final int   diffX             = targetTextureSize.getX() - sourceTextureSize.getX();
                final int   diffY             = targetTextureSize.getY() - sourceTextureSize.getY();
                final float sourceAspectRatio = (float) sourceTextureSize.getX() / (float) sourceTextureSize.getY();
                
                final int xStart, xEnd, yStart, yEnd;
                
                if (diffX > diffY) {
                    // Stretch in y-direction -> final height equals the height of the target texture.
                    final int targetResolutionX = (int) ((float) targetTextureSize.getY() * sourceAspectRatio);
                    
                    xStart = (int) ((targetTextureSize.getX() - targetResolutionX) / 2.0f);
                    xEnd = xStart + targetResolutionX;
                    yStart = ZERO_I;
                    yEnd = targetTextureSize.getY();
                } else {
                    // Stretch in x-direction -> final width equals the width of the target texture.
                    final int targetResolutionY = (int) ((float) targetTextureSize.getX() / sourceAspectRatio);
                    
                    xStart = ZERO_I;
                    xEnd = targetTextureSize.getX();
                    yStart = (int) ((targetTextureSize.getY() - targetResolutionY) / 2.0f);
                    yEnd = yStart + targetResolutionY;
                }
                
                blitTexture(
                    // SRC
                    source, sourceTextureIndex,
                    ZERO_I, ZERO_I,
                    sourceTextureSize.getX(), sourceTextureSize.getY(),
                    // DEST
                    target, finalTargetTextureIndex,
                    xStart, yStart,
                    xEnd, yEnd,
                    // BLIT PARAMETERS
                    mask, filterType
                );
            }
            break;
        }
    }
    
    // TODO: Revisit
    // TODO: Pass FBO instead if int handle.
    
    /**
     * <p>
     * Blit's a texture. This method takes care of binding the correct buffers and textures.
     * </p>
     * <p>
     * Use 0 as {@code srcFBO}/{@code destFBO} to access the default FBO.
     * Use 0, 1, ... , n as {@code srcTexture}/{@code destTexture}.
     * Allowed {@code mask} formats are: GL_COLOR_BUFFER_BIT, GL_DEPTH_BUFFER_BIT and GL_STENCIL_BUFFER_BIT.
     * Use bitwise OR to combine mask-formats.
     * </p>
     *
     * <pre>
     * How the texture parameters work:
     *  ____________________________
     * |__________________srcHeight_|
     * |____________________srcWidth|
     * |srcY________________________|
     * |_srcX_______________________|
     * </pre>
     *
     * @param srcFBO
     *     The source FBO which has
     * @param srcTextureIndex
     *     the source texture.
     *     Rectangular definition of the part that gets copied.
     * @param srcX
     *     Lower left x coordinate.
     * @param srcY
     *     Lower left y coordinate.
     * @param srcTextureWidth
     *     Width of the destination texture.
     * @param srcTextureHeight
     *     Height of the destination texture.
     *     <p>
     * @param destFBO
     *     The destination FBO which has
     * @param destTextureIndex
     *     the destination texture.
     *     Rectangular definition of the destination framebuffer texture.
     * @param destX
     *     Same as above. Now the part of the destination texture is defined.
     * @param destY
     *     -
     * @param destTextureWidth
     *     -
     * @param destTextureHeight
     *     -
     *     </p>
     *     <p>
     * @param mask
     *     Bitfield that specifies the mask.
     * @param filter
     *     Filtering method. Either {@link GL11#GL_NEAREST} or {@link GL11#GL_LINEAR}
     *     </p>
     *
     * @minContextRequired 3.0
     */
    public static void blitTexture(
            final @NotNull GLFBO srcFBO, final int srcTextureIndex,
            final int srcX, final int srcY,
            final int srcTextureWidth, final int srcTextureHeight,
            final @Nullable GLFBO destFBO, final int destTextureIndex,
            final int destX, final int destY,
            final int destTextureWidth, final int destTextureHeight,
            final int mask, final @NotNull GLTextureFilterType filter
    ) {
        srcFBO.bindForReading();
        srcFBO.setReadBuffer(srcTextureIndex);
        
        if (destFBO != null) {
            destFBO.bindForDrawing();
            destFBO.setDrawBuffer(destTextureIndex);
        } else {
            GLFBO.bindDefaultFBOForDrawing();
        }
        
        //GL45.glBlitNamedFramebuffer();
        GL30.glBlitFramebuffer(
            srcX, srcY, srcTextureWidth, srcTextureHeight,
            destX, destY, destTextureWidth, destTextureHeight,
            mask, filter.getId()
        );
    }
    
}
