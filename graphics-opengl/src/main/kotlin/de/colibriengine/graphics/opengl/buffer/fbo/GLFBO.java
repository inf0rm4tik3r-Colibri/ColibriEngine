package de.colibriengine.graphics.opengl.buffer.fbo;

import de.colibriengine.graphics.fbo.FBO;
import de.colibriengine.graphics.opengl.GLUtil;
import de.colibriengine.graphics.opengl.buffer.rbo.RBO;
import de.colibriengine.graphics.opengl.texture.*;
import de.colibriengine.graphics.texture.*;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec4f.*;
import de.colibriengine.util.IntList;
import kotlin.Unit;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_COLOR;
import static org.lwjgl.opengl.GL30.*;

/**
 * Framebuffer object (FBO)
 */
// TODO: Create FBO interface in graphics-api
public abstract class GLFBO implements FBO { // TODO: implements Creatable, Bindable ??

    public static boolean DEBUG = false;

    public static final int DEFAULT_FRAMEBUFFER_ID = 0;

    private static final String MSG_FBO_INCOMPLETE = "FBO incomplete!";

    /**
     * The buffer handle must have this value assigned to it if it is currently not initialized by OpenGL.
     * (Does not represent an OpenGL id/handle.)
     */
    private static final int FBO_UNSET = -1;

    /**
     * The OpenGL handle of this frame buffer object.
     */
    private int fbo = FBO_UNSET;

    /**
     * Stores whether this frame buffer object is created.
     */
    private boolean created;

    private HashMap<Integer, Integer> indexToAttachment;
    private ArrayList<Texture> textures;
    private ArrayList<RBO> renderbuffers;
    private OpenGlTexture depthTexture;
    private RBO depthStencilRenderbuffer;

    /**
     * Stores the buffers which are currently set for drawing.
     */
    private ArrayList<Integer> currentDrawBuffers;
    private boolean allBuffersSetForDrawing;

    private static final ImmutableVec4f DEFAULT_CLEAR_COLOR = StdVec4fFactory.Companion.getBLACK();
    private @NotNull Vec4f clearColor;
    private final @NotNull FloatBuffer clearColorBuffer;

    private static final float DEFAULT_CLEAR_DEPTH = 1.0f;
    private float clearDepth = DEFAULT_CLEAR_DEPTH;

    private static final int DEFAULT_CLEAR_STENCIL = 0;
    private int clearStencil = DEFAULT_CLEAR_STENCIL;

    private static final Logger LOG = LogUtil.getLogger(GLFBO.class);

    public GLFBO() {
        // Create the clear color store.
        clearColor = new StdVec4f();

        // Create the clear color FloatBuffer store.
        clearColorBuffer = BufferUtils.createFloatBuffer(Vec4f.FLOATS);

        // Set the clear color to the default clear color specified in this class.
        setClearColor(DEFAULT_CLEAR_COLOR);
    }

    /**
     * Initializes this FBO.
     * The implementation of this method should:
     * <ul>
     * <li>call {@link GLFBO#create to create the FBO}</li>
     * <li>call {@link GLFBO#addTexture} for any color texture necessary</li>
     * </ul>
     */
    protected abstract void init();

    public void create() {
        indexToAttachment = new HashMap<>();
        textures = new ArrayList<>();
        renderbuffers = new ArrayList<>();
        depthTexture = null;

        currentDrawBuffers = new ArrayList<>();
        allBuffersSetForDrawing = false;

        //FrameBuffer-Handle erstellen
        fbo = glGenFramebuffers();

        //noinspection CallToSimpleSetterFromWithinClass
        setCreated(true);

        //Bind des FrameBuffers: alle spaeteren Modifizierungen werden dieses FBO beeinflussen
        bind(); // TODO Spaeter Aufruf mit GL_READ_FRAMEBUFFER und GL_DRAW_FRAMEBUFFER
    }

    @Override
    public void setCreated(final boolean created) {
        this.created = created;
    }

    @Override
    public boolean isCreated() {
        return created;
    }

    /**
     * NOTE: This method will do nothing if this FBO is not created.
     */
    public void destroy() {
        if (isCreated()) {
            // TODO: Should the framebuffer be deleted after textures?
            GL30.glDeleteFramebuffers(fbo);
            fbo = FBO_UNSET;
            // TODO: Do we really want to release all registered textures and renderbuffers?
            // TODO: They might be used elsewhere!
            for (final Texture texture : textures) {
                texture.destroy();
            }
            for (final RBO renderbuffer : renderbuffers) {
                renderbuffer.release();
            }
            if (depthTexture != null) {
                depthTexture.destroy();
            }
            if (depthStencilRenderbuffer != null) {
                depthStencilRenderbuffer.release();
            }
            textures.clear();
            renderbuffers.clear();
            indexToAttachment.clear();
            // TODO: Tell CST no bond
        }
    }

    /**
     * TEST TEST
     */
    public void addTexture(
            final int width, final int height,
            final @NotNull ImageFormat internalFormat,
            final @NotNull GLTexelDataFormat texelDataFormat, final @NotNull GLTexelDataType texelDataType,
            final @NotNull TextureFilterType minFilter, final @NotNull TextureFilterType magFilter,
            final @NotNull TextureWrapType wrapType, final @NotNull Vec4fAccessor borderColor
    ) {
        // TODO: texelDataFormat and texelDataType not used!
        final TextureOptions options = new TextureOptions(TextureTarget.TEXTURE_2D, wrapType, minFilter, magFilter, false, borderColor);
        final OpenGlTexture texture = OpenGLTextureFactory.INSTANCE.createWithString("internal-fbo", options, internalFormat, width, height);


        final int nextAttachmentArrayIndex = textures.size() + renderbuffers.size();
        final int nextAttachmentIndex = GL_COLOR_ATTACHMENT0 + nextAttachmentArrayIndex;

        // Attach the texture to the framebuffer.
        // 4. parameter = 0 -> remove binding / 5. parameter -> mipmap level
        GL30.glFramebufferTexture2D(
                GL_DRAW_FRAMEBUFFER, nextAttachmentIndex,
                GLTextureTarget.GL_TEXTURE_2D.id, texture.getOpenGLHandle(), 0
        );

        // Save the texture and the attachment used.
        indexToAttachment.put(nextAttachmentArrayIndex, nextAttachmentIndex);
        textures.add(texture);
    }

    public void addTexture(
            final Vec2iAccessor dimension,
            final @NotNull ImageFormat format,
            final @NotNull GLTexelDataFormat texelDataFormat, final @NotNull GLTexelDataType texelDataType,
            final @NotNull TextureFilterType minFilter, final @NotNull TextureFilterType magFilter
    ) {
        addTexture(
                dimension.getX(), dimension.getY(),
                format, texelDataFormat, texelDataType, minFilter, magFilter
        );
    }

    public void addTexture(
            final int width, final int height,
            final @NotNull ImageFormat internalFormat,
            final @NotNull GLTexelDataFormat texelDataFormat, final @NotNull GLTexelDataType texelDataType,
            final @NotNull TextureFilterType minFilter, final @NotNull TextureFilterType magFilter
    ) {
        addTexture(
                width, height, internalFormat, texelDataFormat, texelDataType, minFilter, magFilter,
                TextureWrapType.REPEAT, StdVec4fFactory.Companion.getWHITE()
        );
    }

    public void addTexture(
            final Vec2iAccessor dimension,
            final @NotNull ImageFormat format,
            final @NotNull TextureFilterType filterType
    ) {
        addTexture(dimension.getX(), dimension.getY(), format, filterType);
    }

    public void addTexture(
            final int width, final int height,
            final @NotNull ImageFormat internalFormat,
            final @NotNull TextureFilterType filterType
    ) {
        addTexture(
                width, height,
                internalFormat, GLTexelDataFormat.GL_RGBA, GLTexelDataType.GL_INT,
                filterType, filterType
        );
    }

    protected void initDepthTexture(
            final Vec2iAccessor dimension,
            final @NotNull ImageFormat internalFormat,
            final @NotNull GLFBOAttachment attachment
    ) {
        initDepthTexture(dimension.getX(), dimension.getY(), internalFormat, attachment);
    }

    protected void initDepthTexture(
            final int width, final int height,
            final @NotNull ImageFormat format,
            final @NotNull GLFBOAttachment attachment
    ) {
        // TODO: when already initialized?
        TextureOptions options = new TextureOptions(
                TextureTarget.TEXTURE_2D,
                TextureWrapType.CLAMP_TO_BORDER,
                TextureFilterType.NEAREST, TextureFilterType.NEAREST,
                false, StdVec4fFactory.Companion.getWHITE()
        );
        depthTexture = OpenGLTextureFactory.INSTANCE.createWithString(
                "fbo-depth", options, format, width, height);

        // Attach the texture to the framebuffer.
        // TODO: code paths
        GL30.glFramebufferTexture2D(
                GL_DRAW_FRAMEBUFFER, attachment.getId(),
                depthTexture.getGLTarget().id, depthTexture.getOpenGLHandle(), 0
        );
    }

    /**
     * Attaches the specified {@code renderbuffer} ({@link RBO}) to the specified framebuffer object attachment.
     *
     * @param attachment   The OpenGL framebuffer object attachment at which the renderbuffer should be attached.
     * @param renderbuffer The renderbuffer to attach.
     * @throws UnsupportedOperationException If this operation can not be performed due to too old context being used.
     */
    private void attachRenderbuffer(final @NotNull GLFBOAttachment attachment, final @NotNull RBO renderbuffer)
            throws UnsupportedOperationException {
        attachRenderbuffer(attachment.getId(), renderbuffer);
    }

    /**
     * int-version of {@link GLFBO#attachRenderbuffer(GLFBOAttachment, RBO)}.
     * Prefer the method which gets the {@link GLFBOAttachment} argument so that you have type safety.
     *
     * @param attachmentID The OpenGL framebuffer object attachment id at which the renderbuffer should be attached.
     * @param renderbuffer The renderbuffer to attach.
     * @throws UnsupportedOperationException If this operation can not be performed due to too old context being used.
     */
    private void attachRenderbuffer(final int attachmentID, final @NotNull RBO renderbuffer)
            throws UnsupportedOperationException {
        if (GLUtil.isContextAtLeast(4, 5)) {
            GL45.glNamedFramebufferRenderbuffer(
                    fbo, attachmentID, GL30.GL_RENDERBUFFER, renderbuffer.getOpenGLHandle()
            );
        } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
            ARBDirectStateAccess.glNamedFramebufferRenderbuffer(
                    fbo, attachmentID, GL30.GL_RENDERBUFFER, renderbuffer.getOpenGLHandle()
            );
        } else if (GLUtil.getCaps().GL_EXT_direct_state_access) {
            EXTDirectStateAccess.glNamedFramebufferRenderbufferEXT(
                    fbo, attachmentID, GL30.GL_RENDERBUFFER, renderbuffer.getOpenGLHandle()
            );
        } else if (GLUtil.isContextAtLeast(3, 0)) {
            //ensureBound(); TODO: implement
            GL30.glFramebufferRenderbuffer(
                    GL_FRAMEBUFFER, attachmentID, GL30.GL_RENDERBUFFER, renderbuffer.getOpenGLHandle()
            );
        } else if (GLUtil.getCaps().GL_ARB_framebuffer_object) {
            // TODO: convert to arb attachment id.
            //ensureBound(); TODO: implement
            ARBFramebufferObject.glFramebufferRenderbuffer(
                    ARBFramebufferObject.GL_FRAMEBUFFER, attachmentID, ARBFramebufferObject.GL_RENDERBUFFER,
                    renderbuffer.getOpenGLHandle()
            );
        } else if (GLUtil.getCaps().GL_EXT_framebuffer_object) {
            // TODO: convert to ext attachment id. Filter GL_DEPTH_STENCIL as it has no EXT equivalent.
            //ensureBound(); TODO: implement
            EXTFramebufferObject.glFramebufferRenderbufferEXT(
                    EXTFramebufferObject.GL_FRAMEBUFFER_EXT, attachmentID, ARBFramebufferObject.GL_RENDERBUFFER,
                    renderbuffer.getOpenGLHandle()
            );
        } else {
            GLUtil.throwNoPathError();
        }
    }

    public void addRenderbuffer(
            final Vec2iAccessor dimension,
            final @NotNull ImageFormat imageFormat
    ) {
        addRenderbuffer(dimension.getX(), dimension.getY(), imageFormat);
    }

    /**
     * Creates a renderbuffer in the specified dimensions and format and adds it to this framebuffer object.
     *
     * @param width       The width of the renderbuffer to create.
     * @param height      The height of the renderbuffer to create.
     * @param imageFormat The internal format of the renderbuffer.
     *                    Depth and Depth/Stencil buffers are treated separately by this method.
     */
    public void addRenderbuffer(
            final int width, final int height,
            final @NotNull ImageFormat imageFormat
    ) {
        // The image format can be a depth format.
        if (imageFormat == ImageFormat.DEPTH_COMPONENT ||
                imageFormat == ImageFormat.DEPTH_COMPONENT16 ||
                imageFormat == ImageFormat.DEPTH_COMPONENT24 ||
                imageFormat == ImageFormat.DEPTH_COMPONENT32 ||
                imageFormat == ImageFormat.DEPTH_COMPONENT32F) {
            // TODO: implement depth-only buffer handling.
            throw new UnsupportedOperationException("Not implemented");
            //initDepthBuffer(width, height, imageFormat);
            // return;
        }

        // The image format can be a combined depth/stencil format.
        if (imageFormat == ImageFormat.DEPTH_STENCIL ||
                imageFormat == ImageFormat.DEPTH24_STENCIL8 ||
                imageFormat == ImageFormat.DEPTH32F_STENCIL8) {
            initDepthStencilRenderbuffer(width, height, imageFormat);
            return;
        }

        // Otherwise create a generic renderbuffer.
        final @NotNull RBO renderbuffer = new RBO();
        renderbuffer.create();
        renderbuffer.setFormat(imageFormat);
        renderbuffer.setResolution(width, height);
        renderbuffer.createStorage();
        renderbuffer.finish();

        addRenderbuffer(renderbuffer);
    }

    /**
     * Adds the specified renderbuffer to this framebuffer.
     *
     * @param renderbuffer The renderbuffer which should be added to this framebuffer.
     */
    public void addRenderbuffer(final @NotNull RBO renderbuffer) {
        ensureCreated();

        final int nextAttachmentArrayIndex = textures.size() + renderbuffers.size();
        final int nextAttachmentID = GL_COLOR_ATTACHMENT0 + nextAttachmentArrayIndex;

        // Attach the renderbuffer to this framebuffer.
        attachRenderbuffer(nextAttachmentID, renderbuffer);

        // Save the renderbuffer and the attachment used.
        indexToAttachment.put(nextAttachmentArrayIndex, nextAttachmentID);
        renderbuffers.add(renderbuffer);
    }

    public void setDepthStencilRenderbuffer(final @NotNull RBO depthStencilRenderbuffer) throws IllegalStateException {
        ensureCreated();

        attachRenderbuffer(GLFBOAttachment.GL_DEPTH_STENCIL_ATTACHMENT, depthStencilRenderbuffer);

        // Save the renderbuffer which will now be used as the depth buffer.
        this.depthStencilRenderbuffer = depthStencilRenderbuffer;
    }

    public void initDepthStencilRenderbuffer(
            final Vec2iAccessor dimension,
            final @NotNull ImageFormat format
    ) throws IllegalStateException {
        initDepthStencilRenderbuffer(dimension.getX(), dimension.getY(), format);
    }

    public void initDepthStencilRenderbuffer(
            final int width, final int height,
            final @NotNull ImageFormat format
    ) throws IllegalStateException {
        // TODO: when already initialized?

        // The internal format must be a combine depth/stencil format.
        if (format != ImageFormat.DEPTH_STENCIL &&
                format != ImageFormat.DEPTH24_STENCIL8 &&
                format != ImageFormat.DEPTH32F_STENCIL8) {
            throw new IllegalStateException(
                    "The given format (\"" + format.name() + "\") was not a combine depth/stencil format!"
            );
        }

        final @NotNull RBO depthStencilRenderbuffer = new RBO();
        depthStencilRenderbuffer.create();
        depthStencilRenderbuffer.setFormat(format);
        depthStencilRenderbuffer.setResolution(width, height);
        depthStencilRenderbuffer.createStorage();
        depthStencilRenderbuffer.finish();

        // Attach the renderbuffer to the framebuffer.
        setDepthStencilRenderbuffer(depthStencilRenderbuffer);
    }

    protected void compile() {
        if (DEBUG) {
            checkForErrors();
        }
    }

    private void checkForErrors() throws RuntimeException {
        if (GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE) {
            throw new RuntimeException(MSG_FBO_INCOMPLETE);
        }
    }

    /**
     * Binds this framebuffer so that it will both be available for write and read operations.
     */
    public void bind() {
        if (DEBUG) {
            ensureCreated();
        }
        if (GLUtil.getCST().getBoundDrawFramebuffer() == fbo &&
                GLUtil.getCST().getBoundReadFramebuffer() == fbo) {
            if (DEBUG) {
                LOG.info("This framebuffer (" + fbo + ") is already bound as both the draw and read framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.DRAW_AND_READ, fbo);
    }

    /**
     * Binds the default framebuffer so that it will both be available for write and read operations.
     */
    public static void bindDefaultFBO() {
        if (GLUtil.getCST().getBoundDrawFramebuffer() == DEFAULT_FRAMEBUFFER_ID &&
                GLUtil.getCST().getBoundReadFramebuffer() == DEFAULT_FRAMEBUFFER_ID) {
            if (DEBUG) {
                LOG.info("This default framebuffer is already bound as both the draw and read framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.DRAW_AND_READ, DEFAULT_FRAMEBUFFER_ID);
    }

    @Override
    public void bindForDrawing() {
        if (DEBUG) {
            ensureCreated();
        }
        if (GLUtil.getCST().getBoundDrawFramebuffer() == fbo) {
            if (DEBUG) {
                LOG.info("This framebuffer (" + fbo + ") is already bound as the draw framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.DRAW, fbo);
    }

    /**
     * Binds the default framebuffer so that it will be available for write operations.
     */
    public static void bindDefaultFBOForDrawing() {
        if (GLUtil.getCST().getBoundDrawFramebuffer() == DEFAULT_FRAMEBUFFER_ID) {
            if (DEBUG) {
                LOG.info("The default framebuffer is already bound as the draw framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.DRAW, DEFAULT_FRAMEBUFFER_ID);
    }

    @Override
    public void bindForReading() {
        if (DEBUG) {
            ensureCreated();
        }
        if (GLUtil.getCST().getBoundReadFramebuffer() == fbo) {
            if (DEBUG) {
                LOG.info("This framebuffer (" + fbo + ") is already bound as the read framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.READ, fbo);
    }

    /**
     * Binds the default framebuffer so that it will be available for read operations.
     */
    public static void bindDefaultFBOForReading() {
        if (GLUtil.getCST().getBoundReadFramebuffer() == DEFAULT_FRAMEBUFFER_ID) {
            if (DEBUG) {
                LOG.info("The default framebuffer is already bound as the read framebuffer.");
            }
            return;
        }
        performBind(FrameBufferTarget.READ, DEFAULT_FRAMEBUFFER_ID);
    }

    /**
     * Performs a binding of the given fbo to the specified target in the current context.
     *
     * @param target The target to which the specified framebuffer should be bound.
     * @param fbo    The framebuffer to bind.
     */
    private static void performBind(final @NotNull FrameBufferTarget target, final int fbo) {

        if (GLUtil.getCST().getBoundDrawFramebuffer() == fbo) {
            if (DEBUG) {
                LOG.info("This framebuffer (" + fbo + ") is already bound to target: " + target.name());
            }
            return;
        }
        GL30.glBindFramebuffer(target.id, fbo);
        propagateBindingToCST(target, fbo);
    }

    /**
     * Tell the context state tracker of the current context that the specified framebuffer is no bound to target.
     *
     * @param target The target at which the binding changed.
     * @param fbo    The framebuffer which is now bound.
     */
    private static void propagateBindingToCST(final @NotNull FrameBufferTarget target, final int fbo) {
        GLUtil.getCST().setBoundFramebuffer(target, fbo);
    }

    public void bindTextures(final int... textureIndices) {
        final int amtOfColorTextures = textures.size();
        for (final int textureIndex : textureIndices) {
            if (textureIndex < amtOfColorTextures) {
                textures.get(textureIndex).bind(textureIndex);
            } else {
                assert textureIndex == textures.size() : "Unknown texture index given.";
                bindDepthTexture();
            }
        }
    }

    @Override
    public void bindTextures(final IntList textureIndices) {
        final int amtOfColorTextures = textures.size();
        textureIndices.forEach(textureIndex -> {
            if (textureIndex < amtOfColorTextures) {
                textures.get(textureIndex).bind(textureIndex);
            } else {
                assert textureIndex == textures.size() : "Unknown texture index given.";
                bindDepthTexture();
            }
            return Unit.INSTANCE;
        });
    }

    /**
     * @param colorTextures
     */
    @Deprecated
    public void bindColorTextures(final int... colorTextures) {
        for (final int colorTexture : colorTextures) {
            textures.get(colorTexture).bind(colorTexture);
        }
    }

    /**
     *
     */
    public void bindDepthTexture() {
        // If all COLOR! texture are bound, they will take up texture units 0 to textures.size() - 1.
        // The DEPTH texture should therefore be bound to texture unit: textures.size()
        final int depthTextureUnit = textures.size();
        depthTexture.bind(depthTextureUnit);
    }

    @Override
    public int getDepthTextureIndex() {
        if (depthTexture != null) {
            return textures.size();
        }
        throw new IllegalStateException("This framebuffer has no depth texture!");
    }

    /* READ-BUFFER */

    public void setReadBufferToNONE() {
        GL11.glReadBuffer(GL11.GL_NONE);
    }

    // TODO: go through CST.
    public void setReadBuffer(final int colorAttachment) {
        GL11.glReadBuffer(indexToAttachment.get(colorAttachment));
    }

    /* DRAW-BUFFER */

    public void setDrawBufferToNONE() {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        // TODO: use glNamedFramebufferDrawBuffer  or ensure bound.
        GL11.glDrawBuffer(GL11.GL_NONE);
    }

    /**
     * Defines the color buffer to which fragment color zero is written.
     *
     * @param colorAttachment
     */
    public void setDrawBuffer(final int colorAttachment) {
        // TODO: Perform a check if the specified color attachment is correct and/or possible.
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        final int drawBuffer = GL30.GL_COLOR_ATTACHMENT0 + colorAttachment;
        GL11.glDrawBuffer(drawBuffer);
        currentDrawBuffers.add(colorAttachment);
    }

    /**
     * Specifies a list of color buffers to be drawn into.
     *
     * @param textureIndices
     */
    public void setDrawBuffers(final int... textureIndices) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(textureIndices.length);
            for (int i = 0; i < textureIndices.length; i++) {
                final int drawBuffer = indexToAttachment.get(textureIndices[i]);
                drawBuffers.put(i, drawBuffer);
                currentDrawBuffers.add(textureIndices[i]);
            }

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    @Override
    public void setDrawBuffers(int textureIndex1) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(1);

            drawBuffers.put(0, indexToAttachment.get(textureIndex1));
            currentDrawBuffers.add(textureIndex1);

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    @Override
    public void setDrawBuffers(int textureIndex1, int textureIndex2) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(2);

            drawBuffers.put(0, indexToAttachment.get(textureIndex1));
            drawBuffers.put(1, indexToAttachment.get(textureIndex2));
            currentDrawBuffers.add(textureIndex1);
            currentDrawBuffers.add(textureIndex2);

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    @Override
    public void setDrawBuffers(int textureIndex1, int textureIndex2, int textureIndex3) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(3);

            drawBuffers.put(0, indexToAttachment.get(textureIndex1));
            drawBuffers.put(1, indexToAttachment.get(textureIndex2));
            drawBuffers.put(2, indexToAttachment.get(textureIndex3));
            currentDrawBuffers.add(textureIndex1);
            currentDrawBuffers.add(textureIndex2);
            currentDrawBuffers.add(textureIndex3);

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    @Override
    public void setDrawBuffers(int textureIndex1, int textureIndex2, int textureIndex3, int textureIndex4) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(4);

            drawBuffers.put(0, indexToAttachment.get(textureIndex1));
            drawBuffers.put(1, indexToAttachment.get(textureIndex2));
            drawBuffers.put(2, indexToAttachment.get(textureIndex3));
            drawBuffers.put(3, indexToAttachment.get(textureIndex4));
            currentDrawBuffers.add(textureIndex1);
            currentDrawBuffers.add(textureIndex2);
            currentDrawBuffers.add(textureIndex3);
            currentDrawBuffers.add(textureIndex4);

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    @Override
    public void setDrawBuffers(final IntList textureIndices) {
        allBuffersSetForDrawing = false;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer drawBuffers = stack.mallocInt(textureIndices.getSize());

            textureIndices.forEachIndexed((i, textureIndex) -> {
                drawBuffers.put(i, indexToAttachment.get(textureIndex));
                currentDrawBuffers.add(textureIndex);
                return Unit.INSTANCE;
            });

            // TODO: code paths, outsource to GLSet
            GL20.glDrawBuffers(drawBuffers);
        }
    }

    public void setAllDrawBuffers() {
        allBuffersSetForDrawing = true;
        currentDrawBuffers.clear();

        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer buffers = stack.mallocInt(indexToAttachment.size());
            for (final int key : indexToAttachment.keySet()) {
                buffers.put(indexToAttachment.get(key));
                currentDrawBuffers.add(key);
            }
            buffers.rewind();
            GL20.glDrawBuffers(buffers);
        }
    }

    /**
     * Clears the buffers which are currently set for drawing.
     *
     * @see GLFBO#setDrawBuffer(int)
     * @see GLFBO#setDrawBuffers(IntList)
     * @see GLFBO#setAllDrawBuffers()
     */
    public void clearCurrentDrawBuffers() {
        // Clear every known buffer currently set for drawing.
        for (final int drawBuffer : currentDrawBuffers) {
            clearColorBuffer.rewind();
            GL30.glClearBufferfv(GL_COLOR, drawBuffer, clearColorBuffer);
        }
    }

    /**
     * Clears the specified attachment (0-based).
     */
    public void clearAttachment(final int index) throws IllegalArgumentException {
        if (!indexToAttachment.containsKey(index)) {
            throw new IllegalArgumentException("The specified index does not correspond to any attachment.");
        }

        for (final @NotNull Map.Entry<Integer, Integer> attachment : indexToAttachment.entrySet()) {
            clearColorBuffer.rewind();
            //GL45.glClearNamedFramebufferfv(fbo, GL_COLOR, attachment.getKey(), clearColorBuffer);
            GL30.glClearBufferfv(GL11.GL_COLOR, attachment.getKey(), clearColorBuffer);
        }
    }

    /**
     * May set all draw buffers!!
     */
    public void clearAttachments() {
        setAllDrawBuffers();
        for (final @NotNull Map.Entry<Integer, Integer> attachment : indexToAttachment.entrySet()) {
            clearColorBuffer.rewind();
            //GL45.glClearNamedFramebufferfv(fbo, GL_COLOR, attachment.getKey(), clearColorBuffer);
            GL30.glClearBufferfv(GL11.GL_COLOR, attachment.getKey(), clearColorBuffer);
        }
    }

    public void clearDepthAndStencil() {

        if (depthTexture != null || depthStencilRenderbuffer != null) {
            // DSA available.
            if (GLUtil.isContextAtLeast(4, 5)) {
                GL45.glClearNamedFramebufferfi(fbo, GL_DEPTH_STENCIL, 0, clearDepth, clearStencil);
            } else if (GLUtil.getCaps().GL_ARB_direct_state_access) {
                ARBDirectStateAccess.glClearNamedFramebufferfi(
                        fbo, GL_DEPTH_STENCIL, 0, clearDepth, clearStencil
                );
            }
            // EXTDirectStateAccess does not include a glClearNamedFramebufferfi function.
            // DSA not available.
            else if (GLUtil.isContextAtLeast(3, 0)) {
                // ensureBound(); TODO: implement
                GL30.glClearBufferfi(GL_DEPTH_STENCIL, 0, clearDepth, clearStencil);
            } else {
                GLUtil.throwNoPathError();
            }
        }
    }

    /* GETTER AND SETTER */

    public int getHandle() {
        return fbo;
    }

    @Override
    public ArrayList<Texture> getTextures() {
        return textures;
    }

    public Texture getDepthTexture() {
        if (depthTexture != null) {
            return depthTexture;
        }
        throw new IllegalStateException("This framebuffer has no depth texture!");
    }

    /**
     * @return An immutable color object. Must not be freed!
     */
    public @NotNull Vec4fAccessor getClearColor() {
        return clearColor;
    }

    public void setClearColor(final @NotNull Vec4fAccessor other) {
        clearColor.set(other);
        clearColorBuffer.clear();
        clearColor.storeIn(clearColorBuffer);
        clearColorBuffer.rewind();
    }

    public float getClearDepth() {
        return clearDepth;
    }

    public void setClearDepth(final float clearDepth) {
        this.clearDepth = clearDepth;
    }

    public int getClearStencil() {
        return clearStencil;
    }

    public void setClearStencil(final int clearStencil) {
        this.clearStencil = clearStencil;
    }

}
