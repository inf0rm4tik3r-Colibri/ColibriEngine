package de.colibriengine.graphics.opengl;

import de.colibriengine.asset.NoopResourceLoader;
import de.colibriengine.exception.WindowCreationException;
import de.colibriengine.graphics.opengl.context.ContextStateTracker;
import de.colibriengine.graphics.opengl.context.GLContextInformation;
import de.colibriengine.graphics.opengl.context.GLProfile;
import de.colibriengine.graphics.opengl.rendering.GLBlendFactor;
import de.colibriengine.graphics.opengl.rendering.GLPolygonFace;
import de.colibriengine.graphics.opengl.window.AbstractOpenGLWindow;
import de.colibriengine.graphics.opengl.window.NoopOpenGLWindow;
import de.colibriengine.graphics.opengl.window.OpenGLWindowBuilder;
import de.colibriengine.graphics.window.DebugBehaviour;
import de.colibriengine.graphics.window.NoopWindowManager;
import de.colibriengine.graphics.window.WindowManager;
import de.colibriengine.input.NoopInputManager;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2i.StdVec2i;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import de.colibriengine.math.vector.vec4f.Vec4f;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.util.Collections;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Provides helper methods for (complex) OpenGL functionality.
 */
@SuppressWarnings("WeakerAccess")
public abstract class GLUtil {

    /**
     * Used as an error message to say that no code path is available on the current system for the requested
     * functionality.
     */
    public static final String NO_PATH = "No OpenGL path available.";

    /**
     * openglVersions[i].x -> major
     * openglVersions[i].y -> minor
     * <p>
     * i=0 -> version: 1.0
     * i=length-1 -> version 4.5
     *
     * <pre>
     * Versions:
     * {{1,0}, {1,1}, {1,2}, {1,3}, {1,4}, {1,5}, {2,0}, {2,1}, {3,0}, {3,1},
     * {3,2}, {3,3}, {4,0}, {4,1}, {4,2}, {4,3}, {4,4}, {4,5}};
     * </pre>
     */
    public static final Vec2iAccessor[] OPEN_GL_VERSIONS = {
            new StdVec2i(1, 0), new StdVec2i(1, 1),
            new StdVec2i(1, 2), new StdVec2i(1, 3),
            new StdVec2i(1, 4), new StdVec2i(1, 5),
            new StdVec2i(2, 0), new StdVec2i(2, 1),
            new StdVec2i(3, 0), new StdVec2i(3, 1),
            new StdVec2i(3, 2), new StdVec2i(3, 3),
            new StdVec2i(4, 0), new StdVec2i(4, 1),
            new StdVec2i(4, 2), new StdVec2i(4, 3),
            new StdVec2i(4, 4), new StdVec2i(4, 5),
            new StdVec2i(4, 6)
    };

    /**
     * Index into the OpenGLUtil.OPEN_GL_VERSIONS array. Represents version 3.3
     */
    private static final int MIN_REQUIRED_GL_VERSION = 11;

    /**
     * Index into the OpenGLUtil.OPEN_GL_VERSIONS array. Represents version 4.6
     */
    private static final int HIGHEST_GL_VERSION = OPEN_GL_VERSIONS.length - 1;

    private static final int UNDEFINED_GL_VERSION = -1;

    /**
     * Index into the OpenGLUtil.OPEN_GL_VERSIONS array. Initially undefined (-1).
     *
     * @see GLUtil#findMaxSupportedGLVersion()
     * @see GLUtil#overwriteMaxSupportedGLVersion(int[])
     */
    private static int maxSupportedGLVersion = UNDEFINED_GL_VERSION;

    // TODO: Move determination logic for the maximum GL version somewhere else.
    //       Also remove the dependency on "input", as it is only needed for "new NoopInputManager()"...
    private static final @NotNull OpenGLWindowBuilder TEST_WINDOW_BUILDER = OpenGLWindowBuilder
            .newInstance(new NoopWindowManager(), new NoopInputManager(), new NoopResourceLoader())
            .setTitle("TestWindow")
            .setIconResources(Collections.emptyList())
            .setDesiredMonitor(-1)
            .setPosition(-1, -1)
            .setResolution(-1, -1)
            .setVisible(false)
            .setResizable(false)
            .setDecorated(false)
            .setFullscreen(false)
            .setVSync(false)
            .setOpenGLProfile(GLProfile.CORE_PROFILE)
            .setOpenGLForwardCompatible(true)
            .setSharedContext(NULL)
            .setDebugMode(false)
            .setDebugBehaviour(DebugBehaviour.CREATE_EXCEPTION);

    private static WindowManager windowManagerRef;

    private static final Logger LOG = LogUtil.getLogger(GLUtil.class);

    /**
     * Always throws the exception, indicating that no valid code path exists.
     *
     * @throws UnsupportedOperationException Whenever this method gets called.
     */
    @Contract(" -> fail")
    public static void throwNoPathError() throws UnsupportedOperationException {
        throw new UnsupportedOperationException(NO_PATH);
    }

    public static void clearColorAndDepthBuffer(
            final Vec4f clearColor, final float clearDepth, final int clearStencil
    ) {
        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final FloatBuffer colorBuffer = stack.mallocFloat(Vec4f.FLOATS);
            clearColor.storeIn(colorBuffer);
            colorBuffer.rewind();

            glClearBufferfv(GL_COLOR, 0, colorBuffer);
            glClearBufferfv(GL_DEPTH, 0, stack.floats(clearDepth));
            glClearBufferiv(GL_STENCIL, 0, stack.ints(clearStencil));
        }
    }

    /**
     * Checks for the latest supported version of OpenGL.
     */
    public static void findMaxSupportedGLVersion() {
        LOG.info("Finding the highest available OpenGL version...");

        //for (int i = MIN_REQUIRED_GL_VERSION; i < OPEN_GL_VERSIONS.length; i++) {
        for (int i = HIGHEST_GL_VERSION; i > MIN_REQUIRED_GL_VERSION; i--) {
            LOG.info("OpenGLUtil: trying " + OPEN_GL_VERSIONS[i].getX() + "." + OPEN_GL_VERSIONS[i].getY());

            if (isVersionSupported(OPEN_GL_VERSIONS[i].getX(), OPEN_GL_VERSIONS[i].getY())) {
                maxSupportedGLVersion = i;

                LOG.info(
                        "Found max supported OpenGL version to be: {}, {}",
                        OPEN_GL_VERSIONS[maxSupportedGLVersion].getX(),
                        OPEN_GL_VERSIONS[maxSupportedGLVersion].getY()
                );

                // We do not need to test more GL version.
                break;
            }
        }
    }

    private static void checkIfMaxSupportedGLVersionIsKnown() throws IllegalStateException {
        if (maxSupportedGLVersion < 0) {
            throw new IllegalStateException(
                    "maxSupportedGLVersion is undefined. See: GLUtil#findMaxSupportedGLVersion()"
            );
        }
    }

    /**
     * @throws IllegalStateException if maxSupportedGLVersion is undefined.
     */
    public static @NotNull Vec2iAccessor getMaxSupportedGLVersion() throws IllegalStateException {
        checkIfMaxSupportedGLVersionIsKnown();
        return OPEN_GL_VERSIONS[maxSupportedGLVersion];
    }

    public static int getMaxSupportedGLVersionMajor() throws IllegalStateException {
        checkIfMaxSupportedGLVersionIsKnown();
        return OPEN_GL_VERSIONS[maxSupportedGLVersion].getX();
    }

    public static int getMaxSupportedGLVersionMinor() throws IllegalStateException {
        checkIfMaxSupportedGLVersionIsKnown();
        return OPEN_GL_VERSIONS[maxSupportedGLVersion].getY();
    }

    public static void overwriteMaxSupportedGLVersion(final int[] glVersion) {
        if (glVersion.length != 2) {
            throw new IllegalArgumentException("Specified version array is not of length 2!");
        }
        overwriteMaxSupportedGLVersion(glVersion[0], glVersion[1]);
    }

    public static void overwriteMaxSupportedGLVersion(final int major, final int minor) {
        boolean versionKnown = false;

        // Try to find a array in OPEN_GL_VERSIONS that represents the specified gl version.
        for (int i = MIN_REQUIRED_GL_VERSION; i < OPEN_GL_VERSIONS.length; i++) {
            if (OPEN_GL_VERSIONS[i].getX() == major) {
                if (OPEN_GL_VERSIONS[i].getY() == minor) {
                    versionKnown = true;
                    maxSupportedGLVersion = i;
                }
            }
        }

        if (!versionKnown) {
            throw new IllegalArgumentException("Specified gl version is not one out of OpenGLUtil.OPEN_GL_VERSIONS!");
        }
    }

    /**
     * Checks if the specified OpenGL version is supported by trying to create a window with a context of that
     * specific GL version.
     * This method is slow and should only be called a finite number of times in the initialization phase to determine
     * the highest available OpenGL version.
     *
     * @param glVersionMajor The major version number. For example: 4
     * @param glVersionMinor The minor version number. For example: 6
     * @return True if a window with the specified GL version could be created.
     */
    private static boolean isVersionSupported(final int glVersionMajor, final int glVersionMinor) {
        try {
            TEST_WINDOW_BUILDER
                    .setOpenGLTargetVersion(glVersionMajor, glVersionMinor)
                    .build(NoopOpenGLWindow.class)
                    .release();
            return true;
        }
        // TODO: Do not ignore exceptions completely
        catch (final WindowCreationException ignored) {
            return false;
        }
    }

    /**
     * Sets necessary states of the OpenGL context in order to let the engine render correctly.
     * This method sets and enables culling, as well as setting and enabling depth functions.
     */
    public static void setOpenGLStates() {
        // Enable face culling: The back of a polygon must not be rendered.
        enableCulling(GLPolygonFace.GL_BACK);

        // Specify the winding order. This defines which side of a polygon is actually the front or the back side!
        // GL_CCW (counterclockwise) is the standard of this engine.
        // GL_CW (clockwise) might be used while using a LH projection matrix.
        GL11.glFrontFace(GL11.GL_CCW);

        // Depth Calculation
        GL11.glClearDepth(1.0d);
        GL11.glDepthFunc(GL11.GL_LESS); // GL_LEQUAL could be used as well...
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        //enableDepthClamp();

        // Tell OpenGL to perform automatic gamma correction whenever rendering to an sRGB framebuffer.
        //enableSRGBFramebufferWrites();
    }

    public static void enableCulling(final @NotNull GLPolygonFace polygonFace) {
        glEnable(GL11.GL_CULL_FACE);
        glCullFace(polygonFace.getId());
    }

    public static void enableCulling() {
        GL11.glEnable(GL11.GL_CULL_FACE);
    }

    public static void disableCulling() {
        GL11.glDisable(GL11.GL_CULL_FACE);
    }

    public static void enableSRGBFramebufferWrites() {
        if (isContextAtLeast(3, 0)) {
            GL11.glEnable(GL30.GL_FRAMEBUFFER_SRGB);
        } else if (getCaps().GL_ARB_framebuffer_sRGB) {
            GL11.glEnable(ARBFramebufferSRGB.GL_FRAMEBUFFER_SRGB);
        } else if (getCaps().GL_EXT_framebuffer_sRGB) {
            GL11.glEnable(EXTFramebufferSRGB.GL_FRAMEBUFFER_SRGB_EXT);
        } else {
            throwNoPathError();
        }
    }

    public static void disableSRGBFramebufferWrites() {
        if (isContextAtLeast(3, 0)) {
            GL11.glDisable(GL30.GL_FRAMEBUFFER_SRGB);
        } else if (getCaps().GL_ARB_framebuffer_sRGB) {
            GL11.glDisable(ARBFramebufferSRGB.GL_FRAMEBUFFER_SRGB);
        } else if (getCaps().GL_EXT_framebuffer_sRGB) {
            GL11.glDisable(EXTFramebufferSRGB.GL_FRAMEBUFFER_SRGB_EXT);
        } else {
            throwNoPathError();
        }
    }

    public static void enableBlending() {
        glEnable(GL11.GL_BLEND);
    }

    public static void enableBlending(
            final @NotNull GLBlendFactor sourceFactor,
            final @NotNull GLBlendFactor destinationFactor
    ) {
        enableBlending();
        setBlendFactors(sourceFactor, destinationFactor);
    }

    public static void setBlendFactors(
            final @NotNull GLBlendFactor sourceFactor,
            final @NotNull GLBlendFactor destinationFactor
    ) {
        GL11.glBlendFunc(sourceFactor.getId(), destinationFactor.getId());
    }

    public static void disableBlending() {
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void enableWireframeMode() {
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
    }

    public static void disableWireframeMode() {
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
    }

    public static void enableDepthMask() {
        GL11.glDepthMask(true);
    }

    public static void disableDepthMask() {
        GL11.glDepthMask(false);
    }

    public static void enableDepthTest() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
    }

    public static void disableDepthTest() {
        GL11.glDisable(GL11.GL_DEPTH_TEST);
    }

    public static void enableDepthClamp() {
        GL11.glEnable(GL32.GL_DEPTH_CLAMP);
    }

    public static void disableDepthClamp() {
        GL11.glDisable(GL32.GL_DEPTH_CLAMP);
    }

    public static void enableStencilTest() {
        GL11.glEnable(GL11.GL_STENCIL_TEST);
    }

    public static void disableStencilTest() {
        GL11.glDisable(GL11.GL_STENCIL_TEST);
    }

    /**
     * Calls {@link AbstractOpenGLWindow#getContextInformation()} on the currently active window and returns the result.
     *
     * @return The ContextInformation object of the context of the currently active window.
     */
    public static GLContextInformation getContextInformation() {
        return ((AbstractOpenGLWindow) (windowManagerRef.getActiveWindow())).getContextInformation();
    }

    /**
     * Calls {@link AbstractOpenGLWindow#getContextInformation()#getCaps()} on the currently active window and returns the
     * result.
     *
     * @return The GLCapabilities object of the context of the currently active window.
     */
    public static GLCapabilities getCaps() {
        return ((AbstractOpenGLWindow) (windowManagerRef.getActiveWindow())).getContextInformation().getCaps();
    }

    /**
     * Calls {@link AbstractOpenGLWindow#getContextInformation()#getCST()} on the currently active window to obtain the
     * {@link ContextStateTracker} for the currently active context.
     *
     * @return The ContextStateTracker object for the current context.
     */
    public static ContextStateTracker getCST() {
        return ((AbstractOpenGLWindow) (windowManagerRef.getActiveWindow())).getContextInformation().getCST();
    }

    // TODO: REMOVE! EVIL STATIC METHODS...

    /**
     * Checks whether the context of the currently <b>active window</b> is at least the specified opengl version.
     * Provides simple access to the {@code isContextAtLeast(int, int)} method from
     * {@code AbstractWindow}.
     * Use this method if a reference to the active window is unavailable.
     *
     * @param major The major OpenGL version (e.g. the 4 of version "4.5")
     * @param minor The minor OpenGL version (e.g. the 5 of version "4.5")
     * @return true, if the context version is higher then major.minor (e.g. context: 4.5 >= (major.minor == 3.3))
     * @see GLContextInformation#isContextAtLeast(int, int)
     */
    public static boolean isContextAtLeast(final int major, final int minor) {
        return ((AbstractOpenGLWindow) (windowManagerRef.getActiveWindow())).getContextInformation().isContextAtLeast(major, minor);
    }

    /**
     * @return The version of the context of the currently active window as an array. eg. [4, 5]
     */
    public static @NotNull Vec2iAccessor getContextVersion() {
        return ((AbstractOpenGLWindow) (windowManagerRef.getActiveWindow())).getContextInformation().getContextVersion();
    }

    /**
     * @return The version of the context of the currently active window as a string. eg. "4.5"
     */
    public static String getContextVersionString() {
        final Vec2iAccessor version = getContextVersion();
        return version.getX() + "." + version.getY();
    }

    // TODO: REMOVE THESE DAMN STATIC METHODS!!!
    public static void setWindowManagerRef(final WindowManager windowManagerRef) {
        GLUtil.windowManagerRef = windowManagerRef;
    }

    /**
     * Checks whether an error has occurred. Returns the first error that occurred after the last check call.
     * Method is not time consuming -> can be called fast (looped for debugging)
     * <ul>
     * <li><p>
     * GL_NO_ERROR
     * No error has been recorded. The value of this symbolic constant is guaranteed to be 0.
     * </p></li>
     * <li><p>
     * GL_INVALID_ENUM
     * An unacceptable value is specified for an enumerated argument. The offending command is ignored and has
     * no other side effect than to set the error flag.
     * </p></li>
     * <li><p>
     * GL_INVALID_VALUE
     * A numeric argument is out of range. The offending command is ignored and has no other side effect than
     * to set the error flag.
     * </p></li>
     * <li><p>
     * GL_INVALID_OPERATION
     * The specified operation is not allowed in the current state. The offending command is ignored and has
     * no other side effect than to set the error flag.
     * </p></li>
     * <li><p>
     * GL_INVALID_FRAMEBUFFER_OPERATION
     * The framebuffer object is not complete. The offending command is ignored and has no other side effect
     * than to set the error flag.
     * </p></li>
     * <li><p>
     * GL_STACK_OVERFLOW
     * An attempt has been made to perform an operation that would cause an internal stack to overflow.
     * </p></li>
     * <li><p>
     * GL_STACK_UNDERFLOW
     * An attempt has been made to perform an operation that would cause an internal stack to underflow.
     * </p></li>
     * <li><p>
     * GL_OUT_OF_MEMORY
     * There is not enough memory left to execute the command. The state of the GL is undefined, except for
     * the state of the error flags, after this error is recorded.
     * </p></li>
     * </ul>
     *
     * @return true if a least one error occurred.
     * @minContextRequired 1.1
     * @see GL11#glGetError()
     */
    public static boolean checkForErrors() {
        // Declare the error condition variable. Gets set true if at least one error happened.
        boolean errorOccurred = false;
        int error;

        // Check in loop, whether errors occurred since the last call of checkForErrors().
        while ((error = glGetError()) != GL_NO_ERROR) {
            errorOccurred = true;
            LOG.error("ErrorCheckResult(" + error + ")");

            // Check the error type and print an appropriate message.
            switch (error) {
                case GL_INVALID_ENUM -> LOG.error("  ->  -GL_INVALID_ENUM-");
                case GL_INVALID_VALUE -> LOG.error("  ->  -GL_INVALID_VALUE-");
                case GL_INVALID_OPERATION -> LOG.error("  ->  -GL_INVALID_OPERATION-");
                case GL_INVALID_FRAMEBUFFER_OPERATION -> LOG.error("  ->  -GL_INVALID_FRAMEBUFFER_OPERATION-");
                case GL_STACK_OVERFLOW -> LOG.error("  ->  -GL_STACK_OVERFLOW-");
                case GL_STACK_UNDERFLOW -> LOG.error("  ->  -GL_STACK_UNDERFLOW-");
                case GL_OUT_OF_MEMORY -> LOG.error("  ->  -GL_OUT_OF_MEMORY-");
                default -> LOG.error("  ->  Error occurred! Detection not possible.");
            }
        }

        return errorOccurred;
    }

}
