package de.colibriengine.graphics.opengl.texture

import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL12

enum class GLSamplerParameter(
    /** The value of this constant in OpenGL. */
    val id: Int
) {

    GL_TEXTURE_WRAP_S(GL11.GL_TEXTURE_WRAP_S),
    GL_TEXTURE_WRAP_T(GL11.GL_TEXTURE_WRAP_T),
    GL_TEXTURE_WRAP_R(GL12.GL_TEXTURE_WRAP_R),
    GL_TEXTURE_MIN_FILTER(GL11.GL_TEXTURE_MIN_FILTER),
    GL_TEXTURE_MAG_FILTER(GL11.GL_TEXTURE_MAG_FILTER);

}
