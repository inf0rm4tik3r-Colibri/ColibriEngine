package de.colibriengine.graphics.opengl.window;

import de.colibriengine.asset.ResourceLoader;
import de.colibriengine.graphics.opengl.context.GLProfile;
import de.colibriengine.graphics.window.DebugBehaviour;
import de.colibriengine.graphics.window.WindowManager;
import de.colibriengine.input.InputManager;
import de.colibriengine.math.vector.vec2i.StdVec2i;
import de.colibriengine.math.vector.vec2i.Vec2iAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.system.MemoryUtil.NULL;

@SuppressWarnings("WeakerAccess")
public class OpenGLWindowBuilder {

    private final @NotNull WindowManager windowManager;
    private final @NotNull InputManager inputManager;
    private final @NotNull ResourceLoader resourceLoader;
    private @NotNull String title = "Title";
    private @NotNull List<String> iconResources = new ArrayList<>();
    private int desiredMonitor = -1;
    private final @NotNull StdVec2i position = new StdVec2i(-1, -1);
    private final @NotNull StdVec2i resolution = new StdVec2i(-1, -1);
    private boolean visible = true;
    private boolean resizable = true;
    private boolean decorated = true;
    private boolean fullscreen = false;
    private boolean vSync = false;
    private final @NotNull StdVec2i openGLTargetVersion = new StdVec2i(4, 6);
    private @NotNull GLProfile openGLProfile = GLProfile.CORE_PROFILE;
    private boolean openGLForwardCompatible = true;
    private long sharedContext = NULL;
    private boolean debugMode = true;
    private @NotNull DebugBehaviour debugBehaviour = DebugBehaviour.CREATE_EXCEPTION;

    /**
     * This constructor must only be called from the {@link OpenGLWindowBuilder#newInstance(WindowManager)} method.
     *
     * @param windowManager Each window must know the {@link WindowManager} it resides in.
     */
    private OpenGLWindowBuilder(
            final @NotNull WindowManager windowManager,
            final @NotNull InputManager inputManager,
            final @NotNull ResourceLoader resourceLoader
    ) {
        this.windowManager = windowManager;
        this.inputManager = inputManager;
        this.resourceLoader = resourceLoader;
    }

    /**
     * Windows are to complex to be initialised "by hand".
     * The {@link OpenGLWindowBuilder} instance provided by this method is able to build an instance after getting
     * properly configured.
     * The builders {@link OpenGLWindowBuilder#build(Class)} method is going to instantiate a new window.
     *
     * @param windowManager Each window must know the {@link WindowManager} it resides in.
     * @param inputManager Each window must know the {@link InputManager} to which inputs should be forwarded.
     * @return A builder instance which must be used to build/create window instances.
     */
    public static OpenGLWindowBuilder newInstance(
            final @NotNull WindowManager windowManager,
            final @NotNull InputManager inputManager,
            final @NotNull ResourceLoader resourceLoader
    ) {
        return new OpenGLWindowBuilder(windowManager, inputManager, resourceLoader);
    }

    public @NotNull <ConcreteWindow extends AbstractOpenGLWindow> ConcreteWindow build(
            final @NotNull Class<ConcreteWindow> forClass
    ) throws AssertionError {
        try {
            // Try to obtain the constructor of the window class which accepts the builder instance.
            final Constructor<ConcreteWindow> constructor = forClass.getDeclaredConstructor(OpenGLWindowBuilder.class);
            // Set this constructor to be accessible. This allows us to define private constructors.
            // We do not want users to call window constructors directly. They must be set private!
            constructor.setAccessible(true);

            // Keep in mind: The creation of the window class may throw a WindowCreationException!
            return constructor.newInstance(this);
        } catch (final InvocationTargetException exception) {
            // Rethrow any WindowCreationException. They must be handled separately.
            throw new RuntimeException(exception);
        } catch (final InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            // Any other method resulted from false configuration. This should not happen!
            throw new AssertionError(
                    "Class " + forClass.getName() + " does not contain the required constructor: "
                            + forClass.getSimpleName() + "(WindowBuilder)!", e
            );
        }
    }

    public @NotNull WindowManager getWindowManager() {
        return windowManager;
    }

    public @NotNull InputManager getInputManager() {
        return inputManager;
    }

    public @NotNull ResourceLoader getResourceLoader() {
        return resourceLoader;
    }

    public OpenGLWindowBuilder setTitle(final @NotNull String title) {
        this.title = title;
        return this;
    }

    public @NotNull String getTitle() {
        return title;
    }

    public @NotNull List<String> getIconResources() {
        return iconResources;
    }

    public OpenGLWindowBuilder setIconResources(@NotNull List<String> iconResources) {
        this.iconResources = iconResources;
        return this;
    }

    public OpenGLWindowBuilder setDesiredMonitor(final int desiredMonitor) {
        this.desiredMonitor = desiredMonitor;
        return this;
    }

    public int getDesiredMonitor() {
        return desiredMonitor;
    }

    public OpenGLWindowBuilder setPosition(final @NotNull Vec2iAccessor positionView) {
        position.set(positionView);
        return this;
    }

    public OpenGLWindowBuilder setPosition(final int x, final int y) {
        position.set(x, y);
        return this;
    }

    public @NotNull Vec2iAccessor getPosition() {
        return position;
    }

    public OpenGLWindowBuilder setResolution(final @NotNull Vec2iAccessor resolution) {
        this.resolution.set(resolution);
        return this;
    }

    public OpenGLWindowBuilder setResolution(final int width, final int height) {
        resolution.set(width, height);
        return this;
    }

    public @NotNull Vec2iAccessor getResolution() {
        return resolution;
    }

    public OpenGLWindowBuilder setVisible(final boolean visible) {
        this.visible = visible;
        return this;
    }

    public boolean getVisible() {
        return visible;
    }

    public OpenGLWindowBuilder setResizable(final boolean resizable) {
        this.resizable = resizable;
        return this;
    }

    public boolean getResizable() {
        return resizable;
    }

    public OpenGLWindowBuilder setDecorated(final boolean decorated) {
        this.decorated = decorated;
        return this;
    }

    public boolean getDecorated() {
        return decorated;
    }

    public OpenGLWindowBuilder setFullscreen(final boolean fullscreen) {
        this.fullscreen = fullscreen;
        return this;
    }

    public boolean getFullscreen() {
        return fullscreen;
    }

    public OpenGLWindowBuilder setVSync(final boolean vSync) {
        this.vSync = vSync;
        return this;
    }

    public boolean getVSync() {
        return vSync;
    }

    /**
     * Set the targeted OpenGL version by vector.
     * Creates a copy of the provided {@code openGLTargetVersion} vector. Do not be concerned about the lifetime of the
     * instances put into this method.
     *
     * @param openGLTargetVersionView The targeted OpenGL version.
     *                                A context with this version will be created if possible.
     * @return This builder instance to allow method chaining.
     */
    public OpenGLWindowBuilder setOpenGLTargetVersion(final @NotNull Vec2iAccessor openGLTargetVersionView) {
        openGLTargetVersion.set(openGLTargetVersionView);
        return this;
    }

    /**
     * Set the targeted OpenGL version by primitive values.
     *
     * @param major The "major" part of the version number: For example the "4" out of "4.6".
     * @param minor The "minor" part of the version number: For example the "6" out of "4.6".
     * @return This builder instance to allow method chaining.
     */
    public OpenGLWindowBuilder setOpenGLTargetVersion(final int major, final int minor) {
        openGLTargetVersion.set(major, minor);
        return this;
    }

    public @NotNull Vec2iAccessor getOpenGLTargetVersion() {
        return openGLTargetVersion;
    }

    public OpenGLWindowBuilder setOpenGLProfile(final @NotNull GLProfile openGLProfile) {
        this.openGLProfile = openGLProfile;
        return this;
    }

    public @NotNull GLProfile getOpenGLProfile() {
        return openGLProfile;
    }

    public OpenGLWindowBuilder setOpenGLForwardCompatible(final boolean openGLForwardCompatible) {
        this.openGLForwardCompatible = openGLForwardCompatible;
        return this;
    }

    public boolean getOpenGLForwardCompatible() {
        return openGLForwardCompatible;
    }

    public OpenGLWindowBuilder setSharedContext(final long sharedContext) {
        this.sharedContext = sharedContext;
        return this;
    }

    public long getSharedContext() {
        return sharedContext;
    }

    public OpenGLWindowBuilder setDebugMode(final boolean debugMode) {
        this.debugMode = debugMode;
        return this;
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    public OpenGLWindowBuilder setDebugBehaviour(final @NotNull DebugBehaviour debugBehaviour) {
        this.debugBehaviour = debugBehaviour;
        return this;
    }

    public @NotNull DebugBehaviour getDebugBehaviour() {
        return debugBehaviour;
    }

}
