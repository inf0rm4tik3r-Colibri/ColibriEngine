package de.colibriengine.input

import de.colibriengine.math.vector.vec2d.Vec2dAccessor
import de.colibriengine.math.vector.vec2i.Vec2iAccessor

interface MouseKeyboardManager {

    /** The last known position of the mouse cursor. */
    val mousePos: Vec2iAccessor

    /** The cursor movement since the last call to [resetDelta]. */
    val mouseDelta: Vec2dAccessor

    /** Sets mouseDelta to zero. */
    fun resetDelta()

    /**
     * Keeps track of the cursors position and acceleration.
     * It is highly recommended to call this method every time the cursor is moved. Therefore use the cursor position
     * callback in a AbstractWindow implementation.
     * <p>
     * Accumulates all movement and stores that in a delta value. You can get the amount of movement of the cursor
     * by calling getAndResetCursorDx() and getAndResetCursorDy() at any given time.
     * Every delta value you receive using these methods gets reset to 0 to start accumulating again.
     *
     * @param xpos   The new x position of the cursor.
     * @param ypos   The new y position of the cursor.
     * @see MouseKeyboardManagerImpl#getAndResetCursorDx()
     * @see MouseKeyboardManagerImpl#getAndResetCursorDy()
     * @see MouseKeyboardManagerImpl#getCursorDx()
     * @see MouseKeyboardManagerImpl#getCursorDx()
     */
    fun trackCursor(xPos: Double, yPos: Double)

    fun invokeKeyCallbacks(key: Int, scancode: Int, action: Int, mods: Int)
    fun invokeMousePosCallbacks(x: Int, y: Int)
    fun invokeMouseMoveCallbacks(dx: Int, dy: Int)
    fun invokeMouseButtonCallbacks(button: Int, action: Int, mods: Int)

    /**
     * Check whether the given key was pressed down. Operates on the currently active window set in the window manager
     * object.
     *
     * @param keyCode The key to check. Pass GLFW_KEY_* constants.
     * @return "true" if the given key has been pressed down, "false" otherwise.
     * @throws IllegalStateException if someone tries to obtain key information without having a parent window.
     */
    fun isKeyPressed(keyCode: Int): Boolean

    /**
     * Check whether the given key was released.
     *
     * @param keyCode The key to check. Pass GLFW_KEY_* constants.
     * @return "true" if the given key has been released, "false" otherwise.
     * @throws IllegalStateException if someone tries to obtain key information without having a parent window.
     */
    fun isKeyReleased(keyCode: Int): Boolean

}
