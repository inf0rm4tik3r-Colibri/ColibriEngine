package de.colibriengine.input

/**
 * Provides an interface for actively retrieving the user input for specific windows. It is highly recommended to use
 * the passive way through the input callbacks. Therefore let any class you want to receive inputs in implement the
 * [MouseKeyboardInputCallbacks] interface and/or the [JoystickInputCallbacks] and register instances under an
 * input manager.
 *
 * @since ColibriEngine 0.0.5.3
 */
interface InputManager {

    var activeInputType: InputType
    val isMouseAndKeyboardInput: Boolean
        get() = activeInputType === InputType.MOUSE_KEYBOARD
    val isJoystickInput: Boolean
        get() = activeInputType === InputType.JOYSTICK

    val mouseKeyboardManager: MouseKeyboardManager
    val joystickManager: JoystickManager

    val mkCallbackObjects: List<MouseKeyboardInputCallbacks>
    val jCallbackObjects: List<JoystickInputCallbacks>

    fun pollInput(waitForInput: Boolean)

    fun addMKInputCallback(mouseKeyboardInputCallbacks: MouseKeyboardInputCallbacks)
    fun addJInputCallback(joystickInputCallbacks: JoystickInputCallbacks)

    fun removeMKInputCallback(mouseKeyboardInputCallbacks: MouseKeyboardInputCallbacks)
    fun removeJInputCallback(joystickInputCallbacks: JoystickInputCallbacks)

    fun clearMKInputCallbacks()
    fun clearJInputCallbacks()

}
