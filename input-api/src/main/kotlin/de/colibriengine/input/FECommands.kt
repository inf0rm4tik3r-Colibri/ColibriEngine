package de.colibriengine.input

import de.colibriengine.input.gamepads.XboxOneGamepad
import org.lwjgl.glfw.GLFW

// TODO: Move into engine and abstract further to support dynamic key-mapping that games can change freely.
object FECommands {

    @JvmField
    val CLOSE_COMMAND: Command =
        Command(GLFW.GLFW_KEY_ESCAPE, GLFW.GLFW_RELEASE, XboxOneGamepad.BACK, GLFW.GLFW_RELEASE, -1)

    @JvmField
    val TOGGLE_FULLSCREEN: Command =
        Command(GLFW.GLFW_KEY_F1, GLFW.GLFW_PRESS, XboxOneGamepad.START, GLFW.GLFW_PRESS, -1)

    @JvmField
    val LOCK_MOUSE: Command =
        Command(GLFW.GLFW_KEY_LEFT_CONTROL, GLFW.GLFW_PRESS, XboxOneGamepad.X, GLFW.GLFW_PRESS, -1)

    @JvmField
    val MOVE_FORWARD: Command = Command(GLFW.GLFW_KEY_W, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.LS_Y)

    @JvmField
    val MOVE_BACKWARD: Command = Command(GLFW.GLFW_KEY_S, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.LS_Y)

    @JvmField
    val MOVE_LEFT: Command = Command(GLFW.GLFW_KEY_A, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.LS_X)

    @JvmField
    val MOVE_RIGHT: Command = Command(GLFW.GLFW_KEY_D, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.LS_X)

    @JvmField
    val MOVE_UP: Command = Command(GLFW.GLFW_KEY_SPACE, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.RT)

    @JvmField
    val MOVE_DOWN: Command = Command(GLFW.GLFW_KEY_LEFT_SHIFT, GLFW.GLFW_PRESS, -1, -1, XboxOneGamepad.LT)

    @JvmField
    val SPRINT: Command = Command(GLFW.GLFW_KEY_LEFT_ALT, GLFW.GLFW_REPEAT, -1, -1, XboxOneGamepad.LT)

    @JvmField
    val RESET_CAMERA: Command = Command(GLFW.GLFW_KEY_C, GLFW.GLFW_PRESS, XboxOneGamepad.Y, GLFW.GLFW_PRESS, -1)

    @JvmField
    val CHANGE_PERSPECTIVE: Command =
        Command(GLFW.GLFW_KEY_R, GLFW.GLFW_PRESS, XboxOneGamepad.DPAD_UP, GLFW.GLFW_PRESS, -1)

}
