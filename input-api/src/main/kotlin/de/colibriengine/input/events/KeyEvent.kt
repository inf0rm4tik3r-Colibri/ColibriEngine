package de.colibriengine.input.events

/**
 * Keyboard interaction event.
 *
 * @since ColibriEngine 0.0.6.7
 */
data class KeyEvent(val key: Int = 0, val scancode: Int = 0, val mods: Int = 0)
