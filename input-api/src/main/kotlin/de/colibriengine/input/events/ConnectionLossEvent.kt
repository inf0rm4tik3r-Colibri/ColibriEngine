package de.colibriengine.input.events

import de.colibriengine.objectpooling.ObjectPool

/**
 * An event that represents the loss of a connection.
 *
 * @since ColibriEngine 0.0.6.7
 */
class ConnectionLossEvent private constructor() {

    private val index: Int = 0

    fun set(): ConnectionLossEvent {
        return this
    }

    fun free() {
        POOL.free(this)
    }

    fun reset(): ConnectionLossEvent {
        // Intentionally do nothing.
        return this
    }

    companion object {
        private val POOL = ObjectPool(::ConnectionLossEvent, ConnectionLossEvent::reset)

        @JvmStatic
        fun acquire(): ConnectionLossEvent {
            return POOL.acquire()
        }
    }

}
