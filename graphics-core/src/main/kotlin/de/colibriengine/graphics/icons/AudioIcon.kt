package de.colibriengine.graphics.icons

import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.material.MaterialTextureDefinition
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.model.ModelImpl
import de.colibriengine.graphics.model.concrete.QuadAlignment
import de.colibriengine.graphics.model.concrete.QuadDirection
import de.colibriengine.graphics.model.concrete.QuadImpl
import de.colibriengine.graphics.texture.TextureManager

class AudioIcon(meshFactory: MeshFactory) {

    val model: ModelImpl = QuadImpl.newInstance(
        QuadDirection.XY,
        QuadAlignment.CENTERED,
        meshFactory
    )

    fun createMaterial(textureManager: TextureManager): AudioIcon {
        val ambientTexture = MaterialTextureDefinition()
        ambientTexture.texturePath = texture
        val materialDefinition = MaterialDefinition()
        materialDefinition.ambientColorTexture = ambientTexture
        val material = MaterialImpl(materialDefinition)
        material.loadTextures(textureManager, "")
        model.meshes[0].material = material
        return this
    }

    companion object {
        const val texture = "textures/audio_icon_4.png"
    }

}
