package de.colibriengine.graphics.icons

import de.colibriengine.graphics.material.MaterialDefinition
import de.colibriengine.graphics.material.MaterialImpl
import de.colibriengine.graphics.material.MaterialTextureDefinition
import de.colibriengine.graphics.model.ModelFactory
import de.colibriengine.graphics.model.concrete.Circle
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.math.vector.vec3f.StdVec3fFactory

class LightIcon(modelFactory: ModelFactory, textureManager: TextureManager) {

    val model: Circle = modelFactory.circle(
        StdVec3fFactory.UNIT_X_AXIS,
        StdVec3fFactory.UNIT_Y_AXIS,
        0.25f,
        32u
    )

    init {
        val ambientTexture = MaterialTextureDefinition()
        ambientTexture.texturePath = texture
        val materialDefinition = MaterialDefinition()
        materialDefinition.ambientColorTexture = ambientTexture
        val material = MaterialImpl(materialDefinition)
        material.loadTextures(textureManager, "")
        model.meshes[0].material = material
    }

    companion object {
        const val texture = "textures/sunlight_icon/sunlight_icon.png"
    }

}
