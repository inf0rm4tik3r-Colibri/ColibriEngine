package de.colibriengine.graphics.model.concrete

import de.colibriengine.graphics.model.*
import de.colibriengine.graphics.rendering.PrimitiveMode
import de.colibriengine.math.vector.vec2f.StdVec2f
import de.colibriengine.math.vector.vec2f.Vec2f
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.math.vector.vec3f.Vec3fAccessor

/** Circle - A circle with evenly distributed vertices. */
class CircleImpl(meshFactory: MeshFactory) : ModelImpl(meshFactory, null), Circle {

    companion object {
        private const val ZERO_F = 0.0f

        /** @param radius The radius defines the general size of the sphere. */
        fun create(
            axisA: Vec3fAccessor,
            axisB: Vec3fAccessor,
            radius: Float,
            outerVertexCount: Int,
            meshFactory: MeshFactory
        ): CircleImpl {
            val normalAxis: Vec3fAccessor = StdVec3f().set(axisA).cross(axisB)
            val tmpVec3f: Vec3f = StdVec3f()

            // Create a model definition with a single group.
            val modelDefinition = CustomModelDefinition()
            val group = GroupImpl(GroupImpl.DYNAMIC_SIZE, GroupImpl.DYNAMIC_SIZE)
            group.primitiveMode = PrimitiveMode.TRIANGLES
            modelDefinition.addGroup(group)

            // Add the vertex at the center. (INDEX: 1)
            modelDefinition.addVertex(StdVec3f().set(ZERO_F))
            modelDefinition.addVertexNormal(StdVec3f().set(normalAxis))
            modelDefinition.addVertexTexCoord(StdVec2f().set(0.5f, 0.5f))

            // Add the first vertex on the outer circle. (INDEX: 2)
            var pos: Vec3f = StdVec3f().set(ZERO_F, radius, ZERO_F)
            var norm: Vec3f = StdVec3f().set(normalAxis)
            var texCoord: Vec2f = calcTexCoordFromPosition(tmpVec3f, pos)
            addVertexData(modelDefinition, pos, norm, texCoord)
            val angleStep = 360.0f / outerVertexCount

            // Start at 1: We already added one of the outer vertices!
            for (i in 1 until outerVertexCount) {
                pos = StdVec3f().set(pos).rotate(normalAxis, -angleStep)
                norm = StdVec3f().set(normalAxis)
                texCoord = calcTexCoordFromPosition(tmpVec3f, pos)
                addVertexData(modelDefinition, pos, norm, texCoord)
                val currentIndex = modelDefinition.vertexPositions.size // "3" if in first iteration.
                addFace(
                    group,
                    currentIndex,  // The position we added in this iteration.
                    currentIndex - 1,  // The position we added in the previous iteration ("2" if in first iteration).
                    1 // The center of the circle.
                )
            }

            // Add the last missing face.
            val lastIndex = modelDefinition.vertexPositions.size
            addFace(
                group,
                lastIndex,  // The last vertex of the circle.
                1,  // The center of the circle.
                2 // The first vertex on the circle.
            )

            // Create the circle from the loaded model definition and return it.
            val circle = CircleImpl(meshFactory)
            circle.createFrom(modelDefinition)
            // modelDefinition.free(vec2fFactory, vec3fFactory, null);
            return circle
        }

        private fun calcTexCoordFromPosition(tmpVec3f: Vec3f, position: Vec3fAccessor): Vec2f {
            tmpVec3f.set(position)
                .normalize() // Normalize to be in -1 to +1 range.
                .div(2.0f) // Divide by 2 to get in -0.5 to 0.5 range.
                .plus(0.5f) // Add 0.5 to get in 0 to 1 range (uv coordinates).
            return StdVec2f().set(tmpVec3f.x, tmpVec3f.y)
        }

        private fun addFace(group: GroupImpl, index1: Int, index2: Int, index3: Int) {
            group.addFace(
                FaceVertexAttributeIndicesImpl(
                    index1, index2, index3, // Position indices
                    index1, index2, index3, // Normal indices
                    index1, index2, index3  // TexCoord indices
                )
            )
        }

        private fun addVertexData(
            modelDefinition: CustomModelDefinition,
            position: Vec3f,
            normal: Vec3f,
            texCoord: Vec2f
        ) {
            modelDefinition.addVertex(position)
            modelDefinition.addVertexNormal(normal)
            modelDefinition.addVertexTexCoord(texCoord)
        }
    }

}
