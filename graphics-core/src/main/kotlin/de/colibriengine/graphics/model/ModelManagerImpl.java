package de.colibriengine.graphics.model;

import de.colibriengine.exception.FileTypeUnsupportedException;
import de.colibriengine.exception.ModelLoadException;
import de.colibriengine.exception.ResourceNotFoundException;
import de.colibriengine.graphics.material.MaterialManager;
import de.colibriengine.graphics.model.fbx.loader.FBXLoader;
import de.colibriengine.graphics.model.obj.loader.BOBJLoader;
import de.colibriengine.graphics.model.obj.loader.OBJLoader;
import de.colibriengine.logging.LogUtil;
import de.colibriengine.math.vector.vec2f.StdVec2fFactory;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.StdVec3fFactory;
import de.colibriengine.math.vector.vec3f.Vec3fFactory;
import de.colibriengine.util.FileOps;
import de.colibriengine.util.StringUtil;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.HashMap;

@SuppressWarnings("WeakerAccess")
public class ModelManagerImpl implements ModelManager {

    private static final String OBJ_EXTENSION_NAME = "obj";
    private static final int OBJ_EXTENSION = 1;

    private static final String BOBJ_EXTENSION_NAME = "bobj";
    private static final int BOBJ_EXTENSION = 2;

    private static final String FBX_EXTENSION_NAME = "fbx";
    private static final int FBX_EXTENSION = 3;

    private final ModelFactory modelFactory;

    /**
     * The window to which this model manager belongs.
     * Reference is used to ger the MaterialManager of the window if necessary.
     */
    private final MaterialManager materialManager;

    private static final OBJLoader OBJ_LOADER = new OBJLoader();
    private static final BOBJLoader BOBJ_LOADER = new BOBJLoader();
    private static final FBXLoader FBX_LOADER = new FBXLoader();

    /**
     * The model cache.
     */
    private final HashMap<Integer, Model> loadedModels;
    private static final int LOADED_MATERIAL_LIBS_INITIAL_CAPACITY = 64;

    private static final @NotNull Logger LOG = LogUtil.getLogger(ModelManagerImpl.class);

    public ModelManagerImpl(
            final ModelFactory modelFactory,
            final MaterialManager materialManager
    ) {
        this.modelFactory = modelFactory;
        this.materialManager = materialManager;
        loadedModels = new HashMap<>(LOADED_MATERIAL_LIBS_INITIAL_CAPACITY);
    }

    @Override
    public synchronized @NotNull Model requestModel(final @NotNull String relativePath)
            throws ResourceNotFoundException, FileTypeUnsupportedException, ModelLoadException {

        LOG.info("\"" + relativePath + "\" got requested...");

        // Try to obtain the URL object for the specified relativePath. Throw an IllegalArgumentException if that went wrong.
        final URL modelURL = getResourceURL(relativePath);

        // Generate a mostly unique int value for the model url.
        final int modelURLHash = modelURL.hashCode();

        // If the model is already loaded, we can retrieve its reference by looking up its hash code in loadedModels.
        if (loadedModels.containsKey(modelURLHash)) {
            LOG.info("Found loaded model for: " + relativePath);
            return getLoadedModel(modelURL, modelURLHash);
        }

        // Check whether the requested file is supported by the loader. Determines the loader to use.
        final int fileType = getFileType(modelURL);

        // Otherwise the model has not yet been loaded. Initiate loading...
        return loadModel(fileType, relativePath, modelURL, modelURLHash);
    }

    private @NotNull URL getResourceURL(final @NotNull String path) throws ResourceNotFoundException {
        final URL resourceURL = FileOps.INSTANCE.getAsURL(path);
        if (resourceURL == null) {
            throw new ResourceNotFoundException(
                    "ModelManager#getResourceURL: Path argument does not point to a valid file."
            );
        }
        return resourceURL;
    }

    private synchronized int getFileType(final URL modelURL) throws FileTypeUnsupportedException {
        final String fileTypeString = StringUtil.identifyFileTypeFromExtension(modelURL.getPath());

        return switch (fileTypeString.toLowerCase()) {
            case OBJ_EXTENSION_NAME -> OBJ_EXTENSION;
            case BOBJ_EXTENSION_NAME -> BOBJ_EXTENSION;
            case FBX_EXTENSION_NAME -> FBX_EXTENSION;
            default -> throw new FileTypeUnsupportedException("ModelManager#getFileType: " +
                    "File type: \"" + fileTypeString + "\" is not supported! Unable to load model."
            );
        };
    }

    private synchronized @NotNull Model getLoadedModel(final URL modelURL, final int modelURLHash) {
        final Model storedModel = loadedModels.get(modelURLHash);

        if (storedModel.getConstructedFrom() != null) {
            if (storedModel.getConstructedFrom().equals(modelURL)) {
                LOG.info("Model is already loaded: \"" + modelURL.getPath() + "\"");
                return storedModel;
            }

            LOG.error("Model with same hash code but different path found. Request rejected.");
            LOG.error("stored: \t" + storedModel.getConstructedFrom().getPath());
            LOG.error("requested: \t" + modelURL.getPath());
            throw new RuntimeException("Found multiple objects with the same hash code!");
        }
        throw new RuntimeException("URL was not present on stored model!");
    }

    private synchronized @NotNull Model loadModel(final int type,
                                                      String relativePath,
                                                      final URL url,
                                                      final int urlHash)
            throws ModelLoadException {

        // TODO: Provide factories!
        Vec2fFactory vec2fFactory = new StdVec2fFactory();
        Vec3fFactory vec3fFactory = new StdVec3fFactory();

        final AbstractModelDefinition modelDefinition = switch (type) {
            case OBJ_EXTENSION -> OBJ_LOADER.load(url, relativePath, vec2fFactory, vec3fFactory);
            case BOBJ_EXTENSION -> BOBJ_LOADER.load(url, relativePath, vec2fFactory, vec3fFactory);
            case FBX_EXTENSION -> FBX_LOADER.load(url, relativePath, vec2fFactory, vec3fFactory);
            default -> throw new IllegalStateException("Unknown model type. Should have already been checked!");
        };

        LOG.info("\"" + url.getPath() + "\" got loaded.");

        // Load all material libraries referenced in the model definition.
        for (final String matLibPath : modelDefinition.getMatLibPaths()) {
            modelDefinition.addMatLib(
                    materialManager.requestMaterialLibrary(matLibPath)
            );
        }

        // The model should be "created". This initializes all sorts of OpenGL stuff, which allows us to render the
        // model.
        // TODO: Move GL_TRIANGLES definition.. An .obj file could define multiple groups od different style
        // (TRIANGLES or LINES).
        LOG.debug("Creating model from definition: {}", modelDefinition.getGroups().stream()
                .map(Group::getName).reduce("", (acc, next) -> acc + ", " + next));
        //final ModelImpl model = new ModelImpl(modelFactory.getMeshFactory(), modelDefinition.getConstructedFrom());
        //model.createFrom(modelDefinition);
        Model model = modelFactory.create(modelDefinition);


        /*
         * TODO: We cannot free the model definition!
         *  This leads to the materials being freed / cleaned up.
         *  This leads to visual glitches!!!!!
         */
        // modelDefinition.free(vec2fFactory, vec3fFactory, null);

        // Insert the loaded model with its key in the hash map. Overwrites a previously stored model if necessary.
        loadedModels.put(urlHash, model);
        LOG.debug("Stored model: {}", relativePath);
        return model;
    }

    public synchronized void invalidateCache(final @NotNull String path) {
        final URL url = FileOps.INSTANCE.getAsURL(path);
        if (url == null) {
            throw new ResourceNotFoundException("ModelManager#invalidateCache: Path did not point to a valid file.");
        }
        invalidateCache(url);
    }

    public synchronized void invalidateCache(final @NotNull URL url) {
        final int hash = url.hashCode();
        if (loadedModels.containsKey(hash)) {
            loadedModels.remove(hash);
        } else {
            LOG.error("Specified resource was not loaded.");
        }
    }

    public synchronized void release(final ModelImpl model) {
        final int modelURLHash = model.getConstructedFrom().hashCode();

        if (loadedModels.containsKey(modelURLHash)) {
            LOG.info("Found loaded model. Releasing it now...");

            // Release the model and remove it from this manager.
            model.release();
            loadedModels.remove(modelURLHash);
        }
    }

    @Override
    public synchronized void releaseAllModels() {
        LOG.info("Releasing {} models.", loadedModels.size());

        // Release every model that is still loaded.
        loadedModels.values().forEach(Model::release);

        // And clear the HashMap that held all the model references.
        loadedModels.clear();
    }

    @NotNull
    @Override
    public ModelFactory getModelFactory() {
        return modelFactory;
    }

}
