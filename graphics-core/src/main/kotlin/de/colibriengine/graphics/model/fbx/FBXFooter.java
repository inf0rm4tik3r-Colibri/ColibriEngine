package de.colibriengine.graphics.model.fbx;

import de.colibriengine.logging.LogUtil;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class FBXFooter {
    
    private final @NotNull byte[] payload;
    
    private static final @NotNull Logger LOG = LogUtil.getLogger(FBXFooter.class);
    
    public FBXFooter(final @NotNull byte[] payload) {
        this.payload = payload;
    }
    
    public void print() {
        LOG.info(this);
    }
    
    public @NotNull byte[] getPayload() {
        return payload;
    }
    
    @Override
    public String toString() {
        return "Payload: " + Arrays.toString(payload);
    }
    
}
