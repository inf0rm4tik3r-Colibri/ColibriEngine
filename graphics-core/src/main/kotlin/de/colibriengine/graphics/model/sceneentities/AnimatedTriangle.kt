package de.colibriengine.graphics.model.sceneentities

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.components.RenderComponent
import de.colibriengine.graphics.model.MeshFactory
import de.colibriengine.graphics.model.concrete.Triangle
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.scene.graph.AbstractSceneGraphNode
import de.colibriengine.util.Timer
import kotlin.math.sin

class AnimatedTriangle(
    entity: Entity,
    meshFactory: MeshFactory
) : AbstractSceneGraphNode(entity) {

    private var value = 0f
    private val vec3f = StdVec3f()

    init {
        entity.add(
            RenderComponent(Triangle(meshFactory))
        )
    }

    override fun init() {}

    override fun update(timing: Timer.View) {
        value += timing.deltaSP()
        transform.setTranslation(sin(value.toDouble()).toFloat() * 0.6f, 0.0f, 0.0f)
        transform.rotate(transform.rotation.up(vec3f), timing.deltaSP() * 60.0f)
    }

    override fun destroy() {}

}
