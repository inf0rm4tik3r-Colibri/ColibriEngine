package de.colibriengine.graphics.model.fbx;

public enum FBXArrayType {
    
    BOOLEAN_ARRAY,
    BYTE_ARRAY,
    INT_ARRAY,
    LONG_ARRAY,
    FLOAT_ARRAY,
    DOUBLE_ARRAY

}
