package de.colibriengine.graphics.model.concrete;

import de.colibriengine.graphics.model.*;
import de.colibriengine.graphics.rendering.PrimitiveMode;
import de.colibriengine.math.FEMath;
import de.colibriengine.math.vector.vec2f.StdVec2fFactory;
import de.colibriengine.math.vector.vec2f.Vec2f;
import de.colibriengine.math.vector.vec2f.Vec2fFactory;
import de.colibriengine.math.vector.vec3f.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

/**
 * Icosphere - A sphere with evenly distributed vertices.
 */
public class Icosphere extends ModelImpl {

    private static final Vec2fFactory VEC2F_FACTORY = new StdVec2fFactory();
    private static final Vec3fFactory VEC3F_FACTORY = new StdVec3fFactory();

    private static final StdVec3f HELPER_VEC3F_1 = new StdVec3f();
    private static final StdVec3f HELPER_VEC3F_2 = new StdVec3f();
    private static final StdVec3f HELPER_VEC3F_3 = new StdVec3f();
    private static final StdVec3f HELPER_VEC3F_4 = new StdVec3f();

    private static final float ZERO_F = 0.0f;
    private static final float ONE_F = 1.0f;

    /**
     * Constructs a new icosphere model ready for rendering.
     * It contains properly generated normals and uv coordinates.
     *
     * <b>See: </b> <a href="http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html">
     * Creating an icosphere mesh in code
     * </a>
     *
     * @param radius  The radius defines the general size (radius) of the sphere.
     * @param density The density controls the polygon count. A density level of 1 results in a 20-faced icosphere.
     *                For a higher value, a recursive algorithm will further refine the 20-faced base icosphere, quadrupling the
     *                face count at each iteration.
     */
    public Icosphere(final float radius, final int density, final @NotNull MeshFactory meshFactory) {
        super(meshFactory,null);

        // Create a model definition with a single group.
        final @NotNull CustomModelDefinition modelDefinition = new CustomModelDefinition();
        final @NotNull GroupImpl group = new GroupImpl(GroupImpl.DYNAMIC_SIZE, GroupImpl.DYNAMIC_SIZE);
        group.setPrimitiveMode(PrimitiveMode.TRIANGLES);
        modelDefinition.addGroup(group);

        // Add the 12 initial vertices of the icosahedron.
        addInitialPositions(modelDefinition, radius);

        // Add the 20 initial faces of the icosahedron.
        addInitialFaces(group);

        final @NotNull HashMap<Long, Integer> midPosIndexCache = new HashMap<>(64);

        // Recursively generate a more detailed icosphere.
        refineIcosphere(modelDefinition, group, midPosIndexCache, radius, density);

        // Iterate over all existent faces. Generate their (angle weighted) normal vectors and store them.
        // Store the information which position vector is attached to which normal.
        final @NotNull Vec3f[] angleWeightedNormals = new StdVec3f[group.getFaceVertexAttributeIndices().size() * 3];
        final ArrayList<Integer>[] positionIndexToAngleWeightedNormal = genArrayListArray(
                modelDefinition.getVertexPositions().size(), 6
        );

        computeAngleWeightedFaceNormals(
                modelDefinition, group,
                angleWeightedNormals, positionIndexToAngleWeightedNormal,
                false
        );

        /*
         * Stores a mapping of: positionIndex -> normalIndex. This allows us to check which normal should be used at
         * which position.
         */
        final int[] positionIndexToNormalIndex = new int[modelDefinition.getVertexPositions().size()];

        /*
         * Stores a mapping of: positionIndex -> texCoordIndex. This allows us to check which texCoord should be used at
         * which position.
         */
        final int[] positionIndexToTexCoordIndex = new int[modelDefinition.getVertexPositions().size()];

        // Compute the normal vectors and texture coordinates for each vertex position.
        computeVertexNormalsAndTextureCoordinates(
                modelDefinition, angleWeightedNormals, positionIndexToAngleWeightedNormal,
                positionIndexToNormalIndex, positionIndexToTexCoordIndex
        );

        // Update the faces, so that they contain their correct normal and texture coordinate indices.
        updateFaces(group, positionIndexToNormalIndex, positionIndexToTexCoordIndex);

        // Create this icosphere from the loaded model definition.
        this.createFrom(modelDefinition);
        // modelDefinition.free(VEC2F_FACTORY, VEC3F_FACTORY, null);
    }

    private <T> ArrayList<T>[] genArrayListArray(final int length, final int initialCapacity) {
        @SuppressWarnings("unchecked") final ArrayList<T>[] arrayLists = new ArrayList[length];

        for (int i = 0; i < arrayLists.length; i++) {
            arrayLists[i] = new ArrayList<>(initialCapacity);
        }

        return arrayLists;
    }

    private void addInitialPositions(final @NotNull CustomModelDefinition modelDefinition, final float radius) {
        // xy-plane
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(-ONE_F, FEMath.GOLDEN_RATIO_F, ZERO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ONE_F, FEMath.GOLDEN_RATIO_F, ZERO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(-ONE_F, -FEMath.GOLDEN_RATIO_F, ZERO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ONE_F, -FEMath.GOLDEN_RATIO_F, ZERO_F).normalize().times(radius));
        // yz-plane
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ZERO_F, -ONE_F, FEMath.GOLDEN_RATIO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ZERO_F, ONE_F, FEMath.GOLDEN_RATIO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ZERO_F, -ONE_F, -FEMath.GOLDEN_RATIO_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(ZERO_F, ONE_F, -FEMath.GOLDEN_RATIO_F).normalize().times(radius));
        // xz-plane
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(FEMath.GOLDEN_RATIO_F, ZERO_F, -ONE_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(FEMath.GOLDEN_RATIO_F, ZERO_F, ONE_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(-FEMath.GOLDEN_RATIO_F, ZERO_F, -ONE_F).normalize().times(radius));
        modelDefinition
                .addVertex(VEC3F_FACTORY.acquireDirty().set(-FEMath.GOLDEN_RATIO_F, ZERO_F, ONE_F).normalize().times(radius));
    }

    private void addInitialFaces(final @NotNull GroupImpl group) {
        // 5 faces around point 0.
        group.addFace(new FaceVertexAttributeIndicesImpl(1, 12, 6));
        group.addFace(new FaceVertexAttributeIndicesImpl(1, 6, 2));
        group.addFace(new FaceVertexAttributeIndicesImpl(1, 2, 8));
        group.addFace(new FaceVertexAttributeIndicesImpl(1, 8, 11));
        group.addFace(new FaceVertexAttributeIndicesImpl(1, 11, 12));
        // 5 adjacent faces.
        group.addFace(new FaceVertexAttributeIndicesImpl(2, 6, 10));
        group.addFace(new FaceVertexAttributeIndicesImpl(6, 12, 5));
        group.addFace(new FaceVertexAttributeIndicesImpl(12, 11, 3));
        group.addFace(new FaceVertexAttributeIndicesImpl(11, 8, 7));
        group.addFace(new FaceVertexAttributeIndicesImpl(8, 2, 9));
        // 5 faces around point 3.
        group.addFace(new FaceVertexAttributeIndicesImpl(4, 10, 5));
        group.addFace(new FaceVertexAttributeIndicesImpl(4, 5, 3));
        group.addFace(new FaceVertexAttributeIndicesImpl(4, 3, 7));
        group.addFace(new FaceVertexAttributeIndicesImpl(4, 7, 9));
        group.addFace(new FaceVertexAttributeIndicesImpl(4, 9, 10));
        // 5 adjacent faces.
        group.addFace(new FaceVertexAttributeIndicesImpl(5, 10, 6));
        group.addFace(new FaceVertexAttributeIndicesImpl(3, 5, 12));
        group.addFace(new FaceVertexAttributeIndicesImpl(7, 3, 11));
        group.addFace(new FaceVertexAttributeIndicesImpl(9, 7, 8));
        group.addFace(new FaceVertexAttributeIndicesImpl(10, 9, 2));
    }

    private void refineIcosphere(
            final @NotNull CustomModelDefinition modelDefinition,
            final @NotNull GroupImpl group,
            final @NotNull HashMap<Long, Integer> midPosIndexCache,
            final float radius, final int recursionLevel
    ) {
        @NotNull ListIterator<FaceVertexAttributeIndicesImpl> iterator;

        for (int i = 0; i < recursionLevel; i++) {
            @NotNull FaceVertexAttributeIndicesImpl face;
            int[] facePosIndices;

            iterator = group.getFaceVertexAttributeIndices().listIterator();
            while (iterator.hasNext()) {
                face = iterator.next();
                final int posIndex1 = face.getPositionIndices()[0];
                final int posIndex2 = face.getPositionIndices()[1];
                final int posIndex3 = face.getPositionIndices()[2];

                // Replace triangle by 4 triangles.
                final int midA = getMiddlePosIndex(modelDefinition, midPosIndexCache, radius, posIndex1, posIndex2);
                final int midB = getMiddlePosIndex(modelDefinition, midPosIndexCache, radius, posIndex2, posIndex3);
                final int midC = getMiddlePosIndex(modelDefinition, midPosIndexCache, radius, posIndex3, posIndex1);

                // Removes the current face! We are going to compute 4 new faces.
                iterator.remove();

                // Add the new faces of the more detailed icosphere.
                iterator.add(new FaceVertexAttributeIndicesImpl(posIndex1, midA, midC)); // top
                iterator.add(new FaceVertexAttributeIndicesImpl(posIndex2, midB, midA)); // left
                iterator.add(new FaceVertexAttributeIndicesImpl(posIndex3, midC, midB)); // right
                iterator.add(new FaceVertexAttributeIndicesImpl(midA, midB, midC)); // center
            }
        }
    }

    private long hash(final int posIndex1, final int posIndex2) {
        final boolean firstIsSmaller = posIndex1 < posIndex2;
        final long smallerIndex = firstIsSmaller ? posIndex1 : posIndex2;
        final long greaterIndex = firstIsSmaller ? posIndex2 : posIndex1;
        return (smallerIndex << 32) + greaterIndex; // TODO: Bitwise or possible?
    }

    // return index of point in the middle of p1 and p2
    private int getMiddlePosIndex(
            final @NotNull CustomModelDefinition modelDefinition,
            final @NotNull HashMap<Long, Integer> midPosIndexCache,
            final float radius, final int posIndex1, final int posIndex2
    ) {
        // Compute a key for the cache-map.
        final long key = hash(posIndex1, posIndex2);

        // First check if the position was already created.
        final @Nullable Integer cached = midPosIndexCache.get(key);
        if (cached != null) {
            return cached;
        }

        // Not in cache, calculate it:
        // First normalize the middle position. This makes sure that the new point lies on a unit sphere. Then apply
        // the user defined radius.
        final @NotNull Vec3f point1 = modelDefinition.getVertexPositions().get(posIndex1 - 1);
        final @NotNull Vec3f point2 = modelDefinition.getVertexPositions().get(posIndex2 - 1);
        final @NotNull Vec3f middle = VEC3F_FACTORY.acquireDirty().set(point1).plus(point2).div(2.0f).normalize()
                .times(radius);

        // Add the new vertex position to our model definition.
        modelDefinition.addVertex(middle);

        final int middlePosIndex = modelDefinition.getVertexPositions().size();

        // Store it, return index.
        midPosIndexCache.put(key, middlePosIndex);
        return middlePosIndex;
    }


    private void computeAngleWeightedFaceNormals(
            final @NotNull CustomModelDefinition modelDefinition,
            final @NotNull GroupImpl group,
            final @NotNull Vec3f[] angleWeightedNormals,
            final @NotNull ArrayList<Integer>[] positionIndexToAngleWeightedNormal,
            final boolean areaWeighting
    ) {
        for (int i = 0; i < group.getFaceVertexAttributeIndices().size(); i++) {
            // Compute the i'th face.
            final @NotNull FaceVertexAttributeIndicesImpl face = group.getFaceVertexAttributeIndices().get(i);

            // Note: Indices are normally 1-based.
            final int posIndex1base0 = face.getPositionIndices()[0] - 1;
            final int posIndex2base0 = face.getPositionIndices()[1] - 1;
            final int posIndex3base0 = face.getPositionIndices()[2] - 1;

            // Get the position vectors for the current face.
            final @NotNull Vec3f pos1Vec = modelDefinition.getVertexPositions().get(posIndex1base0);
            final @NotNull Vec3f pos2Vec = modelDefinition.getVertexPositions().get(posIndex2base0);
            final @NotNull Vec3f pos3Vec = modelDefinition.getVertexPositions().get(posIndex3base0);

            // Calculate the edge vectors of the current face.
            HELPER_VEC3F_1.set(pos2Vec).minus(pos1Vec);
            HELPER_VEC3F_2.set(pos3Vec).minus(pos1Vec);
            HELPER_VEC3F_3.set(pos1Vec).minus(pos2Vec);
            HELPER_VEC3F_4.set(pos3Vec).minus(pos2Vec);
            final @NotNull Vec3f normal = VEC3F_FACTORY.acquireDirty().set(HELPER_VEC3F_1).cross(HELPER_VEC3F_2);
            if (!areaWeighting) {
                normal.normalize();
            }

            // Calculate the angle between each to edges of the current face.
            final float p1Angle = HELPER_VEC3F_1.angleDegrees(HELPER_VEC3F_2); // Angle around p1.
            final float p2Angle = HELPER_VEC3F_3.angleDegrees(HELPER_VEC3F_4); // Angle around p2.
            final float p3Angle = 180.0f - p1Angle - p2Angle; // Angle around p3.

            // Calculate the angle-weighted normal vectors for each vertex position and add them to a list.
            final @NotNull Vec3f p1Normal = VEC3F_FACTORY.acquireDirty().set(normal).times(p1Angle);
            final @NotNull Vec3f p2Normal = VEC3F_FACTORY.acquireDirty().set(normal).times(p2Angle);
            final @NotNull Vec3f p3Normal = VEC3F_FACTORY.acquireDirty().set(normal).times(p3Angle);

            final int weightedNormalBaseIndex = i * 3;
            final int weightedNormal2Index = weightedNormalBaseIndex + 1;
            final int weightedNormal3Index = weightedNormalBaseIndex + 2;

            angleWeightedNormals[weightedNormalBaseIndex] = (p1Normal);
            angleWeightedNormals[weightedNormal2Index] = p2Normal;
            angleWeightedNormals[weightedNormal3Index] = p3Normal;

            // Store relation to position indices.
            positionIndexToAngleWeightedNormal[posIndex1base0].add(weightedNormalBaseIndex);
            positionIndexToAngleWeightedNormal[posIndex2base0].add(weightedNormal2Index);
            positionIndexToAngleWeightedNormal[posIndex3base0].add(weightedNormal3Index);
        }
    }

    private void computeVertexNormalsAndTextureCoordinates(
            final @NotNull CustomModelDefinition modelDefinition,
            final @NotNull Vec3f[] angleWeightedNormals,
            final @NotNull ArrayList<Integer>[] positionIndexToAngleWeightedNormal,
            final int[] positionIndexToNormalIndexMap,
            final int[] positionIndexToTexCoordIndexMap
    ) {
        for (int i = 0; i < positionIndexToAngleWeightedNormal.length; i++) {
            final @NotNull ArrayList<Integer> angleWeightedNormalsIndexList = positionIndexToAngleWeightedNormal[i];

            // Accumulate all weighted face normals which belong to faces where the current position index is a part of.
            final @NotNull Vec3f normal = VEC3F_FACTORY.acquireDirty();
            for (final int weightedNormalIndex : angleWeightedNormalsIndexList) {
                normal.plus(angleWeightedNormals[weightedNormalIndex]);
            }
            normal.normalize();

            // Add the calculated vertex normal to the model definition. (Note: Indexes are 1-based)
            modelDefinition.addVertexNormal(normal);
            positionIndexToNormalIndexMap[i] = modelDefinition.getVertexNormals().size();

            // Calculate and store the vertex texture coordinates. (Note: Indexes are 1-based)
            modelDefinition.addVertexTexCoord(calculateTexCoord(normal));
            positionIndexToTexCoordIndexMap[i] = modelDefinition.getVertexTextCoords().size();
        }
    }

    private void updateFaces(
            final @NotNull GroupImpl group,
            final int[] positionIndexToNormalIndexMap,
            final int[] positionIndexToTexCoordIndexMap
    ) {
        for (final @NotNull FaceVertexAttributeIndicesImpl face : group.getFaceVertexAttributeIndices()) {
            // Note: Indices are normally 1-based.
            final int posIndex1based0 = face.getPositionIndices()[0] - 1;
            final int posIndex2based0 = face.getPositionIndices()[1] - 1;
            final int posIndex3based0 = face.getPositionIndices()[2] - 1;

            final int normalIndex1 = positionIndexToNormalIndexMap[posIndex1based0];
            final int normalIndex2 = positionIndexToNormalIndexMap[posIndex2based0];
            final int normalIndex3 = positionIndexToNormalIndexMap[posIndex3based0];
            final int texCoordIndex1 = positionIndexToTexCoordIndexMap[posIndex1based0];
            final int texCoordIndex2 = positionIndexToTexCoordIndexMap[posIndex2based0];
            final int texCoordIndex3 = positionIndexToTexCoordIndexMap[posIndex3based0];

            face.setNormalIndices(normalIndex1, normalIndex2, normalIndex3);
            face.setTexCoordIndices(texCoordIndex1, texCoordIndex2, texCoordIndex3);
        }
    }

    /**
     * <b>See: </b> <a href="http://www.mvps.org/directx/articles/spheremap.htm">
     * Spherical (UV) Mapping with Normals
     * </a>
     */
    private Vec2f calculateTexCoord(final @NotNull Vec3fAccessor normal) {
        return VEC2F_FACTORY.acquireDirty().set(
                //(float) Math.asin(normal.x()) / FEMath.PI_F + 0.5f,
                //(float) Math.asin(normal.y()) / FEMath.PI_F + 0.5f
                ((float) Math.atan2(normal.getX(), normal.getZ()) / FEMath.PI_F) / 2.0f + 0.5f,
                ((float) Math.asin(normal.getY()) / (FEMath.PI_F / 2.0f)) / 2.0f + 0.5f
        );
    }

}
