package de.colibriengine.graphics.model;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;

import org.jetbrains.annotations.NotNull;

/**
 * Face // TODO: Make poolable!!!
 */
public class FaceVertexAttributeIndicesImpl implements FaceVertexAttributeIndices {
    
    public static final int INTS  = 3 + 3 + 3 + 3; // position + normal + texCoord + color
    public static final int BYTES = INTS * Integer.BYTES;
    
    private static final ByteBuffer BYTE_CONVERSION_BUFFER = ByteBuffer.allocate(BYTES);
    
    private static final String EXCEPTION_MSG_INDEX_NEGATIVE = "Indices must not be negative!";
    
    private final int vertexCount;
    
    private int[] positionIndices;
    private int[] normalIndices;
    private int[] texCoordIndices;
    private int[] colorIndices;
    
    /**
     * Private default constructor.
     * Used in static initializer methods.
     *
     * @param vertexCount
     *     The amount of vertex indices this face contains.
     */
    private FaceVertexAttributeIndicesImpl(final int vertexCount) {
        this.vertexCount = vertexCount;
        this.positionIndices = new int[vertexCount];
    }
    
    /**
     * Constructs a new Face object by storing the references of the given arrays.
     */
    public FaceVertexAttributeIndicesImpl(
        final int vertexCount,
        final int[] positionIndices,
        final int[] normalIndices,
        final int[] texCoordIndices
    ) {
        this(vertexCount, positionIndices, normalIndices, texCoordIndices, null);
    }
    
    /**
     * Constructs a new Face object by storing the references of the given arrays.
     */
    public FaceVertexAttributeIndicesImpl(
        final int vertexCount,
        final int[] positionIndices,
        final int[] normalIndices,
        final int[] texCoordIndices,
        final int[] colorIndices
    ) {
        this(vertexCount);
        this.positionIndices = positionIndices;
        this.normalIndices = normalIndices;
        this.texCoordIndices = texCoordIndices;
        this.colorIndices = colorIndices;
    }
    
    /**
     * Constructs a new Face object by storing the references of the given arrays.
     */
    public FaceVertexAttributeIndicesImpl(
        final int positionIndex1, final int positionIndex2, final int positionIndex3
    ) {
        if (positionIndex1 < 0 || positionIndex2 < 0 || positionIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        
        positionIndices = new int[3];
        positionIndices[0] = positionIndex1;
        positionIndices[1] = positionIndex2;
        positionIndices[2] = positionIndex3;
        
        this.vertexCount = 3;
    }
    
    /**
     * Constructs a new Face object by storing the references of the given arrays.
     */
    public FaceVertexAttributeIndicesImpl(
        final int positionIndex1, final int positionIndex2, final int positionIndex3,
        final int normalIndex1, final int normalIndex2, final int normalIndex3,
        final int texCoordIndex1, final int texCoordIndex2, final int texCoordIndex3
    ) {
        if (positionIndex1 < 0 || positionIndex2 < 0 || positionIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        if (normalIndex1 < 0 || normalIndex2 < 0 || normalIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        if (texCoordIndex1 < 0 || texCoordIndex2 < 0 || texCoordIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        
        positionIndices = new int[3];
        positionIndices[0] = positionIndex1;
        positionIndices[1] = positionIndex2;
        positionIndices[2] = positionIndex3;
        
        normalIndices = new int[3];
        normalIndices[0] = normalIndex1;
        normalIndices[1] = normalIndex2;
        normalIndices[2] = normalIndex3;
        
        texCoordIndices = new int[3];
        texCoordIndices[0] = texCoordIndex1;
        texCoordIndices[1] = texCoordIndex2;
        texCoordIndices[2] = texCoordIndex3;
        
        this.vertexCount = 3;
    }
    
    /**
     * Constructs a new Face object by storing the references of the given arrays.
     */
    public FaceVertexAttributeIndicesImpl(
        final int positionIndex1, final int positionIndex2, final int positionIndex3,
        final int normalIndex1, final int normalIndex2, final int normalIndex3,
        final int texCoordIndex1, final int texCoordIndex2, final int texCoordIndex3,
        final int colorIndex1, final int colorIndex2, final int colorIndex3
    ) {
        if (positionIndex1 < 0 || positionIndex2 < 0 || positionIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        if (normalIndex1 < 0 || normalIndex2 < 0 || normalIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        if (texCoordIndex1 < 0 || texCoordIndex2 < 0 || texCoordIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        if (colorIndex1 < 0 || colorIndex2 < 0 || colorIndex3 < 0) {
            throw new IllegalArgumentException(EXCEPTION_MSG_INDEX_NEGATIVE);
        }
        
        positionIndices = new int[3];
        positionIndices[0] = positionIndex1;
        positionIndices[1] = positionIndex2;
        positionIndices[2] = positionIndex3;
        
        normalIndices = new int[3];
        normalIndices[0] = normalIndex1;
        normalIndices[1] = normalIndex2;
        normalIndices[2] = normalIndex3;
        
        texCoordIndices = new int[3];
        texCoordIndices[0] = texCoordIndex1;
        texCoordIndices[1] = texCoordIndex2;
        texCoordIndices[2] = texCoordIndex3;
        
        colorIndices = new int[3];
        colorIndices[0] = colorIndex1;
        colorIndices[1] = colorIndex2;
        colorIndices[2] = colorIndex3;
        
        this.vertexCount = 3;
    }
    
    public static @NotNull FaceVertexAttributeIndicesImpl line(final int positionIndex1, final int positionIndex2) {
        final FaceVertexAttributeIndicesImpl face = new FaceVertexAttributeIndicesImpl(2);
        face.positionIndices[0] = positionIndex1;
        face.positionIndices[1] = positionIndex2;
        return face;
    }

    @Override
    public int getVertexCount() {
        return vertexCount;
    }

    @Override
    public int[] getPositionIndices() {
        return positionIndices;
    }
    
    public void setNormalIndices(final int normalIndex1, final int normalIndex2, final int normalIndex3) {
        if (normalIndices == null) {
            normalIndices = new int[3];
        }
        normalIndices[0] = normalIndex1;
        normalIndices[1] = normalIndex2;
        normalIndices[2] = normalIndex3;
    }

    @Override
    public int[] getNormalIndices() {
        return normalIndices;
    }
    
    public void setTexCoordIndices(final int texCoordIndex1, final int texCoordIndex2, final int texCoordIndex3) {
        if (texCoordIndices == null) {
            texCoordIndices = new int[3];
        }
        texCoordIndices[0] = texCoordIndex1;
        texCoordIndices[1] = texCoordIndex2;
        texCoordIndices[2] = texCoordIndex3;
    }

    @Override
    public int[] getTexCoordIndices() {
        return texCoordIndices;
    }

    @Override
    public int[] getColorIndices() {
        return colorIndices;
    }
    
    public byte[] bytes() {
        BYTE_CONVERSION_BUFFER.clear();
        
        for (final int vi : positionIndices) {
            BYTE_CONVERSION_BUFFER.putInt(vi);
        }
        if (normalIndices != null) {
            for (final int ni : normalIndices) {
                BYTE_CONVERSION_BUFFER.putInt(ni);
            }
        }
        if (texCoordIndices != null) {
            for (final int ti : texCoordIndices) {
                BYTE_CONVERSION_BUFFER.putInt(ti);
            }
        }
        
        return BYTE_CONVERSION_BUFFER.array();
    }
    
    @Override
    public String toString() {
        String s = "Face [ ";
        s += "PositionIndices: " + positionIndices[0] + ", " + positionIndices[1] + ", " + positionIndices[2] + " | ";
        if (normalIndices != null) {
            s += "NormalIndices: " + normalIndices[0] + ", " + normalIndices[1] + ", " + normalIndices[2] + " | ";
        }
        if (texCoordIndices != null) {
            s += "TexCoordIndices: " + texCoordIndices[0] + ", " + texCoordIndices[1] + " ]";
        }
        return s;
    }
    
    @Override
    public ByteBuffer storeIn(@NotNull final ByteBuffer buffer) {
        for (final int vertexPositionIndex : positionIndices) {
            buffer.putInt(vertexPositionIndex);
        }
        
        if (normalIndices != null) {
            for (final int vertexNormalIndex : normalIndices) {
                buffer.putInt(vertexNormalIndex);
            }
        }
        
        if (texCoordIndices != null) {
            for (final int vertexTexCoordIndex : texCoordIndices) {
                buffer.putInt(vertexTexCoordIndex);
            }
        }
        return buffer;
    }
    
    @Override
    public FloatBuffer storeIn(@NotNull final FloatBuffer buffer) {
        for (final int vertexPositionIndex : positionIndices) {
            buffer.put((float) vertexPositionIndex);
        }
        
        if (normalIndices != null) {
            for (final int vertexNormalIndex : normalIndices) {
                buffer.put((float) vertexNormalIndex);
            }
        }
        
        if (texCoordIndices != null) {
            for (final int vertexTexCoordIndex : texCoordIndices) {
                buffer.put((float) vertexTexCoordIndex);
            }
        }
        return buffer;
    }
    
    @Override
    public DoubleBuffer storeIn(@NotNull final DoubleBuffer buffer) throws UnsupportedOperationException {
        for (final int vertexPositionIndex : positionIndices) {
            buffer.put(vertexPositionIndex);
        }
        
        if (normalIndices != null) {
            for (final int vertexNormalIndex : normalIndices) {
                buffer.put(vertexNormalIndex);
            }
        }
        
        if (texCoordIndices != null) {
            for (final int vertexTexCoordIndex : texCoordIndices) {
                buffer.put(vertexTexCoordIndex);
            }
        }
        return buffer;
    }
    
}
