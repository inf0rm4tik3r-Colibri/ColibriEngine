package de.colibriengine.graphics.model

import de.colibriengine.math.vector.vec2f.Vec2fFactory
import de.colibriengine.math.vector.vec3f.Vec3fFactory
import java.net.URL

interface ModelLoader {

    fun load(
        resourceURL: URL,
        relativePath: String,
        vec2fFactory: Vec2fFactory,
        vec3fFactory: Vec3fFactory
    ): AbstractModelDefinition

}
