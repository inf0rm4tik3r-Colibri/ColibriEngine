package de.colibriengine.graphics.model.fbx.loader

import de.colibriengine.exception.ModelLoadException
import de.colibriengine.exception.ResourceInaccessibleException
import de.colibriengine.graphics.model.AbstractModelDefinition
import de.colibriengine.graphics.model.ModelLoader
import de.colibriengine.graphics.model.fbx.*
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.vector.vec2f.Vec2fFactory
import de.colibriengine.math.vector.vec3f.Vec3fFactory
import de.colibriengine.objectpooling.Reusable
import de.colibriengine.util.*
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.io.IOException
import java.net.URL
import java.nio.ByteOrder
import java.nio.charset.StandardCharsets

class FBXLoader : ModelLoader, Reusable<FBXLoader?> {

    private var bytesRead: Long = 0

    override fun load(
        resourceURL: URL,
        relativePath: String,
        vec2fFactory: Vec2fFactory,
        vec3fFactory: Vec3fFactory
    ): AbstractModelDefinition {
        reset()
        val document: FBXDocument = loadFBXDocument(resourceURL)
        document.print()

        // TODO !!!
        return object : AbstractModelDefinition(resourceURL) {}
    }

    private fun loadFBXDocument(resourceURL: URL): FBXDocument {
        try {
            BufferedInputStream(resourceURL.openStream()).use { bis ->
                if (DEBUG) {
                    LOG.debug("AVAILABLE BYTES: " + bis.available())
                }
                val document = FBXDocument
                    .builder()
                    .setHeader(loadHeader(bis))
                    .setRootNode(loadNode(bis))
                    .setFooter(loadFooter(bis))
                    .build()
                if (DEBUG) {
                    LOG.debug("FINISHED")
                    LOG.debug("BYTES_READ: $bytesRead")
                }
                return document
            }
        } catch (e: IOException) {
            throw ResourceInaccessibleException.forPath(resourceURL.path) // TODO: pass resourceName
        }
    }

    private fun loadHeader(bis: BufferedInputStream): FBXHeader {
        val header = FBXHeader
            .builder()
            .setIntroduction(bis.readString(21, Charsets.US_ASCII))
            .setNumber(bis.readUShort(BYTE_ORDER).toInt())
            .setVersionNumber(bis.readUInt(BYTE_ORDER).toLong())
            .build()
        bytesRead += 27
        return header
    }

    private fun loadNode(bis: BufferedInputStream): FBXNode {
        if (DEBUG) {
            LOG.trace("loadNode")
        }
        val endOffset = bis.readUInt(BYTE_ORDER).toLong()
        val numProperties = bis.readUInt(BYTE_ORDER)
        val propertyListLen = bis.readUInt(BYTE_ORDER)
        val nameLen = bis.readUByte()
        val name: String = bis.readString(nameLen.toInt(), StandardCharsets.US_ASCII)
        bytesRead += (4 + 4 + 4 + 1 + nameLen.toInt()).toLong()
        if (DEBUG) {
            LOG.debug("endOffset: $endOffset")
            LOG.debug("numProperties: $numProperties")
            LOG.debug("propertyListLen: $propertyListLen")
            LOG.debug("nameLen: $nameLen")
            LOG.debug("name: $name")
        }

        /* NOTE: The amount of child notes is unknown at this moment and can not be guessed. */
        val node = FBXNode(name, numProperties.toInt(), 0)

        // Skip this node if its name is unknown!
        if (!known(name)) {
            val skipped = bis.skip(endOffset)
            if (skipped != endOffset) {
                throw ModelLoadException("Unexpected error while skipping data.")
            }
            return node
        }

        /* NOTE: propertyListLen tells us the number of bytes which make up the property list! */
        // Load properties.
        for (i in 0 until numProperties.toLong()) {
            node.addProperty(loadPropertyRecord(bis))
        }

        // Load nested nodes...
        while (bytesRead <= endOffset) {
            if (DEBUG) {
                LOG.info("AT_NODE: $name")
                LOG.warn(
                    "[BYTES_READ: " + bytesRead
                        + ", READ_UNTIL: " + endOffset
                        + ", REMAINING: " + bis.available() + "]"
                )
            }
            val nextNode = loadNode(bis)
            node.addChild(nextNode)
        }
        if (DEBUG) {
            LOG.error("LEAVING NODE: $name")
            LOG.warn(
                ("[BYTES_READ: " + bytesRead
                    + ", READ_UNTIL: " + endOffset
                    + ", REMAINING: " + bis.available() + "]")
            )
        }
        return node
    }

    private fun loadPropertyRecord(bis: BufferedInputStream): FBXPropertyRecord {
        if (DEBUG) {
            LOG.trace("loadPropertyRecord")
        }

        val propertyRecord = FBXPropertyRecord()

        val dataTypeCode: Char = bis.read1ByteAsChar()
        if (DEBUG) {
            LOG.debug("Property type code: $dataTypeCode")
        }
        bytesRead += 1

        when (dataTypeCode) {
            /* PRIMITIVE TYPES */
            'C' -> { /* 1 bit boolean (1: true, 0: false) encoded as the LSB of a 1 Byte value. */
                val bitfield = bis.readByteAsBitField()
                propertyRecord.value = bitfield.get(0, Bitfield.AccessDirection.RIGHT) == Bit.ONE
                bytesRead += 1
            }
            'Y' -> { /* Y: 2 byte signed Integer */
                propertyRecord.value = bis.readShort(BYTE_ORDER)
                bytesRead += 2
            }
            'I' -> { /* I: 4 byte signed Integer */
                propertyRecord.value = bis.readInt(BYTE_ORDER)
                bytesRead += 4
            }
            'L' -> { /* L: 8 byte signed Integer */
                propertyRecord.value = bis.readLong(BYTE_ORDER)
                bytesRead += 8
            }
            'F' -> { /* F: 4 byte single-precision IEEE 754 number */
                propertyRecord.value = bis.readFloat(BYTE_ORDER)
                bytesRead += 4
            }
            'D' -> { /* D: 8 byte double-precision IEEE 754 number */
                propertyRecord.value = bis.readDouble(BYTE_ORDER)
                bytesRead += 8
            }

            /* ARRAY TYPES */
            'b' -> { /* b: Array of 1 byte Booleans (always 0 or 1) */
                propertyRecord.value = loadArray(bis, FBXArrayType.BOOLEAN_ARRAY).array
            }
            'i' -> { /* i: Array of 4 byte signed Integer */
                propertyRecord.value = loadArray(bis, FBXArrayType.INT_ARRAY).array
            }
            'l' -> { /* l: Array of 8 byte signed Integer */
                propertyRecord.value = loadArray(bis, FBXArrayType.LONG_ARRAY).array
            }
            'f' -> { /* f: Array of 4 byte single-precision IEEE 754 number */
                propertyRecord.value = loadArray(bis, FBXArrayType.FLOAT_ARRAY).array
            }
            'd' -> { /* d: Array of 8 byte double-precision IEEE 754 number */
                propertyRecord.value = loadArray(bis, FBXArrayType.DOUBLE_ARRAY).array
            }

            /* SPECIAL TYPES */
            'S' -> { /* String */
                val stringLength = bis.readUInt(BYTE_ORDER).toInt()
                propertyRecord.value = bis.readString(stringLength, StandardCharsets.US_ASCII)
                bytesRead += 4 + stringLength
            }
            'R' -> { /* Raw binary data */
                val binaryLength = bis.readUInt(BYTE_ORDER).toInt()
                propertyRecord.value = bis.readByteArray(binaryLength)
                bytesRead += 4 + binaryLength
            }
            else -> throw ModelLoadException(
                "The FBX data type code of a nodes property record could not be recognized: $dataTypeCode"
            )
        }

        if (DEBUG) {
            LOG.debug("Property value: ${propertyRecord.value}")
        }

        return propertyRecord
    }

    private fun loadArray(bis: BufferedInputStream, arrayType: FBXArrayType): FBXArrayData<Any> {
        val arrayInfo: FBXArrayInfo = loadArrayInfo(bis, arrayType)
        var arrayData: FBXArrayData<Any>?

        when (arrayInfo.encoding) {
            0 -> { // The array content is not compressed.
                arrayData = readArray(arrayInfo.arrayType, bis, false, arrayInfo.arrayLength)
                bytesRead += arrayInfo.arrayLength
                return arrayData
            }
            1 -> { // The array content is compressed!
                val compressed = bis.readByteArray(arrayInfo.compressedLength)
                val uncompressed = ZipCompression.decompress(compressed)
                BufferedInputStream(ByteArrayInputStream(uncompressed)).use {
                    arrayData = readArray(arrayInfo.arrayType, it, true, arrayInfo.compressedLength)
                    bytesRead += arrayInfo.compressedLength
                    return arrayData!!
                }
            }
            else -> throw ModelLoadException(
                "The encoding of the FBX array data info could not be recognized: ${arrayInfo.encoding}"
            )
        }
    }

    private fun readArray(
        arrayType: FBXArrayType,
        readStream: BufferedInputStream,
        isCompressed: Boolean,
        arrayLength: Int
    ): FBXArrayData<Any> {
        return when (arrayType) {
            FBXArrayType.BOOLEAN_ARRAY ->
                FBXArrayData(ArrayHelper.box(readStream.readBooleanArray(arrayLength)))
            FBXArrayType.INT_ARRAY ->
                FBXArrayData(
                    ArrayHelper.box(
                        readStream.readIntArray(
                            if (isCompressed) arrayLength / Integer.BYTES else arrayLength,
                            BYTE_ORDER
                        )
                    )
                )
            FBXArrayType.LONG_ARRAY ->
                FBXArrayData(
                    ArrayHelper.box(
                        readStream.readLongArray(
                            if (isCompressed) arrayLength / BYTES_PER_LONG else arrayLength,
                            BYTE_ORDER
                        )
                    )
                )
            FBXArrayType.FLOAT_ARRAY ->
                FBXArrayData(
                    ArrayHelper.box(
                        readStream.readFloatArray(
                            if (isCompressed) arrayLength / BYTES_PER_FLOAT else arrayLength,
                            BYTE_ORDER
                        )
                    )
                )
            FBXArrayType.DOUBLE_ARRAY ->
                FBXArrayData(
                    ArrayHelper.box(
                        readStream.readDoubleArray(
                            if (isCompressed) arrayLength / BYTES_PER_DOUBLE else arrayLength,
                            BYTE_ORDER
                        )
                    )
                )
            else -> throw ModelLoadException("Unexpected array type: " + arrayType.name)
        }
    }

    private fun loadArrayInfo(bis: BufferedInputStream, arrayType: FBXArrayType): FBXArrayInfo {
        val arrayInfo = FBXArrayInfo
            .builder()
            .setArrayType(arrayType)
            .setArrayLength(bis.readInt(BYTE_ORDER)) // +4
            .setEncoding(bis.readInt(BYTE_ORDER)) // +4
            .setCompressedLength(bis.readInt(BYTE_ORDER)) // +4
            .build()
        bytesRead += 12
        return arrayInfo
    }

    private fun known(nodeName: String): Boolean {
        return true // TODO: change back. Just for debugging!
        //return KNOWN_NODE_NAMES.contains(nodeName)
    }

    private fun loadFooter(bis: BufferedInputStream): FBXFooter {
        return FBXFooter(bis.readAll(2048))
    }

    override fun reset(): FBXLoader {
        bytesRead = 0
        return this
    }

    companion object {
        private val KNOWN_NODE_NAMES: Set<String> = mutableSetOf(
            "FBXHeaderExtension",
            "FBXHeaderVersion"
        )
        private val BYTE_ORDER = ByteOrder.LITTLE_ENDIAN
        private const val DEBUG = false
        private val LOG = getLogger<FBXLoader>()
    }

}
