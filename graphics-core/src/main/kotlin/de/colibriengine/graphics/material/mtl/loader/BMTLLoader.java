package de.colibriengine.graphics.material.mtl.loader;

import de.colibriengine.exception.MaterialLoadException;
import de.colibriengine.graphics.material.MaterialLibraryDefinition;
import de.colibriengine.graphics.material.MaterialLoader;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

public class BMTLLoader implements MaterialLoader {

    @NotNull
    @Override
    public MaterialLibraryDefinition load(@NotNull URL resourceURL,
                                          @NotNull String relativePath) throws MaterialLoadException {
        throw new UnsupportedOperationException("not implemented");
    }

}
