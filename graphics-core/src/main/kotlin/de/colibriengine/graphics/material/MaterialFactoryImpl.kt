package de.colibriengine.graphics.material

object MaterialFactoryImpl : MaterialFactory {

    override fun createFrom(materialDefinition: MaterialDefinition): MaterialImpl {
        return MaterialImpl(materialDefinition)
    }

}
