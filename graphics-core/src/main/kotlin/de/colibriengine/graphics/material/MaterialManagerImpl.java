package de.colibriengine.graphics.material;

import de.colibriengine.exception.FileTypeUnsupportedException;
import de.colibriengine.exception.MaterialLoadException;
import de.colibriengine.exception.ResourceNotFoundException;
import de.colibriengine.graphics.material.mtl.loader.BMTLLoader;
import de.colibriengine.graphics.material.mtl.loader.MTLLoader;
import de.colibriengine.graphics.texture.TextureManager;
import de.colibriengine.util.FileOps;
import de.colibriengine.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.util.HashMap;

@SuppressWarnings("WeakerAccess")
public class MaterialManagerImpl implements MaterialManager {

    private static final String MTL_EXTENSION_NAME = "mtl";
    private static final int MTL_EXTENSION = 1;

    private static final String BMTL_EXTENSION_NAME = "bmtl";
    private static final int BMTL_EXTENSION = 2;

    private static final MTLLoader MTL_LOADER = new MTLLoader();
    private static final BMTLLoader BMTL_LOADER = new BMTLLoader();

    private final MaterialFactory materialFactory;

    /**
     * The window to which this material manager belongs.
     */
    private final TextureManager textureManager;

    /**
     * The material library cache.
     */
    private final HashMap<Integer, MaterialLibraryImpl> loadedMaterialLibs;
    private static final int LOADED_MATERIAL_LIBS_INITIAL_CAPACITY = 32;

    /**
     * Constructs a new MaterialManager.
     * Every {@link AbstractWindow} has its own material manager. You should not be in the need to create an instance
     * yourself.
     *
     * @see AbstractWindow#getMaterialManager()
     */
    public MaterialManagerImpl(
            final @NotNull MaterialFactory materialFactory,
            final @NotNull TextureManager textureManager
    ) {
        this.materialFactory = materialFactory;
        this.textureManager = textureManager;
        loadedMaterialLibs = new HashMap<>(LOADED_MATERIAL_LIBS_INITIAL_CAPACITY);
    }

    /**
     * Returns the requested material library.
     * Only the first request call on a specific path will load the material library. Every subsequent call will
     * return a reference to the previously loaded material library. The method only changes back to actually loading
     * the library specified by the path parameter if it previously got removed from the manager through a call to
     * {@link MaterialManager#invalidateCache(String)}.
     * <p>
     * The {@code MaterialManager} is capable of loading the following material files:
     * <pre>
     *     -  .mtl
     *     -  .bmtl
     * </pre>
     *
     * @param relativePath Specify the material library you want by handing a path relative to the working directory.
     *                     Example: "models/myModel.mtl"
     * @return The MaterialLibrary object for the specified material file.
     * @throws ResourceNotFoundException    if the specified path does not point to a valid file.
     * @throws FileTypeUnsupportedException if there is no loader for the specified file type available.
     * @throws MaterialLoadException        see {@link MaterialManager#loadMaterialLibrary(int, URL, int)}
     */
    public synchronized @NotNull MaterialLibraryImpl requestMaterialLibrary(final @NotNull String relativePath)
            throws ResourceNotFoundException, FileTypeUnsupportedException, MaterialLoadException {

        System.out.println("MaterialManager#requestMaterialLibrary: \"" + relativePath + "\" got requested...");

        // Try to obtain the URL object for the specified path. Throw an IllegalArgumentException if that went wrong.
        final URL matLibURL = FileOps.INSTANCE.getAsURL(relativePath);
        if (matLibURL == null) {
            throw new ResourceNotFoundException(
                    "MaterialManager#requestMaterialLibrary: Path: \"" + relativePath + "\" does not point to a valid file."
            );
        }

        // Generate a mostly unique int value for the material library url.
        final int matLibURLHash = matLibURL.hashCode();

        // If the material library is already loaded, we can retrieve its reference by looking up its hash code in
        // loadedModels.
        if (loadedMaterialLibs.containsKey(matLibURLHash)) {
            return loadedMaterialLibs.get(matLibURLHash);
        }

        // Check whether the requested file is supported by the loader. Determines the loader to use.
        final int fileType = getFileType(matLibURL);

        // Otherwise the material library has not yet been loaded. Initiate loading...
        return loadMaterialLibrary(fileType, matLibURL, matLibURLHash, relativePath);
    }

    private synchronized int getFileType(final @NotNull URL matLibURL) throws FileTypeUnsupportedException {
        final String fileTypeString = StringUtil.identifyFileTypeFromExtension(matLibURL.getPath());

        return switch (fileTypeString.toLowerCase()) {
            case MTL_EXTENSION_NAME -> MTL_EXTENSION;
            case BMTL_EXTENSION_NAME -> BMTL_EXTENSION;
            default -> throw new FileTypeUnsupportedException("MaterialManager#getFileType: " +
                    "File type: \"" + fileTypeString + "\" is not supported! Unable to load material library."
            );
        };
    }

    /**
     * @throws MaterialLoadException if the material loader used to load the material was unable to finish its job.
     */
    private synchronized @NotNull MaterialLibraryImpl loadMaterialLibrary(
            int type,
            @NotNull URL url,
            int urlHash,
            @NotNull String relativePath
    ) throws MaterialLoadException {
        final MaterialLibraryDefinition materialLibraryDefinition = switch (type) {
            case MTL_EXTENSION -> MTL_LOADER.load(url, relativePath);
            case BMTL_EXTENSION -> BMTL_LOADER.load(url, relativePath);
            default -> throw new IllegalStateException("Type should have already been checked!");
        };

        // Load up the material library file with the appropriate loader.

        System.out.println("MaterialManager#loadMaterialLibrary: \"" + url.getPath() + "\" got loaded.");

        // Create the actual material library.
        final MaterialLibraryImpl materialLibrary = new MaterialLibraryImpl(materialLibraryDefinition);

        // Load the textures of all materials sored in the library.
        for (final MaterialImpl material : materialLibrary.getMaterials()) {
            // We provide the given URL path as the texture root/base path.
            material.loadTextures(textureManager, StringUtil.removeLastPart(url.getPath()));
        }

        // Insert the loaded materialLibrary with its key in the hash map.
        // Overwrites a previously stored material library if necessary.
        loadedMaterialLibs.put(urlHash, materialLibrary);
        return materialLibrary;
    }

    public synchronized @Nullable MaterialImpl requestMaterial(@NotNull String materialName) {
        // TODO implement;
        throw new UnsupportedOperationException("not implemented");
    }

    public synchronized void invalidateCache(@NotNull String path) {
        final URL url = FileOps.INSTANCE.getAsURL(path);
        if (url == null) {
            throw new ResourceNotFoundException("MaterialManager#invalidateCache: Path did not point to a valid file.");
        }
        invalidateCache(url);
    }

    public synchronized void invalidateCache(@NotNull URL url) {
        final int hash = url.hashCode();
        if (loadedMaterialLibs.containsKey(hash)) {
            loadedMaterialLibs.remove(hash);
        } else {
            System.err.println("MaterialManager#invalidateCache: Specified resource was not loaded.");
        }
    }

    @NotNull
    @Override
    public MaterialFactory getMaterialFactory() {
        return materialFactory;
    }

}
