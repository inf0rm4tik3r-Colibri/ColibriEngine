package de.colibriengine.graphics.material

import de.colibriengine.asset.ResourceName
import de.colibriengine.graphics.texture.Texture
import de.colibriengine.graphics.texture.TextureManager
import de.colibriengine.graphics.texture.TextureOptions.Companion.SMOOTH_2D
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.util.StringUtil

class MaterialImpl(override val materialDefinition: MaterialDefinition) : Material {

    override val ambientTextures: MutableList<Texture> = ArrayList(1)
    override val diffuseTextures: MutableList<Texture> = ArrayList(1)
    override val specularTextures: MutableList<Texture> = ArrayList(1)

    override fun free() {
        ambientTextures.clear()
        diffuseTextures.clear()
        specularTextures.clear()
    }

    override fun loadTextures(textureManager: TextureManager, textureBasePath: String) {
        // We need to add the defined texture names to the constructed path.
        // Texture are always named relative (they do not begin with a path separator).
        // Our constructed path must therefore end with a path separator (=> forward slash).
        var relativeTextureBasePath = textureBasePath
        if (!relativeTextureBasePath.isEmpty() && !StringUtil.endsWithForwardSlash(relativeTextureBasePath)) {
            relativeTextureBasePath += '/'
        }

        // TODO: allow multiple ambient textures in material definition??
        if (materialDefinition.ambientColorTexture != null) {
            LOG.debug("Loading " + materialDefinition.ambientColorTexture!!.texturePath)
            ambientTextures.add(
                textureManager.requestTexture(
                    ResourceName(materialDefinition.ambientColorTexture!!.texturePath),
                    SMOOTH_2D
                )
            )
        }
        if (materialDefinition.diffuseColorTexture != null) {
            LOG.debug("Loading " + materialDefinition.diffuseColorTexture!!.texturePath)
            diffuseTextures.add(
                textureManager.requestTexture(
                    ResourceName(materialDefinition.diffuseColorTexture!!.texturePath),
                    SMOOTH_2D
                )
            )
        }
        if (materialDefinition.specularColorTexture != null) {
            LOG.debug("Loading " + materialDefinition.specularColorTexture!!.texturePath)
            specularTextures.add(
                textureManager.requestTexture(
                    ResourceName(materialDefinition.specularColorTexture!!.texturePath),
                    SMOOTH_2D
                )
            )
        }
        // todo...
    }

    companion object {
        private val LOG = getLogger(this)
    }

}
