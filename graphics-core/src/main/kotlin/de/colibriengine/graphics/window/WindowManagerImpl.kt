package de.colibriengine.graphics.window

import de.colibriengine.logging.LogUtil.getLogger
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.system.APIUtil
import org.lwjgl.system.MemoryUtil
import java.lang.reflect.Field

/**
 * Manages currently opened windows.
 *
 * Every ColibriEngine instance provides an instance of this manager. Use the [ColibriEngine.windowManager]
 * method to obtain that instance. Then add windows using the [WindowManagerImpl.addWindow] method.
 *
 * If a currently active window get released, the last active window will be the new active window. If there is no focus
 * history available, the last created and currently visible window will be picked to be the new active window.
 */
class WindowManagerImpl : WindowManager {

    override val windows: MutableList<Window> = ArrayList(WINDOWS_LIST_INITIAL_CAPACITY)

    private val windowFocusHistory: MutableList<Window> = ArrayList(WINDOW_FOCUS_LIST_INITIAL_CAPACITY)

    override var activeWindow: Window? = null
        set(window) {
            if (field === window) {
                return
            }
            if (field != null) {
                windowFocusHistory.add(field!!)
                //TODO focus history with a maximum length (maybe 20 entries??)
            }
            LOG.info("Setting window active: " + window?.id)
            field = window
            field?.activate()
        }

    override var onlyProcessActiveWindow: Boolean = false

    override operator fun contains(window: Window): Boolean {
        return windows.contains(window)
    }

    override fun addWindow(window: Window, activate: Boolean) {
        // Return immediately if the window was already added.
        if (windows.contains(window)) {
            LOG.info("Window is already registered.")
            return
        }
        windows.add(window)

        if (activate) {
            activeWindow = window
        }
    }

    /*
    /**
     * not implemented yet
     *
     * @param window
     */
    fun removeWindow(window: AbstractWindow) {
        if (hasWindows()) {
            val listChanged = windows.remove(window)

            // Remove the window from its parent worker window list.
            if (listChanged) {
                // TODO?
                LOG.info("Successfully removed: $window")
            } else {
                LOG.error("Window ($window) was not registered.")
            }
        }
    }
    */

    override fun releaseAllWindows() {
        if (hasWindows()) {
            // Make the context of every window current and release it.
            for (window in windows) {
                window.makeContextCurrent()
                window.release()
            }

            // Clear the reference to the currently active window and make no context current.
            activeWindow = null
            GLFW.glfwMakeContextCurrent(MemoryUtil.NULL)

            // Finally delete all window references.
            windows.clear()
            windowFocusHistory.clear()

            LOG.info("Removed all windows!")
        } else {
            LOG.error("No windows to remove.")
        }
    }

    override fun cleanup() {
        // Iterate over all registered windows and remove all windows whose close flag is set true.
        val iterator = windows.iterator()
        var window: Window
        while (iterator.hasNext()) {
            window = iterator.next()

            // Check whether the current window should be removed.
            if (window.shouldClose) {
                closeWindow(window, iterator)
            }
        }
    }

    private fun closeWindow(window: Window, iterator: MutableIterator<Window>) {
        // Release the window from OpenGL and remove its reference from the window list.
        // See release() for why the windows context needs to be current.
        window.makeContextCurrent() // TODO: NOW: Call this in the release() implementation of an OpenGLWindow!
        window.release()
        iterator.remove()
        LOG.info("Removed window: " + window.id)

        // Also remove every entry of the current window from the focus history.
        // The window is terminated by now and can not get the focus back.
        windowFocusHistory.removeAll(setOf(window))

        // Check whether the given window is currently active.
        if (window === activeWindow) {
            // Delete the old reference.
            activeWindow = null

            // Make another window active if there are more windows available.
            if (windows.isEmpty()) {
                // If not, we removed the last window and can print that.
                LOG.info("Removed last window.")
                activeWindow = null
            } else {
                // Pick new active window from the focus history.
                if (windowFocusHistory.isNotEmpty()) {
                    // Pick the last focused window that is currently visible.
                    for (i in 1 until windowFocusHistory.size + 1) {
                        if (windowFocusHistory[windowFocusHistory.size - i].visible) {
                            activeWindow = windowFocusHistory.removeAt(windowFocusHistory.size - i)
                            LOG.info("Restored focus (picked from focus history).")
                        }
                    }
                } else {

                    // Otherwise pick the most recently addN window that is currently visible.
                    for (i in 1 until windows.size + 1) {
                        if (!windows[windows.size - i].visible) {
                            activeWindow = windows[windows.size - i]
                            LOG.info(
                                "Restored focus (picked from remaining visible windows): " +
                                        windows[windows.size - i].id
                            )
                        }
                    }
                }

                // Is activeWindow still null?
                if (activeWindow == null) {
                    LOG.error("Could not restore focus. Focus history empty and remaining windows invisible.")
                }
            }
        }
        if (activeWindow == null) {
            LOG.info("activeWindow => null")
        }
    }

    fun shouldOnlyProcessActiveWindow(): Boolean {
        return onlyProcessActiveWindow
    }

    /**
     * Checks whether there is a window set as the active window.
     *
     * @return false if there is no active window specified.
     */
    fun hasActiveWindow(): Boolean {
        return false
    }

    fun isActive(window: Window): Boolean {
        return window === activeWindow
    }

    companion object {
        private const val WINDOWS_LIST_INITIAL_CAPACITY = 4
        private const val WINDOW_FOCUS_LIST_INITIAL_CAPACITY = 16
        private const val PER_WORKER_MAP_INITIAL_CAPACITY = 4

        /**
         * Condition variable to check whether the GLFW library is currently initialized.
         */
        var isGLFWInitialized = false
            private set

        // Caller needs to be released at termination.
        private var glfwErrorCallback: GLFWErrorCallback? = null

        /**
         * The logger instance used in this class.
         */
        private val LOG = getLogger(WindowManagerImpl::class.java)

        /**
         * Initializes the GLFW library. This method needs to get called once before any ColibriEngine instance gets
         * created.
         *
         * Additional calls to this method after successful initialization but before termination will return
         * immediately.
         *
         * It is not possible to create windows before this method got called.
         */
        fun initializeGLFW() {
            // Return immediately if already initialized.
            if (isGLFWInitialized) {
                return
            }

            // Set an error callback that prints errors in System.err.
            // Call before glfwInit() to get error notifications both during and after initialization.
            glfwErrorCallback = object : GLFWErrorCallback() {
                private val ERROR_CODES = APIUtil.apiClassTokens(
                    { _: Field?, value: Int -> value in 0x10001..0x1ffff },
                    null,
                    GLFW::class.java
                )

                override fun invoke(error: Int, description: Long) {
                    System.err.println("err")
                    val msg = getDescription(description)
                    LOG.error("[LWJGL] error: " + ERROR_CODES[error])
                    LOG.error("\tDescription : $msg")
                    LOG.error("\tStacktrace  :")
                    val stack = Thread.currentThread().stackTrace
                    for (i in 4 until stack.size) {
                        LOG.error("\t\t" + stack[i].toString())
                    }
                }
            }.set()

            // Initialize GLFW to get full functionality (most functions wont work before).
            if (GLFW.glfwInit()) {
                LOG.info("Successfully initialized the GLFW library.")
            } else {
                throw IllegalStateException("Failed to initialize the GLFW library!")
            }

            //Store the initialized status in our helper boolean
            isGLFWInitialized = true
        }

        /**
         * Terminates the GLFW library after releasing the associated error callback.
         * This method needs to get called once after every ColibriEngine instance terminated.
         *
         *
         * Additional calls to this method after successful termination but before another initialization will return
         * immediately.
         *
         *
         * All remaining GLFW resources will get destroyed!
         */
        fun terminateGLFW() {
            // Return immediately if not already initialized.
            if (!isGLFWInitialized) {
                return
            }

            // Free the error callback and terminate the library.
            glfwErrorCallback!!.free()
            GLFW.glfwTerminate()

            // Store the initialized status in our helper boolean.
            isGLFWInitialized = false
        }
    }

}
