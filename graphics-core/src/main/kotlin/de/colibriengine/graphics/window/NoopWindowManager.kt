package de.colibriengine.graphics.window

class NoopWindowManager : WindowManager {

    override val windows: List<Window> = emptyList()

    override var activeWindow: Window? = null

    override var onlyProcessActiveWindow: Boolean = false

    override fun contains(window: Window): Boolean {
        // Do nothing
        return false
    }

    override fun addWindow(window: Window, activate: Boolean) {
        // Do nothing
    }

    override fun cleanup() {
        // Do nothing
    }

    override fun releaseAllWindows() {
        // Do nothing
    }

}
