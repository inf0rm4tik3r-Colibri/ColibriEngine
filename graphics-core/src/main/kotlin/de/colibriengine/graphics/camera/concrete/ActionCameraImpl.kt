package de.colibriengine.graphics.camera.concrete

import de.colibriengine.ecs.Entity
import de.colibriengine.graphics.camera.AbstractCamera
import de.colibriengine.graphics.camera.type.ActionCamera
import de.colibriengine.graphics.window.CursorMode
import de.colibriengine.graphics.window.Window
import de.colibriengine.input.FECommands
import de.colibriengine.input.JoystickInputCallbacks
import de.colibriengine.input.MouseKeyboardInputCallbacks
import de.colibriengine.input.gamepads.XboxOneGamepad
import de.colibriengine.logging.LogUtil.getLogger
import de.colibriengine.math.vector.vec3f.StdVec3f
import de.colibriengine.math.vector.vec3f.StdVec3fFactory.Companion.UNIT_Y_AXIS
import de.colibriengine.math.vector.vec3f.Vec3f
import de.colibriengine.util.Timer
import org.lwjgl.glfw.GLFW

/**
 * Provides a default implementation of an action camera. The camera adds itself to the input manager of its parent
 * window. The camera operates entirely on input callbacks.
 */
class ActionCameraImpl(
    entity: Entity,
    parentWindow: Window?
) : ActionCamera, AbstractCamera(entity, parentWindow), MouseKeyboardInputCallbacks, JoystickInputCallbacks {

    var movementSpeed = 2.0f
    var inSprint = false
    var sprintMultiplier = 2.0f
    var mouseSensitivity = 0.1f
    var gamepadSensitivity = 1.0f
    var degreesPerSecond = 180.0f

    private var frameMovAmt = 0f
    private var frameGamepadRotAmt = 0f
    private val tmpVec3f: Vec3f = StdVec3f()

    private fun shouldUpdate(): Boolean = active
        && parentWindow!!.cursorMode === CursorMode.LOCKED
        && parentWindow!!.cameraManager.activeCamera === this

    override fun init() {}

    override fun update(timing: Timer.View) {
        if (shouldUpdate()) {
            // Calculate the amount of movement in any direction for the current frame.
            frameMovAmt = movementSpeed * timing.deltaSP()
            if (inSprint) frameMovAmt *= sprintMultiplier
            frameGamepadRotAmt = gamepadSensitivity * degreesPerSecond * timing.deltaSP()
        }
    }

    override fun destroy() {}

    override fun mkKeyCallback(key: Int, scancode: Int, action: Int, mods: Int) {
        if (!shouldUpdate()) return
        // TODO: This line seems unnecessary. If the mouse is moved, mk-input is activated and this expression
        //  should evaluate to true. If the joystick is used, no mouse movement occurs and this method will not be called...
        // if (parentInput != null && parentInput!!.isMouseAndKeyboardInput) {
        when {
            key == FECommands.MOVE_FORWARD.kKey -> moveForward(frameMovAmt)
            key == FECommands.MOVE_BACKWARD.kKey -> moveBackward(frameMovAmt)
            key == FECommands.MOVE_LEFT.kKey -> moveLeft(frameMovAmt)
            key == FECommands.MOVE_RIGHT.kKey -> moveRight(frameMovAmt)
            key == FECommands.MOVE_UP.kKey -> moveUp(frameMovAmt)
            key == FECommands.MOVE_DOWN.kKey -> moveDown(frameMovAmt)
            key == FECommands.SPRINT.kKey && action == GLFW.GLFW_PRESS -> inSprint = true
            key == FECommands.SPRINT.kKey && action == GLFW.GLFW_RELEASE -> inSprint = false
            key == FECommands.RESET_CAMERA.kKey && action == FECommands.RESET_CAMERA.kKeyAction -> reset()
        }
        // }
    }

    override fun mkMoveCallback(dx: Int, dy: Int) {
        if (!shouldUpdate()) return
        // TODO: This line seems unnecessary. If the mouse is moved, mk-input is activated and this expression
        //  should evaluate to true. If the joystick is used, no mouse movement occurs and this method will not be called...
        // if (parentInput != null && parentInput!!.isMouseAndKeyboardInput) {
        val rotAmtY = mouseSensitivity * dx
        val rotAmtX = mouseSensitivity * dy
        if (rotAmtY != 0.0f) {
            rotate(UNIT_Y_AXIS, rotAmtY)
        }
        if (rotAmtX != 0.0f) {
            rotate(transform.rotation.right(tmpVec3f), rotAmtX)
        }
        // }
    }

    override fun jAxisCallback(axis: Int, axisValue: Float) {
        if (!shouldUpdate()) return

        // There is no need to check if a value is out of the gamepads threshold values.
        // The jAxisCallback method only gets called if the new axis value is out of the gamepads threshold.
        val movAmt = axisValue * frameMovAmt
        val flyAmt = (axisValue + 1) / 2 * frameMovAmt
        val rotAmt = axisValue * frameGamepadRotAmt

        when (axis) {
            XboxOneGamepad.LS_X -> moveRight(movAmt)
            XboxOneGamepad.LS_Y -> moveBackward(movAmt)
            XboxOneGamepad.LT -> moveDown(flyAmt)
            XboxOneGamepad.RT -> moveUp(flyAmt)
            XboxOneGamepad.RS_X -> rotate(UNIT_Y_AXIS, rotAmt)
            XboxOneGamepad.RS_Y -> rotate(transform.rotation.right(tmpVec3f), rotAmt)
            else -> LOG.warn("Unused axis: $axis")
        }
    }

    override fun jButtonCallback(button: Int, action: Int) {
        if (!shouldUpdate()) return
        if (button == FECommands.RESET_CAMERA.jButton && action == FECommands.RESET_CAMERA.jButtonAction) {
            reset()
        }
    }

    companion object {
        private val LOG = getLogger(this)
    }

}
