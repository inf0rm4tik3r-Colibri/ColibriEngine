package de.colibriengine.graphics.font

import de.colibriengine.graphics.model.MeshFactory

class TextFactoryImpl(override val meshFactory: MeshFactory) : TextFactory {

    override fun create(mode: TextMode, size: UInt, font: BitmapFont): Text {
        return TextImpl(mode, size, font, meshFactory)
    }

}
